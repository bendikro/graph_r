import graph_default
from graph_r import *
from graph_r_ggplot import *

import util
from util import cprint, colored
import re

class DataMatch(object):
    def __init__(self, key, **kw):
        self.key = key
        self.args = kw
        self.name = key
        self.value_regex=None
        self.key_regex = False
        self.__dict__.update(kw)

def conn_info_get_aggregated_data(conf, filename):
    keys = [DataMatch("Number of retransmissions", name="Number of retrans"),
            DataMatch("Occurrences of 1. retransmission",            name="1. retrans",        value_regex=None),
            DataMatch("Occurrences of 2. retransmission",            name="2. retrans",        value_regex=None),
            DataMatch("Total data packets sent",                     name="Data packets sent", value_regex=None, key_regex=True),
            DataMatch("Total bytes sent (payload)",                  name="Total bytes sent",  value_regex=None),
            DataMatch("Number of retransmitted bytes",               name="Nr. retrans bytes", value_regex=None),
            DataMatch("Number of packets with bundled segments",     name="RDB packets",       value_regex=None),
            DataMatch("RDB bytes bundled",                           name="RDB bytes bundled", value_regex="(?P<value>\d+).+"),
            DataMatch("RDB packet hits",                             name="RDB packet hits",   value_regex="(?P<value>\d+).+"),
            DataMatch("RDB packet misses",                           name="RDB packet misses", value_regex="(?P<value>\d+).+"),
            DataMatch("RDB byte   hits",                             name="RDB byte   hits",   value_regex="(?P<value>\d+).+"),
            DataMatch("RDB byte   misses",                           name="RDB byte   misses", value_regex="(?P<value>\d+).+"),
            DataMatch("1th percentile",                              name="ps_1_percentile",   value_regex="(?P<value>\d+)$"),
            DataMatch("5th percentile",                              name="ps_5_percentile",   value_regex="(?P<value>\d+)$"),
            DataMatch("10th percentile",                             name="ps_10_percentile",  value_regex="(?P<value>\d+)$"),
            DataMatch("25th percentile (First quartile)",            name="ps_25_percentile",  value_regex="(?P<value>\d+)$"),
            DataMatch("50th percentile (Second quartile, median)",   name="ps_50_percentile",  value_regex="(?P<value>\d+)$"),
            DataMatch("75th percentile (Third quartile)",            name="ps_75_percentile",  value_regex="(?P<value>\d+)$"),
            DataMatch("90th percentile",                             name="ps_90_percentile",  value_regex="(?P<value>\d+)$"),
            DataMatch("95th percentile",                             name="ps_95_percentile",  value_regex="(?P<value>\d+)$"),
            DataMatch("99th percentile",                             name="ps_99_percentile",  value_regex="(?P<value>\d+)$"),
            DataMatch("1th percentile",                              name="lat_1_percentile",  value_regex="(?P<value>\d+) ms"),
            DataMatch("5th percentile",                              name="lat_5_percentile",  value_regex="(?P<value>\d+) ms"),
            DataMatch("10th percentile",                             name="lat_10_percentile", value_regex="(?P<value>\d+) ms"),
            DataMatch("25th percentile (First quartile)",            name="lat_25_percentile", value_regex="(?P<value>\d+) ms"),
            DataMatch("50th percentile (Second quartile, median)",   name="lat_50_percentile", value_regex="(?P<value>\d+) ms"),
            DataMatch("75th percentile (Third quartile)",            name="lat_75_percentile", value_regex="(?P<value>\d+) ms"),
            DataMatch("90th percentile",                             name="lat_90_percentile", value_regex="(?P<value>\d+) ms"),
            DataMatch("95th percentile",                             name="lat_95_percentile", value_regex="(?P<value>\d+) ms"),
            DataMatch("99th percentile",                             name="lat_99_percentile", value_regex="(?P<value>\d+) ms"),
            ]

    data, data_dict = util.read_output_data_match(keys, filename)
    #print "data_dict:", data_dict.keys()
    return data_dict

def conn_info_get_aggregated_data2(conf, filename):
    #print "probe file:", file_data["tcp_probe_data"]

    keys = [("Number of retrans", "Number of retransmissions", None),
            ("1. retrans", "Occurrences of 1. retransmission", None),
            ("2. retrans", "Occurrences of 2. retransmission", None),
            ("Data packets sent", "Total data packets sent", None),
            ("Total bytes sent", "Total bytes sent (payload)", None),
            ("Nr. retrans bytes", "Number of retransmitted bytes", None),
            ("RDB packets", "Number of packets with bundled segments", None),
            ("RDB bytes bundled", "RDB bytes bundled", "(?P<value>\d+).+"),
            ("RDB packet hits", "RDB packet hits", "(?P<value>\d+).+"),
            ("RDB packet misses", "RDB packet misses", "(?P<value>\d+).+"),
            ("RDB byte   hits", "RDB byte   hits", "(?P<value>\d+).+"),
            ("RDB byte   misses", "RDB byte   misses", "(?P<value>\d+).+"),

            ("ps_1_percentile", "1th percentile", "(?P<value>\d+)$"),
            ("ps_5_percentile", "5th percentile", "(?P<value>\d+)$"),
            ("ps_10_percentile", "10th percentile", "(?P<value>\d+)$"),
            ("ps_25_percentile", "25th percentile (First quartile)", "(?P<value>\d+)$"),
            ("ps_50_percentile", "50th percentile (Second quartile, median)", "(?P<value>\d+)$"),
            ("ps_75_percentile", "75th percentile (Third quartile)", "(?P<value>\d+)$"),
            ("ps_90_percentile", "90th percentile", "(?P<value>\d+)$"),
            ("ps_95_percentile", "95th percentile", "(?P<value>\d+)$"),
            ("ps_99_percentile", "99th percentile", "(?P<value>\d+)$"),

            ("lat_1_percentile", "1th percentile", "(?P<value>\d+) ms"),
            ("lat_5_percentile", "5th percentile", "(?P<value>\d+) ms"),
            ("lat_10_percentile", "10th percentile", "(?P<value>\d+) ms"),
            ("lat_25_percentile", "25th percentile (First quartile)", "(?P<value>\d+) ms"),
            ("lat_50_percentile", "50th percentile (Second quartile, median)", "(?P<value>\d+) ms"),
            ("lat_75_percentile", "75th percentile (Third quartile)", "(?P<value>\d+) ms"),
            ("lat_90_percentile", "90th percentile", "(?P<value>\d+) ms"),
            ("lat_95_percentile", "95th percentile", "(?P<value>\d+) ms"),
            ("lat_99_percentile", "99th percentile", "(?P<value>\d+) ms"),
            ]

    data, data_dict = read_output_data(keys, filename)
    #print "data_dict:", data_dict.keys()
    return data_dict


def conn_info_results_parse(conf, box_conf, file_data, set_key):

    with open(file_data["results"]["conninfo"]["results_file"], 'r') as data_file:
        content = data_file.read()

    regexes = []
    regex = "(?:_clps-t-(?P<collapse_thin>\d))"\
        "(?:_clps-g-(?P<collapse_thick>\d))"\
        "(?:_soff-(?P<segoff>[^_]+))"\
        "(?:_soff-t-(?P<segoff_thin>[^_]+))"\
        "(?:_tso-t-(?P<tso_thin>[^_]+))?"\
        "(?:_gso-t-(?P<gso_thin>[^_]+))?"\
        "(?:_gro-t-(?P<gro_thin>[^_]+))?"\
        "(?:_lro-t-(?P<lro_thin>[^_]+))?"\
        "(?:_soff-g-(?P<segoff_thick>[^_]+))?"\
        "(?:_brate-(?P<bridge_rate>[^_]+))?"
    regexes.append(regex)
    # _clps-t-0_clps-g-0_soff-0_soff-t-0_soff-g-0
    # _clps-t-0_clps-g-0_soff-0_soff-t-0_gso-t-1_soff-g-0_bridge2-eth1.pcap

    regex = "(?:_(?:collapse|clps)_thin_(?P<collapse_thin>\d))?"\
        "(?:_(?:collapse|clps)_thick_(?P<collapse_thick>\d))?"\
        "(?:_segoff_(?P<segoff>[a-z]+))?"\
        "(?:_segoff_thin_(?P<segoff_thin>[a-z]+))?"\
        "(?:_segoff_thick_(?P<segoff_thick>[a-z]+))?"\
        "(?:_brate_(?P<bridge_rate>[^_]+))?"
    regexes.append(regex)

    # _collapse_thin_0_collapse_thick_0_segoff_off_segoff_thin_on
    # _clps_thin_0_clps_thick_0_segoff_off_segoff_thin_off_segoff_thick_off_brate_10-duplex-half_zsender
    # _clps_thin_1_clps_thick_0_segoff_off_segoff_thin_on_segoff_thick_off_zsender.pcap
    # _clps-t-0_clps-g-0_soff-off_soff-t-0_soff-g-0_brate-speed-10-duplex-full_

    #print "EXTRA:", file_data["extra"]
    m_extra = None
    if file_data["extra"]:
        for regex in regexes:
            m_extra = re.search(regex, file_data["extra"], flags=re.DOTALL)
            if m_extra:
                break

             #10.0.0.13-12000-10.0.0.22-5001:            298             9.07 / 9.07  4278         3891            9.05 %          9.06 %        9.05 %
             #10.0.0.15-32000-10.0.0.22-5003                                             900                9010          8979            0.34 %        0.34 %        0.34 %
    regex = "(?P<src_ip>\S+)-(?P<src_port>\S+)-(?P<dst_ip>\S+)-(?P<dst_port>\S+):\s+"\
            "(?P<duration>\d+)\s+(?P<est_loss>\S+\s/\s\S+)\s+(?P<packets_sent>\d+)\s+(?P<packets_received>\d+)\s+"\
            "(?P<packet_loss>\S+)\s\%\s+(?P<bytes_loss>\S+)\s\%\s+(?P<ranges_loss>\S+)\s\%"
    m3 = re.finditer(regex, content, flags=re.DOTALL)
    bytes_loss = []
    packet_loss = []
    if not m3:
        print "Failed to match regex!"
        return

    for l in m3:
        bytes_loss.append(float(l.group("bytes_loss")))
        packet_loss.append(float(l.group("packet_loss")))

    # Handle R loss and latency results
    bytes_loss_dataf = DataFrame({'bytes_loss': FloatVector(bytes_loss)})
    #print "bytes_loss_dataf:", bytes_loss_dataf
    bytes_loss_summary = util.parse_r_summary(r["summary"](bytes_loss_dataf))

    packet_loss_dataf = DataFrame({'packet_loss': FloatVector(packet_loss)})
    packet_loss_summary = util.parse_r_summary(r["summary"](packet_loss_dataf))

    loss_stats = "{Min:03.1f} / {1st Qu:03.1f} / {Median:03.1f} / {3rd Qu:03.1f} / {Max:03.1f}".format(**bytes_loss_summary)

    #print "Latency file:", file_data["results"]["latency"]["results_file"]
    # Latency
    latency_dataf = r["read.csv"](file_data["results"]["latency"]["results_file"], header=True) #, colClasses=StrVector(("integer", "integer")))
    #print "READING:", file_data["results"]["latency"]["results_file"]
    ack_latency_summary = util.parse_r_summary(r["summary"](DataFrame({'ack_latency': latency_dataf[1]})))
    #ack_latency_summary = util.parse_r_summary(r["summary"](latency_dataf[1]))
    #print "ack_latency_summary:", ack_latency_summary
    latency_stats = "{Min:03.0f} / {1st Qu:03.0f} / {Median:03.0f} / {3rd Qu:03.0f} / {Max:05.0f}".format(**ack_latency_summary)
    #print "latency_stats:", latency_stats
    print "LATENCY - stdout_file:", file_data["results"]["latency"]["stdout_file"]
    data_aggr_data = conn_info_get_aggregated_data(conf, file_data["results"]["latency"]["stdout_file"])
    print "data_aggr_data:", data_aggr_data.keys()

    streams = ""
    if "stream_count_thick" in file_data:
        streams = "%s" % (file_data["stream_count_thick"])
    if "stream_count_thin1" in file_data:
        if streams:
            streams += "vs"
        streams += "%s" % (file_data["stream_count_thin1"])
    if file_data.get("stream_count_thin2", None):
        if streams:
            streams += "vs"
        streams += "%s" % (file_data["stream_count_thin2"])

    data = dict(**file_data)
    data["rdb_packet_count"] = data_aggr_data["RDB packets"]
    data["data_packet_count"] = data_aggr_data["Data packets sent"]

    data["bytes_loss_dataf"] = bytes_loss_dataf
    data["packet_loss_dataf"] = packet_loss_dataf

    #print "S:", file_data["streams"][file_data["streams_id"]]

    #data["payload"] = file_data["streams"][file_data["streams_id"]].get("payload", "")
    data.update(file_data["streams"][file_data["streams_id"]])

    #print "data_aggr_data:", data_aggr_data.keys()
    ps_percentiles = "%4s/%4s/%4s/%4s/%4s/%4s/%4s/%4s/%4s" % (data_aggr_data["ps_1_percentile"], data_aggr_data["ps_5_percentile"], data_aggr_data["ps_10_percentile"],
                                                              data_aggr_data["ps_25_percentile"], data_aggr_data["ps_50_percentile"], data_aggr_data["ps_75_percentile"],
                                                              data_aggr_data["ps_90_percentile"], data_aggr_data["ps_95_percentile"], data_aggr_data["ps_99_percentile"])
    latency_percentiles = "%4s/%4s/%4s/%4s/%4s/%4s/%4s/%4s/%4s" % (data_aggr_data["lat_1_percentile"], data_aggr_data["lat_5_percentile"], data_aggr_data["lat_10_percentile"],
                                                                   data_aggr_data["lat_25_percentile"], data_aggr_data["lat_50_percentile"], data_aggr_data["lat_75_percentile"],
                                                                   data_aggr_data["lat_90_percentile"], data_aggr_data["lat_95_percentile"], data_aggr_data["lat_99_percentile"])

    #print "%15s - type_thin1: %s, type_id: %s, type: %s" % (data["hostname"], data["type_thin1"], data["type_id"], data["type"])

    data.update({"bytes_loss" : float("%.2f" % bytes_loss_summary["Mean"]),
                 "packet_loss": float("%.2f" % packet_loss_summary["Mean"]),
                 "R_loss_stats": loss_stats,
                 "R_latency_stats": latency_stats,
                 "streams" : streams,
                 "packet_size": ps_percentiles,
                 "latency": latency_percentiles,
             })

    #"rdb_packet_count"

    if m_extra:
        data.update({"collapse_thin" : m_extra.group("collapse_thin"),
                     "collapse_thick" : m_extra.group("collapse_thick"),
                     "segoff" : m_extra.group("segoff"),
                     "segoff_thin" : m_extra.group("segoff_thin"),
                     "segoff_thick": m_extra.group("segoff_thick"),
                     })

        if "gso_thin" in m_extra.groupdict():
            data["gso_thin"] = m_extra.group("gso_thin") if m_extra.group("gso_thin") else "-"
            data["tso_thin"] = m_extra.group("tso_thin") if m_extra.group("tso_thin") else "-"
            data["gro_thin"] = m_extra.group("gro_thin") if m_extra.group("gro_thin") else "-"
            data["lro_thin"] = m_extra.group("lro_thin") if m_extra.group("lro_thin") else "-"

    with open(file_data["results"]["loss_per_stream"]["stdout_file"], 'r') as data_file:
        content = data_file.read()
        content = content.split("Connections in sender dump")[1]
        content = "%s: %s" % (file_data["hostname"], content)
        conf["results"]["loss_per_stream"]["data"].append(content)

    #print "data:", data.keys()
    #key = "%s:%s-%s:%s" % (l.group("src_ip"), l.group("src_port"), l.group("dst_ip"), l.group("dst_port"))
    conf["results"]["conninfo"]["loss_stats"].append(data)
    return data

def connection_info_file_parse_func_time(plot_conf, file_conf, pcap_file, parse_func_arg):
    if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
    #print "pcap_file:", pcap_file
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    file_conf["results"] = {"conninfo": {}, "latency": {}, "loss_per_stream": {}}

    file_conf["results"]["conninfo"]["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "command.stout"))
    file_conf["results"]["conninfo"]["results_file"] = file_conf["results"]["conninfo"]["stdout_file"]
    file_conf["results"]["conninfo"]["results_cmd"] = \
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -o %(output_dir)s -e 1> %(stdout_file)s" %\
        dict(file_conf, **file_conf["results"]["conninfo"])

    file_conf["results"]["latency"]["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.dat"))
    file_conf["results"]["latency"]["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.stout"))
    file_conf["results"]["latency"]["results_cmd"] = \
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -o %(output_dir)s -A -u%(prefix)s -l -i1,5,10,25,50,75,90,95,99 1> %(stdout_file)s" %\
        dict(file_conf, **file_conf["results"]["latency"])

    results_file = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["common_prefix"], "loss-all-streams.dat"))
    #plot_conf["results"]["loss_per_stream"]["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (, "loss-all-streams.txt"))

    file_conf["results"]["loss_per_stream"]["results_file"] = results_file
    file_conf["results"]["loss_per_stream"]["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "loss-all-streams.stout"))
    file_conf["results"]["loss_per_stream"]["results_cmd"] = \
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -e -vvv 1> %(stdout_file)s" %\
        dict(file_conf, **file_conf["results"]["loss_per_stream"])

    #print "Setting results_file:", file_conf["results"]["loss_per_stream"]["results_file"]

    file_conf["color"] = "darkred" # Default color for dataset

    file_conf["group"] = file_conf["type_id"]

    if file_conf.get("thin_dupack", None) is None:
        file_conf["thin_dupack"] = 0

def connection_info_process_results(conf, groups_list=None):

    with open(conf["output_file"], 'a') as out:
        process_results = conf["process_results"]["conninfo"]
        print "headers:", process_results["headers"]
        sorted_data = conf["results"]["conninfo"]["loss_stats"]
        if process_results.get("sort_keys", False):
            for sort_key in process_results["sort_keys"]:
                sorted_data = sorted(sorted_data, key=lambda k: k[sort_key])

        #print "sorted_data:", sorted_data
        for i, e in enumerate(sorted_data):
            headers = OrderedDict().fromkeys(process_results["headers"], process_results["default_column_width"])

            # Handle the header and column widths
            if i == 0:
                for col in process_results["column_widths"]:
                    if col in headers:
                        headers[col] = process_results["column_widths"][col]
                widths = [headers[v] for v in headers]
                headers_list = headers.keys()
                print "headers_list:", headers_list
                for u, v in enumerate(process_results["value_keys"]):
                    if process_results["value_keys"][v]:
                        # Call the function for this column
                        headers_list[u] = process_results["value_keys"][v](headers_list[u], None)
                        # We have to increase the with by 9 because the color characters use extra space
                        widths[u] += 9

                headers_list = [str(v[0]).center(v[1]) for v in zip(headers_list, widths)]
                line = ''.join('%-*.*s ' % u for u in zip(widths, widths, headers_list))
                print "header liune:", line
                out.write("%s\n" % line)

            arg = []
            for v in process_results["value_keys"]:
                if process_results["value_keys"][v]:
                    # Call the function for this column
                    e[v] = process_results["value_keys"][v](e[v], e)

            arg = list(process_results["value_keys"])
            # Add the regular arg values centered
            arg = [str(e[k[0]]).center(k[1]) if k[0] in e else "" for k in zip(arg, widths)]

            line = ''.join('%-*.*s ' % i for i in zip(widths, widths, arg))
            out.write("%s\n" % line)

            # Create a separation if "separate" key is defined
            if process_results.get("separate", False):
                if process_results["separate"]["func"](i):
                    # Remove the color codes from the string to calculate correct length
                    ansi_escape = re.compile(r'\x1b[^m]*m')
                    out.write((process_results["separate"]["char"] * len(ansi_escape.sub('', line).rstrip())) + "\n")

    cprint("Result written to '%s'" % conf["output_file"], "green")

    outfile = conf["output_file"]
    # Process the -e output
    with open(outfile, 'w') as out:
        data = conf["results"]["loss_per_stream"]["data"]
        for d in data:
            out.write(d)

def _get_default_conf():
    conf = deepcopy(graph_default.get_conf())
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s",
                         "sort_key_func": lambda x: (x["stream_count_thin1"], x["group"]),
                         "sort_keys": ["stream_count_thin1", "group"],
                         #"func" : latency_box_key_func,
                         "box_title_def" : "%(stream_count_thin1)s vs %(stream_count_thick)s" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["page_group_def"] = {"title": "Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s",
                              "sort_keys": ["payload_thin1", "itt_thin1", "rtt"], "sort_key_func": lambda x: (x["payload_thin1"], x["itt_thin1"], x["rtt"])}
    conf["document_info"]["title"] = "Conn info Plots"

    conf["output_file"] = util.cmd_args.connection_info_output_file
    conf["parse_results_func"] = conn_info_results_parse
    conf["plot_conf"]["plot_func"] = None
    conf["file_parse"]["func"] = connection_info_file_parse_func_time
    conf["plot_conf"]["make_plots"] = False
    conf["results"]["conninfo"] = { "loss_stats": []}
    conf["results"]["loss_per_stream"] = { "data": []}

    conf["plot_conf"].update({"n_columns": 2, "n_rows" : 2,
                              "plot_title" : { "title": "PLOT" },
                              #"plot_func": ggplot_cdf_box,
                              "do_plots_func": do_ggplots,
                          })

    #conf["file_parse"]["files_match_exp"] = "*bridge2-eth2*%s" % conf["file_parse"]["pcap_suffix"]

    process_results = {}
    conf["process_results"]["conninfo"] = process_results

    process_results.update({"func": connection_info_process_results,
                            "column_widths" : {},
                            "separate": { "func": lambda line_index: line_index % 4 == 3, "char": "-" },
                            "sort_keys": ["duration", "type", "hostname", "num", "queue_len", "thin_dupack",
                                          # "collapse_thin", "segoff_thin", "segoff_thick",
                                          "packets_in_flight", "stream_count_thin1", "duration", "itt_thin1"], # "bytes_loss", "segoff",
                            "headers": ["Host", "Str", "RDB", "PIF", "QLEN", "RC", "RTT", "ITT", "PS", "Dur", "D-PKT", "B-PKT",
                                        #"clps-t",
                                        #"colps-thick",
                                        #"soff", "soff_t", "soff_g",
                                        #"gso_t", "tso_t", "lro_t", "gro_t",
                                        #"segoff_thick", "bridge_rate",
                                        "BLoss", "PLoss", "Test", "R_loss_stats", "R_latency_stats", "latency"],
                            "value_keys": OrderedDict().fromkeys(["hostname", "streams", "type", "packets_in_flight", "queue_len", "retrans_collapse", "rtt", "itt", "payload",
                                                                  "duration", "data_packet_count", "rdb_packet_count",
                                                                  #"collapse_thin", "segoff", "segoff_thin", "segoff_thick",
                                                                  #"gso_thin", "tso_thin", "lro_thin", "gro_thin",
                                                                  #"segoff_thick", "bridge_rate",
                                                                  "bytes_loss", "packet_loss", "num", "R_loss_stats", "R_latency_stats", "latency"], None),
                            "default_column_width": 12,

                        })

    process_results["column_widths"].update({"Host": 11, "Str": 7, "RDB": 3, "PIF": 3, "QLEN": 6, "RC": 2, "RTT": 4, "D-PKT": 6, "B-PKT": 6,
                                             "ITT": 6, "PS": 4, "Dur": 3, "soff": 4, "soff_t": 4, "soff_g": 4,
                                             "gso_t": 5, "tso_t": 5, "gro_t": 5, "lro_t": 5,
                                             "3rd Qu.": 6, "Test": 4, "bridge_rate" : 15, "clps-t": 6,
                                             "R_loss_stats": 35, "R_latency_stats": 30, "latency": 45,
                                             "BLoss": 8, "PLoss": 5 })

    def hostname_color(hostname, row_values):
        return colored(hostname, "yellow" if hostname == "ysender" else "white")

    process_results["value_keys"]["hostname"] = hostname_color

    def loss_too_high_color(loss, row_values):
        color = "white"
        try:
            if float(loss) > 12:
                color = "red"
        except:
            pass
        return colored(loss, color)
    process_results["value_keys"]["bytes_loss"] = loss_too_high_color

    if os.path.isfile(conf["output_file"]):
        os.remove(conf["output_file"])

    #conf["file_parse"]["files_match_regex"].append(".*min10.*")
    #conf["file_parse"]["files_match_regex"].append(".*num1.*")
    #conf["file_parse"]["files_match_regex"].append("t10-tcp-itt20-ps400_vs_g10_kbit5000_min10_rtt150_loss_pif0_qlen46875_delayfixed_num0_rdbsender.pcap")

    return conf

get_conf = _get_default_conf
