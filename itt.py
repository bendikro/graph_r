def itt_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
    y_axis_max = plot_conf["plot_conf"]["bandwidth"] * plot_conf["plot_conf"]["bandwidth_axis_scale"]
    plot_conf["plot_conf"]["y_axis_lim"] = [0, y_axis_max * 1000] # Multiply by 1000 to get bit/s
    plot_conf["plot_conf"]["y_axis_range"] = xrange(0, int(y_axis_max), 100)
    plot_conf["y_axis_data"] = [i * 1000 for i in plot_conf["plot_conf"]["y_axis_range"]]
    plot_conf["y_axis_labels"] = [i for i in plot_conf["plot_conf"]["y_axis_range"]]

    file_conf["label"] = "TCP" if file_conf["packets_in_flight"] == 0 else "%s PIF:%s" % (file_conf["type_thin1"].upper(), file_conf["packets_in_flight"])
    if file_conf["packets_in_flight"] == 0 and file_conf["type_thin1"] == "rdb":
        # Dynamic PIF
        #print "file_data:", file_data
        file_conf["label"] = "RDB DPIF%s" % file_conf["dpif"]
        #file_data["color"] = graph_default.color_map[file_data["label"]]
        #color_key = file_data["label"].lower()

    file_conf["color"] = "darkred" # Default color for dataset
    pif = file_conf["packets_in_flight"]

    file_conf["plot_type"] = parse_func_arg
    file_conf["stream_id"] = "%s %s" % (file_conf["stream_type"].title(), file_conf["type"].upper())
    #print "stream_type:", file_conf["stream_type"].title()
    #print "stream_id:", file_conf["stream_id"]

    basename = os.path.basename(pcap_file)
    results_dict = {}
    results_dict["data_file_name"] = "%s" % (basename.split(plot_conf["file_parse"]["pcap_suffix"])[0])
    results_dict["csv_file"] = os.path.join(plot_conf["output_dir"], "%s.goodput.csv" % results_dict["data_file_name"])
    results_dict["err_file"] = os.path.join(plot_conf["output_dir"], "%s.goodput.err" % results_dict["data_file_name"])
    results_dict["results_file"] = results_dict["csv_file"]
    results_dict["sample_sec"] = str(int(plot_conf["sample_sec"] * 1000))
    results_dict["results_cmd"] = "./tcp-throughput -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -t %(sample_sec)s 1> %(csv_file)s 2> %(err_file)s" %\
        dict(file_conf, **results_dict)

    file_conf["prefix"] = "%s-" % results_dict["data_file_name"]

    file_conf["results"] = {"itt": results_dict}

    #PAPER_LATENCY_WITH_BUNDLING_LIMIT/r10-tcp-itt30:3-ps120-ccrdb-da0-lt0-er3-tfrc0_vs_w10-tcp-itt30:3-ps120-ccrdb_vs_z5..kbit5000_min5_rtt150_loss_pif0_qlen63_delayfixed_num0_wsender-per-packet-itt-size-aggr.dat

    analystcp_results_dict = {}
    file_conf["results"]["itt"] = analystcp_results_dict
    analystcp_results_dict["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "itt.stdout"))
    analystcp_results_dict["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "per-packet-itt-size-aggr.dat"))
    analystcp_results_dict["results_cmd"] =\
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s -o %(output_dir)s -A -vv 1> %(stdout_file)s" %\
        dict(file_conf, **analystcp_results_dict)
