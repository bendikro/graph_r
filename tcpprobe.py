import graph_default
import graph
from graph_r import *
from graph_r_ggplot import plot_scatterplot_cwnd, do_ggplots

import util
import common

#host_keys_mapping = {
#    "rdbsender": {"type": "thin1", "stream_count": "stream_count_thin1"},
#    "zsender":   {"type": "thick", "strea_count": "stream_count_thick" }
#}

#92.286346017 10.0.0.12:22016 10.0.0.22:5000 32 437241522 437241162 5 4 5888 194 29312
def read_tcp_probe_output(file_conf, only_changes=False):
    from collections import defaultdict
    filename = file_conf["results"]["tcpprobe"]["results_file"]
    #filename = file_conf["tcp_probe_data_file"]
    if not os.path.isfile(filename):
        return

    f = open(filename, "r")
    res = defaultdict(lambda: [])
    stream_col = []
    time_col = []
    cwnd_col = []
    type_col = []
    type_id_col = []

    #print
    print "read_tcp_probe_output (only changes: %s) filename: %s" % (only_changes, filename)
    #print "Type:", file_conf["type"]
    #print "type_id:", file_conf["type_id"]
    lines = f.readlines()
    saved_log = None

    def add_log(log):
        res[log.key] += [(log.s_cwnd, log.s_ssthresh, log.s_type)] # snd_cwnd, ssthresh
        stream_col.append(log.key)
        time_col.append(log.s_time)
        cwnd_col.append(log.s_cwnd)
        type_col.append(log.s_type)
        type_id_col.append(log.s_type_id)


    class Log(object):
        pass

    for i, line in enumerate(lines):
        #xprint "Line:", line
        l = line.split()
        log = Log()
        log.key = l[1] + "-" + l[2]
        log.s_type = l[1].split(":")[0]
        log.s_type = file_conf["type"]
        log.s_type_id = file_conf["type_id"]
        log.s_time = l[0]
        log.s_cwnd = l[6]
        log.s_ssthresh = l[7]

        if only_changes and res[log.key]:
            # If this is not the last value
            if i != (len(lines) -1):
                # Check the cwnd value of the last value
                if res[log.key][-1][0] == log.s_cwnd:
                    # Same value, so ignore
                    saved_log = log
                    continue
                    #pass
            else:
                vprint("USE THE LAST VALUE: s_time: %s, cwnd: %s" % (log.s_time, log.s_cwnd), v=3)

        #if saved_log:
        #    add_log(saved_log)
        #    saved_log = None

        add_log(log)
        #res[key] += [(s_cwnd, s_ssthresh, s_type)] # snd_cwnd, ssthresh
        #stream_col.append(key)
        #time_col.append(s_time)
        #cwnd_col.append(s_cwnd)
        #type_col.append(s_type)
        #type_id_col.append(s_type_id)

        #if i == 10000:
        #    break

    if saved_log:
        add_log(saved_log)
        saved_log = None


    #print "LOSS:", file_conf["loss"]
    #print "RES:", len(res)
    #for k in res:
    #    print "%s : %d" % (k, len(res[k]))
    dataf = DataFrame({'time': FloatVector(time_col), 'stream': StrVector(stream_col), 'cwnd': IntVector(cwnd_col),
                       'type': StrVector(type_col), 'type_id': StrVector(type_id_col),
                       "loss": "Loss\n%s%%" % file_conf["loss"]
                   })
    #print "dataf:", dataf
    #print "dataf:", dir(dataf)
    #print "dataf:", dataf.names
    return dataf

def tcp_probe_results_parse(conf, plot_box_dict, file_conf, set_key):
    #print "tcp_probe_results_parse:", file_conf["hostname"]
    only_changes = True
    if file_conf["hostname"] != "zsender":
        #only_changes = True
        only_changes = False
    data = read_tcp_probe_output(file_conf, only_changes=only_changes)
    file_conf["data"] = data
    #print "DATA:", data
    #print "tcp_probe_data_file:", file_conf["tcp_probe_data_file"]
    plot_box_dict["span"] = 0.1

    plot_box_dict["y_axis_range"] = xrange(0, 10, 50)
    plot_box_dict["y_axis_data"] = [i for i in plot_box_dict["y_axis_range"]]
    plot_box_dict["y_axis_labels"] = [i for i in plot_box_dict["y_axis_range"]]

    #Bytes Loss                                    :       0.87 %

    keys = [
        ("Total bytes sent (payload)", "Total bytes sent (payload)", "(?P<value>\d+).*"),
        ("Data", "Number of unique bytes", "(?P<value>\d+).*"),
        ("Retrans", "Number of retransmitted bytes", "(?P<value>\d+).*"),
        ("RDB Miss", "RDB byte   misses", "(?P<value>\d+).*"),
        ("RDB Hit", "RDB byte   hits", "(?P<value>\d+).*"),
        ("Bytes Loss", "Bytes Loss", "(?P<value>\d+\.\d+).*"),
        ]
    data, data_dict = util.read_output_data(keys, file_conf["results"]["analysetcp"]["stdout_file"])

    file_conf["analysetcp_data"] = data_dict

    #print "analysetcp_data:", data_dict

    plot_box_dict["table_data_keys"] = OrderedDict([("Name",        {"key": "name",  "type": "str", "location": "table_data"}),
                                               ("Type",        {"key": "type",  "type": "str"}),
                                               ("Streams",     {"key": "stream_count",  "type": "int"}),
                                               ("Cong",        {"key": "cong_control", "type": "str"}),
                                               ("PIF Lim",     {"key": "packets_in_flight",  "type": "int"}),
                                               ("Data points", {"key": "data_points", "type": "int", "location": "table_data"}),
                                               ("ITT",         {"key": "itt", "type": "str"}),
                                               ("Payload",     {"key": "payload", "type": "int"}),
                                           ])

    common.do_stream_legend_and_color(plot_box_dict, file_conf, file_conf["data"])

def tcp_probe_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    print "tcp_probe_file_parse_func !!!!"
    if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    util.set_dict_defaults_recursive(file_conf, (("results", "tcpprobe"), {}))
    file_conf["results"]["tcpprobe"]["results_file"] = pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpproberdb.out")

    file_conf["group"] = file_conf["type_id"]


    if not "cong_control_thin1" in file_conf:
        file_conf["cong_control_thin1"] = "cubic"

    #analysetcp_stdout_file = pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".analysetcp.out")
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    analysetcp_stdout_file = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "analysetcp.out"))

    #file_conf["results"] = {"analysetcp": {}}
    util.set_dict_defaults_recursive(file_conf, (("results", "analysetcp"), {}))
    file_conf["results"]["analysetcp"]["stdout_file"] = analysetcp_stdout_file
    file_conf["results"]["analysetcp"]["results_file"] = analysetcp_stdout_file
    file_conf["results"]["analysetcp"]["results_cmd"] =\
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -o %(output_dir)s 1> %(stdout_file)s" %\
        dict(file_conf, **file_conf["results"]["analysetcp"])

    file_conf["color"] = "darkred" # Default color for dataset

    stream_properties = file_conf["streams"][file_conf["streams_id"]]
    stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
                                           "factor": "stream",
                                           "type_id_column": "stream", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                           "breaks_factor": "stream",  # These are for the legend
                                           "aes_args": { "color": 'color_column', "group": 'color_column'} # These are for the general factoring of the data
                                       })

    stream_properties = file_conf["streams"][file_conf["streams_id"]]
    stream_properties.update({"stream_count": 10, "smooth": False, "line": True, "factor": "stream",
                              "type_id_column": "type_id" }) # Used to count the data points for each type

    stream_properties["color_mapping"] = graph_default.stream_type_to_ggplot_properties[file_conf["streams_id"]]["color_mapping"]

    #print "streams_id:", file_conf["streams_id"]

def get_tcpprobe_conf():
    conf = graph_default.get_conf()
    conf["output_file"] = util.cmd_args.tcp_probe_output_file
    #conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s",
    #conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_host:%(hostname)s",
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Streams:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_"\
                             "RTT:%(rtt)s_cong:%(cong_control_thin1)s_PIF:%(packets_in_flight)s_QLEN:%(queue_len)s_NUM:%(num)s_%(thin_dupack)s",
                         "sort_key_func": lambda x: (x["thin_dupack"], x["stream_count_thin1"], x["itt_thin1"], x["cong_control_thin1"]),
                         "sort_keys": ["thin_dupack", "stream_count_thin1", "itt_thin1", "cong_control_thin1"],
                         #"func" : latency_box_key_func,
                         "box_title_def" : "%(stream_count_thin1)dt vs %(stream_count_thick)dg Duration: %(duration)smin Queue: %(queue_type)s (%(queue_len)s) DA:%(thin_dupack)s " }
    #conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIF:%(packets_in_flight)s",
                         "sort_key_func": lambda x: (x["stream_count_thin1"], x["itt_thin1"], int(x["packets_in_flight"])),
                         "sort_keys": ["stream_count_thin1", "itt_thin1"],
                         }
    #conf["page_group_def"] = {"title": "Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s",
    #                          "sort_keys": ["payload_thin1", "itt_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2])}
    conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 1,
                               #"x_axis_lim" : [27, 60],
                               "x_axis_lim" : [10, 350],
                               "y_axis_lim" : [0, 25],
                               "x_axis_font_size": 0.7,
                               "x_axis_main_ticks": 1,
                               "x_axis_minor_ticks": .5,
                               "y_axis_main_ticks": 10,
                               "y_axis_minor_ticks": 5,
                               "theme_args": {"legend.position": "left"},

                               "default_factor": "stream",
                               #"y_axis_title" : "Congestion window size",
                               #"x_axis_title" : { "title": ["Stream type", "THICK", "THIN", "RDB:PIF:3", "RDB:PIF:6", "RDB:PIF:10", "RDB:PIF:20"],
                               #                   "adj": [0, .3, .4, .5, .65, .75, .9],
                               #                   "colors" : [color_map["default"], color_map["thick"], color_map["thin"], color_map["rdb_pif3"],
                               #                               color_map["rdb_pif6"], color_map["rdb_pif10"], color_map["rdb_pif20"]]},
                               #"plot_func": plot_scatterplot,
                               "plot_func": plot_scatterplot_cwnd,
                               "do_plots_func": do_ggplots,
                               })
    conf["parse_results_func"] = tcp_probe_results_parse
    conf["file_parse"].update({ "func": tcp_probe_file_parse_func })
    conf["document_info"]["title"] = "TCP CONGESTION WINDOW Plots"
    conf["document_info"]["show"] = False
    conf["paper_width"] = 11
    conf["paper_width"] = 200
    conf["paper_height"] = 8
    conf["print_page_title"] = False

    #conf["file_parse"]["files_match_regex"].append("t9-rdb-itt100:15-ps120_vs_g5_kbit5000_min10_rtt150_loss_pif20_qlen46875_delayfixed_num0_rdbsender.*")
    #conf["file_parse"]["files_match_regex"].append(".*min10.*")
    #conf["file_parse"]["files_match_regex"].append(".*ps451.*")
    #conf["file_parse"]["files_match_regex"].append("t1-rdb-itt64-ps451-ccvegas_vs_g2_kbit5000_min10_rtt150_loss_pif300_qlen30_delayfixed_num0_.*")
    #conf["file_parse"]["files_match_regex"].append("t1-rdb-itt64.*")
    conf["file_parse"]["files_nomatch_regex"].append(".*qlen46875.*")
    return conf

get_conf = get_tcpprobe_conf
