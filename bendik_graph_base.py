#!/usr/bin/env python
from datetime import datetime
import argparse
import os
import sys

import graph_base

from graph_default_bendik import get_default_conf

import graph_default
graph_default.get_conf = get_default_conf

from graph_default_bendik import get_latency_conf

import latency
latency.get_conf = get_latency_conf

if __name__ == "__main__":
    plot_types = ["latency-netem-pif", "latency-x-traffic-dpif", "misuser-dpif-tfrc", "thin-stream-mod-CDF", "thin-stream-mod-CDF-per-stream"]
    argparser = argparse.ArgumentParser(description="Create Master plots", usage='%(prog)s [options] command directories')
    options = argparser.add_argument_group('Latency CDF options')
    options.add_argument("--plots",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, action='append', default=[])

    args = argparser.parse_args()
    # Remove any arguments on args list
    sys.argv = sys.argv[:1]

    def available_plots():
        print "Avilable plots:"
        for p in plot_types:
            print "- %s" % p

    if not args.plots:
        print "No plot specified"
        argparser.print_help()
        sys.exit()

    for p_arg in args.plots:
        if not p_arg in plot_types:
            print "Invalid plot: '%s'" % p_arg
            available_plots()
            break

        defaults = {}
        if p_arg == "latency-netem-pif":
            from plot_rdb_latencies_netem_PIF import run as plotter_func
            defaults["plot_type"] = "default"

        elif p_arg == "thin-stream-mod-CDF":
            from plot_latency_thin_stream_mod_CDF_steps import run as plotter_func
            defaults["plot_type"] = "lt_du_eretr"

        elif p_arg == "thin-stream-mod-CDF-per-stream":
            from plot_latency_thin_stream_mod_CDF_steps import run as plotter_func
            defaults["plot_type"] = "lt_du_eretr_per_stream"

        elif p_arg == "latency-x-traffic-dpif":
            from plot_latency_rdb_vs_greedy import run as plotter_func
            defaults["plot_type"] = "rwz_greedy_tfrc_with_throughput"

        elif p_arg == "misuser-dpif-tfrc":
            from plot_latency_rdb_vs_greedy import run as plotter_func
            defaults["plot_type"] = "rwz_misuser_dpif_tfrc"

        elif p_arg == "throughput_rdbmisuse":
            from plot_throughput_rdbmisuse import run as plotter_func
            defaults["plot_type"] = "throughput_rdbmisuser"

        elif p_arg == "cwnd_netem_loss_tcp":
            from plot_cwnd import run as plotter_func
            defaults["plot_type"] = "netem_loss_tcp"

        elif p_arg == "cwnd_ryz_tfrc":
            from plot_cwnd import run as plotter_func
            defaults["plot_type"] = "ryz_tfrc"

        plotter_func(defaults)

    #graph_base.run()


"""

# thin_stream_mod_CDF
plot_latency_thin_stream_mod_CDF_steps.py -lpt lt_du_eretr

# latency-netem-pif
plot_rdb_latencies_netem_PIF.py -lpt default -vvv

# latency-x-traffic-dpif
plot_latency_rdb_vs_greedy.py -lpt rwz_greedy_tfrc_with_throughput -vv

# misuser-dpif-tfrc
plot_latency_rdb_vs_greedy.py -lpt rwz_misuser_dpif_tfrc -vv

"""
