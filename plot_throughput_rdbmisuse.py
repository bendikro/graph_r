#!/usr/bin/env python
from datetime import datetime
import argparse
import os, sys
from functools import partial

import graph_base
from graph_default_bendik import get_default_conf
import graph_default

from graph_r_ggplot import ggplot_boxplot_box, do_ggplots, ggplot2
from graph_r import *

from throughput import get_throughput_conf as get_throughput_conf_original
from throughput import throughput_results_parse, throughput_file_parse_func, goodput_file_parse_func, throughput_box_key_func

import latency
import data_tables

import common
import util

graph_default.get_conf = get_default_conf

#./bendik_graph_base.py -vvv  -d ../pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TEST_THIN_RDBMISUSE_NvsN/all_results/ -od RDBSENDER_ZSENDER_TEST_THIN_RDBMISUSE_NvsN -t RDBSENDER_ZSENDER_TEST_THIN_RDBMISUSE_NvsN_throughput2.pdf -frm "t6.*.*itt10-ps400.*num1.*"

def get_table_latex_test_setup_with_name_testindex(*args):
    latex_test_setup_table = data_tables.get_table_latex_test_setup()
    latex_test_setup_table["keys"]["Name"].update({"show": True, "location": "plot_dict", "key": "label"})
    latex_test_setup_table["keys"]["Test"].update({"show": True, "key": "legend_order", "location": "plot_dict"})
    #latex_test_setup_table["keys"]["Type"].update({"key": "legend_name", "location": "color_mapping"})
    latex_test_setup_table["keys"]["Type"].update({"key": "legend_name", "location": "plot_dict", "func": lambda x: x})
    latex_test_setup_table["group_keys"] = ["legend_order"]

    #latex_test_setup_table["sort"] = { "key": "Name", "do_sort": True, "sorted_args": { "key": lambda x: (x["Streams"], get_stdev(x["ITT"], stdev=False), x["legend_order"]), "reverse": False } }
    latex_test_setup_table["sort"] = { "key": "Name", "do_sort": True, "sorted_args": { "key": lambda x: (x["legend_order"], data_tables.get_stdev(x["ITT"], stdev=True)), "reverse": False } }

    def na_if_greedy_func(table_conf, new_table_entry, data, data_name):
        #print "set_analysetcp_data:", table_conf
        #latencies = data
        #print "data:", data
        #print "Bytes Loss:", data["Bytes Loss"]

        if data["hostname"] == "rdbsender":
            return

        for k, v in table_conf["keys"].iteritems():
            #print "K:", k
            #if "location" not in v:
            #    continue
            #print "KEY:", v["key"]
            if v["key"] in ["itt"]:
                new_table_entry[k] = "NA"

            #if v["location"] == data_name:  #  "analysetcp"
            #    #print "Found analysetcp"
            #    value = data[v["key"]]
            #    if "func" in v:
            #        value = v["func"](value)
            #    new_table_entry[k] = value

    latex_test_setup_table["data_func"] = na_if_greedy_func
    latex_test_setup_table["filename"] = "latex_test_setup_table.tex"
    latex_test_setup_table["path_prefix"] = "unset"
    return latex_test_setup_table


stream_type_to_ggplot_properties = {"r": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Blues")[7:7])', "legend_name": "Thick %(type_upper)s"}]},
                                    "y": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                    "t": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thick %(type_upper)s"}]},
                                    "w": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                    "z": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Greedy"}]},
                                    "g": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Greedy"}]}}


def throughput_results_parse_custom(conf, plot_box_dict, file_data, set_key):

    stream_properties = file_data["streams"][file_data["streams_id"]]
    if not "color_mapping" in stream_properties:
        stream_properties["color_mapping"] = deepcopy(stream_type_to_ggplot_properties[file_data["streams_id"]]["color_mapping"])
        #pass
        #print "UPPER:", file_data.get("type_upper", None)

        stream_properties["color_mapping"][0]["colors"] = [file_data["color"]]
        file_data["legend_name"] = util.make_title(plot_box_dict, stream_properties, stream_properties["color_mapping"][0]["legend_name"])

    throughput_results_parse(conf, plot_box_dict, file_data, set_key)

    if not "latex_test_setup_table" in conf["data_tables"]:
        data_tables.fetch_table_conf(table_func=get_table_latex_test_setup_with_name_testindex, conf=conf, table_key="latex_test_setup_table")

    #print "DATA:", file_data["data"].names
    #file_data["data"].

    #print "PLOT TYPE:", file_data["plot_type"]

    #if file_data["hostname"] == "rdbsender":
    if file_data["plot_type"] == "Throughput":
        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data, conf["data_tables"]["latex_test_setup_table"], data_name="greedy_na")

    #common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], plot_box_dict["data_tables"]["analysetcp_results_table"], data_name="analysetcp")

    stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
                                           "factor": "color_column",
                                           "type_id_column": "color_column", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                           "breaks_factor": "color_column",  # These are for the legend
                                           "aes_args": { "color": 'color_column',
                                                         #"col": 'factor(stream_type)',
                                                         #"group": 'factor(type)',
                                                         "x": "type",
                                                         "col": 'factor(type)',
                                                         #"group": 'factor(color_column)'
                                                     } # These are for the general factoring of the data
                                       })


def throughput_process_results(conf, groups_list=None):
    #write_latex_table_results(conf)
    #write_latex_table_test_setup(conf["data_tables"]["latex_test_setup_table"])
    tconf = conf["data_tables"]["latex_test_setup_table"]
    if not util.cmd_args.skip_data:
        latency.write_latex_table_test_setup(tconf)
    pass

def write_latex_table_test_setup(conf):
    # Sort by plot number and then the name
    keys = conf["latex_test_setup_table_keys"]
    data = conf["test_setup_table_data"]
    #print "keys:", keys
    #print "data:", data

    data_list = []
    for d in data:
        #print "D:", d
        new_d = {}
        for k in keys:
            data_key = keys[k]["key"]
            new_d[data_key] = d[k]
        data_list.append(new_d)

    col_count = len(data_list[0])
    print "Col count:", col_count
    #print "data_list:", data_list
    header = r"""\begin{tabular}{%s}""" % ("c" * col_count)

    fname = "%s_%s" % (os.path.splitext(conf["output_file"])[0], "latex_table_test_setup.tex")
    print "fname:", fname
    common.write_latex_table(conf, header, keys, data_list, fname, group_keys=None)


def throughput_rdbmisuse_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if parse_func_arg == "Throughput":
        if throughput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False
    elif parse_func_arg == "Goodput":
        if goodput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False

    stream_type = "thick" if file_conf["stream_type"] == "thin" else "greedy"

    file_conf["stream_id"] = "%s %s" % (stream_type.title(), file_conf["type"].upper())
    #print "stream_id:", file_conf["stream_id"]

    stream_id_to_color = { "Thick RDB": "#0072B2", # "blue"
                           "Thick TCP": "#ffdd36", # "yellow"
                           "Greedy TCP": "#bd0527" # "red"
    }

    file_conf["color"] = stream_id_to_color[file_conf["stream_id"]]

def box_key_func(plot_conf, plot_box_dict, file_data, arg):
    throughput_box_key_func(plot_conf, plot_box_dict, file_data)
    #print "\n\n\n\nconf:", box_conf.keys()
    #print "plot_conf:", plot_conf
    #print "box_conf:", box_conf
    #args = {"sample_sec": plot_conf["sample_sec"], }
    #file_conf["plot_type"] = parse_func_arg
    #print "box_conf:", box_conf
    #plot_conf["plot_conf"]["axis_conf"].update({ "x_axis_title" : { "title": "TCP variation used for competing streams", },
    #plot_box_dict["y_axis_title"]["title"] = "%(plot_type)s (Kbit/second aggregated over %(sample_sec)d seconds)" % dict(file_data, **plot_conf)
    plot_box_dict["axis_conf"]["y_axis_title"]["title"] = "%(plot_type)s (Kbit/second aggregated over %(sample_sec)d seconds)" % dict(file_data, **plot_conf)
    file_data["legend_order"] = file_data["packets_in_flight"]
    if file_data["legend_order"] == 0:
        file_data["legend_order"] = 1
    elif file_data["legend_order"] == 4:
        file_data["legend_order"] = 2
    elif file_data["legend_order"] == 20:
        file_data["legend_order"] = 3
    elif file_data["legend_order"] == 100:
        file_data["legend_order"] = 4

    #print "plot_box_dict:", plot_box_dict.keys()

def get_throughput_conf(defaults):
    conf = get_throughput_conf_original(defaults)
    conf["document_info"]["show"] = False
    conf["paper_width"] = 5
    conf["paper_height"] = 4
    conf["plot_conf"].update({"n_columns": 1, "n_rows": 1})
    conf["box_conf"].update({ "box_titles": { "main": { "title_def": "%(stream_count_thin1)d Greedy streams vs %(stream_count_thick)d misusers", "order": 1, "func": "scriptstyle" },
                                              "sub":  { "title_def": "ITT: %(itt_thin1)s ms   RTT: %(rtt)s ms  QLEN: %(queue_len)s", "order": 2 },
                                              "plot_type": { "title_def": "%(plot_type)s", "order": 0 } },
                              "func" : box_key_func})

    #"box_titles": { "main": { "title_def": "ITT: %(itt)s ms   RTT: %(rtt)s ms  QLEN: %(queue_len)s", "order": 2 },
    #                                          "top_title2": { "title_def": "Duration: %(duration)s min   TFRC: %(tfrc)s   Thin %(stream_count_thin1)s vs Greedy %(stream_count_thick)s", "order": 1 },
    #                                      },


    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1, #"x_axis_lim" : [0, 5000],
                              #"y_axis_lim" : [0, 5000],
                              "do_plots_func": do_ggplots, #do_plots,
                              "plot_func": ggplot_boxplot_box, #plot_boxplot,
                              })



    conf["plot_conf"]["axis_conf"].update({ "x_axis_title" : { "title": "TCP variation used for competing streams", },
                                            "y_axis_lim" : [0, 5000],
                                            "set_x_axis": False, "set_y_axis": False,
                                            "y_axis_main_ticks": [1000, 5001, 1000], "y_axis_minor_ticks": 200,
                                            "x_axis_label_key": "label",
                                            #"x_axis_main_ticks": [200, 800, 100], "x_axis_minor_ticks": 50,
                                            "y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                        })

    conf["parse_results_func"] = throughput_results_parse_custom

    conf["plot_conf"]["theme_args"].update({#"legend.position": FloatVector([0.87, 0.35]),
        #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
        #"legend.key.size": r('unit(6, "mm")'),
        #"legend.title": r('element_text(size=10, color="grey20", face="plain")'),
        "legend.title": ggplot2.element_blank(),
        "legend.position": "bottom",
        "legend.text": r('element_text(size=8, color="grey20")'),
        "axis.title.x": r('element_text(size = 9, hjust=0.5, vjust=-0.5, color="grey20")'),
        "axis.title.y": r('element_text(size = 9, hjust=0.5, vjust=1.3, color="grey20")'),
        "axis.text.x": r('element_text(size = 9, hjust=0.5, vjust=0.5)'), # , color="grey50"
        "axis.text.y": r('element_text(size = 9, hjust=0.0, vjust=0.5)'),
        #"plot.title": r('element_text(size=8, lineheight=1, vjust=1.1, hjust=0.5, color="grey20")'), # , face="bold"
        "plot.title": r('element_text(size=14, lineheight=1, vjust=1.3, hjust=0.5, color="grey20")'), # , face="bold"
        #"panel.margin": r('unit(0 , "lines")'),
        #"panel.border": r('element_rect(fill=NA,color="darkred", size=0.5, linetype="solid")'),
        #  "plot.margin": r('rep(unit(0,"null"),4)'),
        "plot.margin": r('unit(c(0.4, 0, 0, 0.3), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
    })


    conf["file_parse"].update({ "func": throughput_rdbmisuse_file_parse_func })
    #conf["process_results"] = { "func": throughput_process_results }
    conf["process_results"]["throughput"] = { "func": throughput_process_results }

    conf["file_parse"]["parse_func_arg"] = {"Goodput", "Throughput"}
    #conf["file_parse"]["parse_func_arg"] = {"Throughput"}

    conf["box_conf"]["plot_and_table"] = conf["box_conf"].get("plot_and_table", {})
    from common import TABLE_ACTION
    conf["box_conf"]["plot_and_table"].update({"make_table": TABLE_ACTION.NO, #TABLE_ACTION.APPEND_TO_PLOT_BOX,
                                               #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                               "heights": (.65, .03, .3),
                                           })

    #conf["plot_conf"]["y_axis_title"] = "Throughput "
    #conf["file_parse"]["files_match_exp"] = "t6.*.*itt10-ps400.*num1.*"
    return conf

import throughput
throughput.get_conf = get_throughput_conf

def run(defaults={}):
    plot_types = ["rdb_latency_misuse", "rdb_dpif_misuse"]

    argparser = graph_base.make_parser()
    options = argparser.add_argument_group('Throughput Misuser options')
    options.add_argument("-pt", "--plot-type",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, default=plot_types[0])

    defaults = {}
    args = graph_base.parse_args(argparser, defaults=defaults)
    plot_type = args.plot_type

    if plot_type == "rdb_latency_misuse":
        def_conf = {"data_table_defaults": {"latex_test_setup_table": { "path_prefix": "rdb_latency_misuse" }}}
        throughput.get_conf = partial(get_throughput_conf, defaults=def_conf)

        defaults["throughput_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_RDBMISUSE_NvsN_throughput_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TEST_THIN_RDBMISUSE_NvsN"
        defaults["file_regex_match"] = ["t6.*itt10-ps400.*g6.*num0.*"]
        #*t6*itt10-ps400*g6*"
        #defaults["file_regex_nomatch"] = [".*min20.*", ".*zsender.*", ".*z5.*", ".*r5.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TEST_THIN_RDBMISUSE_NvsN/all_results/"]
    elif plot_type == "rdb_dpif_misuse":
        defaults["throughput_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_MISUSE_throughput_dpif.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_MISUSE"
        #defaults["file_regex_match"] = [".*z5.*"]
        #defaults["file_regex_nomatch"] = [".*min20.*", ".*zsender.*", ".*z5.*", ".*r5.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_vs_GREEDY_MISUSE/all_results/"]
    else:
        cprint("Invalid plot type: '%s'" % plot_type, "red")
        sys.exit()

    args = graph_base.parse_args(argparser, defaults=defaults)
    graph_base.main()


if __name__ == "__main__":
    run()
