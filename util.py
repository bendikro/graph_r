from collections import OrderedDict
import re
from copy import deepcopy
import signal
import sys
from copy import deepcopy
import argparse, os

cmd_args = None
DEBUG_CONF = {"types": [], "funcs": []}

try:
    from termcolor import colored, cprint
    termcolor = True
except:
    print("termcolor could not be found. To enable colors in terminal output, install termcolor.")
    termcolor = False
    def cprint(*arg, **kwargs):
        print arg
    def colored(text, color):
        return text

def vprint(*arg, **kwargs):
    if "v" in kwargs:
        if cmd_args.verbose < kwargs["v"]:
            return
    print ' '.join(map(str, arg))

def signal_handler(signal, frame):
    cprint('SIGINT caught. Exiting!', "red")
    sys.exit(0)

def install_sigint_handler():
    signal.signal(signal.SIGINT, signal_handler)

install_sigint_handler()
"""
       def_conf = {"data_table_defaults": { "latex_test_setup_table": { "path_prefix": "latency-x-traffic-dpif"},
                                             "analysetcp_results_table": { "recurse": "keys", "data": {"Name": {"show": False}}} },
                }
"""


def debug_print(*arg, **kwargs):
    debug_type = kwargs.get("debug_type", "default")
    func_stack_depth = kwargs.get("func_stack_depth", 2)
    import inspect
    callfunc = inspect.stack()[1][3]
    if debug_type not in DEBUG_CONF["types"] and callfunc not in DEBUG_CONF["funcs"]:
        return
    import traceback
    fname = traceback.extract_stack(None, func_stack_depth)[0][2]
    if type(arg) is not tuple:
        arg = (arg,)
    print "%s %s" % (colored("%s:" % (fname), 'magenta'), ' '.join(str(a) for a in arg))

def dict_set_default(d, defaults):
    if type(defaults) is not list:
        defaults = [defaults]
    for k, v in defaults:
        if k not in d:
            d[k] = v


def set_dict_defaults_recursive(d, defaults):
    """
    Usage: set_dict_defaults_recursive(file_conf, (("results", "tcpprobe"), {}))
    """
    if type(defaults) is not list:
        defaults = [defaults]
    for keys, v in defaults:
        d_tmp = d
        #print "d_tmp:", d_tmp
        if type(keys) is str:
            keys = (keys,)
        if type(keys) is not list:
            keys = list(keys)
        while len(keys) > 1:
            k = keys.pop(0)
            d_tmp[k] = d_tmp.get(k, {})
            d_tmp = d_tmp[k]
        k = keys.pop(0)
        d_tmp[k] = d_tmp.get(k, v)

def update_dict(defaults, _dict, split="."):
    #print "update_with_defaults:", defaults
    for k, v in defaults.iteritems():
        #print "update_dict - %s : %s" % (k, v)
        tmp_conf = _dict
        keys = k.split(split)
        #print "keys:", keys
        tmp_key = keys[0]
        while len(keys) > 1:
            tmp_key = keys.pop(0)
            #print "entering:", tmp_key
            tmp_conf = tmp_conf[tmp_key]
            tmp_key = keys[0]
        #print "Updatting '%s' with: '%s'" % (tmp_key, v)
        if tmp_key in tmp_conf and type(tmp_conf[tmp_key]) is dict:
            tmp_conf[tmp_key].update(v)
        else:
            tmp_conf[tmp_key] = v
        #print "tmp_conf.keys:", tmp_conf.keys()
        #print "conf.data_table_defaults:", conf["data_table_defaults"]

def make_box_title(plot_box_dict, file_conf):
    import rpy2.robjects.lib.ggplot2 as ggplot2
    from rpy2.robjects import r, Formula
    import rpy2.rinterface as ri
    #print "make_box_title:", plot_box_dict.keys()
    #print "make_box_title:", plot_box_dict["box_conf"].keys()
    if "box_titles" not in plot_box_dict["box_conf"]:
        return make_title(plot_box_dict, file_conf, plot_box_dict["box_conf"]["box_title_def"])

    titles = plot_box_dict["box_conf"]["box_titles"]
    title = ""
    append = False
    for t in sorted(titles, key=lambda t: titles[t].get("order", "title_def"), reverse=True):
        #print " %s: %s" % (t, titles[t])
        title_tmp = '%s' % make_title(plot_box_dict, file_conf, titles[t]["title_def"])
        #print "Making title (%s): %s" % (t, title)

        if "func" in titles[t]:
            #print "FUNC (%s): (%s)" % (titles[t]["func"], title_tmp)
            title_tmp = '%s("%s")' % (titles[t]["func"], title_tmp)
        else:
            title_tmp = '"%s"' % title_tmp
        if title:
            if append:
                title = 'plain("%s %s")' % (title, title_tmp)
            else:
                title = 'atop(%s, %s)' % (title_tmp, title)
        else:
            title = title_tmp
        append = titles[t].get("append", False)
    vprint("box title: '%s'" % title, v=3)
    try:
        ret = ri.parse(title)
    except ValueError as e:
        cprint("Error while parsing title ('%s'): %s" % (title, e), "red")
        raise
    return ret


def make_title(plot_box_dict, file_conf, title_def):
    from collections import defaultdict
    title_values_dict = defaultdict(lambda: "<missing>", **file_conf)
    title_values_dict.update(plot_box_dict)
    #print "plot_box_dict:", plot_box_dict.keys()
    try:
        title_def = title_def % title_values_dict
    except:
        import traceback
        traceback.print_exc()
        traceback.print_stack()
        print "title_def:", title_def
        print "title_values_dict:", title_values_dict
        print_keys(title_values_dict, keys=["stream_count_thin1", "stream_count_thick"])
        #print "title_values_dict:", title_values_dict
    return title_def

def make_r_named_list(names, values):
    from rpy2.robjects.vectors import StrVector

    if len(names) != len(values):
        print "Names and values do not match! Names: %d, Values: %d" % (len(names), len(values))
        print "names:", names
        print "values:", values
        return None
    else:
        named_list = StrVector(values)
        named_list.names = StrVector(names)
        return named_list

def make_mods_label(file_data):
    mods_label = ""
    sep = "/"
    #==0; disables TLP and ER.
    #==1; enables RFC5827 ER.
    #==2; delayed ER.
    #==3; TLP and delayed ER. [DEFAULT]
    #==4; TLP only.
    mods = {"thin_dupack":      [(1, "DA")],
            "linear_timeout":   [(1, "LT")],
            "early_retransmit": [(1, "ER"), (3, "ER+TLP"), (4, "TLP")],
        }

    stream_properties = file_data["streams"][file_data["streams_id"]]
    #print "MODS: stream_properties:", stream_properties
    for m in mods:
        for v in mods[m]:
            #if file_data[m] == v[0]:
            if stream_properties.get(m, -1) == v[0]:
                if mods_label:
                    mods_label += sep
                mods_label += v[1]
    return mods_label

def get_strings(d):
    return dict([(k, v) for k,v in d.iteritems() if type(v) is str])

def get_dataframe_head(df, count=6):
    #[v for v in dataf.iter_row()]
    #plot["data
    from rpy2.robjects import r
    #robjects.globalenv['df'] = df
    #robjects.r('cut <- head(df, n=%d)' % count)
    cut = r.head(df, n=count)
    #cut = robjects.r["cut"]
    return cut
    #robjects.globalenv['cut']

def get_dataframe_col_index(df, column):
    for i, col in enumerate(df.names):
        if col == column:
            return i
    return -1

import time
start_time = time.time()

def time_since_start():
    end = time.time()
    return end - start_time

def print_keys(d, keys=None, prefix=None):
    if prefix:
        print prefix
    if keys is None:
        keys = d.keys()
    for k in keys:
        print "%s: %s" % (k, d[k])


def frange(*args):
    """Handles creating range of floats"""
    try:
        return range(args)
    except TypeError:
        from numpy import arange
        return arange(*args).tolist()

def read_output_data2(conf, plot_box_dict, sets, keys, filename):
    data = []
    colnames = []
    col_colors = ["cyan"]
    rownames = []
    for name, key, regex in keys:
        rownames.append(name)

    sorted_sets = sorted(sets.keys(), key=lambda k: sets[k]["plots"][0]["column"])
    for s in sorted_sets:
        for plot_conf in sets[s]["plots"]:
            colnames.append(plot_conf["stream_id"])
            col_colors.append(plot_conf["color"])
            info = read_output_from_file(filename)
            for name, key, regex in keys:
                if regex and key in info:
                    m = re.match(regex, info[key], flags=re.DOTALL)
                    if m:
                        groups = m.groups()
                        print "groups:", groups
                        if "value" in groups:
                            data.append(m.group("value"))

                    data.append(m.group("value") if m else "")
                else:
                    data.append(info[key] if key in info else "")
    return data, colnames, col_colors, rownames

def read_output_data_match(keys, filename):
    data = []
    data_dict = {}
    info = read_output_from_file(filename)
    #print "info:", info
    for match in keys:
        #print "key:", key
        #print "regex:", regex
        key = match.key
        name = match.name
        regex = match.value_regex
        if match.key_regex is True:
            print "Found key regex!"
            # Must find which keys matches
            for v in info:
                #print "V:", v
                #print "key:", key
                if not v:
                    continue
                m = re.match(key, v, flags=re.DOTALL)
                if m:
                    print "Matches key val:", v
                    key = v
                    break
            if not m:
                print "Failed to find matching key"

        else:
            if not key in info:
                data.append("")
                data_dict[key] = ""
                continue
        if regex:
            m = None
            for val in info[key]:
                m = re.match(regex, val, flags=re.DOTALL)
                if m:
                    #print "%s : Matched '%s' with regex '%s'" % (name, val, regex)
                    data.append(m.group("value"))
                    data_dict[name] = m.group("value")
                    break
                else:
                    #print "%s : Failed to match '%s' with regex '%s'" % (name, val, regex)
                    pass
                #print "%s -> %s" % (key, m.group("value"))
            if not m:
                data.append("")
                data_dict[name] = ""
        else:
            data.append(info[key][0])
            data_dict[name] = info[key][0]
    return data, data_dict


def read_output_data(keys, filename, content_func=None):
    data = []
    data_dict = {}
    info = read_output_from_file(filename, content_func=content_func)

    for name, key, regex in keys:
        #print "key:", key
        #print "regex:", regex
        if not key in info:
            if data_dict.get(name, "") == "":
                data.append("")
                data_dict[name] = ""
            continue
        if regex:
            m = None
            for val in info[key]:
                m = re.match(regex, val, flags=re.DOTALL)
                if m:
                    #print "%s : Matched '%s' with regex '%s'" % (name, val, regex)
                    if "value" in m.groupdict():
                        data.append(m.group("value"))
                        data_dict[name] = m.group("value")
                    else:
                        if m.groupdict():
                            data.append(m.groupdict())
                            data_dict[name] = m.groupdict()
                    break
                else:
                    print "%s : Failed to match '%s' with regex '%s'" % (name, val, regex)
                #print "%s -> %s" % (key, m.group("value"))
            if not m:
                data.append("")
                data_dict[name] = ""
        else:
            data.append(info[key][0])
            data_dict[name] = info[key][0]
    return data, data_dict

# The content func will take the file content as input and return the content to process
def read_output_from_file(filename, content_func=None):
    d = {}
    f = open(filename, "r")
    return content_to_dict(f.read(), content_func=content_func)

def content_to_dict(content, split_func=None, content_func=None):
    if split_func is None:
        def splitter(line):
            split = line.split(":")
            if len(split) >= 2:
                return split[0].strip(), split[1].strip()
            else:
                return None, None
        split_func = splitter
    d = {}

    if content_func:
        content = content_func(content)

    #print "content:", content
    lines = content.splitlines()
    #print "Lines:", len(lines)

    for line in lines:
        #print "Line:", line

        key, value = split_func(line)
        if not key in d:
            d[key] = []
        d[key].append(value)

    return d

#def read_output_info(filename):
#    d = {}
#    f = open(filename, "r")
#    for line in f.readlines():
#        split = line.split(":")
#        if len(split) == 2:
#            #print "split:", split
#            d[split[0].strip()] = split[1].strip()
#    return d

def content_to_dict_old(content, split_func):
    d = {}
    lines = content.splitlines()
    for line in lines:
        split = line.split(":")
        if len(split) >= 2:
            #print "split:", split
            d[split[0].strip()] = split[1].strip()
    return d

def parse_r_summary(data):
    result = OrderedDict()
    stats_lines = str(data).splitlines()
    # This is summary of a Vector
    for l in stats_lines[1:]:
        w = l.split(":")
        w0 = w[0].strip().rstrip('.')
        try:
            result[w0] = float(w[1].strip())
        except ValueError, e:
            print "Failed to convert '%s' to float" % w[1].strip()
            result[w0] = w[1].strip()
    return result

# Input a dataframe column
#    Min. 1st Qu.  Median    Mean 3rd Qu.    Max.
#  150.0   150.0   150.0   160.7   150.0  4348.0

def parse_r_column_summary(data, value_func=lambda x: x):
    result = OrderedDict()
    stats_lines = str(data).splitlines()
    #print "stats_lines:", stats_lines
    # This is summary of a Vector
    for l in stats_lines[1:]:
        w = l.split()
        result["Min"] = value_func(w[0])
        result["1st Qu"] = value_func(w[1])
        result["Median"] = value_func(w[2])
        result["Mean"] = value_func(w[3])
        result["3rd Qu"] = value_func(w[4])
        result["Max"] = value_func(w[5])
        return result
