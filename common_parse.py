import os
import util
from util import cprint
from rpy2.robjects import r
from rpy2.robjects.vectors import Vector, IntVector, DataFrame, StrVector, FloatVector

def add_analysetcp_parsing(plot_conf, file_conf, pcap_file, parse_func_arg):
    analysetcp_stdout_file = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "analysetcp.out"))
    #file_conf["results"] = {"analysetcp": {}}
    util.set_dict_defaults_recursive(file_conf, (("results", "analysetcp"), { "extra_cmd_args": "" }))
    file_conf["results"]["analysetcp"]["stdout_file"] = analysetcp_stdout_file
    file_conf["results"]["analysetcp"]["results_file"] = analysetcp_stdout_file
    file_conf["results"]["analysetcp"]["results_cmd"] =\
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s  %(extra_cmd_args)s -u%(prefix)s -o %(output_dir)s -A 1> %(stdout_file)s" %\
        dict(file_conf, **file_conf["results"]["analysetcp"])

    #analystcp_results_dict["results_cmd"] =\
    #    "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s -o %(output_dir)s -A -vv 1> %(stdout_file)s" %\


def tcpinfo_results_parse(conf, plot_box_dict, file_data, set_key):
    #print "tcpinfo_results_parse:",  file_data["plot_type"]
    #print "results:", file_data["results"]
    #print "loss:", file_data["loss"]

    if util.cmd_args.verbose > 1:
        cprint("Reading TCPINFO stats: %s" % file_data["results"]["tcpinfo"]["results_file"], "yellow")

    plot_box_dict["y_axis_range"] = xrange(0, 10, 50)
    plot_box_dict["y_axis_data"] = [i for i in plot_box_dict["y_axis_range"]]
    plot_box_dict["y_axis_labels"] = [i for i in plot_box_dict["y_axis_range"]]

    try:
        d = DataFrame.from_csvfile(file_data["results"]["tcpinfo"]["results_file"], header=True,
                               sep=',', quote='"', dec='.', fill=True, comment_char='', as_is=False)
        d.names[0] = "time"
        d.names[1] = "stream"
        for i, col in enumerate(d.names):
            if col == "CWND":
                d.names[i] = "cwnd"
        d = r.cbind(d, loss=file_data["loss"], stream_type=file_data["stream_id"])
        #print "COLS:", d.names
        #print "ROWS:", r.nrow(d)
        file_data["tcpinfo_data"] = d
    except:
        raise
        pass
