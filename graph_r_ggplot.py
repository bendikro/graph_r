import itertools, rpy2
from rpy2.robjects import r, Formula
from rpy2.robjects.packages import importr
from rpy2.robjects.vectors import Vector, IntVector, DataFrame, StrVector, FloatVector

import rpy2.robjects.lib.ggplot2 as ggplot2
from rpy2.robjects.lib import grid

import graph
from util import cprint
from util import frange, vprint
import util
from copy import deepcopy

from common import build_custom_data_dict, get_values_sort_order, SUBPLOT_ACTION

zoo = importr("zoo")
gridExtra = importr("gridExtra")
gridr = importr("grid")
scales = importr("scales")
RColorBrewer = importr("RColorBrewer")

def scatterplot_cwnd_thick(conf, plot_box_dict, plot, gp, aes_args):
    data = plot["data"]
    #print "DATA:", data.names
    #print "DATA:", data
    average_window = 10
    ma = zoo.rollmean(data.rx2("cwnd"), average_window, fill=IntVector([rpy2.rinterface.NA_Integer for i in range(average_window)]))
    #print "MA LEN:", len(ma)
    #data[2] = ma

    if gp is None:
        gp = ggplot2.ggplot(data)
        #gp += aes_mapping

    aes_mapping = ggplot2.aes_string(**aes_args)

    gp += ggplot2.geom_line(data=data, mapping=aes_mapping)
    gp += ggplot2.geom_point(data=data, mapping=aes_mapping)
    return gp

def scatterplot_cwnd_thin(conf, plot_box_dict, plot, gp, aes_args):
    data = plot["data"]
    stream_properties = plot["streams"][plot["streams_id"]]
    meta_info = stream_properties["meta_info"]
    factor = meta_info["factor"]
    #print "factor:", factor

    aes_args["group"] = "factor(%s)" % factor

    #print "COLUMSN:\n", util.get_dataframe_head(data)

    if gp is None:
        gp = ggplot2.ggplot(data)
        gp += ggplot2.aes_string(**aes_args)

    line = meta_info.get("line", False)
    smooth = meta_info.get("smooth", False)
    if line is False and smooth is False:
        line = True

    #print "Data:", data
    #print "SUMMARY:", r["summary"](data)

    #print "line:", line
    #print "smooth:", smooth

    if line:# and stream_properties["stream_count"] >= 1:
        #print "geom_point:", data
        aes_mapping = ggplot2.aes_string(**aes_args)
        gp += ggplot2.geom_line(data=data, size=0.5, mapping=aes_mapping)
        #gp += ggplot2.geom_point(data=data, size=0.6)
        #(ymax = ..density..,  ymin = -..density..)
        pass
    if smooth:
        #smooth_line_groups = "interaction(factor(stream), factor(type))"
        smooth_line_groups = "interaction(factor(%s))" % factor
        aes_args["group"] = smooth_line_groups
        #aes_mapping = ggplot2.aes_string(x='time', y='cwnd', group=smooth_line_groups)
        aes_mapping = ggplot2.aes_string(**aes_args)
        gp += ggplot2.stat_smooth(data=data,
                                  method='loess',
                                  formula=Formula("y ~ x"),
                                  #size=0.1,
                                  span=0.01,
                                  n=440,
                                  alpha=0.8,
                                  mapping=aes_mapping,
                                  se=True,
                                  #se=False,
                                  #,color=scale_alpha_color2
                                  )
    return gp



def plot_scatterplot_cwnd(conf, plot_box_dict, sets):
    """
    Creates a scatterplot

    """
    gp = None
    data = None

    #print "box_title:", plot_box_dict["box_title"]
    #+ ggplot2.aes_string(x='X', y='Y', col='factor(color_column)')
    #print "x name:", conf.get("x_column_name", 'time')
    #print "y name:", conf.get("y_column_name", 'cwnd')

    legend_conf = {"colors": [], "color_labels": [], "breaks": []}

    aes_args = {}
    aes_args["x"] = conf.get("x_column_name", 'time')
    aes_args["y"] = conf.get("y_column_name", 'cwnd')
    aes_args["col"] = 'factor(%s)' % conf["plot_conf"].get("default_factor", "stream")

    for plot_set in sets.itervalues():
        for plot in plot_set["plots"]:
            #print "plot:", plot.keys()
            data = plot["data"]
            print "PLOT:"
            print util.get_dataframe_head(data)

            plot_box_dict["box_title"] = util.make_box_title(plot_box_dict, plot)
            #print "stream_type:", plot["stream_type"]
            # Limit data based on time on x axis
            if "x_axis_lim" in plot_box_dict["axis_conf"]:
                x_axis_time_lim_min, x_axis_time_lim_max = plot_box_dict["axis_conf"]["x_axis_lim"]
                #plot["data"] = data.rx((data.rx2("time").ro < x_axis_time_lim_max).ro & (data.rx2("time").ro > x_axis_time_lim_min), True)

            stream_properties = plot["streams"][plot["streams_id"]]
            #print "stream_properties:", stream_properties
            if "meta_info" in stream_properties:
                meta_info = stream_properties["meta_info"]
                meta_info["factor"] = meta_info.get("factor", conf["plot_conf"].get("default_factor", "stream"))
                aes_args = deepcopy(aes_args)
                aes_args.update(meta_info.get("aes_args", {}))
                print "aes_args:", aes_args
            #aes_mapping = ggplot2.aes_string(**aes_args)

            #print "stream_type:", plot.get("stream_type", None)
            if plot.get("stream_type", None) != "thick":
                gp = scatterplot_cwnd_thin(conf, plot_box_dict, plot, gp, aes_args)
            else:
                gp = scatterplot_cwnd_thick(conf, plot_box_dict, plot, gp, aes_args)

            if "custom_ggplot2_plot_cmds" in plot_box_dict["box_conf"]:
                for k, cmd in plot_box_dict["box_conf"]["custom_ggplot2_plot_cmds"].iteritems():
                    for e in cmd(**{"plot_conf": conf["plot_conf"],"plot_box_dict": plot_box_dict, "plot": plot, "gp": gp}):
                        gp += e


    legend_conf = plot_box_dict["box_conf"]["legend"]
    #print "legend_conf:", legend_conf
    legend_sort = legend_conf["legend_sort"]
    labels, colors, breaks, color_values = get_legend_values_from_legend_conf(legend_conf)

    #print "labels:", labels
    #print "breaks:", breaks
    #print "colors:", colors

    #breaks = legend_conf["breaks"]
    #colors = legend_conf["colors"]

    print "breaks:", breaks
    print "colors:", colors

    print "default_legend_and_scaling:", legend_conf.get("default_legend_and_scaling", True)
    if legend_conf.get("default_legend_and_scaling", True) == True:
        if breaks:
            #color_values = util.make_r_named_list(breaks, colors)
            #print "scale_colour_manual:", color_values
            #print "Labels:", labels
            gp += ggplot2.ggplot2.scale_color_discrete(name="Streams",
                                                       breaks=breaks,
                                                       labels=labels)
            #gp += ggplot2.ggplot2.scale_colour_brewer(type="seq", palette="Set1")
            #if color_values:
            #    gp += ggplot2.ggplot2.scale_colour_manual(values=color_values,
            #                                              name="Streams",
            #                                              labels=StrVector(labels),
            #                                              breaks=StrVector(breaks)
            #                                          )

            #print "colors:", colors
            #print "breaks:", breaks
            #print "labels:", labels
            #print "color_values:", color_values
            #gp += ggplot2.scale_colour_manual(name=legend_conf["legend_title"],
            #                                  breaks=StrVector(breaks),
            #                                  labels=StrVector(labels),
            #                                  values=color_values,
            #                              )
            #gp += ggplot2.scale_colour_manual(name=legend_conf["legend_title"],
            #                                  breaks=StrVector(breaks),
            #                                  labels=StrVector(labels),
            #                                  values=color_values,
            #                              )

            colors_list = legend_conf["colors"]
            #print "legend_conf:", legend_conf
            # Sort the default legend values
            if legend_sort.get("do_sort", False) is True:
                indexes_ordered = get_values_sort_order(colors_list, legend_sort.get("sorted_args", {}), ordered_list=legend_sort.get("sort_order_list", None))
            else:
                indexes_ordered = range(len(colors_list))
            # Order the values
            labels = [colors_list[i]["label"] for i in indexes_ordered]
            colors = [colors_list[i]["color"] for i in indexes_ordered]
            breaks = [colors_list[i]["breaks"] for i in indexes_ordered]
            color_values = util.make_r_named_list(breaks, colors)
            if "legend_columns" in conf["plot_conf"]:
                columns = conf["plot_conf"].get("legend_columns", 1 + (len(colors) // 14))
                gp += ggplot2.guides(color=ggplot2.guide_legend(ncol=columns)) #, title.hjust = 0.4, title.theme = theme_text(size = 12, face = "bold")))


    if plot_box_dict["box_conf"].get("show_box_title", True):
        gp += ggplot2.ggtitle(plot_box_dict["box_title"])

    if "y_axis_title" in plot_box_dict["axis_conf"]:
        gp += r.ylab(plot_box_dict["axis_conf"]["y_axis_title"]["title"])
    if "x_axis_title" in plot_box_dict["axis_conf"]:
        gp += r.xlab(plot_box_dict["axis_conf"]["x_axis_title"]["title"])

    util.debug_print("axis_conf2: %s" % plot_box_dict["axis_conf"], debug_type="axis")

    gp = do_scale_x_continuous(plot_box_dict, gp)
    gp = do_scale_y_continuous(plot_box_dict, gp)

    theme_args = plot_box_dict.get("theme_args", None)
    if theme_args is None:
        theme_args = conf["plot_conf"].get("theme_args", {})

    theme_args.update({#"legend.position": "left",
                       #"axis.title.y": ggplot2.element_blank()
    })

    # Makes legend and axis configs
    gp += ggplot2.theme(**theme_args)
    util.debug_print("theme_args: %s" % theme_args, debug_type="axis")

    gp = handle_guides(plot_box_dict["legends"], gp)

    if not plot_box_dict["box_conf"]["plot_and_table"]["make_table"] is SUBPLOT_ACTION.NO:
        return handle_plot_table(plot_box_dict, legend_conf["legend_sort"], gp)
    return [gp]


def scatterplot_senttime(conf, plot_box_dict, plot, gp):
    """
    Creates a scatterplot

    """
    data = plot["data"]

    thick_colours = r('c(brewer.pal(9, "Reds")[3:8])')
    thin_colours = r('c(brewer.pal(9, "Blues")[3:8])')

    #print "data.names:\n", data.names

    x_axis_time_lim_min, x_axis_time_lim_max = plot_box_dict["axis_conf"]["x_axis_lim"]
    data = data.rx((data.rx2("X").ro < x_axis_time_lim_max).ro & (data.rx2("X").ro > x_axis_time_lim_min), True)

    if gp is None:
        gp = ggplot2.ggplot(data)

    gp = (gp
          + ggplot2.aes_string(x='X', y='Y', col='factor(color_column)')
          + ggplot2.geom_point(data=data)

          #+ ggplot2.ggplot2.scale_colour_manual(#values=colours
          #  values=StrVector((plot["color"], ))
          #  , name="Streams"
          #  , labels=StrVector(["Greedy", "Thin"])
          #  )
          #+ ggplot2.theme(
          #  **{"legend.position": "right",
          #     #"axis.title.y": ggplot2.element_blank()
          #     }
          #    )
          #+ ggplot2.guides(color=ggplot2.guide_legend(ncol=1 + (len(colours) // 14))) #, title.hjust = 0.4, title.theme = theme_text(size = 12, face = "bold")))
          #+ ggplot2.stat_smooth(data=data,
          #                      method='loess', formula=Formula("y ~ x"), span=0.07, size=0.4, n=80,
          #                      #mapping=ggplot2.aes_string(x='senttime', y='latency', col='factor(stream)')
          #                      #mapping=ggplot2.aes_string(x='time', y='cwnd', group=smooth_line_groups,
          #                      #                           #col='factor(type)'
          #                      #                           col='factor(stream, levels=c(%s))' % levels_thick
          #                      #                           #col='factor(stream)'
          #                      #                           #,linetype='stream'
          #                      #                           )
          #                      #,alpha=0.1
          #                      #,color=scale_alpha_color
          #                      )
          )
    gp += ggplot2.geom_line(data=data)
    return gp


def plot_scatterplot_senttime(conf, plot_box_dict, sets):
    """
    Creates a scatterplot

    """
    gp = None
    data = None

    for plot_set in sets.itervalues():
        for plot in plot_set["plots"]:
            gp = scatterplot_senttime(conf, plot_box_dict, plot, gp)

    if "y_axis_title" in plot_box_dict["axis_conf"]:
        gp += r.ylab(plot_box_dict["axis_conf"]["y_axis_title"]["title"])
    if "x_axis_title" in plot_box_dict["axis_conf"]:
        gp += r.xlab(plot_box_dict["axis_conf"]["x_axis_title"]["title"])

    gp = do_scale_x_continuous(plot_box_dict, gp)
    gp = do_scale_y_continuous(plot_box_dict, gp)

    if plot_box_dict["box_conf"].get("show_box_title", True):
        gp += ggplot2.ggtitle(plot_box_dict["box_title"])

    theme_args = plot_box_dict.get("theme_args", {})
    theme_args.update({"legend.position": "left",
                       #"axis.title.y": ggplot2.element_blank()
                   })
    # Makes legend and axis configs
    gp += ggplot2.theme(**theme_args)

    gp += ggplot2.scale_colour_identity(guide="legend", name="Stream types2",
                                        breaks=StrVector(("darkblue", "darkred")),
                                        labels=StrVector(("Thin", "Greedy")),
    )
    return gp


def make_scale_config(lim_min, lim_max, main_ticks, minor_ticks):
    scale_args = {}

    if lim_min is not None and lim_max is not None:
        scale_args["limits"] = IntVector((lim_min, lim_max))
        #print "limits=(%d, %d)" % (lim_min, lim_max)
    if main_ticks:
        breaks = get_tick_range(main_ticks, lim_min=lim_min, lim_max=lim_max)
        if breaks:
            #print "breaks=%s" % breaks
            scale_args["breaks"] = FloatVector(breaks)
    if minor_ticks:
        breaks = get_tick_range(minor_ticks, lim_min=lim_min, lim_max=lim_max)
        if breaks:
            #print "minor_breaks=%s" % breaks
            scale_args["minor_breaks"] = FloatVector(breaks)
    return scale_args

def do_scale_x_continuous(plot_box_dict, gp, do_lim=True):
    return do_scale_axis_continuous(plot_box_dict, gp, do_lim=do_lim, axis="x")

def do_scale_y_continuous(plot_box_dict, gp, do_lim=True):
    return do_scale_axis_continuous(plot_box_dict, gp, do_lim=do_lim, axis="y")

def do_scale_axis_continuous(plot_box_dict, gp, do_lim=True, axis="x"):
    # Scale the axis and set tick options
    axis_conf = plot_box_dict["axis_conf"]
    axis_lim_min, axis_lim_max = axis_conf.get("%s_axis_lim" % axis, (None, None))
    axis_main_ticks = axis_conf.get("%s_axis_main_ticks" % axis, None)
    axis_minor_ticks = axis_conf.get("%s_axis_minor_ticks" % axis, None)
    args = make_scale_config(axis_lim_min, axis_lim_max, axis_main_ticks, axis_minor_ticks)
    args.update(axis_conf.get("%s_axis_args" % axis, {}))

    util.debug_print("args: %s" % args, "test", debug_type="axis", func_stack_depth=3)
    if args:
        if do_lim is False and "limits" in args:
            del args["limits"]
        gp += getattr(ggplot2, 'scale_%s_continuous' % axis)(**args)
    return gp


def get_tick_range(ticks, lim_min=None, lim_max=None):
    if type(ticks) is list:
        return frange(*ticks)
    elif not lim_min is None and not lim_max is None:
        return frange(lim_min, lim_max +1, ticks)
    else:
        return None


def ggplot_barplot(conf, plot_box_dict, plot, gp):
    title = plot_box_dict["box_title"]
    print "ggplot_barplot"
    # Data is a dataframe
    data = plot["barplot_dataframe"]
    #print "data:", data

    stream_properties = plot["streams"][plot["streams_id"]]
    meta_info = stream_properties["meta_info"]
    #stream_properties["factor"] = stream_properties.get("factor", conf["plot_conf"].get("default_factor", "stream"))

    if gp is None:
        gp = ggplot2.ggplot(data)

    #col='factor(%s)' % factor_col

    aes_args = {}
    aes_args.update(meta_info.get("aes_args", {}))
    aes_args["x"] = plot.get("y_column_name", 'time')
    #aes_args["linetype"] = "dashed"

#    #aes_args["linetype"] = "factor(color_column)"
#    #del aes_args["color"]
#    #aes_args["color"] = "factor(color_column)"

    #del aes_args["group"]
    #aes_args["col"] = "color_column"
    #print "aes_args:", aes_args
    #mapping = ggplot2.aes_string(**aes_args)
    plot_args = meta_info.get("plot_args", {})

    #plot_args["geom"] = "line"
#    #plot_args["geom"] = "smooth"

    #plot_args["se"] = False
    #plot_args["size"] = 1

    #n = 500
    #print "plot_args:", plot_args

    plot_args = { "data": data,
                  #"stat": "boxplot",
                  "position": "dodge",# "outlier.colour": "black",
                  #"outlier.shape": 16, "outlier.size": 2, #
                  #"notch": False, "notchwidth": 0.5,
                  #"outlier.shape": 10,
                  #"outlier.size": 1.5,
                  "mapping": ggplot2.aes_string(**aes_args),
                  #"size": 0.2,
                  #"outlier.colour": r("NULL"),
                  #"fill": r("NA"),
                  #"fatten": 3, # Thickness of median
                  #rpy2.rinterface.NULL,
                  #"notch": True,
              }

    gp = (gp
          #+ ggplot2.aes_string(x=plot.get("y_column_name", 'time'), col='factor(color_column)')
          #+ ggplot2.ggplot2.qplot(data=data, mapping=mapping, geom="bar") # **plot_args
          + ggplot2.ggplot2.geom_bar(**plot_args)
          #qplot(factor(cyl), data=mtcars, geom="bar", fill=factor(vs))
          #+ ggplot2.guides(color=ggplot2.guide_legend(ncol=1 + (len(colours) // 14))) #, title.hjust = 0.4, title.theme = theme_text(size = 12, face = "bold")))
          #+ ggplot2.geom_line(ggplot2.aes_string(linetype="factor(color_column)"))
          )

    axis_limit = {}
    if "x_axis_lim" in plot_box_dict["axis_conf"]:
        axis_limit["xlim"] = Vector(plot_box_dict["axis_conf"]["x_axis_lim"])
        util.debug_print("axis xlim: %s" % plot_box_dict["axis_conf"]["x_axis_lim"], debug_type="Barplot")

    if "y_axis_lim" in plot_box_dict["axis_conf"]:
        axis_limit["ylim"] = Vector(plot_box_dict["axis_conf"]["y_axis_lim"])
        util.debug_print("axis ylim: %s" % plot_box_dict["axis_conf"]["y_axis_lim"], debug_type="Barplot")

    gp += ggplot2.coord_cartesian(**axis_limit)
    return gp


# Called by ggplot_cdf_box
def ggplot_ecdf(conf, plot_box_dict, plot, gp):
    title = plot_box_dict["box_title"]
    # Data is a dataframe
    data = plot["ecdf_ggplot2_values"]

    #print "DATA:"
    #print util.get_dataframe_head(data)

    stream_properties = plot["streams"][plot["streams_id"]]
    meta_info = stream_properties["meta_info"]

    if gp is None:
        gp = ggplot2.ggplot(data)

    aes_args = {}
    aes_args.update(meta_info.get("aes_args", {}))
    if not "x" in aes_args:
        aes_args["x"] = plot.get("y_column_name", 'time')
    #aes_args["linetype"] = "dashed"

#    #aes_args["linetype"] = "factor(color_column)"
#    #del aes_args["color"]
#    #aes_args["color"] = "factor(color_column)"

    #del aes_args["group"]
    #aes_args["col"] = "color_column"
    #print "plot aes_args:", aes_args
    mapping = ggplot2.aes_string(**aes_args)
    plot_args = meta_info.get("plot_args", {})

    #print "DATA:"
    #print util.get_dataframe_head(data, 15)

    #plot_args["geom"] = "line"
#    #plot_args["geom"] = "smooth"

    #plot_args["se"] = False
    #plot_args["size"] = 1

    vprint("plot_args:", plot_args, v=3)

    gp += ggplot2.ggplot2.stat_ecdf(data=data, mapping=mapping, **plot_args)

    #gp = (gp
          #+ ggplot2.aes_string(x=plot.get("y_column_name", 'time'), col='factor(color_column)')
          #+ ggplot2.guides(color=ggplot2.guide_legend(ncol=1 + (len(colours) // 14))) #, title.hjust = 0.4, title.theme = theme_text(size = 12, face = "bold")))
          #+ ggplot2.geom_line(ggplot2.aes_string(linetype="factor(color_column)"))
    #      )

    #util.debug_print("mapping: %s" % mapping, debug_type="ECDF")
    #util.debug_print("plot_args: %s" % plot_args, debug_type="ECDF")

    axis_limit = {}
    if "x_axis_lim" in plot_box_dict["axis_conf"]:
        axis_limit["xlim"] = Vector(plot_box_dict["axis_conf"]["x_axis_lim"])
        util.debug_print("axis xlim: %s" % plot_box_dict["axis_conf"]["x_axis_lim"], debug_type="ECDF")

    if "y_axis_lim" in plot_box_dict["axis_conf"]:
        axis_limit["ylim"] = Vector(plot_box_dict["axis_conf"]["y_axis_lim"])
        util.debug_print("axis ylim: %s" % plot_box_dict["axis_conf"]["y_axis_lim"], debug_type="ECDF")

    gp += ggplot2.coord_cartesian(**axis_limit)
    return gp


# Creates a CDF plot.
# Can be used as argument for plot_conf.plot_func config
def ggplot_cdf_box(conf, plot_box_dict, sets):
    """
    Creates a CDF box

    """
    gp = None
    data = None
    colors = []

    for plot_set in sets.itervalues():
        for plot in plot_set["plots"]:
            #print "plot:", plot.keys()
            colors.append(plot["color"])
            #plot_box_dict["box_title"] = util.make_title(plot_box_dict, plot, plot_box_dict["box_conf"]["box_title_def"])
            plot_box_dict["box_title"] = util.make_box_title(plot_box_dict, plot)
            #print "make title:", plot_box_dict["box_title"]
            #print "stream_type:", plot["stream_type"]
            #labels.append(plot["stream_type"])
            gp = ggplot_ecdf(conf, plot_box_dict, plot, gp)
            if "custom_ggplot2_plot_cmds" in plot_box_dict["box_conf"]:
                for k, cmd in plot_box_dict["box_conf"]["custom_ggplot2_plot_cmds"].iteritems():
                    for e in cmd(**{"plot_conf": conf["plot_conf"],"plot_box_dict": plot_box_dict, "plot": plot, "gp": gp, "conf": conf }):
                        gp += e
        if "custom_ggplot2_plotset_cmds" in plot_box_dict["box_conf"]:
            for k, cmd in plot_box_dict["box_conf"]["custom_ggplot2_plotset_cmds"].iteritems():
                for e in cmd(**{"plot_conf": conf["plot_conf"],"plot_box_dict": plot_box_dict, "plotset": plot_set["plots"], "gp": gp, "conf": conf }):
                    #print "E:", type(e)
                    #print "E:", e
                    gp += e

    if "y_axis_title" in plot_box_dict["axis_conf"]:
        gp += r.ylab(plot_box_dict["axis_conf"]["y_axis_title"]["title"])
    if "x_axis_title" in plot_box_dict["axis_conf"]:
        gp += r.xlab(plot_box_dict["axis_conf"]["x_axis_title"]["title"])

    if plot_box_dict["box_conf"].get("show_box_title", True):
        gp += ggplot2.ggtitle(plot_box_dict["box_title"])

    gp = do_scale_x_continuous(plot_box_dict, gp, do_lim=False)
    gp = do_scale_y_continuous(plot_box_dict, gp, do_lim=False)

    theme_args = plot_box_dict.get("theme_args", None)
    if theme_args is None:
        theme_args = conf["plot_conf"].get("theme_args", {})

    if "custom_ggplot2_cmds" in plot_box_dict["box_conf"]:
        for cmd in plot_box_dict["box_conf"]["custom_ggplot2_cmds"]:
            for e in cmd(conf["plot_conf"], gp):
                gp += e
            pass

    legend_conf = plot_box_dict["box_conf"]["legend"]
    legend_sort = legend_conf["legend_sort"]
    labels, colors, breaks, color_values = get_legend_values_from_legend_conf(legend_conf)
    #print "legend_conf:", legend_conf
    #
    #print "colors:", colors
    #print "breaks:", breaks
    #print "labels:", labels
    #print "color_values:", color_values

    #plot_box_dict["legends"]

    #gp += ggplot2.scale_colour_manual(name=legend_conf["legend_title"],
    #                                  breaks=StrVector(breaks),
    #                                  labels=StrVector(labels),
    #                                  values=color_values,
    #                                  #guide=None,
    #)

    #gp += ggplot2.scale_fill_manual(name=legend_conf["legend_title"],
    #                                breaks=StrVector(breaks),
    #                                labels=StrVector(labels),
    #                                values = color_values,
    #                            )


    #linetype_values = util.make_r_named_list(colors, ["solid", "dashed", "dotted"])
    #linetype_values = r('c(rep("solid", "khaki3"), rep("dashed", "yellowgreen"), rep("dashed", "darkred"))')
    #print "linetype_values:", linetype_values
    #gp += ggplot2.scale_linetype_manual(name=legend_conf["legend_title"],
    #                                    breaks=StrVector(breaks),
    #                                    #breaks=StrVector(colors),
    #                                    labels=StrVector(labels),
    #                                    #khaki3', 'yellowgreen', 'darkred
    #                                    values = linetype_values,
    #                                )

    #print "LEGENDS:", plot_box_dict["legends"]

    gp = handle_guides(plot_box_dict["legends"], gp)

    #values = util.make_r_named_list(['rdb', 'tcp', 'tcp-ref'], ["solid", "solid", "longdash"])
    #gp += ggplot2.scale_linetype_manual(values=values, guide=False)


    #gp += ggplot2.scale_linetype_manual(guide=False)

    #gp += ggplot2.scale_linetype_manual(values=StrVector(["solid", "solid", "dashed"]),
    #                                    guide=False)

    #print "theme_args:", theme_args
    #print "legend.key:", len(theme_args["legend.key"])
    #print "legend.key:", theme_args["legend.key"][0][0]
    #gp += ggplot2.scale_shape_discrete(values=StrVector(["solid", "dashed", "dashed"]))


    # Makes legend and axis configs
    gp += ggplot2.theme(**theme_args)

    if not plot_box_dict["box_conf"]["plot_and_table"]["make_table"] is SUBPLOT_ACTION.NO:
        return handle_plot_table(plot_box_dict, legend_sort, gp)
    #if not plot_box_dict["box_conf"]["plot_and_subplot"]["action"] is SUBPLOT_ACTION.NO:
    #    return handle_subplot(plot_box_dict, legend_sort, gp)
    return [gp]


# Adds a subplot or return gp if error
def handle_subplot(plot_box_dict, legend_sort, gp):
    subplot_conf = plot_box_dict["box_conf"]["plot_and_subplot"]
    if not subplot_conf.get("table_key", None) in plot_box_dict["data_tables"]:
        cprint("make_table is set in config but 'table_key' is not set or does not exist. 'table_key': %s" % subplot_conf.get("table_key", None), "red")
        return [gp]
    #table_conf = plot_box_dict["data_tables"][subplot_conf["table_key"]]
    #print "table_conf:", table_conf

    pp_list = plot_box_dict["plot_func"](conf, plot_box_dict, sets)

    #custom_data = build_custom_data_dict(table_conf, legend_sort)
    #print "custom_data:", table_conf.keys()
    #print "custom_data:", table_conf["data"]
    #print "table_key:", subplot_conf["table_key"]
    table_grob = make_table(table_conf, custom_data)
    if subplot_conf["action"] is SUBPLOT_ACTION.APPEND_TO_PLOT_BOX:
        subplot_conf_grob = make_plot_and_table_grob(plot_box_dict, subplot_conf, table_grob, gp)
        return [subplot_conf_grob]

    #ain + annotation_custom(ggplotGrob(sub), xmin=2.5, xmax=5, ymin=0, ymax=2.5) +
    #scale_x_continuous(limits=c(0, 5)) + scale_y_continuous(limits=c(0,4))


def handle_guides(legends, gp):
    guides_args = {}

    for legend_type, legend_args in legends.iteritems():
        #print "\nLEGEND: %s, legend_args: %s" % (legend_type, legend_args)
        guide_args = legend_args["guide_args"]
        #print "SHOW:", guide_args.get("show", True)
        if type(guide_args) is dict:
            if guide_args.get("show", True) == False:
                gp += ggplot2.guides(**{legend_type : False})
                continue

            #if legend_type == "linetype":
            #    print "LINETYPE:", guide_args
            if "override.aes" in guide_args:
                override_aes = guide_args["override.aes"]
                if "linetype" in override_aes:
                    override_aes["linetype"] = IntVector(override_aes["linetype"])
                guide_args["override.aes"] = r.list(**guide_args["override.aes"])
                #print "NEW override.aes:", guide_args["override.aes"]
            #print "CAlling guide_legend:", guide_args
            # Only add if guide args is non-empty
            if guide_args:
                guides_args[legend_type] = ggplot2.guide_legend(**guide_args)
        else:
            guides_args[legend_type] = guide_args

        if legend_args.get("scale", None):
            #print "Calling scale func on %s: %s" % (legend_type, legend_args["scale"]["args"])
            args = deepcopy(legend_args["scale"]["args"])
            #args = legend_args["scale"]["args"]
            for k, v in args.iteritems():
                if type(v) is list:
                    args[k] = StrVector(v)
            #print "args:", args
            gp += legend_args["scale"]["func"](**args)

    #print "guides_args:", guides_args.keys()
    #print "guides_args:", guides_args
    #print "guides_args:", guides_args["linetype"]
    #print "guides_args:", guides_args["color"]
    if guides_args:
        gp += ggplot2.guides(**guides_args)

    return gp

def get_legend_values_from_legend_conf(legend_conf):
    #print "legend_conf:", legend_conf
    #print "get_legend_values_from_legend_conf"

    colors_list = legend_conf["colors"]
    #print "colors_list (%d): %s" % (len(colors_list), colors_list)
    #labels = legend_conf["color_labels"]
    #colors = legend_conf["colors"]
    #print "colors:", colors
    #print "labels:", labels
    legend_sort = legend_conf["legend_sort"]
    #print "legend_sort2:", legend_sort
    #print "DO_SORT:", legend_sort.get("do_sort", False)

    # Sort the default legend values
    if legend_sort.get("do_sort", False) is True:
        #print "sorted_args:", legend_sort.get("sorted_args", {})
        #print "sort_order_list:", legend_sort.get("sort_order_list", None)
        indexes_ordered = get_values_sort_order(colors_list, legend_sort.get("sorted_args", {}), ordered_list=legend_sort.get("sort_order_list", None))
    else:
        indexes_ordered = range(len(colors_list))

    # Order the values
    labels = [colors_list[i]["label"] for i in indexes_ordered]
    colors = [colors_list[i]["color"] for i in indexes_ordered]
    breaks = [colors_list[i]["breaks"] for i in indexes_ordered]
    color_values = util.make_r_named_list(breaks, colors)
    return labels, colors, breaks, color_values

# Adds a table or return gp if error
def handle_plot_table(plot_box_dict, legend_sort, gp):
    plot_and_table = plot_box_dict["box_conf"]["plot_and_table"]
    if not plot_and_table.get("table_key", None) in plot_box_dict["data_tables"]:
        cprint("make_table is set in config but 'table_key' is not set or does not exist. 'table_key': %s" % plot_and_table.get("table_key", None), "red")
        return [gp]
    table_conf = plot_box_dict["data_tables"][plot_and_table["table_key"]]
    #print "table_conf:", table_conf

    custom_data = build_custom_data_dict(table_conf, legend_sort)
    #print "custom_data:", table_conf.keys()
    #print "custom_data:", table_conf["data"]
    #print "table_key:", plot_and_table["table_key"]
    table_grob = make_table(table_conf, custom_data)
    if plot_and_table["make_table"] is SUBPLOT_ACTION.APPEND_TO_PLOT_BOX:
        plot_and_table_grob = make_plot_and_table_grob(plot_box_dict, plot_and_table, table_grob, gp)
        return [plot_and_table_grob]


def make_plot_and_table_grob(plot_box_dict, plot_and_table, table_grob, gp):
    if "title" in plot_and_table:
        title_args = deepcopy(plot_and_table["title"])
        title = util.make_title(plot_box_dict, plot, title_args["label"])
        title_args["label"] = title
        #print "title_args:", title_args
        print "plot_number:", plot_box_dict["plot_number"]
        main_title = grid.text(**title_args)
    else:
        main_title = rpy2.rinterface.NULL

    spacing = grid.rect(gp=grid.gpar(col="white")) # make a white spacer grob
    heights = (.7, .05, .25)
    if "heights" in plot_and_table:
        heights = plot_and_table["heights"]
    plot_and_table_grob = gridExtra.arrangeGrob(gp, spacing, table_grob, nrow=3, ncol=1, heights=FloatVector(heights), #lengths=FloatVector((10, 3)), #, main="Test"
                                                main=main_title)
    return plot_and_table_grob

def make_table(table_conf, custom_data):
    column_names = custom_data.keys()
    tableArgs = get_table_args(table_conf)
    #print "column_names:", column_names
    #print "tableArgs:", tableArgs
    #print "tableArgs:", tableArgs.keys()
    #print "custom_data:", custom_data
    #print "data len:", len(custom_data)
    #print "Data:\n", DataFrame(custom_data)
    #print "df len:", len(DataFrame(custom_data))

    table = gridExtra.tableGrob(DataFrame(custom_data), cols=StrVector(column_names), name="TABLE", **tableArgs)
    return table

def get_table_args(table_conf):
    def_size = table_conf.get("default_font_size", 8)
    tableArgs = { "gpar.coretext": grid.gpar(fontsize=def_size), "gpar.coltext": grid.gpar(fontsize=def_size),
                  "gpar.rowtext": grid.gpar(fontsize=def_size), "show.rownames": False }
    if "args" in table_conf:
        tableArgs.update(table_conf["args"])
    return tableArgs


# Called by ggplot_boxplot_box
def ggplot_boxplot(conf, plot_box_dict, plot, gp):
    title = plot_box_dict["box_title"]
    data = plot["data"]
    stream_ids = plot_box_dict.get("stream_ids", {})
    stream_id = plot["stream_id"]
    stream_ids[stream_id] = {"id": stream_id, "color": plot["color"]}
    plot_box_dict["stream_ids"] = stream_ids

    #colsum = plot["colsum"]
    #s = r["summary"](colsum)
    #print "Summary:", s

    #print "Names:", data.names
    #print "Data:\n", data

    #cut = util.get_dataframe_head(data, 3)
    #print "CUT:\n", cut
    #d = data.rx((data.rx2("time").ro < x_axis_time_lim_max).ro & (data.rx2("time").ro > x_axis_time_lim_min), True)

    #print "Coltypes:", [column.rclass[0] for column in data]

    stream_properties = plot["streams"][plot["streams_id"]]
    meta_info = stream_properties["meta_info"]
    #stream_properties["factor"] = stream_properties.get("factor", conf["plot_conf"].get("default_factor", "stream"))

    vprint("PLOT_DATA:\n", util.get_dataframe_head(data, count=1), v=3)

    if gp is None:
        gp = ggplot2.ggplot(data)

    aes_args = {}
    aes_args.update(meta_info.get("aes_args", {}))

    #print "boxplot aes_args1:", aes_args

    if not aes_args:
        aes_args["x"] = "stream_type"
        aes_args["y"] = "Throughput"
        aes_args["col"] = 'factor(stream_type)'

    if not "y" in  aes_args:
        aes_args["y"] = "Throughput"

    #aes_args["x"] = plot.get("y_column_name", 'time')

    #aes_args["group"] = 'factor(type)'
    #aes_args["col"] = 'factor(type)'
    #aes_args["group"] = 'factor(type)'
    #aes_args["color"] = 'color_column'

    #print "geom_boxplot aes_args:", aes_args
    #mapping = ggplot2.aes_string(x=plot.get("y_column_name", 'time'), **aes_args)
    #print "%s order: %s" % (plot["label"], plot["legend_order"])

    plot_args = { "data": data, "stat": "boxplot", #"position": "dodge",# "outlier.colour": "black",
                  #"outlier.shape": 16, "outlier.size": 2, #
                  #"notch": False, "notchwidth": 0.5,
                  #"outlier.shape": 10,
                  "outlier.size": 1.5,
                  "mapping": ggplot2.aes_string(**aes_args),
                  "size": 0.2,
                  #"outlier.colour": r("NULL"),
                  #"fill": r("NA"),
                  "fatten": 3, # Thickness of median
                  #rpy2.rinterface.NULL,
                  #"notch": True,
                  #"position": "position_dodge(width = .9))",
              }

    # Removing the default color value for points
    # This makes sure the outlier values have the same color as the box color
    ggplot2.ggplot2.update_geom_defaults("point", r('list(colour = NULL)'))

    gp += ggplot2.ggplot2.geom_boxplot(**plot_args)

    axis_limit = {}
    if "x_axis_lim" in plot_box_dict["axis_conf"] and plot_box_dict["axis_conf"].get("set_x_axis", True):
        axis_limit["xlim"] = Vector(plot_box_dict["axis_conf"]["x_axis_lim"])

    if "y_axis_lim" in plot_box_dict["axis_conf"] and plot_box_dict["axis_conf"].get("set_y_axis", True):
        axis_limit["ylim"] = Vector(plot_box_dict["axis_conf"]["y_axis_lim"])

    if axis_limit:
        gp += ggplot2.coord_cartesian(**axis_limit)
    return gp


# Creates a boxplot for one box
# Can be used as argument for plot_conf.plot_func config
def ggplot_boxplot_box(conf, plot_box_dict, sets):
    """
    Creates a scatterplot

    """
    gp = None
    x_axis_labels = []
    x_axis_label_key = plot_box_dict["axis_conf"].get("x_axis_label_key", None)
    #print "x_axis_label_key:", x_axis_label_key

    for plot_set in sets.itervalues():
        for plot in plot_set["plots"]:
            #print "ggplot_boxplot_box - PLOT TYPE:", plot["plot_type"]
            #colors.append(plot["color"])
            #plot_box_dict["box_title"] = util.make_title(plot_box_dict, plot, plot_box_dict["box_conf"]["box_title_def"])
            plot_box_dict["box_title"] = util.make_box_title(plot_box_dict, plot)
            vprint("box_title:", plot_box_dict["box_title"], v=3)
            #print "stream_type:", plot["stream_type"]
            #labels.append(plot["stream_type"])
            #plot["colsum"]
            if x_axis_label_key:
                #print "x_axis_label_key:", plot[x_axis_label_key]
                x_axis_labels.append(plot[x_axis_label_key])
            gp = ggplot_boxplot(conf, plot_box_dict, plot, gp)
            if "custom_ggplot2_plot_cmds" in plot_box_dict["box_conf"]:
                for k, cmd in plot_box_dict["box_conf"]["custom_ggplot2_plot_cmds"].iteritems():
                    for e in cmd(**{"plot_conf": conf["plot_conf"], "plot_box_dict": plot_box_dict, "plot": plot, "gp": gp}):
                        gp += e

    if "y_axis_title" in plot_box_dict["axis_conf"]:
        gp += r.ylab(plot_box_dict["axis_conf"]["y_axis_title"]["title"])
    if "x_axis_title" in plot_box_dict["axis_conf"]:
        gp += r.xlab(plot_box_dict["axis_conf"]["x_axis_title"]["title"])

    if plot_box_dict["box_conf"].get("show_box_title", True):
        gp += ggplot2.ggtitle(plot_box_dict["box_title"])

    #stream_ids = plot_box_dict["stream_ids"]
    #print "stream_ids:", stream_ids
    #labels = sorted(stream_ids.keys())
    #breaks = labels
    #colors = [stream_ids[l]["color"] for l in labels]


    legend_conf = plot_box_dict["box_conf"]["legend"]
    #print "legend_conf:", legend_conf
    print "default_legend_and_scaling:", legend_conf.get("default_legend_and_scaling", True)
    if legend_conf.get("default_legend_and_scaling", True) == True:
        legend_sort = legend_conf["legend_sort"]
        labels, colors, breaks, color_values = get_legend_values_from_legend_conf(legend_conf)

        #print ""
        vprint("x_axis_labels:", x_axis_labels, v=3)

        if x_axis_label_key:
            x_axis_labels = sorted(list(set(x_axis_labels)), reverse=True)
        else:
            x_axis_labels = labels


        labels = x_axis_labels
        #print "colors:", colors
        #print "breaks:", breaks
        vprint("labels:", labels, v=3)
        vprint("limits(x_axis_labels):", x_axis_labels, v=3)

        #print "color_values:\n", color_values
        #labels = []

        if breaks:
            print "scale_colour_manual:", color_values
            print "Labels:", labels
            print "breaks:", breaks
            print "color_values:", color_values
            if color_values:
                gp += ggplot2.scale_colour_manual(name=legend_conf["legend_title"],
                                                  breaks=StrVector(breaks),
                                                  labels=StrVector(labels),
                                                  values = color_values,
                                              )
                gp += ggplot2.scale_fill_manual(name=legend_conf["legend_title"],
                                                breaks=StrVector(breaks),
                                                labels=StrVector(labels),
                                                values = color_values,
                                            )
                pass

        #gp += ggplot2.scale_x_discrete(limits=StrVector(labels))
        gp += ggplot2.scale_x_discrete(
            #breaks=StrVector(breaks),
            #breaks=StrVector(test_ids),
            # When x_axis_label_key is set, labels and limits will differ
            labels=StrVector(labels),
            limits=StrVector(x_axis_labels),
        )
    gp = do_scale_y_continuous(plot_box_dict, gp, do_lim=True)

    gp = handle_guides(plot_box_dict["legends"], gp)

    if "custom_ggplot2_plotbox_cmds" in plot_box_dict["box_conf"]:
        for cmd in plot_box_dict["box_conf"]["custom_ggplot2_plotbox_cmds"]:
            for e in cmd(conf["plot_conf"], gp):
                gp += e
            pass

    theme_args = plot_box_dict.get("theme_args", None)
    # Makes legend and axis configs
    gp += ggplot2.theme(**theme_args)

    legend_sort = None
    if "legend" in conf:
        legend_sort = conf["legend"].get("legend_sort", None)

    if not plot_box_dict["box_conf"]["plot_and_table"]["make_table"] is SUBPLOT_ACTION.NO:
        return handle_plot_table(plot_box_dict, legend_sort, gp)
    return [gp]


first_page = True # HACK

def do_plots_ggplot2(conf, plot_groups):
    plot_list = plot_groups["plot_boxes"]
    n_rows = conf["plot_conf"]["n_rows"]
    n_columns = conf["plot_conf"]["n_columns"]

    plots_per_page = n_columns * n_rows
    if conf["plots_per_page"]:
        max_plots_per_page = conf["plots_per_page"]
        plots_per_page = conf["plots_per_page"]

    plots_on_last_page = len(plot_list) % plots_per_page
    index_last_row_on_last_page = len(plot_list) - (plots_per_page - (plots_on_last_page - n_rows))
    max_plots_per_page = 0
    plot_index_on_page = 0

    def make_plot_page(items_to_plot, row_count, col_count):
        if util.cmd_args.verbose > 3:
            print "make_plot_page - rows: %d, cols: %d, items: %d" % (row_count, col_count, len(items_to_plot))
        global first_page
        while items_to_plot:
            if first_page is True:
                first_page = False
            else:
                #print "newpage"
                grid.newpage()
                pass
            #print "print_page_title:", conf["print_page_title"]

            grid_row_count = row_count
            page_title = None
            if plot_groups.get("page_title", None) and conf["print_page_title"]:
                #print "Setting TITLE"
                #print "page_title:", plot_groups["page_title"]
                #r.mtext(plot_groups["page_title"], side=3, line=2, outer=True, cex=0.95)
                #grid.text("MAIN TITLE", vp = viewport(layout.pos.row = 1, layout.pos.col = 1:2))
                #grid.text("The user adds a title!", gp=r["gpar(fontsize = 20)"])
                page_title = "The user adds a title!"
                #grid_row_count += 1
                #row_count += 1

                #heights = unit(c(0.5, 5, 5), "null"))))
            heights = [1] * row_count
            #heights[0] = 0
            #print "heights:", heights

            vp = grid.viewport(layout=grid.layout(row_count, col_count, heights=r.unit(FloatVector(heights), "null")))
            r.pushViewport(vp)

            # IntVector((1,2)) gp=grid.gpar(fontsize=20),
            #rect = grid.rect(gp=grid.gpar(fill="grey90"))
            #rect = r.textGrob("Daily QC: Blue", gp=grid.gpar(fontsize=20, fill="black"))
            #t = grid.text("MAIN TITLE", vp=grid.viewport(**{"layout.pos.row": 1, "layout.pos.col": 1}))
            #print "rect:", rect
            #r["print"](rect, vp=grid.viewport(**{"layout.pos.row": 1, "layout.pos.col": 1 }))

            #r["print"](v, vp=grid.viewport(**{"layout.pos.row": row, "layout.pos.col": c }))
            #top <- arrangeGrob(items_to_plot, r, nrow=2)
            #print "items_to_plot:", len(items_to_plot)

            #pushViewport(viewport(layout = grid.layout(4, 2, heights = unit(c(0.5, 5, 5, 5), "null"))))
            page_plot_count = 0
            for row in range(1, row_count+1):
                for c in range(1, col_count+1):
                    page_plot_count += 1
                    v = items_to_plot.pop(0)
                    r["print"](v, vp=grid.viewport(**{"layout.pos.row": row, "layout.pos.col": c }))
                    if not items_to_plot:
                        break
                if not items_to_plot:
                    break

    items_to_plot = []
    for index, plot_box_dict in enumerate(plot_list):
        #print "%d %% %d: %d" % (plots_per_page, index, index % plots_per_page)
        #print "plot_box_dict:", plot_box_dict.keys()
        plot_type = plot_box_dict.get("plot_type", None)
        cprint("Creating plot %d of %d %s" % (index + 1, len(plot_list), "(%s)" % plot_type if plot_type else ""), "green")
        if util.cmd_args.verbose > 2:
            cprint("box_key: %s" % plot_box_dict["box_key"], "yellow")

        # plot_set contains all the plots we want in the same box
        title = plot_box_dict["box_title"]
        sets = plot_box_dict["sets"]
        index_within_page = index % plots_per_page
        column_index = index % n_columns

        # Margin parameteres
        #plot_box_dict["mar"] = IntVector(box_margins)
        plot_box_dict["column_index"] = column_index
        plot_box_dict["plot_index"] = index
        plot_box_dict["plot_number"] = index + 1
        plot_box_dict["index_within_page"] = index_within_page
        plot_box_dict["index_last_row_on_last_page"] = index_last_row_on_last_page

        if not plot_box_dict["axis_conf"].get("y_axis_on_plot_func", None) is None and not plot_box_dict["axis_conf"]["y_axis_on_plot_func"](plot_box_dict):
            plot_box_dict["theme_args"].update({"axis.title.y": ggplot2.element_blank()})

        if not plot_box_dict["axis_conf"].get("x_axis_on_plot_func", None) is None and not plot_box_dict["axis_conf"]["x_axis_on_plot_func"](plot_box_dict):
            plot_box_dict["theme_args"].update({"axis.title.x": ggplot2.element_blank()})

        # Copy the values from the first file_conf (sets[0])
        #title_values_dict = deepcopy(sets[0])
        #title_values_dict.update(plot_conf["box_conf"])
        #plot_box_dict["box_title"] = plot_box_dict["box_title_def"] % title_values_dict

        plot_box_dict["pdf_page_number"] = conf["pdf_page_number"]

        try:
            # Run the plot function for this box
            #print "Calling plot box func - y axis title:", plot_box_dict["y_axis_title"]["title"]
            if conf["plot_conf"]["make_plots"]:
                pp_list = plot_box_dict["plot_func"](conf, plot_box_dict, sets)
                items_to_plot.extend(pp_list)
            #print "items_to_plot:", items_to_plot
        except:
            cprint("Exception occured when running plot function for plot box '%s', sets: %s" % (plot_box_dict["box_key"], sets.keys()), "red")
            raise

        col_count = conf["plot_conf"]["n_columns"]
        row_count = conf["plot_conf"]["n_rows"]

        vprint("index: %d, plot_index_on_page: %d, plots_per_page: %d" % (index, plot_index_on_page, plots_per_page), v=3)

        #if ((((index + 1) % plots_per_page) == 0) or (max_plots_per_page and plot_index_on_page == (max_plots_per_page -1))):
        if ((index + 1) % plots_per_page) == 0:
            plot_index_on_page = 0
            if conf["plot_conf"]["make_plots"]:
                make_plot_page(items_to_plot, row_count, col_count)
            conf["pdf_page_number"] += 1
        else:
            plot_index_on_page += 1

        #if plot_groups.get("page_title", None) and conf["print_page_title"]:
        #    #print "page_title:", plot_groups["page_title"]
        #    r.mtext(plot_groups["page_title"], side=3, line=2, outer=True, cex=0.95)

        #if plot_groups.get("y_axis_title", None):
        #    r.mtext(plot_groups["y_axis_title"], side=2, line=4, outer=True, cex=0.95)

        #if plot_groups.get("x_axis_title", None):
        #    adj = robjects.NA_Character
        #    colors = robjects.NA_Character
        #    if "adj" in plot_groups["x_axis_title"]:
        #        adj = Vector(plot_groups["x_axis_title"]["adj"])
        #    if "colors" in plot_groups["x_axis_title"]:
        #        colors = StrVector(plot_groups["x_axis_title"]["colors"])
        #    r.mtext(text=Vector(plot_groups["x_axis_title"]["title"]), side=1, line=1, outer=True,
        #            adj=adj, col=colors,cex=0.95)

    if items_to_plot:
        make_plot_page(items_to_plot, row_count, col_count)

do_ggplots = do_plots_ggplot2
