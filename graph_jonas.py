#!/usr/bin/env python
from graph_r import *
from graph import do_plots, throughput_results_parse, latency_results_parse, parse_filenames
from copy import deepcopy
from operator import itemgetter, attrgetter
from graph_time_test import get_latency_conf
from graph import get_files
from graph_default import *

filename_regexes = []
# Old files
jonas_filename_regex = r"fair-stable-(?P<num>\d+)-(?P<cap>\d+)kbit-rtt(?P<rtt>\d+)-(?P<tcp_type>[^\-]*)-(?P<queue>[^\-]*)-(?P<duration>\d+)s-"\
    "(?P<stream_count_thin1>\d+)vs(?P<stream_count_thick>\d+)-streams-itt(?P<itt>\d+)-ps(?P<packet_size>\d+)-.*"
filename_regexes.append(jonas_filename_regex)

# New files
jonas_filename_regex = r"fair-stable-(?P<num>\d+)-(?P<duration>\d+)secs-(?P<cap>\d+)kbit-rtt(?P<rtt>\d+)-tcp_(?P<tcp_type>[^\-]+)-opts(?P<sender_options>\d+)-"\
    "(?P<queue>[^\-]+)-(?P<stream_count_thin1>\d+)vs(?P<stream_count_thick>\d+)-itt(?P<itt>\d+)-ps(?P<packet_size>\d+)-.*"
filename_regexes.append(jonas_filename_regex)

jonas_filename_regex = r"fair-stable-(?P<num>\d+)-(?P<duration>\d+)secs-(?P<cap>\d+)kbit-rtt(?P<rtt>\d+)-tcp_(?P<tcp_type>[^\-]+)-opts(?P<sender_options>\d+)-(?P<queue>[^\-]+)-(?P<stream_count_thick>\d+)_greedy-(?P<stream_count_thin1>\d+)_itt(?P<itt_thin1>[^-]+)(:?-(?P<stream_count_thin2>\d+)_itt(?P<itt_thin2>[^-]+))?-ps(?P<packet_size>\d+)-.*"
filename_regexes.append(jonas_filename_regex)

def set_defaults(conf):
    default = { "file_parse": {}, "plot_conf": {} }
    for d in default:
        if not d in conf:
            conf[d] = {}
    conf["file_parse"]["filename_regex"] = filename_regexes
    #conf["file_parse"]["filename_regex"] = jonas_filename_regex
    conf["file_parse"]["pcap_suffix"] = "front.dump"
    conf["file_parse"]["files_match_exp"] = "*%s" % conf["file_parse"]["pcap_suffix"]
    conf["plot_conf"]["bandwidth"] = 2000 # The bandwidth cap of the link in Kbit/s
    conf["sample_sec"] = 30

default_conf = {}
set_defaults(default_conf)

def jonas_file_parse_generic(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    file_conf["pcap_file"] = pcap_file
    file_conf["src_ip"] = "10.0.0.10"
    file_conf["dst_ip"] = "10.0.1.10"
    file_conf["payload_thin1"] = file_conf["packet_size"]
    file_conf["output_dir"] = plot_conf["output_dir"]

    thick_start = 12345
    thin1_start = 23456
    thin2_start = 34567

    file_conf["type"] = parse_func_arg
    file_conf["itt"] = None

    if file_conf["type"] == "thin1":
        file_conf["dst_port"] = "%d-%d" % (thin1_start, thin1_start + file_conf["stream_count_thin1"])
        file_conf["itt"] = file_conf["itt_thin1"]
    elif file_conf["type"] == "thin2":
        file_conf["dst_port"] = "%d-%d" % (thin2_start, thin2_start + file_conf["stream_count_thin2"])
        file_conf["itt"] = file_conf["itt_thin2"]
    else:
        file_conf["dst_port"] = "%d-%d" % (thick_start, thick_start + file_conf["stream_count_thick"])

    file_conf["data_file_name"] = "%s-%s" % (basename.split(plot_conf["file_parse"]["pcap_suffix"])[0], file_conf["type"])
    file_conf["label"] = file_conf["queue"]
    #file_conf["column"] = queues[file_conf["queue"]]["order"]
    #file_conf["color"] = queues[file_conf["queue"]]["color"][file_conf["type"]]

def connection_info_file_parse_func_jonas(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    jonas_file_parse_generic(plot_conf, file_conf, pcap_file, basename, parse_func_arg)
    #file_conf["delay_type"] = "fixed"

    #file_conf["pcap_file_receiver"] = pcap_file.replace("%s%s" % (file_conf["hostname"], plot_conf["file_parse"]["pcap_suffix"]), "zreceiver" + plot_conf["file_parse"]["pcap_suffix"])
    file_conf["pcap_file_receiver"] = pcap_file.replace("front", "after")

    file_conf["streams_str"] = " %(stream_count_thick)d vs %(stream_count_thin1)d" % file_conf

    if "stream_count_thin2" in file_conf:
        file_conf["streams_str"] += " vs %(stream_count_thin2)d" % file_conf

    if "cap" in file_conf:
        plot_conf["plot_conf"]["bandwidth"] = file_conf["cap"]

    file_conf["results"] = {"conninfo": {}}
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    file_conf["results"]["conninfo"]["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "command.stout"))
    file_conf["results"]["conninfo"]["results_file"] = file_conf["results"]["conninfo"]["stdout_file"]
    file_conf["results"]["conninfo"]["results_cmd"] = "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -o %(output_dir)s -e 1> %(stdout_file)s" %\
        dict(file_conf, **file_conf["results"]["conninfo"])
    file_conf["color"] = "darkred" # Default color for dataset
    file_conf["group"] = file_conf["num"]
    file_conf["hostname"] = ""

def jonas_throughput_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    jonas_file_parse_generic(plot_conf, file_conf, pcap_file, basename, parse_func_arg)
    file_conf["results"]["throughput"]["csv_file"] = os.path.join(plot_conf["output_dir"], "%s.csv" % file_conf["data_file_name"])
    file_conf["results"]["throughput"]["err_file"] = os.path.join(plot_conf["output_dir"], "%s.err" % file_conf["data_file_name"])
    file_conf["results"]["throughput"]["results_file"] = file_conf["results"]["throughput"]["csv_file"]
    file_conf["sample_sec"] = str(int(plot_conf["sample_sec"] * 1000))
    file_conf["results"]["throughput"]["results_cmd"] = "./tcp-throughput -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -t %(sample_sec)s 1> %(csv_file)s 2> %(err_file)s" %\
        dict(file_conf, **file_conf["results"]["throughput"])

def jonas_latency_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    jonas_file_parse_generic(plot_conf, file_conf, pcap_file, basename, parse_func_arg)
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    file_conf["results"]["latency"]["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "all-aggr.dat"))
    file_conf["results"]["latency"]["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "all-aggr.stout"))
    file_conf["results"]["latency"]["results_cmd"] = "./analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -u%(prefix)s -o %(output_dir)s -A 1> %(stdout_file)s" %\
        dict(file_conf, **file_conf["results"]["latency"])

    if "percentiles" in queues[file_conf["queue"]] and file_conf["type"] in queues[file_conf["queue"]]["percentiles"]:
        file_conf["percentiles"] = queues[file_conf["queue"]]["percentiles"][file_conf["type"]]

def throughput_box_key_func(box_conf, conf):
    box_conf["plot_func"] = plot_latency_box

throughput_conf = deepcopy(default_conf)
throughput_conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Packet_size:%(packet_size)s_IAT:%(iat)s",
                                "sort_key_func": itemgetter("stream_count_thin1"),
                                "sort_keys": ["stream_count_thin1"],
                                "func" : throughput_box_key_func,
                                "box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d" }
throughput_conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Packet_size:%(packet_size)s_IAT:%(iat)s_Queue:%(queue)s" }
throughput_conf["page_group_def"] = {"title": "Packet_size: %(packet_size)s, IAT: %(iat)sms",
                                     "sort_keys": ["packet_size", "iat"], "sort_key_func": itemgetter(0,1) }
throughput_conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 2,
                                      "x_axis_title" : { "title": ["TCP variation used for competing streams"]},
                                      "plot_func": plot_boxplot })
throughput_conf["parse_results_func"] = throughput_results_parse
throughput_conf["file_parse"].update({ "func": jonas_throughput_file_parse_func, "parse_func_arg": ["thin", "thick"]})
throughput_conf["plot_conf"]["y_axis_title"] = "Throughput (Kbit/second aggregated over %(sample_sec)d seconds)" % throughput_conf
throughput_conf["plot_conf"]["y_axis_lim"] = throughput_conf["plot_conf"]["bandwidth"] * 1000 # Multiply by 1000 to get bit/s
throughput_conf["plot_conf"]["y_axis_range"] = xrange(0, int(throughput_conf["plot_conf"]["bandwidth"] * 1.2), 50)
throughput_conf["box_commands"] = { "xline": {"command": "abline(h=%d, lty=2)", "func": lambda plot_conf, file_conf: xline(plot_conf["bandwidth"],
                                                                                                                           file_conf["packet_size"],
                                                                                                                           file_conf["iat"],
                                                                                                                           file_conf["stream_count_thin1"],
                                                                                                                           file_conf["stream_count_thick"])
                                          }
                                }
throughput_conf["y_axis_data"] = [i * 1000 for i in throughput_conf["plot_conf"]["y_axis_range"]]
throughput_conf["y_axis_labels"] = [i for i in throughput_conf["plot_conf"]["y_axis_range"]]

queues = {
    "pfifo": {"order": 1, "color": {"thin": "white", "thick": "white" } },
    "sfb": { "order": 3, "color": {"thin": "darkblue", "thick": "darkred" } },
    "red": { "order": 6, "color": {"thin": "darkblue", "thick": "darkred" } },
    "choke": { "order": 7, "color": {"thin": "darkblue", "thick": "darkred" } },
    "bfifo": { "order": 8, "color": {"thin": "cyan", "thick": "darkred" } },
    "sfq":  { "order": 2, "color": {"thin": "deepskyblue", "thick": "darkmagenta" } },
    "codel": { "order": 4, "color": {"thin": "chartreuse", "thick": "darkred" } },
    "fq_codel": { "order": 5, "color": {"thin": "bisque", "thick": "darkgreen" }, "percentiles": {"thin": [.50, .90]} },
}


def latency_box_key_func(box_conf, conf):
    conf["color"] = queues[conf["queue"]]["color"][conf["type"]]

def verify_trace_file_parse_func_jonas(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    jonas_file_parse_generic(plot_conf, file_conf, pcap_file, basename, parse_func_arg)

    file_conf["pcap_file_receiver"] = pcap_file.replace("front", "after")
    file_conf["streams_str"] = "%(stream_count_thin1)d vs %(stream_count_thick)d" % file_conf
    file_conf["group"] = file_conf["num"]
    file_conf["hostname"] = ""

    if "cap" in file_conf:
        plot_conf["plot_conf"]["bandwidth"] = file_conf["cap"]
    verify_trace_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg)

def get_verify_connection_conf():
    conf = get_default_latency_conf()
    set_defaults(conf)
    set_trace_verification_conf(conf)
    conf["file_parse"].update({ "func": verify_trace_file_parse_func_jonas, "parse_func_arg": ["thin", "thick"] })
    conf["file_parse"]["func"] = verify_trace_file_parse_func_jonas

    #conf["file_parse"]["parse_func_arg"] = ["thin", "thick"]
    return conf

def get_connection_info_conf():
    connection_info_conf = get_default_latency_conf()
    set_defaults(connection_info_conf)

    connection_info_conf["output_file"] = args.connection_info_output_file
    connection_info_conf["parse_results_func"] = conn_info_results_parse
    connection_info_conf["plot_conf"]["plot_func"] = None
    connection_info_conf["plot_conf"]["make_plots"] = False

    connection_info_conf["results"]["conninfo"] = { "loss_stats": []}
    #connection_info_conf["file_parse"].update({ "func": connection_info_file_parse_func_jonas, "parse_func_arg": ["thin1", "thick"] })
    #connection_info_conf["file_parse"].update({ "func": connection_info_file_parse_func_jonas, "parse_func_arg": ["thin1", "thin2", "thick"] })
    connection_info_conf["file_parse"].update({ "func": connection_info_file_parse_func_jonas, "parse_func_arg": ["thin1"] })

    connection_info_conf["process_results"] = { "func": connection_info_process_results, "column_widths" : {} }
    connection_info_conf["process_results"]["value_keys"] = OrderedDict().fromkeys(["hostname", "itt", "queue", "collapse", "conns", "bytes_loss", "ranges_loss", "num", "loss_stats"], None)
    connection_info_conf["process_results"]["headers"] = ["Host", "itt", "queue", "collapse", "Conns", "bytes loss", "ranges loss", "Num", "R loss (Min/1st Qu/Median/3rd Qu/Max)"]
    connection_info_conf["process_results"]["default_column_width"] = 12
    connection_info_conf["process_results"]["column_widths"].update({"Host": 25, "itt": 6, "3rd Qu.": 6, "Median": 6, "Conns": 10, "Num": 4, "R loss (Min/1st Qu/Median/3rd Qu/Max)": 40 })

    # Enable sorting
    #connection_info_conf["process_results"]["sort_keys"] = ["queue", "collapse"]

    def loss_too_high_color(loss, row_values):
        color = "white"
        try:
            if float(loss) > 12:
                color = "red"
        except:
            pass
        return colored(loss, color)
    connection_info_conf["process_results"]["value_keys"]["bytes_loss"] = loss_too_high_color

    def hostname_color(hostname, row_values):
        color = "white"
        try:
            p = int(row_values["dst_port"].split("-")[0])
            if p == 12345:
                color = "yellow"
        except:
            pass
        return colored(hostname, color)
    connection_info_conf["process_results"]["value_keys"]["hostname"] = hostname_color

    if os.path.isfile(connection_info_conf["output_file"]):
        os.remove(connection_info_conf["output_file"])
    return connection_info_conf

def conn_info_results_parse(conf, box_conf, file_data, set_key):
    print "conn_info_results_parse:", file_data["results"]["conninfo"]["results_file"]
    with open(file_data["results"]["conninfo"]["results_file"], 'r') as data_file:
        content = data_file.read()

    #"Average                                    0                   9816          14.5 %        14.1 %"
    regex = "Average\s+0\s+(?P<packets_sent>\d+)\s+(?P<bytes_loss>\S+)\s\%\s+(?P<ranges_loss>\S+)\s\%"
    m = re.search(regex, content, flags=re.DOTALL)

    collapse = False
    for line in open(file_data['pcap_file'].replace('front.dump', 'options_sender.log')).readlines():
        if not re.search(r'sysctl -w net\.ipv4\.tcp_retrans_collapse=1', line) is None:
            collapse = True
            break

    regex = "(?P<src_ip>\S+)-(?P<src_port>\S+)-(?P<dst_ip>\S+)-(?P<dst_port>\S+)\s+(?P<duration>\d+)\s+(?P<packets_sent>\d+)\s+(?P<bytes_loss>\S+)\s\%\s+(?P<ranges_loss>\S+)\s\%"
    m3 = re.finditer(regex, content, flags=re.DOTALL)
    if not m3:
        print "Failed to match regex!"
        return

    bytes_loss = []
    ranges_loss = []

    for l in m3:
        bytes_loss.append(float(l.group("bytes_loss")))
        ranges_loss.append(float(l.group("ranges_loss")))

    dataf = DataFrame({'bytes_loss': FloatVector(bytes_loss)})
    result = parse_r_summary(r["summary"](dataf))
    loss_stats = "{Min:03.1f} / {1st Qu:03.1f} / {Median:03.1f} / {3rd Qu:03.1f} / {Max:03.1f}".format(**result)

    if not "queue" in file_data:
        if file_data["num"] <= 4:
            file_data["queue"] = "Short"
        elif file_data["num"] <= 8:
            file_data["queue"] = "Medium"
        elif file_data["num"] <= 12:
            file_data["queue"] = "Long"

    conns = "%svs%s" % (file_data["stream_count_thick"], file_data["stream_count_thin1"])
    if "stream_count_thin2" in file_data:
        conns += "vs%s" % (file_data["stream_count_thin2"])


    data = {"hostname" : "%s:%s" % (file_data["src_ip"], file_data["dst_port"]), "collapse" : collapse, "segoff" : False,
            "bytes_loss" : float(m.group("bytes_loss")), "ranges_loss": float(m.group("ranges_loss")), "loss_stats": loss_stats,
            "queue": file_data["queue"], "conns": conns, "itt": file_data["itt"],
            "num": file_data["num"], "dst_port" : file_data["dst_port"] }

    key = "%s:%s-%s:%s" % (l.group("src_ip"), l.group("src_port"), l.group("dst_ip"), l.group("dst_port"))
    conf["results"]["conninfo"]["loss_stats"].append(data)

def main(args):
    if args.goodput_output_file:
        goodput_conf = get_goodput_conf()
        do_all_plots(args, goodput_conf=goodput_conf)

    if args.throughput_output_file:
        throughput_conf = get_throughput_conf()
        do_all_plots(args, throughput_conf=throughput_conf)

    if args.latency_output_file:
        latency_conf = get_latency_conf()
        do_all_plots(args, latency_conf=latency_conf)

    if args.latency_table_output_file:
        latency_conf = get_latency_conf()
        latency_conf["plots_per_page"] = 5
        latency_conf["add_meta_data"] = True
        latency_conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 2 })
        latency_conf["output_file"] = args.latency_table_output_file
        print "output_file:", latency_conf["output_file"]
        do_all_plots(args, conf=latency_conf)

    if args.duration_output_file:
        duration_conf = get_duration_conf()
        do_all_plots(args, duration_conf=duration_conf)

    if args.data_transfer_output_file:
        data_transfer_conf = get_data_transfer_conf()
        do_all_plots(args, data_trans_bars_conf=data_transfer_conf)

    if args.connection_info_output_file:
        connection_info_conf = get_connection_info_conf()
        do_all_plots(args, conf=connection_info_conf)

    if args.verify_trace_output_file:
        verification_conf = get_verify_connection_conf()
        do_all_plots(args, conf=verification_conf)


if __name__ == "__main__":
    argparser = argparse.ArgumentParser(description="Run test sessions")
    argparser.add_argument("-lt", "--latency-table-output-file",  help="Create latency plots with data table and save to specified file.", required=False, default=False)
    argparser.add_argument("-ci", "--connection-info-output-file",  help="Write connection info to the to specified file.", required=False, default=False)
    args = parse_args(argparser=argparser)
    main(args)
    end()
