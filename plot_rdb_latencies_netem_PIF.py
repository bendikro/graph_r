#!/usr/bin/env python
from datetime import datetime
import argparse
import os
from graph_r import *

from graph_base import main, parse_args
from graph_default_bendik import get_default_conf
from graph_r_ggplot import ggplot_cdf_box, do_ggplots, ggplot2

from rpy2.robjects.lib import grid
from latency import get_latency_conf as get_latency_conf_original

import common
import data_tables
import util
import latency

import graph_base
import graph_default
graph_default.get_conf = get_default_conf


def custom_latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    file_conf["pre_results"] = { "setup_table": {"func": custom_pre_results_func}}

    #print "custom_latency_file_parse_func - type_id: %s, group: %s" % (file_conf["type_id"], file_conf["group"])

def custom_pre_results_func(conf, plot_box_dict, file_data, set_key):
    #print "custom_pre_results_func"

    if not "latex_test_setup_table" in conf["data_tables"]:
        data_tables.fetch_table_conf(table_func=data_tables.get_table_netem_loss_test_setup, conf=conf, table_key="latex_test_setup_table")

    file_data["fig_label"] = "%s-%s" % (conf["data_tables"]["latex_test_setup_table"]["path_prefix"], plot_box_dict["box_key"].replace("_", "-"))

    #print "streams_id:", file_data["streams_id"]
    stream_properties = file_data["streams"][file_data["streams_id"]]
    #print "stream_properties (%s): %s" % (file_data["streams_id"], stream_properties)
    stream_properties["color_mapping"] = deepcopy(latency.stream_type_to_ggplot_properties[file_data["streams_id"]]["color_mapping"])
    #print "color_mapping (%s): %s" % (file_data["streams_id"], stream_properties["color_mapping"])

    if file_data["type_id"] == "type_thin2":
        pass
    else:
        if file_data["group"] != 0:
            stream_properties["color_mapping"][0].update({"legend_name": "RDB %d" % file_data["group"],
                                                          #"color_code": 'c(brewer.pal(9, "Blues")[7:7])'
                                                          "colors": [file_data["color"]],
                                                      })
        else:
            stream_properties["color_mapping"][0].update({"legend_name": "TCP",
                                                          #"color_code": 'c(brewer.pal(9, "Greens")[7:7])'
                                                          "colors": [file_data["color"]],
                                                          })

    #print "Legend name:", stream_properties["color_mapping"][0]["legend_name"]

    if conf["box_conf"]["latency_options"].get("per_stream", False) is True:
        stream_properties.update({"factor": "stream_id",
                                  "type_id_column": "stream_id",
                                  "legend_count": 1,
                                  "breaks_factor": "color_column",
                                  #"aes_args": { "color": 'stream_id'},
                                  "aes_args": { "color": 'color_column', "group": 'stream_id'},
                                  #"plot_args": {"show_guide": False },
                              })
    else:
        stream_properties.update({"factor": "color_column",
                                  "type_id_column": "color_column", # Only used for "Data points"
                                  "legend_count": 1,
                                  "breaks_factor": "color_column",
                                  #"aes_args": { "color": 'stream_id'},
                                  #"aes_args": { "color": 'color_column', "group": 'stream_id'},
                                  "aes_args": { "color": 'color_column',
                                                #"group": 'color_column'
                                            },
                                  #"plot_args": {"size": 0.4 },
                              })

    common.do_stream_custom_table_data(file_data, plot_box_dict, {}, conf["data_tables"]["latex_test_setup_table"])


def custom_rdb_latency_results_parse(conf, plot_box_dict, file_data, set_key):

    if not "table_latency_data" in plot_box_dict["data_tables"]:
        data_tables.fetch_table_conf(table_func=data_tables.get_table_latency_stats, conf=plot_box_dict, table_key="table_latency_data")
        plot_box_dict["data_tables"]["table_latency_data"]["args"].update({"padding.v": r('unit(2, "mm")'), "padding.h": r('unit(1.7, "mm")'), "show.box": True})
        plot_box_dict["data_tables"]["table_latency_data"]["default_font_size"] = 7

    latency.latency_results_parse(conf, plot_box_dict, file_data, set_key)
    common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["ecdf_ggplot2_values"], plot_box_dict["data_tables"]["table_latency_data"])

    #latencies = file_data["ecdf_ggplot2_values"]
    #bytes_latencies_summary = util.parse_r_column_summary(r["summary"](latencies[1]))
    #plot_box_dict["data_tables"]["table_latency_data"]["data"][-1].update(bytes_latencies_summary)
    #plot_box_dict["data_tables"]["table_latency_data"]["data"][-1]["pif"] = file_data["group"]
    #
    #def get_percentiles(data, percentiles):
    #    p = sorted(percentiles)
    #    pf = [x/100.0 for x in p]
    #    res = r["quantile"](latencies[1], FloatVector(pf))
    #    return dict(("%dth" % int(p[i]), r) for i, r in enumerate(res))
    #percentiles = get_percentiles(latencies[1], [25, 50, 75, 80, 90, 95, 99])
    #plot_box_dict["data_tables"]["table_latency_data"]["data"][-1].update(percentiles)

def custom_latency_process_results(conf, groups_list=None):
    tconf = conf["data_tables"]["latex_test_setup_table"]
    latency.write_latex_table_test_setup(tconf)

    if groups_list:
        print "groups_list:", len(groups_list)
        common.write_latex_figures(conf, groups_list, conf["output_file"], path_prefix=tconf["path_prefix"],
                                   plots_per_page=4, plots_per_subfig=4, bookmark_level=2,
                                   bookmark_title="Loss: %(loss)s, ITT: %(itt_thin1)s, PIF lim: %(packets_in_flight)s")


def get_latency_conf_ggplot():
    conf = get_latency_conf_original()
    conf["box_conf"].update({#"key": "Streams:%(stream_count_thin1)s_Streams_thin2:%(stream_count_thin2)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(linear_timeout)s_Loss:%(loss)s",
        "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_%(duration)s_%(thin_dupack)s_%(linear_timeout)s_Loss:%(loss)s",
        #"sort_key_func": lambda x: (x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["itt_thin1"]),
        #"sort_keys": ["stream_count_thin1", "stream_count_thick", "group", "itt_thin1"],
        "sort_key_func": lambda x: (float(x["loss"]), int(str(x["itt"]).split(":")[0]), x["stream_count_thin1"], x["group"]),
        "sort_keys": ["loss", "itt", "stream_count_thin1", "group"],
        "func" : latency.latency_box_key_func,
        "latency_options": {"per_stream": False},
        #"box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d %(cong_control_thin1)s " }
        "box_titles": { "main": { "title_def": "Loss: %(loss)s %%  ITT: %(itt)s ms", "order": 2 },
                        #                        "Duration: %(duration)s min    ITT: %(itt)s ms   Loss: %(loss)s %%    RTT: %(rtt)s ms"
                        "top_title2": { "title_def": "Duration: %(duration)s min   Streams: %(stream_count_thin1)s   RTT: %(rtt)s ms  QLEN: %(queue_len)s", "order": 1 },
                    },
        #"box_title_def" : "Duration: %(duration)s min    ITT: %(itt)s ms   Loss: %(loss)s %%    RTT: %(rtt)s ms" }
    })
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["page_group_def"] = {
        "title": "%(stream_count_thin1)d Thin streams  - Duration: %(duration)s min\n"
        "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets, Cong: %(cong_control_thin1)s",
        "common_key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_RTT:%(rtt)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(linear_timeout)s_Loss:%(loss)s",
        "sort_keys": ["loss"],
        #"sort_keys": ["payload_thin1", "rtt", "stream_count_thin1"],
        "sort_key_func": lambda x: (float(x["loss"])),
        #"sort_key_func": lambda x: (x["payload_thin1"], x["rtt"], x["stream_count_thin1"]),
        #"sort_key_func": lambda x: (x[0], x[1], x[2])
    }
    conf["process_results"]["custom_latency_process_results"] = { "func": custom_latency_process_results }

    #conf["box_conf"].update({ "custom_ggplot2_cmds": [thin_itt_mark_line_func_ggplot2], "show_box_title": True,                         })
    conf["box_conf"]["plot_and_table"].update({#"make_table": common.TABLE_ACTION.NO,
        "make_table": common.TABLE_ACTION.APPEND_TO_PLOT_BOX,
                                               "table_key": "table_latency_data",
                                               #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                               "heights": (.8, .01, .19),
                                           })

    from operator import itemgetter, attrgetter
    conf["box_conf"]["legend"].update({#"legend_title": "Streams",
        "legend_sort": { "do_sort": True, "sorted_args": {"key": lambda x: x["order"], "reverse": False }}})

    conf["parse_results_func"] = custom_rdb_latency_results_parse

    def do_ggplot_cdf_box(conf, plot_box_dict, sets):
        #print "Conf:", conf
        #if plot_box_dict["plot_index"] % 2 == 1:
        #    plot_box_dict["theme_args"].update({"axis.title.y": ggplot2.element_blank() })
        return ggplot_cdf_box(conf, plot_box_dict, sets)

    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1,
                              "r.plot_args": {"cex.main": 0.9 },
                              "r.mtext_page_title_args": {"cex.main": 1.2 },
                              "plot_func": do_ggplot_cdf_box,
                              "do_plots_func": do_ggplots, #do_plots,
                              #"do_plots_func": do_plots,
    })
    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [100, 800], "y_axis_lim" : [0.5, 1.05],
                                            "y_axis_title" : { "title": "Cumulative Distribution Function", },
                                            "x_axis_title" : { "title": "ACK latency per segment (ms)", },
                                            "y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": .1,
                                            "x_axis_main_ticks": [0, 800, 100], "x_axis_minor_ticks": 50,
                                            "y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                        })

    conf["paper_width"] = 4.4
    conf["paper_height"] = 3.3

    conf["file_parse"].update({ "func": custom_latency_file_parse_func })

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [0, 1200], "y_axis_lim" : [0.0, 1.05]})

    #conf["paper_height"] = 15
    conf["paper_width"] = 4.5
    conf["paper_height"] = 4

    conf["box_conf"]["plot_and_table"].update({"heights": (.7, .01, .29)})


    conf["plot_conf"]["theme_args"].update({"legend.position": FloatVector([0.86, 0.38]),
                                            "legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
                                            "legend.text": r('element_text(size=6)'),
                                            "legend.key.size": r('unit(5, "mm")'),
                                            "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
                                            "axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
                                            "axis.text.x": r('element_text(size = 8)'),
                                            "axis.text.y": r('element_text(size = 8)'),
                                            #"plot.title": r('element_text(size=8, lineheight=1, vjust=1.1, hjust=0.9, color="grey20")'), # , face="bold"
                                            "plot.title": r('element_text(size=9, lineheight=1, hjust=0.5, color="grey20")'), # , face="bold"
                                            #"panel.margin": r('unit(0 , "lines")'),
                                            #"panel.border": r('element_rect(fill=NA,color="darkred", size=0.5, linetype="solid")'),
                                            #"plot.margin": r('rep(unit(0,"null"),4)'),
                                            "plot.margin": r('unit(c(0.1, 0.1, 0.2, 0), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                        })
    #conf["box_conf"]["latency_options"].update({"per_stream": True})
    #conf["box_conf"]["custom_ggplot2_plot_cmds"].update({"itt_marks": latency.thin_itt_mark_line_func_ggplot2})
    conf["data_table_defaults"].update({"latex_test_setup_table": {"path_prefix": "latency-netem-pif" }})
    return conf


def latency_paper_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    print "latency_paper_file_parse_func: '%s'" % parse_func_arg
    #sys.exit()
    #print "latency_barplots_file_parse_func: '%s'" % pcap_file

    file_conf["itt_int_thin1"] = file_conf["itt_thin1"].split(":")[0]

    #if file_conf["type_thin1"] == "tcp" and file_conf["hostname"] == "wsender":
    #    file_conf["color"] = "red"
    #    file_conf["label"] = "TCP-reference"

    if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    stream_properties = file_conf["streams"][file_conf["streams_id"]]

    file_conf["default_meta_info"] = False
    stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
                                           "factor": "id_column",
                                           #"breaks_factor": "color_column",  # These are for the legend
                                           "breaks_factor": "id_column",  # These are for the legend
                                           "aes_args": {
                                               "color": 'factor(id_column)',
                                               #"linetype": "factor(id_column)",
                                               #"color": 'id_column',
                                               #"linetype": "id_column",
                                               #"group": 'color_column'
                                               #"group": 'id_column',
                                               #"group": 'factor(color_column)',
                                                     } # These are for the general factoring of the data
                                       })

    #if file_conf["type_thin1"] == "tcp" and file_conf["hostname"] == "wsender":
    #    stream_properties["color_mapping"] = deepcopy(latency.stream_type_to_ggplot_properties[file_conf["streams_id"]]["color_mapping"])
    #    stream_properties["color_mapping"][0].update({"legend_name": "TCP Reference", #
    #                                                  #"color_code": 'c(brewer.pal(9, "Reds")[7:7])'
    #                                                  "colors": ["coral2"]
    #                                              })

def get_latency_conf_paper():
    conf = get_latency_conf_ggplot()
    print "get_latency_conf_paper:"
    conf["paper_width"] = 4.5
    conf["paper_height"] = 2.7

    attrs = {}
    attrs["grid_c"] = "grey70"
    # "solid", "dashed", "dotted", "dotdash", "longdash", and "twodash".
    attrs["grid_minor_t"] = "solid"
    attrs["grid_major_t"] = "solid"
    attrs["ticks_c"] = "grey30"
    attrs["axis_line_c"] = attrs["ticks_c"]
    attrs["axis_text_c"] = "grey20"
    attrs["axis_title_c"] = "grey20"
    attrs["plot_title_c"] = "black"
    attrs["legend_key_c"] = "black"

    conf["plot_conf"]["theme_args"].update({
        "plot.margin": r('unit(c(0, 0.3, 0, 0.1), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        "axis.ticks": r('element_line(colour="%(ticks_c)s")' % attrs),
        "panel.grid.minor": r('element_line(colour="%(grid_c)s", size=0.1, linetype="%(grid_minor_t)s")' % attrs),
        "panel.grid.major": r('element_line(colour="%(grid_c)s", size=0.2, linetype="%(grid_major_t)s")' % attrs),
        "panel.background": ggplot2.element_blank(),
        "plot.title": r('element_text(size=8, lineheight=1, hjust=0.5, color="%(plot_title_c)s")' % attrs), # , face="old"
        #"axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
        "axis.text.x": r('element_text(size = 8, color="%(axis_text_c)s")' % attrs),
        "axis.text.y": r('element_text(size = 7, color="%(axis_text_c)s")' % attrs),
        "axis.line": r('element_line(size=0.4, colour="%(axis_line_c)s")' % attrs), #
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0.3, color="%(axis_title_c)s")' % attrs),
        "axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 1, color="%(axis_title_c)s")' % attrs),
        #"axis.line.x": line along x axis (element_line; inherits from axis.line)
        #"axis.line.y": r('element_line(colour="grey20", size=0.5)'), #
        "legend.key": r('element_rect(fill=NA, size=0.1, linetype="solid", color="%(legend_key_c)s")' % attrs),
        #"legend.background": r('element_rect(fill=NA,color="darkred", size=0.5, linetype="solid")'),
    })
    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [100, 1200], "y_axis_lim" : [-0.05, 1.05],
                                            "y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": [0.1, 1.1, .2],
                                            "x_axis_main_ticks": [0, 1200, 100], "x_axis_minor_ticks": 50,
                                        })

    conf["paper_width"] = 7
    conf["paper_height"] = 4

    attrs = {}
    attrs["grid_minor_c"] = "grey80"
    attrs["grid_major_c"] = "grey80"
    attrs["grid_minor_c"] = attrs["grid_major_c"] = "#808080"
    # "solid", "dashed", "dotted", "dotdash", "longdash", and "twodash".
    attrs["grid_major_t"] = "solid"
    attrs["grid_minor_t"] = "solid"
    attrs["grid_major_s"] = "0.4"
    attrs["grid_minor_s"] = "0.2"
    #attrs["grid_minor_s"] = "0.5"
    #attrs["grid_major_t"] = "dotted"
    #attrs["grid_minor_t"] = "dotted"
    attrs["ticks_c"] = "grey30"
    attrs["axis_line_c"] = attrs["ticks_c"]
    attrs["axis_line_s"] = "0.5"
    attrs["axis_text_c"] = "grey10"
    attrs["axis_title_c"] = "grey10"
    attrs["plot_title_c"] = "black"
    attrs["legend_key_c"] = "black"
    attrs["legend_border"] = "grey20"

    conf["plot_conf"]["theme_args"].update({
        "plot.margin": r('unit(c(0.5, 0.1, 0.7, 0.7), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        #"panel.margin": r('unit(c(0.2, 0.3, 2, 0.8), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        "axis.ticks": r('element_line(size=0.4, colour="%(ticks_c)s")' % attrs),
        "axis.ticks.length": r('unit(2.5, "mm")'),
        "panel.grid.minor": r('element_line(colour="%(grid_minor_c)s", size=%(grid_minor_s)s, linetype="%(grid_minor_t)s")' % attrs),
        "panel.grid.major": r('element_line(colour="%(grid_major_c)s", size=%(grid_major_s)s, linetype="%(grid_major_t)s")' % attrs),
        "panel.background": ggplot2.element_blank(),
        "plot.title": r('element_text(size=12, lineheight=1, hjust=0.5, vjust = 1.5, color="%(plot_title_c)s")' % attrs), # , face="old"
        #"axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
        "axis.text.x": r('element_text(size = 13, color="%(axis_text_c)s")' % attrs),
        "axis.text.y": r('element_text(size = 12, color="%(axis_text_c)s")' % attrs),
        "axis.line": r('element_line(size=%(axis_line_s)s, colour="%(axis_line_c)s")' % attrs), #
        "axis.title.x": r('element_text(size = 12, hjust = 0.5, vjust = -1, color="%(axis_title_c)s")' % attrs),
        "axis.title.y": r('element_text(size = 12, hjust = 0.4, vjust = 2, color="%(axis_title_c)s")' % attrs),
        #"axis.line.x": line along x axis (element_line; inherits from axis.line)
        #"axis.line.y": r('element_line(colour="grey20", size=0.5)'), #
        "legend.key": r('element_rect(fill=NA, size=0.2, linetype="solid", color="%(legend_key_c)s")' % attrs),
        "legend.key.size": r('unit(8, "mm")'),
        "legend.text": r('element_text(size=10, color="%(axis_title_c)s")' % attrs), # hjust = 0.5, vjust = 0.3,
        "legend.position": FloatVector([0.87, 0.45]),
        #"legend.margin": r('unit(10, "lines")'),
        #"legend.background": r('element_rect(fill=NA,color="black", size=0.5, linetype="solid")'),
        "legend.background": r('element_rect(fill="white", color="%(legend_border)s", size=0.3, linetype="solid")' % attrs),
    })
    conf["plot_conf"]["plot_args"] = {"size": 0.8 }
    #conf["plot_conf"]["plot_args"].update({"geom": "line" })
    #plot_args["geom"] = "line"

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [0, 1150], "y_axis_lim" : [-0.05, 1.05],
                                            "y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": [0.1, 1.1, .2],
                                            "x_axis_main_ticks": [0, 1200, 100], "x_axis_minor_ticks": 50,
                                        })


    def results_parse_hook(conf, box_conf, file_data, set_key):
        print "results_parse_hook"
        #stream_id = "%s" % (file_data["type"])

        if file_data["packets_in_flight"] == 0:
            stream_id = "tcp"
        elif file_data["packets_in_flight"] == 4:
            stream_id = "rdb4"
        elif file_data["packets_in_flight"] == 8:
            stream_id = "rdb8"

        #if file_data["type_thin1"] == "tcp" and file_data["hostname"] == "wsender":
        #stream_id = "tcp-ref"
        print "Setting id_column:", stream_id
        file_data["ecdf_ggplot2_values"] = r.cbind(file_data["ecdf_ggplot2_values"], id_column=stream_id)
        #print "DATAF:", type(file_data["ecdf_ggplot2_values"])
        #print util.get_dataframe_head(file_data["ecdf_ggplot2_values"])


    conf["results_parse_hook"] = results_parse_hook

    conf["box_conf"]["box_titles"] = { "main": { "title_def": "Streams: %(stream_count_thin1)s   ITT: %(itt_int_thin1)s ms  Seg. size: %(payload_thin1)s B   Loss: %(loss)s %%", "order": 1 }}
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO})

    conf["file_parse"].update({ "func": latency_paper_file_parse_func })
    conf["process_results"] = None

    def box_key_func_latency_paper(plot_conf, plot_box_dict, file_data, arg):
        file_data["legend_order"] = file_data["packets_in_flight"]
        #box_key_func(plot_conf, plot_box_dict, file_data, arg)
        #file_data["bundling_limit"] = "None"

        #print "ID:", plot_conf.get("experiment_id")
        #if plot_conf.get("experiment_id") == "E3":
        #    file_data["bundling_limit"] = "1"

        #print "box_key_func_latency_paper"
        #util.print_keys(file_data, keys=["type_thin1", "hostname", "packets_in_flight"])

        #print "DATAF:", type(dataf)
        #print util.get_dataframe_head(dataf)

        #if file_data["packets_in_flight"] == 0:
        #
        #elif file_data["packets_in_flight"] == 4:
        #
        #elif file_data["packets_in_flight"] == 8:

        #if file_data["type_thin1"] == "tcp" and file_data["hostname"] == "wsender":
        #    file_data["color"] = "coral2"
        #if file_data["type_thin1"] == "rdb" and file_data["hostname"] == "rdbsender":
        #    file_data["color"] = "red"

        # COLOR
        util.dict_set_default(plot_box_dict["legends"], ("color", {"guide_args": {}, "scale": {"func": ggplot2.scale_colour_manual, "args": {}}}))
        colorleg = plot_box_dict["legends"]["color"]
        colorleg["guide_args"].update({"order": 1})
        color_args = colorleg["scale"]["args"]
        #util.dict_set_default(color_args, [("labels", []), ("values", [])])
        util.dict_set_default(color_args, [("values", [])])
        color_args["name"] = ggplot2.element_blank()

        #util.dict_set_default(linetype["guide_args"], ("override.aes", {"colour": []}))
        colorleg["guide_args"].update({"override.aes": {"linetype": [1, 1, 1]}})

        # LINETYPE
        #util.dict_set_default(plot_box_dict["legends"], ("linetype", {"guide_args": {}, "scale": {"func": ggplot2.scale_linetype_manual, "args": {}}}))
        #linetype = plot_box_dict["legends"]["linetype"]
        #linetype["guide_args"].update({"order": 2 })

        #linetype_args = linetype["scale"]["args"]
        #util.dict_set_default(linetype_args, [("values", [])])
        #linetype_args["name"] = ggplot2.element_blank()
        #linetype_args["name"] = color_args["name"]

        #color_args["labels"] = ['RDB', 'TCP', 'TCP Reference']
        #color_args["values"] = ['#A52A2A', 'darkgreen', 'blue']

        #color_args["breaks"] = ['rdb8', 'rdb4', 'tcp']
        #color_args["values"] = rlc.TaggedList(['rdb8', 'rdb4', 'tcp'], tags=(tuple(['#A52A2A', 'darkgreen', 'blue'])))

        #from rpy2.robjects.vectors import Vector, IntVector, DataFrame, StrVector, FloatVector
        anno_colors = StrVector(['red3', 'orchid4', 'blue'])
        anno_colors.names = ['rdb8', 'rdb4', 'tcp']
        color_args["values"] = anno_colors
        color_args["labels"] = ['RDB SPIFL 3', 'RDB SPIFL 7', 'TCP']

        ####scale_linetype_manual(values = c(west = "solid", east = "dashed"))
        ####linetype_args["values"] = ["solid", "longdash", "twodash"]
        #anno_linetype = StrVector(["solid", "longdash", "dotted"])
        #anno_linetype.names = ['rdb8', 'rdb4', 'tcp']
        #linetype_args["values"] = anno_linetype
        #linetype_args["guide"] = False

    conf["box_conf"].update({"func" : box_key_func_latency_paper})

    return conf

def get_latency_conf_presentation():
    conf = get_latency_conf_ggplot()

    conf["paper_width"] = 7
    conf["paper_height"] = 4

    attrs = {}
    attrs["grid_c"] = "grey70"
    attrs["grid_minor_c"] = "grey80"
    attrs["grid_major_c"] = "grey80"
    attrs["grid_minor_c"] = attrs["grid_major_c"] = "#808080"
    # "solid", "dashed", "dotted", "dotdash", "longdash", and "twodash".
    attrs["grid_major_t"] = "solid"
    attrs["grid_minor_t"] = "solid"
    attrs["grid_major_s"] = "0.4"
    attrs["grid_minor_s"] = "0.2"
    #attrs["grid_minor_s"] = "0.5"
    #attrs["grid_major_t"] = "dotted"
    #attrs["grid_minor_t"] = "dotted"
    attrs["ticks_c"] = "grey30"
    attrs["axis_line_c"] = attrs["ticks_c"]
    attrs["axis_line_s"] = "0.5"
    attrs["axis_text_c"] = "grey10"
    attrs["axis_title_c"] = "grey10"
    attrs["plot_title_c"] = "black"
    attrs["legend_key_c"] = "black"
    attrs["legend_border"] = "grey20"

    conf["plot_conf"]["theme_args"].update({
        "plot.margin": r('unit(c(0.5, 0.1, 0.7, 0.7), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        #"panel.margin": r('unit(c(0.2, 0.3, 2, 0.8), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        "axis.ticks": r('element_line(size=0.4, colour="%(ticks_c)s")' % attrs),
        "axis.ticks.length": r('unit(2.5, "mm")'),
        "panel.grid.minor": r('element_line(colour="%(grid_minor_c)s", size=%(grid_minor_s)s, linetype="%(grid_minor_t)s")' % attrs),
        "panel.grid.major": r('element_line(colour="%(grid_major_c)s", size=%(grid_major_s)s, linetype="%(grid_major_t)s")' % attrs),
        "panel.background": ggplot2.element_blank(),
        "plot.title": r('element_text(size=12, lineheight=1, hjust=0.5, vjust = 1.5, color="%(plot_title_c)s")' % attrs), # , face="old"
        #"axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
        "axis.text.x": r('element_text(size = 13, color="%(axis_text_c)s")' % attrs),
        "axis.text.y": r('element_text(size = 12, color="%(axis_text_c)s")' % attrs),
        "axis.line": r('element_line(size=%(axis_line_s)s, colour="%(axis_line_c)s")' % attrs), #
        "axis.title.x": r('element_text(size = 12, hjust = 0.5, vjust = -1, color="%(axis_title_c)s")' % attrs),
        "axis.title.y": r('element_text(size = 12, hjust = 0.4, vjust = 2, color="%(axis_title_c)s")' % attrs),
        #"axis.line.x": line along x axis (element_line; inherits from axis.line)
        #"axis.line.y": r('element_line(colour="grey20", size=0.5)'), #
        "legend.key": r('element_rect(fill=NA, size=0.2, linetype="solid", color="%(legend_key_c)s")' % attrs),
        "legend.key.size": r('unit(8, "mm")'),
        "legend.text": r('element_text(size=10, color="%(axis_title_c)s")' % attrs), # hjust = 0.5, vjust = 0.3,
        #"legend.position": FloatVector([0.87, 0.45]),
        "legend.position": FloatVector([0.78, 0.45]),
        #"legend.position": "none",
        #"legend.margin": r('unit(10, "lines")'),
        #"legend.background": r('element_rect(fill=NA,color="black", size=0.5, linetype="solid")'),
        "legend.background": r('element_rect(fill="white", color="%(legend_border)s", size=0.3, linetype="solid")' % attrs),
    })
    conf["plot_conf"]["plot_args"] = {"size": 0.8 }
    #plot_args["geom"] = "line"

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [0, 1150], "y_axis_lim" : [-0.05, 1.05],
                                            "y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": [0.1, 1.1, .2],
                                            "x_axis_main_ticks": [0, 1200, 100], "x_axis_minor_ticks": 50,
                                        })


    def results_parse_hook(conf, box_conf, file_data, set_key):
        print "results_parse_hook"
        #stream_id = "%s" % (file_data["type"])

        if file_data["packets_in_flight"] == 0:
            stream_id = "tcp"
        #else:
        #    print "INCORRECT PIF"
        elif file_data["packets_in_flight"] == 4:
            stream_id = "rdb4"
        elif file_data["packets_in_flight"] == 8:
            stream_id = "rdb8"
        else:
            print "EMPTY PIF!!!!!!!!!!!!!!!!!!!!!!!!"

        #if file_data["type_thin1"] == "tcp" and file_data["hostname"] == "wsender":
        #stream_id = "tcp-ref"
        print "Setting id_column:", stream_id
        file_data["ecdf_ggplot2_values"] = r.cbind(file_data["ecdf_ggplot2_values"], id_column=stream_id)
        #print "DATAF:", type(file_data["ecdf_ggplot2_values"])
        #print util.get_dataframe_head(file_data["ecdf_ggplot2_values"])


    conf["results_parse_hook"] = results_parse_hook

    conf["box_conf"]["box_titles"] = { "main": { "title_def": "RTT: %(rtt)s ms   ITT: %(itt_int_thin1)s ms  Loss: %(loss)s %%", "order": 1 }}
    #conf["box_conf"]["box_titles"] = { }
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO})

    conf["file_parse"].update({ "func": latency_paper_file_parse_func })
    conf["process_results"] = None

    def box_key_func_latency_paper(plot_conf, plot_box_dict, file_data, arg):
        file_data["legend_order"] = file_data["packets_in_flight"]

        # COLOR
        util.dict_set_default(plot_box_dict["legends"], ("color", {"guide_args": {}, "scale": {"func": ggplot2.scale_colour_manual, "args": {}}}))
        colorleg = plot_box_dict["legends"]["color"]
        colorleg["guide_args"].update({"order": 1})
        color_args = colorleg["scale"]["args"]
        #util.dict_set_default(color_args, [("labels", []), ("values", [])])
        util.dict_set_default(color_args, [("values", [])])
        color_args["name"] = ggplot2.element_blank()

        #util.dict_set_default(linetype["guide_args"], ("override.aes", {"colour": []}))

        # LINETYPE
        #util.dict_set_default(plot_box_dict["legends"], ("linetype", {"guide_args": {}, "scale": {"func": ggplot2.scale_linetype_manual, "args": {}}}))
        #linetype = plot_box_dict["legends"]["linetype"]
        #linetype["guide_args"].update({"order": 2 })

        #linetype_args = linetype["scale"]["args"]
        #util.dict_set_default(linetype_args, [("values", [])])
        #linetype_args["name"] = ggplot2.element_blank()
        #linetype_args["name"] = color_args["name"]

        names = ['RDB (Bundle while PIFs <= 3)', 'RDB (Bundle while PIFs <= 7)', 'TCP']
        colors = ['red3', 'orchid4', 'blue']
        color_names = ['rdb8', 'rdb4', 'tcp']

        #color_args["breaks"] = ['rdb8', 'rdb4', 'tcp']
        #color_args["values"] = rlc.TaggedList(['rdb8', 'rdb4', 'tcp'], tags=(tuple(['#A52A2A', 'darkgreen', 'blue'])))
        #colors = ['blue']
        #names = ['tcp']

        #from rpy2.robjects.vectors import Vector, IntVector, DataFrame, StrVector, FloatVector
        anno_colors = StrVector(colors)
        anno_colors.names = color_names
        color_args["values"] = anno_colors
        color_args["labels"] = names

        #anno_colors = StrVector(['red3', 'orchid4', 'blue'])
        #anno_colors.names = ['rdb8', 'rdb4', 'tcp']
        #color_args["values"] = anno_colors
        #color_args["labels"] = ['RDB SPIFL 3', 'RDB SPIFL 7', 'TCP']


        colorleg["guide_args"].update({"override.aes": {"linetype": [1 for i in names]}})
        #colorleg["guide_args"].update({"override.aes": {"linetype": [1, 1, 1]}})

        ####scale_linetype_manual(values = c(west = "solid", east = "dashed"))
        ####linetype_args["values"] = ["solid", "longdash", "twodash"]
        #anno_linetype = StrVector(["solid", "longdash", "dotted"])
        #anno_linetype.names = ['rdb8', 'rdb4', 'tcp']
        #linetype_args["values"] = anno_linetype
        #linetype_args["guide"] = False

    conf["box_conf"].update({"func" : box_key_func_latency_paper})

    return conf


import latency
latency.get_conf = get_latency_conf_ggplot

def run(defaults={}):
    plot_types = ["default", "RDBSENDER_NETEM_RDB_LOSS_QL63", "paper_latency_netem"]

    argparser = graph_base.make_parser()
    options = argparser.add_argument_group('Latency RDB CDF options')
    options.add_argument("-pt", "--plot-type",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, default=plot_types[0])

    args = graph_base.parse_args(argparser, defaults=defaults)
    plot_type = args.plot_type
    #print "plot_type:", plot_type

    if plot_type == "default":
        defaults["latency_output_file"] = "RDBSENDER_NETEM_RDB_LOSS_latency_CDF_ggplot_5min.pdf"
        defaults["output_dir"] = "RDBSENDER_NETEM_RDB_LOSS"
        #defaults["file_regex_match"] = ["-frm", "r20.*itt50:\d+-.*da1-lt1-pif4.*min5.*"]
        #defaults["file_regex_match"] = ["-frm", "r20.*itt30.*loss0.5.*"]
        defaults["file_regex_match"] = ["-frm", "r20.*min5.*"]
        defaults["file_regex_nomatch"] = [".*min20.*", ".*pif100.*", ".*pif12.*",
                                          ".*loss0.2.*", ".*loss0.1.*",
                                          #".*loss10.*",
                                          #".*loss5.*",".*loss2.*",".*loss0.5.*",
                                          #".*itt100.*", ".*itt75.*",".*itt50.*",
        ]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_NETEM_RDB_LOSS_latency/all_results/"]
    elif plot_type == "paper_latency_netem":
        defaults["latency_output_file"] = "expr1_spif_netem.pdf"
        defaults["latency_output_file"] = "expr1_spif_netem_2.pdf"
        latency.get_conf = get_latency_conf_paper

        defaults["output_dir"] = "RDBSENDER_NETEM_RDB_LOSS"
        #defaults["file_regex_match"] = ["-frm", "r20.*itt50:\d+-.*da1-lt1-pif4.*min5.*"]
        #defaults["file_regex_match"] = ["-frm", "r20.*itt30.*loss0.5.*"]
        defaults["file_regex_match"] = ["-frm", "r20.*itt30:3.*min5.*loss10.*"] # r20-rdb-itt50:5-ps120-ccrdb-da1-lt1-pif8..kbit5000_
        defaults["file_regex_nomatch"] = [".*min20.*", ".*pif100.*", ".*pif12.*",
                                          ".*loss0.2.*", ".*loss0.1.*",
                                          #".*loss10.*",
                                          #".*loss5.*",".*loss2.*",".*loss0.5.*",
                                          #".*itt100.*", ".*itt75.*",".*itt50.*",
        ]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_NETEM_RDB_LOSS_latency/all_results/"]
    elif plot_type == "presentation_latency_netem":
        defaults["latency_output_file"] = "expr1_spif_netem_presentation.pdf"
        latency.get_conf = get_latency_conf_presentation

        defaults["output_dir"] = "RDBSENDER_NETEM_RDB_LOSS"
        #defaults["file_regex_match"] = ["-frm", "r20.*itt50:\d+-.*da1-lt1-pif4.*min5.*"]
        #defaults["file_regex_match"] = ["-frm", "r20.*itt30.*loss0.5.*"]
        defaults["file_regex_match"] = ["-frm", #"r20-tcp.*itt75:7.*min5.*loss10.*",
                                        "r20.*itt30:3.*min5.*loss10.*"]
        defaults["file_regex_nomatch"] = [".*min20.*", ".*pif100.*", ".*pif12.*",
                                          ".*loss0.2.*", ".*loss0.1.*",
                                          #".*loss10.*",
                                          #".*loss5.*",".*loss2.*",".*loss0.5.*",
                                          #".*itt100.*", ".*itt75.*",".*itt50.*",
        ]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_NETEM_RDB_LOSS_latency/all_results/"]
    elif plot_type == "RDBSENDER_NETEM_RDB_LOSS_QL63":
        defaults["latency_output_file"] = "RDBSENDER_NETEM_RDB_LOSS_latency_QL63_CDF_ggplot_5min.pdf"
        defaults["output_dir"] = "RDBSENDER_NETEM_RDB_LOSS_QL63"
        #defaults["file_regex_match"] = ["-frm", "r20.*itt50:\d+-.*da1-lt1-pif4.*min5.*"]
        #defaults["file_regex_match"] = ["-frm", "r20.*itt30.*loss0.5.*"]
        defaults["file_regex_match"] = ["-frm", "r20.*min5.*"]
        #defaults["file_regex_match"] = ["-frm", "r20.*min2.*"]
        defaults["file_regex_nomatch"] = [".*min20.*", ".*pif100.*", ".*pif12.*",
                                          ".*loss0\\.2.*", ".*loss0\\.1.*",
                                          #".*loss10.*",
                                          #".*loss5.*",".*loss2.*",".*loss0.5.*",
                                          #".*itt100.*", ".*itt75.*",".*itt50.*",
        ]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_NETEM_RDB_LOSS_latency_QL63/all_results/"]
    elif plot_type == "RDBSENDER_NETEM_RDB_LOSS_QL63_2":
        defaults["latency_output_file"] = "RDBSENDER_NETEM_RDB_LOSS_latency_QL63_2_CDF_ggplot_5min.pdf"
        defaults["output_dir"] = "RDBSENDER_NETEM_RDB_LOSS_QL63_2"
        #defaults["file_regex_nomatch"] = [#".*min20.*", ".*pif100.*", ".*pif12.*",]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_NETEM_RDB_LOSS_latency_QL63_2/all_results/"]
    elif plot_type == "RDBSENDER_NETEM_RDB_LOSS_QL63_3":
        defaults["latency_output_file"] = "RDBSENDER_NETEM_RDB_LOSS_latency_QL63_3_CDF_ggplot_5min.pdf"
        defaults["output_dir"] = "RDBSENDER_NETEM_RDB_LOSS_QL63_3"
        #defaults["file_regex_nomatch"] = [#".*min20.*", ".*pif100.*", ".*pif12.*",]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_NETEM_RDB_LOSS_latency_QL63_3/all_results/"]
    elif plot_type == "RDBSENDER_ZSENDER_TO_ZRECEIVER_THIN_PIF_NETEM_OLDCONG":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TO_ZRECEIVER_THIN_PIF_NETEM_OLDCONG_CDF_ggplot_5min.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TO_ZRECEIVER_THIN_PIF_NETEM_OLDCONG"
        #defaults["file_regex_match"] = [".*itt75.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_ZSENDER_TO_ZRECEIVER_THIN_PIF_NETEM_OLDCONG/all_results/"]
    elif plot_type == "RDBSENDER_TO_ZRECEIVER_V3_18":
        defaults["latency_output_file"] = "RDBSENDER_TO_ZRECEIVER_V3_18.pdf"
        defaults["output_dir"] = "RDBSENDER_TO_ZRECEIVER_V3_18"
        #defaults["file_regex_match"] = [".*itt75.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZSENDER_TEST_3_18/all_results/"]
    else:
        cprint("Invalid plot type: '%s'" % plot_type, "red")
        sys.exit()

    args = graph_base.parse_args(argparser, defaults=defaults)
    graph_base.main()

if __name__ == "__main__":
    #import sys
    #sys.argv.extend([
    #    "-od", "RDBSENDER_NETEM_RDB_LOSS",
    #    "-l", "RDBSENDER_NETEM_RDB_LOSS_latency_CDF_ggplot_5min.pdf",
    #    #"-l", "RDBSENDER_NETEM_RDB_LOSS_latency_CDF_ggplot_20min.pdf",
    #    #"-frm", "r20-tcp.*",
    #    #"-frm", ".*itt100-.*loss5.*",
    #    #"-frm", ".*itt50-.*loss5.*",
    #    #"-frm", ".*itt50-.*loss5.*",
    #    #"-frm", ".*itt50:5-.*min20.*loss10.*",
    #    #"-frm", ".*itt50.*min5.*loss5.*",
    #    #"-frm", ".*itt100-.*min5.*loss5.*",
    #    "-frm", ".*.*itt\d+:\d+-.*min5.*",
    #    #"-frm", ".*.*itt50:\d+-.*min5.*loss5.*",
    #    #"-frm", ".*.*min5.*",
    #    "-frn", ".*pif100.*",
    #    "-frn", ".*pif12.*",
    #    #"-frn", ".*itt100.*",
    #    "-frn", ".*itt75.*",
    #    #"-frn", ".*loss0.5.*",
    #    #r20-rdb-itt100-ps120-cccubic-da1-lt1-pif100_vs_y20-tcp-itt100-ps120..kbit5000_min5_rtt150_loss5_pif100_qlen60_delayfixed_num0_rdbsender.pcap
    #    #r21-tcp-itt100:15-ps120-cccubic-da0-lt0_vs_z21..kbit5000_min5_rtt150_loss_pif0_qlen60_delayfixed_num0_zsender.pcap
    #    #r21-tcp-itt100-ps120-cccubic-da0-lt1_vs_z10
    #    #"-frm", "t21.*itt100-.*cubic-da1-lt1.*vs_g10.*min5.*qlen30.*",
    #    #"/root/bendiko/pcap/rdbsender_ysender_to_zreceiver_loss_rate/RDBSENDER_YSENDER_NETEM_RDB_LOSS/all_results/"])
    #    "/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_NETEM_RDB_LOSS_latency/all_results/"])
    #args = parse_args()
    run()
