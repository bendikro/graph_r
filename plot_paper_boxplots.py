#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
import argparse
import os, sys

from util import vprint

import graph_base

from graph_r import *
from rpy2.robjects.lib import grid

from graph_default_bendik import get_default_conf
from graph_r_ggplot import do_ggplots, ggplot2, ggplot_cdf_box, ggplot_boxplot_box

from functools import partial
import data_tables
import latency
import common
from latency import get_latency_conf as get_latency_conf_original
import throughput
import graph_default
import tcpprobe
graph_default.get_conf = get_default_conf


import plot_latency_rdb_vs_greedy
from plot_latency_rdb_vs_greedy import get_latency_conf, get_latency_conf_with_throughput_separate_on_groups

def custom_pre_results_func(conf, plot_box_dict, file_data, set_key):

    stream_properties = file_data["streams"][file_data["streams_id"]]

    if not "color_mapping" in stream_properties:
        stream_properties["color_mapping"] = deepcopy(latency.stream_type_to_ggplot_properties[file_data["streams_id"]]["color_mapping"])

    if file_data["hostname"] != "zsender":
        vprint("Results parse, using label: %s, color: %s" % (file_data["label"], file_data["color"]), v=3)
        stream_properties["color_mapping"][0]["legend_name"] = file_data["label"]
        stream_properties["color_mapping"][0]["colors"] = [file_data["color"]]

    if not "latex_test_setup_table" in conf["data_tables"]:
        data_tables.fetch_table_conf(table_func=data_tables.get_table_latex_test_setup_with_tfrc_and_tcp_mods, conf=conf, table_key="latex_test_setup_table")

    if file_data["plot_type"] == "Latency":

        file_data["fig_label"] = "%s-%s" % (conf["data_tables"]["latex_test_setup_table"]["path_prefix"], plot_box_dict["box_key"].replace("_", "-"))
        file_data["fig_label"] = file_data["fig_label"].replace("-Plot-type:Latency", "")

        if file_data["hostname"] == "rdbsender":
            common.do_stream_custom_table_data(file_data, plot_box_dict, {}, conf["data_tables"]["latex_test_setup_table"])

        stream_properties["aes_args"].update({"color": "factor(color_column)", "linetype": "factor(color_column)"})
        stream_properties["plot_args"].update({"geom": "smooth"})

def custom_results_parse(conf, plot_box_dict, file_data, set_key):

    stream_properties = file_data["streams"][file_data["streams_id"]]

    if file_data["plot_type"] == "Throughput":
        throughput.throughput_results_parse(conf, plot_box_dict, file_data, set_key)
    if file_data["plot_type"] == "Goodput":
        throughput.throughput_results_parse(conf, plot_box_dict, file_data, set_key)
    elif file_data["plot_type"] == "Latency":
        latency.latency_results_parse(conf, plot_box_dict, file_data, set_key)

    if file_data["plot_type"] == "Latency":
        if not "table_latency_data" in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_latency_stats, conf=plot_box_dict, table_key="table_latency_data")

        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["ecdf_ggplot2_values"], plot_box_dict["data_tables"]["table_latency_data"])

    if file_data["plot_type"] == "Throughput":
        if "analysetcp_results_table" not in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_results_stats, conf=plot_box_dict, table_key="analysetcp_results_table")

        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], plot_box_dict["data_tables"]["analysetcp_results_table"], data_name="analysetcp")

        stream_properties["meta_info"]["aes_args"].update({"x": "stream_type", "col": 'factor(stream_type)', "color": "color_column" }) # "color": "factor(color_column)",

    if file_data["plot_type"] == "Goodput":
        if "test_results_itt_table" not in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_results_itt, conf=plot_box_dict, table_key="test_results_itt_table")

        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], plot_box_dict["data_tables"]["test_results_itt_table"], data_name="analysetcp")
        stream_properties["meta_info"]["aes_args"].update({"x": "stream_type", "col": 'factor(stream_type )', "color": "color_column" })

        only_changes = True
        if file_data["hostname"] != "zsender":
            #only_changes = True
            only_changes = False
        tcpprobe_data = tcpprobe.read_tcp_probe_output(file_data, only_changes=only_changes)

        def value_func(x):
            d = int(round(float(x), 0))
            return d
        #print "DATAF:\n", util.get_dataframe_head(tcpprobe_data)
        #print "tcpprobe_data:", dir(tcpprobe_data)
        #print "tcpprobe_data:", tcpprobe_data.names

        cwnd_index = util.get_dataframe_col_index(tcpprobe_data, "cwnd")
        if cwnd_index == -1:
            cprint("Failed to find index for column 'cwnd'", "red")
        # rx2 uses R style extracting which starts at index 1, so must increase column index
        cwnd_index += 1

        #print "Summary:", r["summary"](tcpprobe_data.rx2(cwnd_index))
        d = util.parse_r_column_summary(r["summary"](tcpprobe_data.rx2(cwnd_index)), value_func=value_func)
        #print "D:", d
        #d["CWND"] = "%(Min)s/%(1st Qu)s/%(Median)s/%(Mean)s/%(3rd Qu)s/%(Max)s" % d
        d["CWND"] = "%(Mean)s/%(Min)s/%(1st Qu)s/%(Median)s/%(3rd Qu)s/%(Max)s" % d
        #print "CWND:", d["CWND"]
        file_data["tcpprobe_data"] = d
        #print "tcp probe data:", data
        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["tcpprobe_data"],
                                           plot_box_dict["data_tables"]["test_results_itt_table"],
                                           data_name="tcpprobe", append=True)


def itt_box_key_func(plot_conf, plot_box_dict, file_data, arg):
    file_data["legend_order"] = file_data["packets_in_flight"]
    color_key = "thick"
    #file_data["label"] = ("%s %s" % (file_data["label"], util.make_mods_label(file_data))).strip()
    file_data["color"] = graph_default.color_map[color_key]

    if arg == "ITT_Boxplot":
        plot_box_dict["plot_func"] = ggplot_boxplot_box
        tp_conf = plot_latency_rdb_vs_greedy.get_throughput_conf()
        plot_box_dict["axis_conf"] = deepcopy(tp_conf["plot_conf"]["axis_conf"])
        #plot_box_dict["axis_conf"]["x_axis_label_key"]
        plot_box_dict["axis_conf"]["x_axis_label_key"] = "stream_id"
        plot_box_dict["axis_conf"].update({ "y_axis_lim" : [0, 100],
                                            "y_axis_title" : { "title": "ITT", },
                                            "x_axis_title" : { "title": "Stream type", },
                                            #"y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": [0, 1.1, .1],
                                            #"x_axis_main_ticks": [200, 800, 100], "x_axis_minor_ticks": 50,
                                            #"y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                            #"x_axis_label_key": "label",
                                        })


    #conf["box_conf"]["legend"].update({#"legend_title": ggplot2.element_blank(), #"Streams",
    #    "legend_sort": { "do_sort": True, "sorted_args": { "key": lambda x: x["order"], "reverse": False }
    #                     #{}
    #                 }})

    plot_box_dict["plot_type"] = file_data["plot_type"]

    file_data["bundling_limit"] = "None"

    if plot_conf.get("experiment_id") == "E3":
        file_data["bundling_limit"] = "1"

    if file_data["type_thin1"] == "tcp" and file_data["hostname"] == "wsender":
        file_data["color"] = "coral2"
    if file_data["type_thin1"] == "rdb" and file_data["hostname"] == "rdbsender":
        file_data["color"] = "red"

    # COLOR
    util.dict_set_default(plot_box_dict["legends"], ("color", {"guide_args": {}, "scale": {"func": ggplot2.scale_colour_manual, "args": {}}}))
    colorleg = plot_box_dict["legends"]["color"]
    colorleg["guide_args"].update({"order": 1})
    color_args = colorleg["scale"]["args"]
    util.dict_set_default(color_args, [("labels", []), ("values", [])])
    color_args["name"] = ggplot2.element_blank()

    colorleg["guide_args"].update({"override.aes": {"linetype": [1, 1, 2]}})

    color_args["labels"] = ['RDB', 'TCP', 'TCP Reference']
    color_args["values"] = ['#A52A2A', 'darkgreen', 'blue']
    color_args["breaks"] = ['rdb', 'tcp', 'tcp-ref']



#def custom_latency_merged_results_parse(conf, plot_box_dict, file_data, set_key):
#    print "custom_latency_merged_results_parse"
#    #print "PLOT TYPE:", file_data["plot_type"]
#    #custom_results_parse(conf, plot_box_dict, file_data, set_key)
#    if file_data["plot_type"] == "ITT_Boxplot":
#        #latency.latency_results_parse(conf, plot_box_dict, file_data, set_key)
#        itt_results_parse(conf, plot_box_dict, file_data, set_key)
#        conf["parse_results_func"] = throughput_results_parse
#
#    #util.print_keys(file_data)
#    #stream_id = "%svs%s" % (file_data["type_thin1"], file_data["type_thin2"])
#    stream_id = "%s" % (file_data["type"])
#    #dataf = file_data["ecdf_ggplot2_values"]
#    #file_data["ecdf_ggplot2_values"] = dataf = r.cbind(dataf, id_column=stream_id)
#    #print "DATAF:", type(dataf)
#    #print util.get_dataframe_head(dataf)

def itt_results_parse(conf, box_conf, file_data, set_key):
    print "itt_results_parse:", set_key
    print "results:", file_data["results"]["latency"].keys()
    #r10-rdb-itt30:3-ps120-ccrdb-da0-lt0-er3-dpif10-tfrc0_vs_w10-tcp-itt30:3-ps120-ccrdb_vs_z5..kbit5000_min5_rtt150_loss_pif0_qlen63_delayfixed_num0_zsender-per-packet-itt-size-aggr.dat
    #"r10-rdb-itt30:3-ps120-ccrdb-da0-lt0-er3-dpif10-tfrc0_vs_w10-tcp-itt30:3-ps120-ccrdb_vs_z5..kbit5000_min5_rtt150_loss_pif0_qlen63_delayfixed_num0_rdbsender-packet-byte-count-and-itt-all.dat
    file_data["stream_id"] = "%s" % (file_data["type"])
    try:
        print "FILE: '%s'" % file_data["results"]["latency"]["packets_stats_file"]
        data = DataFrame.from_csvfile(file_data["results"]["latency"]["packets_stats_file"], header=True,
                                      sep=',', quote='"', dec='.', fill=True, comment_char='', as_is=False)
    except:
        cprint("Failed to read file: '%s'" % file_data["results"]["latency"]["packets_stats_file"], "red")
        raise

    try:
        #colSums = robjects.r.get('colSums')
        #colsum = r['colSums'](d)
        #print "data:", data.names
        itts = data.rx2(2)
        stream_id = data.rx(4)
        #print "ITTS:\n", util.get_dataframe_head(itts, count=6)
        #print "ITTS:", type(itts)
        payload_bytes = data.rx2(3)
        stream_ids = data.rx2(4)

        as_factor = robjects.r.get('as.factor')
        #itts = as_factor(itts)

        #data = r.cbind(data, color_column=file_data["color"], id_column=file_data["stream_id"]) # type=file_data["label"],

        #print "DATA:\n", util.get_dataframe_head(data, count=6)
        #print "label:", file_data["label"]

        #data_var = { "ITT": d, "type": file_data["label"], "stream_type": file_data["stream_id"], "color_column": file_data["color"] }
        #dataf = DataFrame({"itt": itts}) # plot["type"]: colsum
        #dataf = DataFrame({"itt": itts, "payload_bytes": payload_bytes, "stream_id": stream_ids}) # plot["type"]: colsum
        dataf = DataFrame({"itt": itts, "stream_id": stream_ids }) # plot["type"]: colsum
        #print "DATAF:\n", util.get_dataframe_head(dataf, count=6)
        #dataf = r.cbind(dataf, color_column=file_data["color"], id_column=file_data["stream_id"], stream_id="skrot") # type=file_data["label"],
        #, "stream_id": stream_id
        #if "legend_order" in file_data:
        #    data_var["order"] = file_data["legend_order"]
        #data = DataFrame(data_var) # plot["type"]: colsum
        #file_data["colsum"] = d
        #require(reshape2)
        #ggplot(data = melt(dd),
        reshape2 = importr("reshape2")
        #data = reshape2.melt(data)
        melted = reshape2.melt(dataf)
        #print "MELT:\n", util.get_dataframe_head(melted, count=6)
        #print "MELT:\n", type(melted)
        #payload_bytes
        #file_data["data"] = data

        data_var = { "itt": melted, "stream_type": file_data["stream_id"], "color_column": file_data["color"] }
        data = DataFrame(data_var) # plot["type"]: colsum

        #file_data["data"] = reshape2.melt(dataf)
        file_data["data"] = data
        #print "MELT:\n", util.get_dataframe_head(data, count=6)
    except Exception as e:
        cprint("Failed on file: '%s' : Error: %s" % (file_data["results"]["latency"]["packets_stats_file"], e), "red")
        import pdb; pdb.set_trace()

def itt_segsize_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    print "itt_segsize_file_parse_func: '%s'" % parse_func_arg

    if parse_func_arg == "ITT_Boxplot":
        #util.set_dict_defaults_recursive(file_conf, (("results", "latency"), {}))
        #file_conf["results"]["latency"].update({"extra_cmd_args": ""})
        print "CAlling latency_file_parse_func"
        if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False

        results_dict = file_conf["results"]["latency"]
        print "results.latency:", results_dict.keys()
        results_dict["packets_stats_file"] = results_dict["output_path"] + "per-packet-itt-size-aggr.dat" # packet-byte-count-and-itt-all
        print "Setting packets_stats_file:", results_dict["packets_stats_file"]

        file_conf["default_meta_info"] = False
        stream_properties = file_conf["streams"][file_conf["streams_id"]]
        #aes_args["x"] = "stream_type"
        #aes_args["y"] = "Throughput"
        #aes_args["col"] = 'factor(stream_type)'
        #aes_args["x"] = plot.get("y_column_name", 'time')
        aes_args = {}
        #aes_args["x"] = "factor(host)"
        #aes_args["x"] = "factor(host)"
        #aes_args["y"] = "factor(rdb_bytes)"

        #stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
        #                                   "factor": "color_column",
        #                                   #"factor": "stream_type",
        #                                   "type_id_column": "color_column", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
        #                                   "breaks_factor": "color_column",  # These are for the legend
        #                                   #"breaks_factor": "stream_type",  # These are for the legend
        #                                   "aes_args": { "color": 'color_column',
        #                                                 "group": 'color_column'
        #                                                 #"group": 'factor(stream_type)'
        #                                             } # These are for the general factoring of the data
        #                               })

        stream_properties["meta_info"].update({"legend_count": 1,# "smooth": False, "line": True,
                                               "factor": "color_column",
                                               #"breaks_factor": "color_column",  # These are for the legend
                                               "breaks_factor": "color_column",  # These are for the legend
                                               "aes_args": {
                                                   "color": 'factor(stream_type)',
                                                   #"group": 'color_column',
                                                   "y": "itt.value",
                                                   "x": "stream_type",
                                                   #"col": 'factor(stream_type)',
                                                   #"size": "1",
                                                   #"color": 'color_column',
                                                   #"linetype": "color_column",
                                                   #"color": 'id_column',
                                                   #"linetype": "id_column",
                                                   #"color": 'factor(color_column)',
                                                   #"linetype": "factor(color_column)",
                                                   #"color": 'factor(id_column)',
                                                   #"linetype": "factor(id_column)",
                                                   #"group": 'color_column',
                                                   #"group": 'factor(color_column)',
                                            } # These are for the general factoring of the data
                                           })

#    if parse_func_arg == "Throughput":
#        if throughput.throughput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
#            return False
#    elif parse_func_arg == "Goodput":
#        if throughput.goodput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
#            return False
#        file_conf["tcp_probe_data_file"] = pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpproberdb.out")
#    elif parse_func_arg == "Latency":
#        if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
#            return False
#    else:
#        cprint("Invalid arguement: %s" % parse_func_arg, "red")
#
#    print "host:", file_conf["hostname"]
#    if file_conf["hostname"] == "zsender":
#        return False
#
#    file_conf["itt_int_thin1"] = file_conf["itt_thin1"].split(":")[0]
#
#    if file_conf["type_thin1"] == "tcp" and file_conf["hostname"] == "rdbsender":
#        return False
#
#    if file_conf["type_thin1"] == "tcp" and file_conf["hostname"] == "wsender":
#        file_conf["color"] = "red"
#        file_conf["label"] = "TCP-reference"
#
#    if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
#        return False



def get_latency_conf_with_tcp_thin_together_with_rdb_test(defaults, conf_apply=None):
    conf = plot_latency_rdb_vs_greedy.get_latency_conf_with_throughput(defaults)
    conf["file_parse"].update({"func": itt_segsize_file_parse_func,
                               "parse_func_arg": ["ITT_Boxplot"]})

    def set_box_key_value(file_conf, arg):
        group = "PIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["tfrc"])
        file_conf["test_name"] = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        file_conf["group"] = group
        file_conf["dpif"] = 10

    def itt_results_parse_hook(conf, box_conf, file_data, set_key):
        print "itt_results_parse_hook"
        stream_id = "%s" % (file_data["type"])
        if file_data["type_thin1"] == "tcp" and file_data["hostname"] == "wsender":
            stream_id = "tcp-ref"
        file_data["ecdf_ggplot2_values"] = r.cbind(file_data["ecdf_ggplot2_values"], id_column=stream_id)

    conf["parse_results_func"] = itt_results_parse
    conf["results_parse_hook"] = itt_results_parse_hook
    conf["process_results"] = None
    #conf["process_results"]["custom_write_tables"] = {"func": custom_latency_merged_process_results}
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO})

    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_ER:%(early_retransmit)s_QLEN:%(queue_len)s_CONG2:%(cong_control_thin2)s_Plot_type:%(plot_type)s", # Plot_type:%(plot_type)s
                              "sort_key_func": lambda x: (x["num"], x["cong_control_thin2"], data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                                          x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"], x["plot_type"][2]),
                              "sort_keys": ["num", "cong_control_thin2", "thin_dupack", "linear_timeout", "early_retransmit", "stream_count_thin1", "stream_count_thick", "group", "itt_thin1", "plot_type", "queue_len"],
                              "pre_key_func": set_box_key_value,
                              "func" : itt_box_key_func,
                          })

    page_sort_keys = conf["box_conf"]["sort_keys"]
    page_sort_keys = list(filter(lambda x: x != "plot_type", page_sort_keys))
    #print "page_sort_keys:", page_sort_keys
    conf["page_group_def"] = {
        "title": "%(stream_count_thin1)d Thin streams vs %(stream_count_thick)d Greedy streams - Duration: %(duration)s min\n"
        "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets, Cong: %(cong_control_thin1)s",
        "sort_keys": page_sort_keys,
        #"sort_key_func": lambda x: x,
        #"sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
        #                            x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"]),
        "sort_key_func": lambda x: (x["stream_count_thin1"], data_tables.get_stdev(x["itt_thin1"], stdev=False), #x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                    x["stream_count_thick"], x["group"], x["queue_len"]),
    }

    conf["box_conf"]["box_titles"] = { #"top_title": {"title_def" : "%(plot_type)s", "order": 0 },
        "main": { "title_def": "RDB: %(stream_count_thin1)s   TCP: %(stream_count_thin1)s   ITT: %(itt_int_thin1)s ms   Seg. size: %(payload_thin1)s B   DPIF: %(dpif)s   Bndl. limit: %(bundling_limit)s", "order": 1 },
    }

    conf["paper_width"] = 4.5
    conf["paper_height"] = 3

    conf["plot_conf"]["theme_args"].update({
        "legend.position": "top",
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0.3, color="grey20")'),
        "axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 0.9, color="grey20")'),
        "plot.margin": r('unit(c(0, 0.5, 0, 0.1), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        "axis.ticks": r('element_line(colour="black")'),
        "panel.grid.minor": r('element_line(colour="grey40", size=0.1, linetype="dashed")'),
        "panel.grid.major": r('element_line(colour="grey40", size=0.2, linetype="dashed")'),
        "panel.background": ggplot2.element_blank(),
        "plot.title": r('element_text(size=8, lineheight=1, hjust=0.5, color="black")'), # , face="old"
        #"axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
        "axis.text.x": r('element_text(size = 8, color="black")'),
        "axis.text.y": r('element_text(size = 7, color="black")'),
        "axis.line": r('element_line(colour="grey20", size=0.4)'), #
        #"axis.line.x": line along x axis (element_line; inherits from axis.line)
        #"axis.line.y": r('element_line(colour="grey20", size=0.5)'), #
        "legend.key": r('element_rect(fill=NA,color="black", size=0.1, linetype="solid")'),
        #"legend.background": r('element_rect(fill=NA,color="darkred", size=0.5, linetype="solid")'),
                                        })
    conf["plot_conf"]["bandwidth_axis_scale"] = 1.1 # Multiplied with bandwidth to get y axis limit

    if conf_apply:
        util.update_dict(conf_apply, conf)

    return conf

#def goodput_throughput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
#    plot_latency_rdb_vs_greedy.custom_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg)


def goodput_throughput_results_parse(conf, box_conf, file_data, set_key):

    vprint("goodput_throughput_results_parse:", file_data["plot_type"], v=3)

    plot_latency_rdb_vs_greedy.custom_results_parse(conf, box_conf, file_data, set_key)

    vprint("TYPE:", file_data["plot_type"], v=3)

    #if file_data["plot_type"] == "Goodput":
    file_data["data"] = r.cbind(file_data["data"], plot_type=file_data["plot_type"])
    vprint("PLOT_DATA:\n", util.get_dataframe_head(file_data["data"], count=5), v=3)

    #   elif file_data["plot_type"] == "Throughput":

    stream_properties = file_data["streams"][file_data["streams_id"]]
    stream_properties["meta_info"].update({"legend_count": 1,# "smooth": False, "line": True,
                                           "factor": "color_column",
                                           #"breaks_factor": "color_column",  # These are for the legend
                                           "breaks_factor": "color_column",  # These are for the legend
                                           "aes_args": {
                                               "color": 'color_column',
                                               "group": 'factor(stream_type)',
                                               #"group": 'color_column',
                                               "y": "Throughput",
                                               "x": "factor(stream_type)",
                                               #"x": "interaction(factor(plot_type), factor(stream_type))",
                                               #"x": "interaction(factor(plot_type), factor(stream_type))",
                                               #"x": "factor(plot_type)",
                                               #"color": 'interaction(factor(plot_type))',
                                               #"group": 'factor(stream_type)',
                                               #"group": 'interaction(factor(plot_type), factor(stream_type))',
                                               #"group": 'stream_type',
                                               #"group": 'interaction(factor(plot_type))',
                                               #"group": 'interaction(factor(plot_type), factor(stream_type))',
                                               #"group": 'interaction(factor(plot_type))',
                                               #"dodge": 'interaction(factor(plot_type))',
                                               #"dodge": 'factor(plot_type, stream_type)',
                                               #"group": 'interaction(plot_type)',
                                           } # These are for the general factoring of the data
                                       })


def get_conf_paper_goodput_throughput(defaults, conf_apply=None):
    conf = plot_latency_rdb_vs_greedy.get_latency_conf_with_throughput(defaults)

    def box_key_func_latency_goodput_throughput(plot_conf, plot_box_dict, file_data, arg):
        plot_latency_rdb_vs_greedy.box_key_func(plot_conf, plot_box_dict, file_data, arg)
        import graph_r_ggplot
        plot_box_dict["axis_conf"].update({"y_axis_title": { "title": "Kbit/s aggregated over %(sample_sec)d second intervals" % plot_conf }})
        plot_box_dict["box_conf"]["legend"]["default_legend_and_scaling"] = False
        file_data["dpif"] = 10
        #util.print_keys(file_data, keys=["type_thin1", "hostname"], prefix="SATAN")

        if file_data["hostname"] == "zsender":
            file_data["stream_id"] = "Greedy"
        elif file_data["hostname"] == "wsender":
            file_data["stream_id"] = "ISOCH"
        elif file_data["hostname"] == "rdbsender":
            file_data["stream_id"] = "RDB"

        util.dict_set_default(plot_box_dict["legends"], ("color", {"guide_args": {}, "scale": {"func": ggplot2.scale_colour_manual, "args": {}}}))
        colorleg = plot_box_dict["legends"]["color"]
        colorleg["guide_args"].update({"order": 1})
        color_args = colorleg["scale"]["args"]
        util.dict_set_default(color_args, [("labels", []), ("values", [])])
        color_args["name"] = ggplot2.element_blank()
        colorleg["guide_args"].update({"override.aes": {"linetype": [1, 1, 2]}})
        color_args["labels"] = ['RDB', 'ISOCH', 'Greedy']
        color_args["values"] = ['darkgreen', 'blue', '#A52A2A']
        color_args["breaks"] = ['RDB', 'ISOCH', 'Greedy']

        # Presentation
        if file_data["hostname"] == "zsender":
            file_data["stream_id"] = "TCP (Greedy)"
        elif file_data["hostname"] == "wsender":
            file_data["stream_id"] = "TCP (ISOCH)"
        elif file_data["hostname"] == "rdbsender":
            file_data["stream_id"] = "RDB (ISOCH)"
        color_args["labels"] = ['RDB (ISOCH)', 'TCP (ISOCH)', 'TCP (Greedy)']
        color_args["values"] = ['darkgreen', 'blue', '#A52A2A']
        color_args["breaks"] = color_args["labels"]


    def set_box_key_value(file_conf, arg):
        group = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        file_conf["test_name"] = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        #print "set_box_key_value:", group
        file_conf["group"] = group
        #plot_box_dict["plot_type"] = file_data["plot_type"]
        #file_conf["hack"] = arg

    conf["file_parse"].update({#"func": goodput_throughput_file_parse_func,
        "parse_func_arg": ["Throughput", "Goodput"]})

    conf["process_results"] = None#["custom_write_tables"] = {"func": custom_latency_process_results}

    conf["parse_results_func"] = goodput_throughput_results_parse

    conf["box_conf"]["plot_and_table"].update({"heights": (.8, .03, .17)})
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO})

    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_ER:%(early_retransmit)s_QLEN:%(queue_len)s_CONG2:%(cong_control_thin2)s", # Plot_type:%(plot_type)s
                              "sort_key_func": lambda x: (x["num"], x["cong_control_thin2"], data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                                          x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"], x["plot_type"][2]),
                              "sort_keys": ["num", "cong_control_thin2", "thin_dupack", "linear_timeout", "early_retransmit", "stream_count_thin1", "stream_count_thick", "group", "itt_thin1", "plot_type", "queue_len"],
                              "pre_key_func": set_box_key_value,
                              "func" : box_key_func_latency_goodput_throughput,
                          })

    conf["box_conf"]["box_titles"] = { #"top_title": {"title_def" : "%(plot_type)s", "order": 0 },
            "main": { "title_def": "RDB (ISOCH): %(stream_count_thin1)s   TCP (ISOCH): %(stream_count_thin1)s   TCP (Greedy): %(stream_count_thick)s", "order": 1 },
            "subtitle": { "title_def": "ITT: %(itt_int_thin1)s ms   Segment size: %(payload_thin1)s B   DPIF: %(dpif)s   Redundancy level: Unlimited", "order": 2 },
    }

    conf["plot_conf"]["theme_args"].update({
        "legend.position": "top",
        #"legend.position": FloatVector([0.87, 0.35]),
        #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
        #"legend.text": r('element_text(size=6)'),
        #"legend.key.size": r('unit(5, "mm")'),
        "plot.title": r('element_text(size=8, lineheight=0.7, hjust=0.5, color="black")'), # , face="old"
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
        "axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 0.9, color="grey20")'),
        #"panel.grid.minor": r('element_line(colour="grey40", size=0.1, linetype="dashed")'),
        "panel.grid.minor": ggplot2.element_blank(),
        "panel.grid.major.y": r('element_line(colour="grey40", size=0.1, linetype="dashed")'),
        "panel.grid.major.x": ggplot2.element_blank(),
        #"panel.grid.major.x"
        #"panel.background": ggplot2.element_blank(),
        "panel.background": r('element_rect(fill=NA,color="grey20", size=0.5, linetype="solid")'),
        "panel.margin": r('unit(0.2, "lines")'), # Margin between plots in facet_grid
        # Facet grid
        "axis.text.x": r('element_text(size = 8, color="black")'),
        "axis.text.y": r('element_text(size = 8, color="black")'),
        #"strip.text.x": r('element_text(size=8, angle=75)'),
        #"strip.text.y": r('element_text(size=12, face="bold")'),
        #"strip.background": r('element_rect(colour="red", fill="#CCCCFF")'),
        "plot.margin": r('unit(c(0, 0.1, 0.1, 0.1), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        "strip.background": ggplot2.element_blank(),
        })

    def do_facet_grid(plot_conf=None, plot_box_dict=None, gp=None, **kw):
        if not "plot" in kw:
            return
        plot = kw["plot"]
        #stream_properties = plot["streams"][plot["streams_id"]]
        yield ggplot2.facet_grid(". ~ plot_type", scales="free_y")
        #gp += ggplot2.facet_grid(". ~ plot_type", scales="free_y")
        #util.dict_set_default(plot_box_dict["legends"], ("colour", {"guide_args": {"title": "Streams"}, "scale": False }))
        return

    def do_remove_legend(plot_conf=None, plot_box_dict=None, gp=None, **kw):
        yield ggplot2.guides(color=False)
        return

    def do_set_order(plot_conf=None, plot_box_dict=None, gp=None, **kw):
        labels = ['TCP (Greedy)', 'TCP (ISOCH)', 'RDB (ISOCH)']
        yield ggplot2.scale_x_discrete(
            #breaks=StrVector(breaks),
            #breaks=StrVector(test_ids),
            # When x_axis_label_key is set, labels and limits will differ
            labels=StrVector(labels),
            limits=StrVector(labels),
            )
        return

    conf["box_conf"]["custom_ggplot2_plot_cmds"].update({"facet_grid": do_facet_grid})
    conf["box_conf"]["custom_ggplot2_plot_cmds"].update({"remove_legend": do_remove_legend})
    conf["box_conf"]["custom_ggplot2_plotbox_cmds"].append(do_set_order)

    page_sort_keys = conf["box_conf"]["sort_keys"]
    page_sort_keys = list(filter(lambda x: x != "plot_type", page_sort_keys))
    #print "page_sort_keys:", page_sort_keys

    conf["page_group_def"] = False

    conf["paper_width"] = 5.3
    conf["paper_height"] = 2.7

    conf["paper_width"] = 7
    conf["paper_height"] = 3.5

    attrs = {}
    attrs["grid_major_c"] = "grey60"
    attrs["grid_major_c"] = "#808080"
    attrs["grid_major_s"] = "0.4"
    # "solid", "dashed", "dotted", "dotdash", "longdash", and "twodash".
    attrs["grid_major_t"] = "solid"
    attrs["axis_line_s"] = "0.4"

    attrs["ticks_c"] = "grey30"
    attrs["axis_line_c"] = attrs["ticks_c"]
    attrs["axis_text_c"] = "grey7"
    attrs["axis_title_c"] = "grey7"
    attrs["plot_title_c"] = "black"

    conf["plot_conf"]["theme_args"].update({
        "plot.margin": r('unit(c(0, 0.1, 0.4, 0.1), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        "axis.line": r('element_line(size=%(axis_line_s)s, colour="%(axis_line_c)s")' % attrs), #
        "axis.ticks": r('element_line(size=0.4, colour="%(ticks_c)s")' % attrs),
        "axis.ticks.length": r('unit(1.5, "mm")'),
        "panel.grid.major.y": r('element_line(colour="%(grid_major_c)s", size=%(grid_major_s)s, linetype="%(grid_major_t)s")' % attrs),
        "plot.title": r('element_text(size=10, lineheight=1, hjust=0.5, color="%(plot_title_c)s")' % attrs), # , face="old"
        #"axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0.3, color="grey20")'),
        #"axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 0.9, color="grey20")'),
        "axis.title.x": r('element_text(size = 11, hjust = 0.5, vjust = -0.4, color="%(axis_title_c)s")' % attrs),
        "axis.title.y": r('element_text(size = 11, hjust = 0.4, vjust = 1, color="%(axis_title_c)s")' % attrs),
        "axis.text.x": r('element_text(size = 10, color="%(axis_text_c)s")' % attrs),
        "axis.text.y": r('element_text(size = 10, color="%(axis_text_c)s")' % attrs),
        "strip.text.x": r('element_text(size=12)'),
    })
    conf["plot_conf"]["plot_args"] = {"size": 0.8 }


    if conf_apply:
        util.update_dict(conf_apply, conf)

    return conf


import latency
latency.get_conf = get_latency_conf

def run(defaults={}):
    plot_types = ["rwz_paper_itt_and_payload_boxplot", "rwz_misuser_dpif_tfrcsp_oldcong_paper"]

    argparser = graph_base.make_parser()
    options = argparser.add_argument_group('Latency RDB CDF options')
    options.add_argument("-pt", "--plot-type",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, default=plot_types[0])

    latency_def_conf = {"data_table_defaults": {"latex_test_setup_table": {"path_prefix": "latency-x-traffic-dpif"} },
                        "box_conf.data_table_defaults": {"analysetcp_results_table": {"recurse": "keys", "data": {"recurse": "Name", "data": {"show": False}}},
                                                         "table_latency_data": {"recurse": "keys", "data": {"recurse": "Name", "data": {"show": False}}}},
                    }

    args = graph_base.parse_args(argparser, defaults=defaults)
    plot_type = args.plot_type

    if plot_type == "rz_greedy_tfrc_with_throughput":
        latency.get_conf = plot_latency_rdb_vs_greedy.get_latency_conf_with_throughput
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC_rdb_vs_greedy_latency_tfrc_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC"
        defaults["file_regex_match"] = [".*tfrc.*"]
        #defaults["file_regex_match"] = ["r21.*itt100-.*cubic.*vs_z10.*min5.*qlen60.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC/all_results/"]
    elif plot_type == "rwz_paper_itt_and_payload_boxplot":
        def_conf = deepcopy(latency_def_conf)
        def_conf["experiment_id"] = "E3"
        latency.get_conf = partial(get_latency_conf_with_tcp_thin_together_with_rdb_test, defaults=def_conf)
        defaults["latency_output_file"] = "PAPER_LATENCY_itt_segment_size_boxplot.pdf"
        defaults["output_dir"] = "PAPER_LATENCY_WITH_BUNDLING_LIMIT"
        defaults["file_regex_nomatch"] = [".*tfrc1.*", ".*dpif20.*"]
        defaults["file_regex_match"] = ["r10-...-itt30:3-ps120-ccrdb-da0-lt0-er3.*"]
        defaults["file_regex_nomatch"] = [".*tfrc1.*", ".*dpif20.*",
                                          ".*wsender.*", ".*zsender.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_RDBPLIM/all_results/"]
    elif plot_type == "rwz_misuser_dpif_tfrcsp_oldcong_paper":
        misuser_def_conf = {"data_table_defaults": {"latex_test_setup_table": {"path_prefix": "misuser-dpif-tfrcsp"} },
                            "box_conf.data_table_defaults": {"analysetcp_results_table": {"recurse": "keys", "data": {"recurse": "Name", "data": {"show": False}}},
                                                             "table_latency_data": {"recurse": "keys", "data": {"recurse": "Name", "data": {"show": False}}}},
                        }
        def_conf = deepcopy(misuser_def_conf)
        def_conf["experiment_id"] = "E4"
        #latency_conf_apply = {"box_conf.box_titles.main": {"title_def": "ITT: %(itt_int_thin1)s ms   Bundle rate: DPIFL %(dpif)s   Redundancy level: %(bundling_limit)s", "order": 1 },
        #                      "plot_conf.theme_args": {"plot.title": r('element_text(size=8, lineheight=1, hjust=0.498, color="black")')}
        #}
        misuser_conf_apply = {
            "box_conf.box_titles.main": {"title_def": "RDB (ISOCH): %(stream_count_thin1)s   TCP (ISOCH): %(stream_count_thin1)s   TCP (Greedy): %(stream_count_thick)s", "order": 1},
            "box_conf.box_titles.subtitle": { "title_def": "ITT: %(itt_int_thin1)s ms   Segment size: %(payload_thin1)s B   DPIFL: %(dpif)s   Redundancy level: ∞", "order": 2 },
        }
        latency.get_conf = partial(get_conf_paper_goodput_throughput, defaults=def_conf, conf_apply=misuser_conf_apply)
        defaults["latency_output_file"] = "expr4-throughput-isoc.pdf"
        defaults["output_dir"] = "PAPER_THROUGHPUT_WITH_BUNDLING_LIMIT"
        defaults["file_regex_nomatch"] = [".*tfrc1.*", ".*dpif0.*", "r5-tcp.*"]
        #defaults["file_regex_match"] = ["r5-tcp-itt5:1.*"]
        defaults["file_regex_match"] = ["r5-.*-itt30:3.*"]
        #defaults["file_regex_match"] = ["r7-.*-itt5:1.*rdbsender.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_MISUSER_TFRC_OLDCONG/all_results/"]
    else:
        cprint("Invalid plot type: '%s'" % plot_type, "red")
        sys.exit()

    args = graph_base.parse_args(argparser, defaults=defaults)
    graph_base.main()

if __name__ == "__main__":
    run()
