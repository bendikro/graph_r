import graph_default
from graph_r import *
import glob
import util

import data_tables
import common

def latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]

    util.set_dict_defaults_recursive(file_conf, (("results", "latency"), { "extra_cmd_args": "" }))
    results_dict = file_conf["results"]["latency"]

    if file_conf["thin_dupack"] is None:
        file_conf["thin_dupack"] = 0

    results_dict["output_path"] = output_path = os.path.join(file_conf["output_dir"], file_conf["prefix"])

    results_dict["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.dat"))
    results_dict["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.stout"))

    files = []
    d = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-%(src_ip)s*" % file_conf))
    results_dict["latency_by_stream_files"] = d
    file_conf["plot_type"] = parse_func_arg

    #print "results_file:", results_dict["results_file"]
    # --analyse-start=300 --analyse-end=60

    results_dict["packets_stats_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "packet-byte-count-and-itt-all.dat"))
    results_dict["per_packet_stats_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "per-packet-stats-all.dat"))

    # Produce latency results
    results_dict["results_cmd"] = ("analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s "
                                   "-o %(output_dir)s %(extra_cmd_args)s -l -P -a -i -vv 1> %(stdout_file)s" %
                                   dict(file_conf, **results_dict))
    file_conf["color"] = "darkred" # Default color for dataset

    analysetcp_stdout_file = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "analysetcp.out"))

    stream_properties = file_conf["streams"][file_conf["streams_id"]]
    if not "color_mapping" in stream_properties:
        stream_properties["color_mapping"] = deepcopy(graph_default.stream_type_to_ggplot_properties[file_conf["streams_id"]]["color_mapping"])

    #analysetcp_conf = {"analysetcp": {}}
    #analysetcp_conf["stdout_file"] = analysetcp_stdout_file
    #analysetcp_conf["results_file"] = analysetcp_stdout_file
    #analysetcp_conf["results_cmd"] =\
    #    "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -o %(output_dir)s 1> %(stdout_file)s" %\
    #    dict(file_conf, **analysetcp_conf)
    #
    #file_conf["results"]["analysetcp"] = analysetcp_conf

    file_conf["group"] = file_conf["type_id"]
    file_conf["group"] = ""
    file_conf["group"] = file_conf["packets_in_flight"]

    if "percentiles" in plot_conf["box_conf"]:
        file_conf["percentiles"] = plot_conf["box_conf"]["percentiles"]

def latency_process_results(conf, groups_list=None):
    print "latency_process_results"
    #write_latex_table_results(conf)
    write_latex_table_test_setup(conf["data_tables"]["latex_test_setup_table"])

def write_latex_table_test_setup(conf):
    # Sort by plot number and then the name
    #print "CONF:", conf.keys()
    keys = conf["keys"]
    data = conf["data"]
    #print "Data:", data
    #print "Keys:", keys
    #data_sorted = sorted(data, key=lambda el: (el["Name"]))
    #print "SORT:", conf["sort"]

    if "sort" in conf and conf["sort"].get("do_sort", False) is True:
        #print "SORTING:", conf["sort"]["sorted_args"]
        data = sorted(data, **conf["sort"]["sorted_args"])

    #data_list = data_sorted
    data_list = []
    #print "data:", data

    # Convert dict to use key from keys as dict key
    for d in data:
        data_list.append({})
        for k in keys:
            data_key = keys[k]["key"]
            if "store_key" in keys[k]:
                data_key = keys[k]["store_key"]
            data_list[-1][data_key] = d[k]
        #print "data_list[-1]:", data_list[-1]
    col_count = len(data_list[0])
    #print "Col count:", col_count
    #print "data_list:", data_list
    print "COLS(%s): %s" % (conf["filename"], [k for k, v in keys.iteritems() if v.get("show", True)])

    visible_colums_count = len([k for k, v in keys.iteritems() if v.get("show", True)])
    column_format = "c" * visible_colums_count
    #print "get.path_prefix:", conf.get("path_prefix", None)
    #header = r"""\begin{tabular}{%s}""" % ("c" * col_count)
    common.write_latex_table(column_format, keys, data_list, **conf)

def write_latex_table_results(conf):
    print "write_latex_table_results!!!!!!!!!!!"
    # Sort by plot number and then the name
    from operator import itemgetter, attrgetter
    data_sorted = sorted(conf["analysetcp_data"], key=lambda el: (el[0]["plot_number"], el[1]["packets_in_flight"]))
    #data_sorted = sorted(conf["analysetcp_data"], key=attrgetter("Test", "Name"))
    data_list = []
    data_keys = []
    col_count = 0
    conf["latex_results_table_keys"] = OrderedDict([("Test", {"key": "Test"}), ("ITT", {"key": "itt"}), ("Name", {"key": "name"}),
                                                    ("Data packets", {"key": "Total data packets sent"}),
                                                    ("Retrans", {"key": "Retransmissions"}),
                                                    ("Payload bytes", {"key": "Total bytes sent (payload)", "func": common.bytes_short }),
                                                    ("Bytes Loss", {"key": "Bytes Loss", "title":  "Bytes Loss \\%"}),
                                                    ("Redundant bytes", {"key": "Redundant bytes", "func": common.bytes_short }),
                                                    ("Retrans bytes", {"key": "Retrans", "func": common.bytes_short }),
                                                    ("DupACKs", {"key": "DupACKs"}),
                                                    ("Packets sent", {"key": "Packets sent"}),
                                                ])
    keys = conf["latex_results_table_keys"]

    for box_conf, data_dict in data_sorted:
        #print "data_dict:", data_dict
        plot_number = box_conf["plot_number"]
        data_dict["Test"] = plot_number
        col_count = len(data_dict)
        #data_list.append(data_dict)
        data_list.append({})
        for k in keys:
            data_key = keys[k]["key"]
            #print "data_key:", data_key
            #print "k:", k
            #print "data_list[-1]:", data_list[-1]
            #print "data_dict:", data_dict
            data_list[-1][data_key] = data_dict[data_key]

    #data_keys = [v["key"] for k, v in conf["latex_results_table_keys"].iteritems()]
    data_keys = conf["latex_results_table_keys"]

    # Convert dict to use key from keys as dict key
    #keys = conf["latex_results_table_keys"]
    #for box_conf, data_dict in data_sorted:
    #for d in data_sorted:
    #    data_list.append({})
    #    for k in keys:
    #        data_key = keys[k]["key"]
    #        print "data_list[-1]:", data_list[-1]
    #        print "d:", d
    #        data_list[-1][data_key] = d[k]

    header = r"""\begin{tabular}{%s}""" % ("c" * col_count)
    write_latex_table(header, data_keys, data_list, "latex_table_results.tex", group_keys=["Test"])


def latency_results_parse(conf, box_conf, file_data, set_key):
    file_key = "results_file"
    file_key = "per_packet_stats_file"
    if util.cmd_args.verbose > 1:
        cprint("Reading latency file: %s" % file_data["results"]["latency"][file_key], "yellow")

    #print "latency_results_parse"
    stream_properties = file_data["streams"][file_data["streams_id"]]

    dataframe = r["read.csv"](file_data["results"]["latency"][file_key], header=True)
    file_data["ecdf_ggplot2_values"] = dataframe
    #dataframe = DataFrame.from_csvfile(file_data["results"]["latency"]["results_file"], header=False)
    #print "latency_results_parse color:", file_data["color"]

    # Add column with color name
    #dataframe = r.cbind(dataframe, color_column=file_data["color"], stream_type=file_data["stream_id"])
    # TODO: FIX inconsitency with stream_id
    dataframe = r.cbind(dataframe, color_column=file_data["color"], stream_type=file_data.get("stream_id", ""))

    dataframe.names[4] = "latency" # Column names are read from the first line (header)

    globalenv['tmpframe'] = dataframe
    dataframe = r("subset(tmpframe, payload_bytes != 0 & latency!=0)")

    latency_index = util.get_dataframe_col_index(dataframe, "latency")
    dataframe[latency_index] = dataframe[latency_index].ro / 1000

    #print "DATAF:", type(file_data["ecdf_ggplot2_values"])
    #print util.get_dataframe_head(file_data["ecdf_ggplot2_values"])
    #print "SUMMARY:", r["summary"](dataframe)

    file_data["ecdf_ggplot2_values"] = dataframe

    #print "latency dataframe:", dataframe.names

    if conf.get("results_parse_hook", None) is not None:
        conf["results_parse_hook"](conf, box_conf, file_data, set_key)

    #print "DATAF:", type(file_data["ecdf_ggplot2_values"])
    #print util.get_dataframe_head(file_data["ecdf_ggplot2_values"])


    #dataframe.names[0] = "time" # Column names are read from the first line (header)

    if file_data["default_meta_info"]:
        stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
                                               "factor": "color_column",
                                               "type_id_column": "color_column", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                               "breaks_factor": "color_column",  # These are for the legend
                                               "aes_args": { "color": 'color_column',
                                                             #"group": 'color_column'
                                                             "group": 'factor(color_column)'
                                                         } # These are for the general factoring of the data
                                       })

    if (box_conf["box_conf"].get("latency_options", None) and
            box_conf["box_conf"]["latency_options"].get("per_stream", None) is True):
        # PER STREAM FACTOR (show values for each stream separately)
        stream_properties["meta_info"].update({"legend_count": 1,
                                               "factor": "stream_id",
                                               "type_id_column": "stream_id",
                                               #"breaks_factor": "stream_id",
                                               "aes_args": { "color": 'color_column',
                                                             "group": 'stream_id'
                                                         }})

    file_data["x_column_name"] = "time"
    file_data["y_column_name"] = "latency"

    #print "stream_properties:", stream_properties

    common.do_stream_legend_and_color(box_conf, file_data, file_data["ecdf_ggplot2_values"])

    data, data_dict = data_tables.parse_analysetcp_output(file_data, file_data["results"]["latency"]["stdout_file"])
    file_data["analysetcp_data"] = data_dict
    #print "analysetcp_data:", data_dict

    #print "packets_in_flight:", file_data["packets_in_flight"]
    #print "file_data:", file_data.keys()
    #print "file_data:", file_data

    data_dict["name"] = file_data.get("label", "Missing label")
    data_dict["packets_in_flight"] = file_data["packets_in_flight"]
    #data_dict["itt"] = int(get_stdev(file_data["itt"], stdev=False))
    data_dict["itt"] = file_data["itt"]
    #data_dict["name"] = stream_type_to_ggplot_properties[file_data["streams_id"]]["color_mapping"][0]["legend_name"]

    analysetcp_data = conf.get("analysetcp_data", [])
    #analysetcp_data["Test"] = box_conf["plot_number"]
    analysetcp_data.append((box_conf, data_dict))
    conf["analysetcp_data"] = analysetcp_data


def latency_box_key_func(plot_conf, plot_box_dict, file_data, arg):
    file_data["legend_order"] = file_data["packets_in_flight"]

    if file_data["hostname"] == "rdbsender":
        if file_data["type_thin1"] == "tcp":
            file_data["color"] = "darkblue"
        elif file_data["type_thin1"] == "rdb":
            pif = "rdb_pif%d" % file_data["packets_in_flight"]
            if pif in graph_default.color_map:
                file_data["color"] = graph_default.color_map[pif]
            else:
                file_data["color"] = graph_default.color_map["default"]
    elif file_data["hostname"] == "zsender":
        if file_data["type_thin1"] == "rdb":
            file_data["color"] = "firebrick1"
    else:
        file_data["color"] = graph_default.color_map["thin"]

    if file_data["type_id"] == "type_thin2":
        file_data["color"] = "firebrick1"
    else:
        if file_data["group"] == 0:
            file_data["color"] = graph_default.color_map["thin"]
        if file_data["group"] == 4:
            file_data["color"] = graph_default.color_map["rdb_pif4"]
        if file_data["group"] == 6:
            file_data["color"] = graph_default.color_map["rdb_pif6"]
        if file_data["group"] == 8:
            file_data["color"] = graph_default.color_map["rdb_pif8"]
        if file_data["group"] == 12:
            file_data["color"] = graph_default.color_map["rdb_pif10"]
        if file_data["group"] == 100:
            file_data["color"] = graph_default.color_map["rdb_pif20"]

    #print "COLOR:", file_data["color"]

def thin_itt_mark_line_func(plot_conf, file_conf):
    line_start = 200
    for i in range(0, 7):
        yield "abline(v=%d, lty=3, lwd=1)" % (line_start + 100*i)

def thin_itt_mark_line_func_ggplot2(plot_conf=None, plot_box_dict=None, gp=None, **kw):
    import rpy2.robjects.lib.ggplot2 as ggplot2

    if not "plot" in kw:
        return

    plot = kw["plot"]

    if plot["group"] != 0:
        return

    #print "plot_conf:", plot_conf.keys()
    #print "plot:", plot.keys()

    stream_properties = plot["streams"][plot["streams_id"]]
    #print "stream_properties:", stream_properties

    if "itt" not in stream_properties:
        return

    rtt = plot["rtt"]
    #print "stream_properties:", stream_properties

    line_start = rtt
    #itt = 100
    itt = stream_properties["itt"]
    if not type(itt) is int:
        itt = int(itt.split(":")[0])

    line_start += itt

    count = ((int(rtt) / itt) * 2) + 2

    if "count" in kw:
        count = kw["count"]

    xintercept = IntVector([line_start + itt * i for i in range(0, count)])

    # This creates the vertical ITT lines
    gp1 = ggplot2.geom_vline(xintercept=xintercept, linetype="dashed", size=0.2, show_guide=False)
    yield gp1

    # This creates the separate legend
    r("vlines <- data.frame(x=c(-100), y=c(-Inf, Inf), vlines=factor('dashed') )")
    aes = ggplot2.aes_string(x="x", y="y", linetype="vlines", col="NULL")
    gp2 = ggplot2.geom_line(r["vlines"], mapping=aes, size=0.5)

    yield gp2

    #print "legends:", plot_box_dict["legends"]

    # Handle linetype legend
    util.dict_set_default(plot_box_dict["legends"], ("linetype", {"guide_args": {}, "scale": {"func": ggplot2.scale_linetype_manual, "args": {}}}))
    util.dict_set_default(plot_box_dict["legends"], ("fill", {"guide_args": {} }))
    linetype = plot_box_dict["legends"]["linetype"]
    linetype["guide_args"].update({"order": 1})
    util.dict_set_default(linetype["guide_args"], ("override.aes", {"colour": []}))
    #util.dict_set_default(linetype["guide_args"]["override.aes"], ("colour", []))
    linetype["guide_args"]["override.aes"]["colour"].append('black')

    scale_args = linetype["scale"]["args"]
    util.dict_set_default(scale_args, [("labels", []), ("values", [])])
    scale_args["name"] = ggplot2.element_blank()
    scale_args["labels"].extend(["ITT intervals"])
    scale_args["values"].extend(["dotted"])

    # Handle colour legend
    util.dict_set_default(plot_box_dict["legends"], ("colour", {"guide_args": {}, "scale": False }))

    fill = plot_box_dict["legends"]["fill"]

    colour = plot_box_dict["legends"]["colour"]
    colour["guide_args"].update({"order": 2 }) #"override.aes": r('list(linetype=c(2, 1), fill=NA)')

    return

def get_color_latency(plot):
    ret_color = None
    stream_id = plot["type_id"]
    #print "streams_id:", plot["streams_id"]
    #print "streams:", plot["streams"][plot["streams_id"]]

    pif_type = plot["type_id_pif"]

    if stream_id == "type_thick":
        if plot["num"] == 0:
            ret_color = graph_default.color_map["thick"]
        else:
            ret_color = graph_default.color_map["thick2"]

    print "Get_color_latency:", plot["hostname"]
    if ret_color == None:
        if plot["num"] == 0:
            ret_color = graph_default.host_type_color_map[plot["hostname"]][pif_type]
        elif plot["num"] == 1:
            ret_color = "cyan"
        else:
            ret_color = "darkgreen"

    #print "ret_color (stream_type %s:, type: %s, group: %s): %s" % (file_data["stream_type"], file_data["type"], file_data["group"], ret_color)
    return ret_color

def get_latency_conf(defaults=None):
    conf = deepcopy(graph_default.get_conf())
    conf["output_file"] = util.cmd_args.latency_output_file
    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(linear_timeout)s",
                              "sort_key_func": lambda x: (x["thin_dupack"], x["linear_timeout"], x["stream_count_thin1"], x["stream_count_thick"], x["group"]),
                              "sort_keys": ["thin_dupack", "linear_timeout", "stream_count_thin1", "stream_count_thick", "group"],
                              "func" : latency_box_key_func,
                              #"percentiles": [.5, .75, .9],
                              "show_box_title": True,
                              #"box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d %(cong_control_thin1)s " }
                              "box_title_def" : "Thin Dupack: %(thin_dupack)s   Thin Linear Timeout: %(linear_timeout)s" })
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["page_group_def"] = {#"title": "Payload: %(payload_thin1)s  RTT: %(rtt)sms  ITT: %(itt_thin1)sms  Queue Len: %(queue_len)s  Duration: %(duration)s min",
        "title": "%(stream_count_thin1)d Thin streams vs %(stream_count_thick)d Greedy streams - Duration: %(duration)s min\n Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets",
                              "sort_keys": ["payload_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1])}
    conf["plot_conf"].update({"n_columns": 2, "n_rows" : 2,
                              "plot_func": plot_ecdf_box,
                              #"plot_func": plot_ecdf_box_ggplot,
                              #"legend_colors": color_map
                              "legend": { "color_func": get_color_latency, "values": {"type_thick": "Greedy", "type_thin1": "Thin", "type_thin2": "Thin2", "type_thin3": "Thin3"}},
                              "box_commands": thin_itt_mark_line_func,
                              "r.plot_args": {"cex.main": 0.9 },             # Only for regular R plots
                              "r.mtext_page_title_args": {"cex.main": 0.9 }, # Only for regular R plots
                              "theme_args": {}, # Only for ggplot
                              }
                             )

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [130, 1200], "y_axis_lim" : [0.5, 1],
                                            "x_axis_main_ticks": [200, 1200, 200],
                                            "x_axis_minor_ticks": 50,
                                            #"y_axis_main_ticks": [0, 1, .2],
                                            #"y_axis_minor_ticks": .1,
                                            "y_axis_title" : { "title": "ECDF" },
                                            "x_axis_title" : {
                                                #"title": ["Latency in milliseconds", "THICK", "THIN", "RDB:PIF:3", "RDB:PIF:6", "RDB:PIF:10", "RDB:PIF:20"],
                                                "title": "ACK Latency in milliseconds",
                                                #"adj": [0, .3, .4, .5, .65, .75, .9],
                                                #"colors" : [color_map["default"], color_map["thick"], color_map["thin"], color_map["rdb_pif3"],
                                                #            color_map["rdb_pif6"], color_map["rdb_pif10"], color_map["rdb_pif20"]]
                                            },
                                        })

    #conf["plot_conf"].update({"x_axis_title": {"title": ["RDB packets in flight limit", "Greedy", "misuser", "misuser with RDB"], "adj": [0, .45, .6, .9],
    #                                           "colors" : ["black", "darkred", "darkgoldenrod1", "darkblue"] },
    #                          })

    #conf["pdf_args"].update({"title": os.path.basename(conf["output_file"])})
    conf["parse_results_func"] = latency_results_parse

    conf["file_parse"].update({ "func": latency_file_parse_func })
    conf["document_info"]["title"] = "Latency Plots"
    conf["document_info"]["show"] = False
    conf["paper_width"] = 10
    conf["paper_height"] = 8

    conf["file_parse"]["files_nomatch_regex"].append(".*qlen46875.*")
    #conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 1})

    if defaults:
        util.update_dict(defaults, conf)

    return conf

get_conf = get_latency_conf
