import graph_default
from graph_r import *
import util

from rpy2.robjects.lib import grid

from common import do_stream_custom_table_data
from common import do_stream_legend_and_color

import data_tables

def throughput_box_key_func(plot_conf, box_conf, conf):
    # Save the pif in a list to later get the correct column in the plot
    order = box_conf.get("order", [])
    order.append(int(conf["packets_in_flight"]))
    box_conf["order"] = list(set(order))

def goodput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
    y_axis_max = plot_conf["plot_conf"]["bandwidth"] * plot_conf["plot_conf"]["bandwidth_axis_scale"]
    plot_conf["plot_conf"]["y_axis_lim"] = [0, y_axis_max * 1000] # Multiply by 1000 to get bit/s
    plot_conf["plot_conf"]["y_axis_range"] = xrange(0, int(y_axis_max), 100)
    plot_conf["y_axis_data"] = [i * 1000 for i in plot_conf["plot_conf"]["y_axis_range"]]
    plot_conf["y_axis_labels"] = [i for i in plot_conf["plot_conf"]["y_axis_range"]]

    file_conf["label"] = "TCP" if file_conf["packets_in_flight"] == 0 else "%s PIF:%s" % (file_conf["type_thin1"].upper(), file_conf["packets_in_flight"])
    if file_conf["packets_in_flight"] == 0 and file_conf["type_thin1"] == "rdb":
        # Dynamic PIF
        #print "file_data:", file_data
        file_conf["label"] = "RDB DPIF%s" % file_conf["dpif"]
        #file_data["color"] = graph_default.color_map[file_data["label"]]
        #color_key = file_data["label"].lower()

    file_conf["color"] = "darkred" # Default color for dataset
    pif = file_conf["packets_in_flight"]

    file_conf["plot_type"] = parse_func_arg
    file_conf["stream_id"] = "%s %s" % (file_conf["stream_type"].title(), file_conf["type"].upper())
    #print "stream_type:", file_conf["stream_type"].title()
    #print "stream_id:", file_conf["stream_id"]

    basename = os.path.basename(pcap_file)
    results_dict = {}
    results_dict["data_file_name"] = "%s" % (basename.split(plot_conf["file_parse"]["pcap_suffix"])[0])
    results_dict["csv_file"] = os.path.join(plot_conf["output_dir"], "%s.goodput.csv" % results_dict["data_file_name"])
    results_dict["err_file"] = os.path.join(plot_conf["output_dir"], "%s.goodput.err" % results_dict["data_file_name"])
    results_dict["results_file"] = results_dict["csv_file"]
    results_dict["sample_sec"] = str(int(plot_conf["sample_sec"] * 1000))
    results_dict["results_cmd"] = "./tcp-throughput -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -t %(sample_sec)s 1> %(csv_file)s 2> %(err_file)s" %\
        dict(file_conf, **results_dict)

    file_conf["prefix"] = "%s-" % results_dict["data_file_name"]

    file_conf["results"] = {"throughput": results_dict}

    analystcp_results_dict = {}
    file_conf["results"]["loss"] = analystcp_results_dict
    analystcp_results_dict["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "loss-aggr.stout"))
    analystcp_results_dict["results_file"] = analystcp_results_dict["stdout_file"]
    analystcp_results_dict["results_cmd"] =\
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s -o %(output_dir)s -A -vv 1> %(stdout_file)s" %\
        dict(file_conf, **analystcp_results_dict)


def throughput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if goodput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    #plot_conf["plot_conf"]["y_axis_lim"] = [0, 10000 * 1000]
    results_dict = file_conf["results"]["throughput"]

    results_dict["csv_file"] = os.path.join(plot_conf["output_dir"], "%s.throughput.csv" % results_dict["data_file_name"])
    results_dict["err_file"] = os.path.join(plot_conf["output_dir"], "%s.throughput.err" % results_dict["data_file_name"])
    results_dict["results_file"] = results_dict["csv_file"]
    results_dict["results_cmd"] = "./tput -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -t %(sample_sec)s %(pcap_file_receiver)s  1> %(csv_file)s 2> %(err_file)s" %\
        dict(file_conf, **results_dict)
    #print "y_axis_range:", plot_conf["plot_conf"]["y_axis_range"]
    #conf["plot_conf"]["y_axis_lim"]

    if "set_pif_order" in plot_conf["set_conf"]:
        def get_col(plot):
            #print "ORDER:", plot["box_conf"]["order"]
            for i, pif in enumerate(sorted(plot["box_conf"]["order"])):
                if file_conf["packets_in_flight"] == pif:
                    return i +1
            return 0
        file_conf["column"] = get_col #plot_conf["set_conf"]["set_pif_order"][file_conf["packets_in_flight"]]

    file_conf["color"] = graph_default.host_to_color_map[file_conf["hostname"]]
    if file_conf["hostname"] == "rdbsender":
        if file_conf["type"] == "tcp":
            file_conf["color"] = "blue"
        elif file_conf["type"] == "rdb":
            file_conf["color"] = "darkblue"


def throughput_results_parse(conf, box_conf, file_data, set_key, keyname="throughput"):
    print "Reading throughput data:", file_data["results"][keyname]["csv_file"]
    try:
        d = DataFrame.from_csvfile(file_data["results"][keyname]["csv_file"], header=True,
                                   sep=',', quote='"', dec='.', fill=True, comment_char='', as_is=False)
    except:
        cprint("Failed to read file: '%s'" % file_data["results"][keyname]["csv_file"], "red")
        raise

    try:
        #print "THROUGHPUT:\n", util.get_dataframe_head(d, count=6)
        colsum = r['colSums'](d)
        #print "COLSUM:\n", util.get_dataframe_head(d, count=6)
    except:
        cprint("colSum failed on file: '%s'" % file_data["results"][keyname]["csv_file"], "red")
        import pdb; pdb.set_trace()

    #print "sample_sec:", conf["sample_sec"]

    # Divide each value in the vector by the sample sec
    colsum = colsum.ro / conf["sample_sec"]
    # Multiply each value in the vector by 8 to get bits
    # Divide by 1000 to get kilobits
    colsum = colsum.ro * 8
    colsum = colsum.ro / 1000

    #dataframe = r.cbind(dataframe, color_column=file_data["color"])

    #print "stream_id:", file_data["stream_id"]
    data_var = { "Throughput": colsum, "type": file_data["label"], "stream_type": file_data["stream_id"], "color_column": file_data["color"] }
    if "legend_order" in file_data:
        data_var["order"] = file_data["legend_order"]

    #print "Set order:", data_var["order"]

    data = DataFrame(data_var) # plot["type"]: colsum

    plot_var = { "data": data, "colsum" : colsum, "plot_group" : set_key, "csv_file" : file_data["results"][keyname]["csv_file"] }
    file_data.update(plot_var)

    data, data_dict = data_tables.parse_analysetcp_output(file_data, file_data["results"]["loss"]["stdout_file"])
    file_data["analysetcp_data"] = data_dict

    throughput_results_make_table(conf, box_conf, file_data, set_key)
    stream_properties = file_data["streams"][file_data["streams_id"]]
    #print "stream_properties:", stream_properties
    stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
                                           "factor": "color_column",
                                           #"factor": "stream_type",
                                           "type_id_column": "color_column", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                           "breaks_factor": "color_column",  # These are for the legend
                                           #"breaks_factor": "stream_type",  # These are for the legend
                                           "aes_args": { "color": 'color_column',
                                                         "group": 'color_column'
                                                         #"group": 'factor(stream_type)'
                                                     } # These are for the general factoring of the data
                                       })

    do_stream_legend_and_color(box_conf, file_data, file_data["data"])

def throughput_results_make_table(conf, plot_box_dict, file_data, set_key):
    #print "Reading latency file:", file_data["results"]["latency"]["results_file"]
    from operator import itemgetter, attrgetter

    if not "table_test_data" in conf["data_tables"]:
        setup_test_status_table(conf, plot_box_dict)

    do_stream_custom_table_data(file_data, plot_box_dict, file_data["colsum"], conf["data_tables"]["table_test_data"])
    #do_stream_custom_table_data(file_data, plot_box_dict, file_data["colsum"], plot_box_dict["table_data"]["values"])


def setup_test_status_table(conf, plot_box_dict):
    table_test_data = {}
    conf["data_tables"]["table_test_data"] = table_test_data
    table_test_data.update({ "keys": OrderedDict([("Test",         {"key": "label",  "type": "str", "location": "plot_dict"}),
                                                  #("Duration",    {"key": "type",  "type": "str", "func": lambda x: x.upper()}),
                                                  #("Type",        {"key": "type",  "type": "str", "func": lambda x: x.upper()}),
                                                  ("Stream",       {"key": "stream_id",  "type": "str", "func": lambda x: x, "location": "plot_dict"}),
                                                  ("Num",          {"key": "stream_count",  "type": "int"}),
                                                  ("Cong",         {"key": "cong_control", "type": "str", "func": lambda x: x.title()}),
                                                  ("PIF Lim",      {"key": "packets_in_flight",  "type": "int", "location": "plot_dict", "func": lambda x: x if x > 0 else rpy2.rinterface.NA_Integer}),
                                                  #("Data points", {"key": "data_points", "type": "int", "location": "table_data"}),
                                                  ("RTT",          {"key": "rtt", "type": "str", "location": "plot_dict"}),
                                                  ("ITT",          {"key": "itt", "type": "str"}),
                                                  ("Payload",      {"key": "payload", "type": "int"}),
                                                  #("Duration",    {"key": "duration", "type": "int"}),
                                                  #("Limit",       {"key": "duration", "type": "int"}),
                                                  #plot_conf["plot_conf"]["bandwidth"]
                                                  #("mFR",         {"key": "thin_dupack", "type": "str", "func": on_off_na }),
                                                  #("LT",          {"key": "linear_timeout", "type": "str", "func": on_off_na }),
                                              ]),
                             "sort": { "key": "Test", "do_sort": True, "sorted_args": { "key": lambda x: (x['Test'], x['Stream']), "reverse": True} },
                             "default_font_size": 7,
                             "data": [],
                             "args": { "padding.v": r('unit(3.5, "mm")'), "padding.h": r('unit(3, "mm")'), "show.box": True,
                                       "h.odd.alpha": 0.3, "h.even.alpha": 0.8, "gpar.corefill": grid.gpar(fill="lightblue", col="white")},
                         })

def get_throughput_conf(defaults=None):
    conf = deepcopy(graph_default.get_conf())
    conf["output_file"] = util.cmd_args.throughput_output_file
    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Duration:%(duration)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_%(thin_dupack)s_%(plot_type)s",
                              "sort_key_func": lambda x: (x["stream_count_thin1"]),
                              "sort_keys": ["stream_count_thin1"],
                              "func" : throughput_box_key_func,
                              "box_title_def": "%(stream_count_thin1)d vs %(stream_count_thick)d DA:%(thin_dupack)s" })
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIF:%(packets_in_flight)s_Type:%(type_thin1)s",
                         "set_pif_order": {0: 1, 4: 2, 20: 3} }

    conf["page_group_def"] = {
        "title": "Duration: %(duration)smin, Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s, Cap: %(cap)skbit",
        #"common_key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_RTT:%(rtt)s_%(cong_control_thin1)s_%(duration)s_ER:%(early_retransmit)s",
        "sort_keys": ["duration", "payload_thin1", "itt_thin1", "rtt"],
        "sort_key_func": lambda x: (x["duration"], x["payload_thin1"], x["itt_thin1"], x["rtt"])
    }
    #conf["page_group_def"] = { "title": "Duration: %(duration)smin, Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s, Cap: %(cap)skbit",
    #                           "sort_keys": ["duration", "payload_thin1", "itt_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2], x[3])}
    conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 2,
                               "plot_func": plot_boxplot,
                               #"legend": { #"color_func": get_color_latency,
                               #    "values": {"type_thick": "Greedy", "type_thin1": "Thin", "type_thin2": "Thin2", "type_thin3": "Thin3"}},
                           })

    #conf["plot_conf"].update({     "legend": { "color_func": get_color_latency, "values": {"type_thick": "Greedy", "type_thin1": "Thin", "type_thin2": "Thin2", "type_thin3": "Thin3"}},
    #                          "r.plot_args": {"cex.main": 0.9 },             # Only for regular R plots
    #                          "r.mtext_page_title_args": {"cex.main": 0.9 }, # Only for regular R plots
    #                          "theme_args": {}, # Only for ggplot
    #                          }
    #                         )


    conf["parse_results_func"] = throughput_results_parse
    conf["file_parse"].update({ "func": throughput_file_parse_func })
    conf["document_info"]["title"] = "Throughput plots"

    conf["plot_conf"]["axis_conf"].update({"y_axis_title": { "title": "Throughput (Kbit/second aggregated over %(sample_sec)d seconds)" % conf },
                                           "x_axis_title" : { "title": "TCP variation used for competing streams",
                                                              #"title": ["TCP variation used for competing streams", "THICK", "THIN", "RDB"], "adj": [0.2, 0.70, .8, .9],
                                                              "colors" : ["black", "darkred", "darkgoldenrod1", "darkblue"] },
                                           #"x_axis_lim" : [0, 5000],
                                           "y_axis_lim" : [0, 5000],})

    conf["box_commands"] = { "xline": {"command": "abline(h=%d, lty=2)", "func": lambda plot_conf, file_conf: xline(plot_conf["bandwidth"],
                                                                                                                           file_conf["payload_thin1"],
                                                                                                                           file_conf["itt_thin1"],
                                                                                                                           file_conf["stream_count_thin1"],
                                                                                                                           file_conf["stream_count_thick"])}}
    conf["sample_sec"] = 5
    conf["plot_conf"]["bandwidth_axis_scale"] = 1.1 # Multiplied with bandwidth to get y axis limit
    #conf["plot_conf"]["bandwidth_axis_scale"] = 0.7
    #conf["file_parse"]["files_match_exp"] = "8_thin_*_vs_8*%s" % conf["file_parse"]["pcap_suffix"]
    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1})

    if defaults:
        util.update_dict(defaults, conf)

    return conf

def get_goodput_conf():
    goodput_conf = deepcopy(get_conf())
    setup_default_goodput_conf(goodput_conf)
    return goodput_conf

def setup_default_goodput_conf(conf):
    conf["output_file"] = util.cmd_args.goodput_output_file
    conf["document_info"]["title"] = "Goodput plots"
    conf["file_parse"]["func"] = goodput_file_parse_func
    conf["plot_conf"]["y_axis_title"] = "Goodput (Kbit/second aggregated over %(sample_sec)d seconds)" % conf

get_conf = get_throughput_conf
