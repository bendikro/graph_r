#!/usr/bin/env python
from datetime import datetime
import argparse
import os, sys
from graph import do_all_plots
import graph
from graph_default import *

import util
from util import cprint

util.install_sigint_handler()


def main():
    args = util.cmd_args

    if util.cmd_args.verbose > 1:
        print "args:", args

    if args.throughput_output_file:
        from throughput import get_conf
        do_all_plots(get_conf())

    if args.goodput_output_file:
        from throughput import get_goodput_conf
        do_all_plots(get_goodput_conf())

    if args.loss_output_file:
        from loss import get_conf
        do_all_plots(get_conf())

    if args.latency_output_file:
        from latency import get_conf
        do_all_plots(get_conf())

    if args.verify_trace_output_file:
        from verify_conn import get_conf
        do_all_plots(get_conf())

    if args.tcp_probe_output_file:
        from tcpprobe import get_conf
        do_all_plots(get_conf())

    if args.cwnd_tests_output_file:
        from plot_test_cwnd import get_conf
        print "plot_test_cwnd import get_conf:", get_conf
        do_all_plots(get_conf())

    if args.duration_output_file:
        from duration import get_conf
        do_all_plots(get_conf())

    if args.queueing_delay_output_file:
        from queueing_delay import get_conf
        do_all_plots(get_conf())

    if args.burstiness_output_file:
        from burstiness import get_conf
        do_all_plots(get_conf())

    if args.connection_info_output_file:
        from conn_info import get_conf
        do_all_plots(get_conf())

    if args.data_transfer_output_file:
        from data_trans import get_conf
        do_all_plots(get_conf())

    if args.tfrc_output_file:
        from tfrc import get_conf
        do_all_plots(get_conf())

    if args.queue_output_file:
        from queue import get_conf
        do_all_plots(get_conf())


def end():
    end_time = datetime.now()
    print "Finished creating plots in %s (at %s)" % (str((end_time - start_time)), str(end_time.strftime("%H:%M:%S")))

def make_parser(argparser=None):
    if argparser is None:
        argparser = argparse.ArgumentParser(description="Create graphs", usage='%(prog)s [options] command directories')

    options = argparser.add_argument_group('Options')
    options.add_argument("-od", "--output-dir",  help="The output directory to save the the csv results.", required=False, default="data_dir")
    options.add_argument("-f", "--force",  help="Force recreating the throughput CSV data files.", action='store_true', required=False, default=False)
    options.add_argument("-p", "--print-conf",  help="Pretty print the page/box/set/file config structure.", action='store_true', required=False, default=False)
    options.add_argument("-fp", "--file-parse", help="Exit after parsing input files.", action='store_true', required=False, default=False)
    options.add_argument("-v", "--verbose",  help="Enable verbose output. Can be applied multiple times (Max 3)", action='count', default=0, required=False)
    options.add_argument("-dt", "--debugt",  help="Enable debug output of the specified type. (axis)", required=False, action='append', default=None)
    options.add_argument("-df", "--debugf",  help="Enable debug output of the specified function.", required=False, action='append', default=None)
    options.add_argument("-frm", "--file-regex-match",  help="Use regex to match filenames", required=False, action='append', default=None)
    options.add_argument("-frn", "--file-regex-nomatch",  help="Use regex to match filenames", required=False, action='append', default=None)
    options.add_argument("-vi", "--view-results",  help="Open the results file.", action='store_true', required=False, default=False)
    options.add_argument("-wt", "--write-tables",  help="Write only data tables and skip producing plots.", action='store_true', required=False, default=False)
    options.add_argument("--skip-data",  help="Skip generating data.", action='store_true', required=False, default=False)
    options.add_argument("--input-dir",  help="Directory for input files. This overrides the default directories.", required=False, default=None)
    options.add_argument("directories", help="The directories containing the pcap files.", nargs='*')

    commands = argparser.add_argument_group('Commands')
    commands.add_argument("-vc", "--verify-trace-output-file",  help="Verify.", required=False, default=False)
    commands.add_argument("-t", "--throughput-output-file",  help="Create throughput plots and save to specified file.",  required=False, default=False)
    commands.add_argument("-g", "--goodput-output-file",  help="Create goodput plots and save to specified file.",  required=False, default=False)
    commands.add_argument("-loss", "--loss-output-file",  help="Create loss plots and save to specified file.",  required=False, default=False)
    commands.add_argument("-l", "--latency-output-file",  help="Create latency plots and save to specified file.", required=False, default=False)
    commands.add_argument("-du", "--duration-output-file",  help="Create latency plots and save to specified file.", required=False, default=False)
    commands.add_argument("-da", "--data-transfer-output-file",  help="Create data transfer plots and save to specified file.", required=False, default=False)
    commands.add_argument("-tp", "--tcp-probe-output-file",  help="Create TCP probe plots and save to specified file.", required=False, default=False)
    commands.add_argument("-cwt", "--cwnd-tests-output-file",  help="Create CWND tests plots and save to specified file.", required=False, default=False)
    commands.add_argument("-lt", "--latency-table-output-file",  help="Create latency plots with data table and save to specified file.", required=False, default=False)
    commands.add_argument("-ci", "--connection-info-output-file",  help="Write connection info to the to specified file.", required=False, default=False)
    commands.add_argument("-qd", "--queueing-delay-output-file",  help="Write queueing delay to specified file.", required=False, default=False)
    commands.add_argument("-bn", "--burstiness-output-file",  help="Write burstiness values to specified file.", required=False, default=False)
    commands.add_argument("-tfrc", "--tfrc-output-file",  help="Write tfrc values to specified file.", required=False, default=False)
    commands.add_argument("-qu", "--queue-output-file",  help="Write router queue values to specified file.", required=False, default=False)
    return argparser

def parse_args(argparser, defaults=None):
    global start_time
    start_time = datetime.now()

    if defaults:
        #print "Setting default argument values:", defaults
        argparser.set_defaults(**defaults)

    # Other files import cmd_args from util
    args = util.cmd_args = argparser.parse_args()

    #input-dir
    #print "args.directories:", args.directories

    if args.directories is None and args.input_dir is None:
        cprint("Directory must be specified!", "red")
        argparser.print_help()
        sys.exit()

    if not args.input_dir is None:
        args.directories = [args.input_dir]

    if args.output_dir:
        if not os.path.isdir(args.output_dir):
            os.mkdir(args.output_dir)
    else:
        args.output_dir = os.getcwd()
    return args

def run():
    argparser = make_parser()
    parse_args(argparser=argparser)
    main()

if __name__ == "__main__":
    run()
