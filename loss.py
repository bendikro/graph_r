import graph_default
from graph_r import *

from conn_info import connection_info_file_parse_func_time, conn_info_results_parse

host_to_color_map = { 'rdbsender': "darkblue",
                      'zsender': "red",
                      'ysender': "darkgoldenrod1"}

def loss_box_key_func(box_conf, conf):
    conf["color"] = host_to_color_map[conf["hostname"]]

    if conf["hostname"] == "rdbsender":
        if conf["type"] == "tcp":
            conf["color"] = "blue"
        elif conf["type"] == "rdb":
            conf["color"] = "darkblue"

    # Save the pif in a list to later get the correct column in the plot
    order = box_conf.get("order", [])
    order.append(int(conf["packets_in_flight"]))
    box_conf["order"] = list(set(order))

def loss_results_parse(loss_conf, box_conf, file_data, set_key):

    data = conn_info_results_parse(loss_conf, box_conf, file_data, set_key)
    #print "results:\n\n\n\n", loss_conf["results"]["conninfo"]["loss_stats"]
    #print "\n\n\n\n"
    #print "results:", len(loss_conf["results"]["conninfo"]["loss_stats"])

    #plot_var = { "colsum" : colsum, "plot_group" : set_key, "csv_file" : file_data["results"]["throughput"]["csv_file"] }

    #colsum = r['colSums'](data["packet_loss_dataf"])
    colsum = data["packet_loss_dataf"].rx2(1)
    plot_var = {"colsum" : colsum}
    file_data.update(plot_var)
    print "colsum:", type(colsum)
    print "Summary:", r["summary"](data["packet_loss_dataf"])


    #try:
    #    d = DataFrame.from_csvfile(file_data["results"]["throughput"]["csv_file"], header=True, sep=',', quote='"', dec='.', fill=True, comment_char='', as_is=False)
    #except:
    #    print "Failed to read file:", file_data["results"]["throughput"]["csv_file"]
    #    raise
    #colsum = r['colSums'](d)
    ## Divide each value in the vector by the sample sec
    #colsum = colsum.ro / throughput_conf["sample_sec"]
    ## Multiply each value in the vector by the 8 to get bits
    #colsum = colsum.ro * 8
    #plot_var = { "colsum" : colsum, "plot_group" : set_key, "csv_file" : file_data["results"]["throughput"]["csv_file"] }
    #file_data.update(plot_var)


def loss_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):

    if connection_info_file_parse_func_time(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    file_conf["retrans_collapse"] = 0
    print "Adding conf values:", file_conf["streams"][file_conf["streams_id"]]
    #file_conf.update(file_conf["streams"][file_conf["streams_id"]])
    file_conf["retrans_collapse"] = file_conf["streams"]["y"].get("retrans_collapse", 0)
    print "file_conf retrans_collapse:", file_conf["retrans_collapse"]

    #y_axis_max = plot_conf["plot_conf"]["bandwidth"] * plot_conf["plot_conf"]["bandwidth_axis_scale"]
    y_axis_min, y_axis_max = plot_conf["plot_conf"]["y_axis_lim"]
    plot_conf["plot_conf"]["y_axis_range"] = xrange(0, int(y_axis_max), 10)

    plot_conf["y_axis_data"] = [i for i in plot_conf["plot_conf"]["y_axis_range"]]
    plot_conf["y_axis_labels"] = [i for i in plot_conf["plot_conf"]["y_axis_range"]]

    #file_conf["results"]["conninfo"] = { "loss_stats": []}
    #file_conf["results"]["loss_per_stream"] = { "data": []}

    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    #results_dict = {}

    #plot_conf["plot_conf"]["y_axis_lim"] = [0, 10000 * 1000]
    #results_dict = file_conf["results"]["throughput"]
    results_dict = file_conf["results"]["conninfo"]

    results_dict["csv_file"] = os.path.join(plot_conf["output_dir"], "%s.loss.csv" % file_conf["data_file_name"])
    results_dict["err_file"] = os.path.join(plot_conf["output_dir"], "%s.loss.err" % file_conf["data_file_name"])
    #results_dict["results_file"] = results_dict["csv_file"]

    #results_dict["results_cmd"] = "./tput -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -t %(sample_sec)s %(pcap_file_receiver)s  1> %(csv_file)s 2> %(err_file)s" %\
    #    dict(file_conf, **results_dict)
    #print "y_axis_range:", plot_conf["plot_conf"]["y_axis_range"]
    #conf["plot_conf"]["y_axis_lim"]

    #if "set_pif_order" in plot_conf["set_conf"]:
    #    def get_col(plot):
    #        #print "ORDER:", plot["box_conf"]["order"]
    #        for i, pif in enumerate(sorted(plot["box_conf"]["order"])):
    #            if file_conf["packets_in_flight"] == pif:
    #                return i +1
    #        return 0
    #    file_conf["column"] = get_col #plot_conf["set_conf"]["set_pif_order"][file_conf["packets_in_flight"]]

    def get_col(plot):
        #print "get_col:", plot["retrans_collapse"]
        plot["label"] = "RC%d" % plot["retrans_collapse"]
        return plot["retrans_collapse"] + 1
    file_conf["column"] = get_col #plot_conf["set_conf"]["set_pif_order"][file_conf["packets_in_flight"]]
    file_conf["results"]["loss"] = results_dict

def get_loss_conf():
    conf = deepcopy(graph_default.get_conf())
    conf["output_file"] = util.cmd_args.loss_output_file
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin2)s_Duration:%(duration)s_Payload:%(payload_thin2)s_ITT:%(itt_thin2)s_RTT:%(rtt)s_%(thin_dupack)s",
                         "sort_key_func": lambda x: (x["stream_count_thin2"]),
                         "sort_keys": ["stream_count_thin2"],
                         "func" : loss_box_key_func,
                         "box_title_def": "%(stream_count_thin2)d vs %(stream_count_thick)d DA:%(thin_dupack)s" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin2)s_Payload:%(payload_thin2)s_ITT:%(itt_thin2)s_RTT:%(rtt)s_RC:%(retrans_collapse)s_Type:%(type_thin2)s",
                         "set_pif_order": {0: 1, 4: 2, 20: 3} }
    conf["page_group_def"] = { "title": "Duration: %(duration)smin, Payload: %(payload_thin2)s, ITT: %(itt_thin2)sms, RTT: %(rtt)s, Cap: %(cap)skbit",
                                          "sort_keys": ["duration", "payload_thin2", "itt_thin2", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2], x[3])}
    conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 2, "x_axis_lim" : [0, 3], "y_axis_lim" : [10, 22],
                               "x_axis_title" : { "title": ["TCP variation used for competing streams", "THICK", "THIN", "RDB"], "adj": [0.2, 0.70, .8, .9],
                                                  "colors" : ["black", "darkred", "darkgoldenrod1", "darkblue"] },
                               "plot_func": plot_boxplot,
                               })
    conf["parse_results_func"] = loss_results_parse
    conf["results"]["conninfo"] = { "loss_stats": []}
    conf["results"]["loss_per_stream"] = { "data": []}

    conf["file_parse"].update({ "func": loss_file_parse_func })
    conf["document_info"]["title"] = "Throughput plots"
    conf["plot_conf"]["y_axis_title"] = "Throughput (Kbit/second aggregated over %(sample_sec)d seconds)" % conf
    conf["box_commands"] = { "xline": {"command": "abline(h=%d, lty=2)", "func": lambda plot_conf, file_conf: xline(plot_conf["bandwidth"],
                                                                                                                           file_conf["payload_thin2"],
                                                                                                                           file_conf["itt_thin2"],
                                                                                                                           file_conf["stream_count_thin2"],
                                                                                                                           file_conf["stream_count_thick"])}}
    conf["sample_sec"] = 5
    conf["plot_conf"]["bandwidth_axis_scale"] = 1.1 # Multiplied with bandwidth to get y axis limit
    #conf["plot_conf"]["bandwidth_axis_scale"] = 0.7
    #conf["file_parse"]["files_match_exp"] = "8_thin_*_vs_8*%s" % conf["file_parse"]["pcap_suffix"]
    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1})

    return conf

get_conf = get_loss_conf
