#!/usr/bin/env python
from graph_default import *

from graph_base import parse_args

def throughput_box_key_time_func(box_conf, conf):
    throughput_box_key_func(box_conf, conf)
    column = 1
    for plot_set in box_conf["sets"]:
        #print 'len(plot_set["plots"]):', len(box_conf["sets"][plot_set]["plots"])
        #print "plot_set_key:", plot_set
        for plot in box_conf["sets"][plot_set]["plots"]:
            plot["column"] = column
            #print "Setting column:", column
        column += 1

def throughput_file_parse_func_time(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    if throughput_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False
    pif = file_conf["packets_in_flight"]

def latency_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    if file_parse_generic(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    file_conf["results"]["latency"]["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.dat"))
    file_conf["results"]["latency"]["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.stout"))

    file_conf["results"]["latency"]["results_cmd"] = \
        "./analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s -o %(output_dir)s -A 1> %(stdout_file)s" % \
        dict(file_conf, **file_conf["results"]["latency"])
    file_conf["color"] = "darkred" # Default color for dataset

    #if file_conf["packets_in_flight"] > 4:
    #    file_conf["group"] = 0
    #else:
    #    file_conf["group"] = 1

    file_conf["group"] = file_conf["type_id"]


def latency_meta_data_box_func(conf, plot_box_dict, sets):
    keys = [("Number of retrans", "Number of retransmissions", None),
            ("1. retrans", "Occurrences of 1. retransmission", None),
            ("2. retrans", "Occurrences of 2. retransmission", None),
            ("Data packets sent", "Total data packets sent", None),
            ("Total bytes sent", "Total bytes sent (payload)", None),
            ("Nr. retrans bytes", "Number of retransmitted bytes", None),
            ("Avg. min latency", "Average latencies (min/avg/max)", "(?P<value>\d+),+"),
            ("Avg. avg latency", "Average latencies (min/avg/max)", "\d+,\s+(?P<value>\d+),+"),
            ("Avg. max latency", "Average latencies (min/avg/max)", "\d+,\s+\d+,\s+(?P<value>\d+)\sms"),
            ("Avg. packet size", "Average of all packets in all connections", None),
            ("Estimated loss", "Estimated loss rate based on retransmissions", None),
            ("Packet  Loss", "Packet  Loss", None),
            ("Ranges Loss", "Ranges Loss", None),
            ("Bytes Loss", "Bytes Loss", None),
            ("Redundancy", "Redundancy", None),
            ("RDB packets", "Number of packets with bundled segments", None),
            ("RDB bytes bundled", "RDB bytes bundled", "(?P<value>\d+).+"), # :     460896 (1.316981% of total bytes sent)
            ("RDB packet hits", "RDB packet hits", "(?P<value>\d+).+"),     # :      47585 (84.271951% of RDB packets sent)
            ("RDB packet misses", "RDB packet misses", "(?P<value>\d+).+"), # :       8881 (15.728049% of RDB packets sent)
            ("RDB byte   hits", "RDB byte   hits", "(?P<value>\d+).+"),     # :   12927268 (2804.812365% of RDB bytes)
            ("RDB byte   misses", "RDB byte   misses", "(?P<value>\d+).+"), # :    3642404 (790.287614% of RDB bytes)
            ]

    data = []
    colnames = []
    col_colors = ["cyan"]
    rownames = []
    for name, key, regex in keys:
        rownames.append(name)

    sorted_sets = sorted(sets.keys(), key=lambda k: sets[k]["plots"][0]["column"])
    #print "sorted_sets:", sorted_sets
    #for s in sets:
    for s in sorted_sets:
        for plot_conf in sets[s]["plots"]:
            #print "plot_conf:", plot_conf
            colnames.append(plot_conf["stream_id"])
            col_colors.append(plot_conf["color"])
            info = read_output_info(plot_conf["stdout_file"])
            for name, key, regex in keys:
                if regex and key in info:
                    m = re.match(regex, info[key], flags=re.DOTALL)
                    data.append(m.group("value") if m else "")
                else:
                    data.append(info[key] if key in info else "")

    col_colors = StrVector(col_colors)

    #print "colnames:", colnames
    #print "rownames:", rownames
    #print "col_colors:", col_colors
    #print "Data:", data

    #data = StrVector(data)
    matrix = r.matrix(data, ncol=len(colnames))
    colnames = StrVector(colnames)
    rownames = StrVector(rownames)
    matrix.colnames = colnames
    matrix.rownames = rownames

    color_data = StrVector(["darkred" for c in data])
    color_matrix = r.matrix(color_data, ncol=2)

    if False:
        r["source"]('/root/sit.gz')
        r["plot.table"](matrix, highlight=color_matrix)
    else:
        gplots = importr('gplots')
        textplot = SignatureTranslatedFunction(gplots.textplot, init_prm_translate = {'show_colnames': 'show.colnames', 'show_rownames': 'show.rownames', 'col_data': 'col.data',
                                                                                      'col_colnames': 'col.colnames'})
        # cex controls text size
        textplot(matrix, col_colnames=col_colors, cmar=0.5, cex=0.45, valign="top")

def latency_file_parse_func_time(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    if latency_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False

    if file_conf["hostname"] == "rdbsender":
        file_conf["percentiles"] = plot_conf["box_conf"]["percentiles"]

def get_throughput_conf():
    throughput_conf = get_default_throughput_conf()
    throughput_conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Duration:%(duration)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_Type:%(type_id)s",
                                    "sort_key_func": itemgetter("stream_count_thin1", "duration", "packets_in_flight"),
                                    "sort_keys": ["stream_count_thin1", "duration", "packets_in_flight"],
                                    "func" : throughput_box_key_time_func,
                                    "box_title_def": "%(stream_count_thin1)dvs%(stream_count_thick)d ID: %(type_id)s"}
    throughput_conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIF:%(packets_in_flight)s_Num:%(num)s"}

    throughput_conf["plot_conf"]["n_columns"] = 3
    throughput_conf["file_parse"]["func"] = throughput_file_parse_func_time
    throughput_conf["document_info"]["title"] = "Throughput Plots"

    ########################################
    # Group by streams instead of duration
    throughput_conf["page_group_def"] = { "title": "Streams: %(streams_str)s, Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s",
                                          "sort_keys": ["stream_count_thin1", "payload", "itt_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2], x[3]) }
    throughput_conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Duration:%(duration)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_Type:%(type_id)s",
                                    "sort_key_func": lambda x: (x["duration"], x["stream_count_thin1"], x["packets_in_flight"]),
                                    "sort_keys": ["duration", "stream_count_thin1", "packets_in_flight"],
                                    "func" : throughput_box_key_time_func,
                                    "box_title_def": "Dur: %(duration)s, ID: %(type_id)s" }
    return throughput_conf

#def get_goodput_conf():
#    goodput_conf = get_throughput_conf()
#    setup_default_goodput_conf(goodput_conf)
#    #conf["file_parse"]["func"] = goodput_file_parse_func_rdb
#    return goodput_conf

def latency_box_key_func_time(box_conf, conf):
    if latency_box_key_func(box_conf, conf) is False:
        return False
    #duration_sec = conf["duration"] * 60
    #box_conf["x_axis_lim"][1] = duration_sec

    # Set the meta data function
    #box_conf["meta_data"]["enabled"] = True
    box_conf["meta_data"]["func"] = latency_meta_data_box_func

    for plot_set in box_conf["sets"]:
        for plot in box_conf["sets"][plot_set]["plots"]:
            plot["column"] = set_order[plot["packets_in_flight"]]
            #print "Setting column %d for %s" % (plot["column"], plot["type_id"])

def latency_before_graphs_func_time(plot_conf):
    if "percentiles" in plot_conf["box_conf"]:
        plot_conf["document_info"]["info"].append("Percentiles: %s" % " ".join([str(p) for p in plot_conf["box_conf"]["percentiles"]]))
        print "PERCENTILES!:", plot_conf["document_info"]["info"]

def get_latency_conf():
    latency_conf = deepcopy(get_default_latency_conf())
    latency_conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Duration:%(duration)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_Num:%(num)s_delay:%(delay_type)s",
                                 "sort_key_func": lambda x: (x["duration"], x["stream_count_thin1"], x["delay_type"], x["num"]),
                                 "sort_keys": ["stream_count_thin1", "duration", "delay_type", "num"],
                                 "func" : latency_box_key_func_time,
                                 "box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d Num: %(num)s Delay: %(delay_type)s" }
    latency_conf["box_conf"]["percentiles"] = [.8, .95]
    latency_conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_Num:%(num)s_delay:%(delay_type)s",
                                 "sort_key_func": lambda x: x["dst_port"], }
    latency_conf["page_group_def"] = {"title": "Duration: %(duration)s min, Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s, Queue Length: %(queue_len)s",
                                      "sort_keys": ["duration", "payload_thin1", "itt_thin1", "rtt"],  "sort_key_func": lambda x: (x[0], x[1], x[2], x[3])}
    latency_conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 4, "x_axis_lim" : [0, 800] })
    latency_conf["parse_results_func"] = latency_results_parse
    latency_conf["file_parse"].update({ "func": latency_file_parse_func_time })
    latency_conf["document_info"]["title"] = "Latency Plots"
    latency_conf["func_before_graphs"] = latency_before_graphs_func_time
    #latency_conf["plots_per_page"] = 5
    latency_conf["add_meta_data"] = True

    #latency_conf["file_parse"]["files_match_regex"].append(".*8.*30.*")
    #latency_conf["file_parse"]["files_nomatch_regex"].append(".*60m.*")
    #latency_conf["file_parse"]["files_nomatch_regex"].append(".*120m.*")
    return latency_conf



def get_data_transfer_conf():
    conf = get_default_data_transfer_conf()
    #conf["file_parse"]["files_nomatch_regex"].append(".*num_0.*")
    conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 2 } )
    #conf["page_group_def"].update(default_rdb_conf["page_group_def"])
    conf["box_conf"].update( { "func" : data_trans_box_key_func } )

    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Duration:%(duration)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_Num:%(num)s_delay:%(delay_type)s",
                              "sort_key_func": lambda x: (x["duration"], x["stream_count_thin1"], x["delay_type"], x["num"]),
                              "sort_keys": ["stream_count_thin1", "duration", "delay_type", "num"],
                              "func" : latency_box_key_func_time,
                              "box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d Num: %(num)s Delay: %(delay_type)s" })
    return conf



def main(args):
    if args.goodput_output_file:
        goodput_conf = get_goodput_conf()
        do_all_plots(goodput_conf=goodput_conf)

    if args.throughput_output_file:
        throughput_conf = get_throughput_conf()
        do_all_plots(throughput_conf=throughput_conf)

    if args.latency_output_file:
        latency_conf = get_latency_conf()
        do_all_plots(latency_conf=latency_conf)

    if args.latency_table_output_file:
        latency_conf = get_latency_conf()
        latency_conf["plots_per_page"] = 5
        latency_conf["add_meta_data"] = True
        latency_conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 2 })
        latency_conf["output_file"] = args.latency_table_output_file
        do_all_plots(conf=latency_conf)

    if args.duration_output_file:
        duration_conf = get_duration_conf()
        do_all_plots(duration_conf=duration_conf)

    if args.data_transfer_output_file:
        data_transfer_conf = get_data_transfer_conf()
        do_all_plots(data_trans_bars_conf=data_transfer_conf)

    if args.connection_info_output_file:
        connection_info_conf = get_connection_info_conf()
        do_all_plots(conf=connection_info_conf)

    if args.verify_trace_output_file:
        verification_conf = get_verify_trace_conf()
        do_all_plots(conf=verification_conf)

    if args.tcp_probe_output_file:
        tcpprobe_conf = get_default_tcp_probe_conf()
        do_all_plots(tcpprobe_conf=tcpprobe_conf)

if __name__ == "__main__":
    main()
    end()
