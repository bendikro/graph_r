from graph_default import *


def verify_trace_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    file_conf["results"] = {"verify_analysetcp": {}, "verify_tcptrace": {}}
    file_conf["results"]["verify_analysetcp"]["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "command.stout"))
    file_conf["results"]["verify_tcptrace"]["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "tcptrace.stout"))

    # graph.generate reults require results_file to be defined
    file_conf["results"]["verify_analysetcp"]["results_file"] = file_conf["results"]["verify_analysetcp"]["stdout_file"]
    file_conf["results"]["verify_tcptrace"]["results_file"] = file_conf["results"]["verify_tcptrace"]["stdout_file"]

    file_conf["results"]["verify_analysetcp"]["results_cmd"] =\
        "./analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -o %(output_dir)s 1> %(stdout_file)s" %\
        dict(file_conf, **file_conf["results"]["verify_analysetcp"])

    tcptrace_cmd = "tcptrace -l -r -f'port==%(dst_port)s' %(pcap_file)s 1> %(stdout_file)s"

    # If this is a port range we must give a different filter to tcptrace
    if type(file_conf["dst_port"]) is str:
        dst_port_start, dst_port_end = file_conf["dst_port"].split("-")
        file_conf["results"]["verify_tcptrace"]["dst_port_start"] = dst_port_start
        file_conf["results"]["verify_tcptrace"]["dst_port_end"] = dst_port_end
        tcptrace_cmd = "tcptrace -l -r -f'port>=%(dst_port_start)s AND port<=%(dst_port_end)s' %(pcap_file)s 1> %(stdout_file)s"

    file_conf["results"]["verify_tcptrace"]["results_cmd"] = tcptrace_cmd % dict(file_conf, **file_conf["results"]["verify_tcptrace"])
    file_conf["color"] = "darkred" # Default color for dataset


def verify_connection_results_parse(conf, box_conf, set_data, set_key):

    if args.verbose:
        print "stdout_file:", set_data["results"]["verify_analysetcp"]["stdout_file"]
        print "stdout_file_tcptrace:", set_data["results"]["verify_tcptrace"]["stdout_file"]

    analysetcp_results = {}
    with open(set_data["results"]["verify_analysetcp"]["stdout_file"], 'r') as data_file:
        content = data_file.read()
        conns = content.split("STATS FOR CONN")

        regex = "\s*(?P<src_ip>[^:]+):(?P<src_port>\d+)\s->\s(?P<dst_ip>[^:]+):(?P<dst_port>\d+)(?P<data>.*)------------"
        for conn in conns:
            conn_data = re.search(regex, conn, flags=re.DOTALL)
            if conn_data is None:
                continue
            d = content_to_dict(conn_data.group("data"))
            conn_d = {}

            #if "Total packets sent" in d:
            #    analysetcp_results["total packets"] = d["Total packets sent"]
            conn_d["total packets"] = d.get("Total packets sent", [None])[0]
            if conn_d["total packets"] is None:
                conn_d["total packets"] = d.get("Total packets sent (found in dump)", [None])[0]

            conn_d["total data packets sent"] = d.get("Total data packets sent")[0]
            if conn_d["total data packets sent"] is None:
                conn_d["total data packets sent"] = d.get("Total data packets sent (found)", [None])[0]

            conn_d["total unique bytes"] = d["Number of unique bytes"][0]
            conn_d["total retransmissions"] = int(d.get("Number of retransmissions")[0]) + int(d.get("Number of packets with bundled segments")[0])
            conn_d["total payload sent"] = d.get("Total bytes sent (payload)")[0]
            conn_d["redundant bytes"] = d.get("Redundant bytes (bytes already sent)")[0].split(" ")[0]

            key = (conn_data.group("src_ip"), conn_data.group("src_port"), conn_data.group("dst_ip"), conn_data.group("dst_port"))
            analysetcp_results[key] = conn_d

        #analysetcp_results

    tcptrace_results = {}

    with open(set_data["results"]["verify_tcptrace"]["stdout_file"], 'r') as data_file:
        content = data_file.read()
        conns = content.split("================================")

        regex = "TCP connection \d+:\s+host [^:]+:\s+(?P<src_ip>[^:]+):(?P<src_port>\d+)\s+host [^:]+:\s+(?P<dst_ip>[^:]+):(?P<dst_port>\d+)\s+(?P<data>.+)"
        for conn in conns:
            conn_data = re.search(regex, conn, flags=re.DOTALL)
            if conn_data is None:
                continue
            d = content_to_dict(conn_data.group("data"))
            conn_d = {}

            conn_d["total packets"] = d["total packets"][1].split(":")[0].strip().split(" ")[0]
            conn_d["total unique bytes"] = d["unique bytes sent"][0].split(":")[0].strip().split(" ")[0]
            conn_d["total data packets sent"] = d["actual data pkts"][0].split(":")[0].strip().split(" ")[0]
            conn_d["total retransmissions"] = int(d["rexmt data pkts"][0].split(":")[0].strip().split(" ")[0])
            conn_d["total payload sent"] = d.get("actual data bytes")[0].split(":")[0].strip().split(" ")[0]
            conn_d["redundant bytes"] = d.get("rexmt data bytes")[0].split(":")[0].strip().split(" ")[0]
            conn_d["FIN count"] = int(d.get("SYN/FIN pkts sent")[0].split(":")[0].strip().split(" ")[0].split("/")[1])
            conn_d["SYN count"] = int(d.get("SYN/FIN pkts sent")[0].split(":")[0].strip().split(" ")[0].split("/")[0])

            key = (conn_data.group("src_ip"), conn_data.group("src_port"), conn_data.group("dst_ip"), conn_data.group("dst_port"))
            tcptrace_results[key] = conn_d

    for key, tcptrace_data in tcptrace_results.items():
        analysetcp_data = analysetcp_results[key]
        differs = False
        if analysetcp_data != tcptrace_data:
            for key in analysetcp_data:
                if analysetcp_data[key] != tcptrace_data[key]:
                    if key == "redundant bytes":
                        diff = int(tcptrace_data[key]) - int(analysetcp_data[key])
                        if tcptrace_data["SYN count"] > 1:
                            diff -= tcptrace_data["SYN count"] - 1
                        if tcptrace_data["FIN count"] > 1:
                            diff -= tcptrace_data["FIN count"] - 1
                        if diff == 0:
                            continue
                    differs = True

            if differs:
                cprint("\nConnection differs: %s file: %s" % (key, set_data["pcap_file"]), "red")
                print "Differing fields:"
                for key in analysetcp_data:
                    if analysetcp_data[key] != tcptrace_data[key]:
                        print "   Key: '%s', analysetcp: '%s', tcptrace: '%s'" % (key, colored(analysetcp_data[key], "red"), colored(tcptrace_data[key], "red"))

                print "stdout_file:", set_data["stdout_file"]
                print "stdout_file_tcptrace:", set_data["stdout_file_tcptrace"]
                print "Results command:", set_data["results_cmd"]

def verify_trace_file_parse_func_time(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    #print "verify_trace_file_parse_func_time:", pcap_file

    if file_parse_generic(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False

    file_conf["group"] = file_conf["type_id"]
    ##file_conf["delay_type"] = "fixed"
    #file_conf["pcap_file_receiver"] = pcap_file.replace("front", "after")
    ##print "pcap_file_receiver:", file_conf["pcap_file_receiver"]
    #
    #file_conf["streams_str"] = "%(stream_count_thin1)d vs %(stream_count_thick)d" % file_conf
    #
    #if "cap" in file_conf:
    #    plot_conf["plot_conf"]["bandwidth"] = file_conf["cap"]
    #
    ##file_conf["results_cmd"] = "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s -o %(output_dir)s -e 1> %(stdout_file)s" % file_conf
    ##file_conf["results_cmd"] = "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -o %(output_dir)s -e 1> %(stdout_file)s" % file_conf
    #file_conf["group"] = file_conf["num"]
    #file_conf["hostname"] = ""

    verify_trace_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg)

def set_trace_verification_conf(conf):
    conf["output_file"] = args.verify_trace_output_file
    conf["parse_results_func"] = verify_connection_results_parse
    conf["plot_conf"]["plot_func"] = None
    conf["plot_conf"]["make_plots"] = False
    conf["results"] = { "loss_stats" : []}

    #connection_info_conf["file_parse"]["func"] = verify_trace_file_parse_func
    if os.path.isfile(conf["output_file"]):
        os.remove(conf["output_file"])

    return conf

def get_conf():
    from latency import get_conf as get_latency_conf
    conf = get_latency_conf()
    set_trace_verification_conf(conf)

    conf["file_parse"]["func"] = verify_trace_file_parse_func_time
    conf.pop("page_group_def", None)

    #conf["file_parse"]["files_match_regex"].append(".*t60-tcp_vs_g60_kbit10000_min20_ps120_itt100_rtt150_loss_pif0_qlen122_delayfixed_num0_clps-t-0_clps-g-0_soff-off_soff-t-1_soff-g-0_brate-speed-10-duplex-full_ysender.*")

    #conf["file_parse"]["parse_func_arg"] = ["thin", "thick"]
    set_trace_verification_conf(conf)
    return conf
