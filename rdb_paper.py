#!/usr/bin/env python
from datetime import datetime
import argparse
import os
import sys

import graph_base

from graph_default_bendik import get_default_conf

import graph_default
graph_default.get_conf = get_default_conf

from graph_default_bendik import get_latency_conf

import latency
latency.get_conf = get_latency_conf

if __name__ == "__main__":
    plot_types = ["rwz_paper_latency_no_bundling_limit", "rwz_paper_latency_with_bundling_limit", "paper_latency_netem", "rwz_misuser_dpif_tfrcsp_oldcong_paper"]
    argparser = argparse.ArgumentParser(description="Create RDB paper plots", usage='%(prog)s [options] command directories')
    options = argparser.add_argument_group('RDB paper plots')
    options.add_argument("--plots",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, action='append', default=[])

    args = argparser.parse_args()
    # Remove any arguments on args list
    sys.argv = sys.argv[:1]

    print "args.plots:", args.plots

    def available_plots():
        print "Avilable plots:"
        for p in plot_types:
            print "- %s" % p

    if not args.plots:
        args.plots = plot_types

    for p_arg in args.plots:
        print "p_arg: '%s'" % p_arg
        print "plot_types: '%s'" % plot_types
        print "IN: %s" %  (p_arg in plot_types)
        if p_arg not in plot_types:
            print "Invalid plot: '%s'" % p_arg
            available_plots()
            break

        defaults = {}
        if p_arg == "rwz_paper_latency_no_bundling_limit":
            #.py -lpt rwz_paper_latency_no_bundling_limit -vvv  -f
            from plot_latency_rdb_vs_greedy import run as plotter_func
            defaults["plot_type"] = "rwz_paper_latency_no_bundling_limit"
        elif p_arg == "rwz_paper_latency_with_bundling_limit":
            from plot_latency_rdb_vs_greedy import run as plotter_func
            defaults["plot_type"] = "rwz_paper_latency_with_bundling_limit"
        elif p_arg == "paper_latency_netem":
            from plot_rdb_latencies_netem_PIF import run as plotter_func
            defaults["plot_type"] = "paper_latency_netem"
        elif p_arg == "rwz_misuser_dpif_tfrcsp_oldcong_paper":
            from plot_paper_boxplots import run as plotter_func
            defaults["plot_type"] = "rwz_misuser_dpif_tfrcsp_oldcong_paper"

        plotter_func(defaults)

    #graph_base.run()
