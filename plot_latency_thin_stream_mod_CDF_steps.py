#!/usr/bin/env python
from datetime import datetime
import argparse
import os, sys
from graph_r import *

import graph_base
from graph_default_bendik import get_default_conf
from graph_r_ggplot import ggplot_cdf_box, do_ggplots, ggplot2

from common import do_stream_custom_table_data
import common
from functools import partial

from latency import get_latency_conf as get_latency_conf_original
import latency
import data_tables

import graph_default
graph_default.get_conf = get_default_conf

def get_table_latex_test_setup_with_name_testindex_tcp_mods(*args):
    latex_test_setup_table = data_tables.get_table_latex_test_setup()
    latex_test_setup_table["keys"]["Name"].update({"show": True, "location": "plot_dict", "key": "label"})
    latex_test_setup_table["keys"]["Mechanisms"].update({"show": True}) # , "location": "plot_dict", "key": "label"
    latex_test_setup_table["keys"]["Test"].update({"show": True})
    latex_test_setup_table["keys"]["Type"].update({"show": False})

    latex_test_setup_table["group_keys"] = ["plot_index"]
    latex_test_setup_table["filename"] = "latex_test_setup_table.tex"
    latex_test_setup_table["path_prefix"] = "unset"
    return latex_test_setup_table


def custom_latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):

    util.set_dict_defaults_recursive(file_conf, (("results", "latency"), {}))
    file_conf["results"]["latency"].update({"extra_cmd_args": ""})

    if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    #results_dict = {}
    #results_dict["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.dat"))
    #results_dict["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.stout"))
    ## --analyse-start=300 --analyse-end=60
    #
    ## Produce latency results
    #results_dict["results_cmd"] =\
    #    "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s -o %(output_dir)s %(extra_args)s -l -a -vv 1> %(stdout_file)s" %\
    #    dict(file_conf, **results_dict)
    #
    #file_conf["results"] = {"latency": results_dict}

    file_conf["pre_results"] = { "setup_table": {"func": custom_pre_results_func}}


def custom_pre_results_func(conf, plot_box_dict, file_data, set_key):

    if not "latex_test_setup_table" in conf["data_tables"]:
        data_tables.fetch_table_conf(table_func=get_table_latex_test_setup_with_name_testindex_tcp_mods, conf=conf, table_key="latex_test_setup_table")
        conf["data_tables"]["latex_test_setup_table"]["header_fmt"] = r"""\begin{tabular}{%(columns)s}"""
        conf["data_tables"]["latex_test_setup_table"]["header_end"] = r"""\end{tabular}"""

    file_data["fig_label"] = "%s-%s" % (conf["data_tables"]["latex_test_setup_table"]["path_prefix"], plot_box_dict["box_key"].replace("_", "-"))
    #file_data["label"] = "JAU"

    name_fmt = file_data["streams"][file_data["streams_id"]]["color_mapping"][0]["legend_name"]
    name = util.make_title(file_data["streams"][file_data["streams_id"]], file_data, name_fmt)
    file_data["label"] = name

    #if file_data["hostname"] != "zsender":
    #    file_data["label"] = "TCP" if file_data["packets_in_flight"] == 0 else "%s PIF:%s" % (file_data["type_thin1"].upper(), file_data["packets_in_flight"])

    common.do_stream_custom_table_data(file_data, plot_box_dict, {}, conf["data_tables"]["latex_test_setup_table"])
#    file_data["fig_label"] = "fiLabel"

#        file_data["fig_label"] = "%s-%s" % (conf["data_tables"]["latex_test_setup_table"]["path_prefix"], plot_box_dict["box_key"].replace("_", "-"))
#        file_data["fig_label"] = file_data["fig_label"].replace("-Plot-type:Latency", "")


def custom_latency_results_parse(conf, plot_box_dict, file_data, set_key):
    stream_properties = file_data["streams"][file_data["streams_id"]]
    #print "HOSTNAME:", file_data["hostname"]
    #if file_data["hostname"] != "zsender":
    #    file_data["label"] = "TCP" if file_data["packets_in_flight"] == 0 else "%s PIF:%s" % (file_data["type_thin1"].upper(), file_data["packets_in_flight"])
        #print "Setting label:", file_data["label"]
        #stream_properties["color_mapping"][0]["legend_name"] = file_data["label"]
        #stream_properties["color_mapping"][0]["colors"] = [file_data["color"]]

    latency.latency_results_parse(conf, plot_box_dict, file_data, set_key)

    #if not "table_data" in plot_box_dict:
    #    latency.setup_test_status_table(conf, plot_box_dict)

    if not "table_latency_data" in plot_box_dict["data_tables"]:
        #plot_box_dict["data_tables"]["table_latency_data"] = data_tables.get_table_latency_stats()
        data_tables.fetch_table_conf(table_func=data_tables.get_table_latency_stats, conf=plot_box_dict, table_key="table_latency_data")
        #conf["data_tables"]["table_test_data"] = data_tables.get_table_test_setup()
        data_tables.fetch_table_conf(table_func=data_tables.get_table_test_setup, conf=conf, table_key="table_test_data")

        #latency.setup_test_status_table(conf, plot_box_dict)
        #latency.setup_latency_stats_table(conf, plot_box_dict)
        #plot_box_dict["data_tables"]["analysetcp_results_table"] = data_tables.get_table_results_stats(plot_box_dict)

    if not "test_results_latency_table" in conf["data_tables"]:
        data_tables.fetch_table_conf(table_func=data_tables.get_table_results_latency, conf=conf, table_key="test_results_latency_table")

    common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["ecdf_ggplot2_values"], plot_box_dict["data_tables"]["table_latency_data"])
    common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["ecdf_ggplot2_values"], conf["data_tables"]["table_test_data"])

    common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], conf["data_tables"]["test_results_latency_table"], data_name="analysetcp")
    #common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], plot_box_dict["data_tables"]["test_results_itt_table"], data_name="analysetcp")

    #common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["ecdf_ggplot2_values"], plot_box_dict["data_tables"]["table_latency_data"])
    #common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["ecdf_ggplot2_values"], plot_box_dict["table_data"]["values"])
    #conf["test_setup_table_data"] = plot_box_dict["table_data"]["values"]

def custom_latency_process_results(conf, groups_list=None):

    tconf = conf["data_tables"]["latex_test_setup_table"]
    #print "tconf:", tconf
    latency.write_latex_table_test_setup(tconf)

    if not util.cmd_args.skip_data:
        latency.write_latex_table_test_setup(conf["data_tables"]["test_results_latency_table"])

    tconf["filename"] = "latex_figures_list.tex"
    if groups_list:
        print "groups_list:", len(groups_list)
        common.write_latex_figures(conf, groups_list, conf["output_file"],
                                   bookmark_title="Fig: Early retransmit: %(early_retransmit)s",
                                   figcaption_continuous=True,
                                   **tconf)

def get_latency_conf(defaults):
    conf = get_latency_conf_original()
    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_DA:%(thin_dupack)s_LT:%(linear_timeout)s",
                              #"sort_key_func": lambda x: (x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["itt_thin1"]),
                         #"sort_keys": ["stream_count_thin1", "stream_count_thick", "group", "itt_thin1"],
                         "sort_key_func": lambda x: (x["thin_dupack"], x["linear_timeout"], x["stream_count_thin1"], x["stream_count_thick"], x["group"]),
                         "sort_keys": ["thin_dupack", "linear_timeout", "stream_count_thin1", "stream_count_thick", "group"],
                         "func" : latency.latency_box_key_func,
                         "latency_options": {"per_stream": False},
                         #"box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d %(cong_control_thin1)s " }
                         "box_title_def" : "mFR: %(thin_dupack)s   Linear Timeout: %(linear_timeout)s" })
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["page_group_def"] = {
        "title": "%(stream_count_thin1)d Thin streams vs %(stream_count_thick)d Greedy streams - Duration: %(duration)s min\n"
        "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets, Cong: %(cong_control_thin1)s",
        "common_key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_RTT:%(rtt)s_%(cong_control_thin1)s_%(duration)s_ER:%(early_retransmit)s",
        "sort_keys": ["payload_thin1", "rtt", "stream_count_thin1"],
        "sort_key_func": lambda x: (x["payload_thin1"], x["rtt"], x["stream_count_thin1"])
    }


    conf["file_parse"].update({ "func": custom_latency_file_parse_func })

    conf["plot_conf"].update({"n_columns": 2, "n_rows" : 2,
                              "r.plot_args": {"cex.main": 0.9 },
                              "r.mtext_page_title_args": {"cex.main": 1.2 },
                              "plot_title" : { "title": "PLOT" },
                              }
                             )
    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [150, 1200], "y_axis_lim" : [0.5, 1],
                                            "y_axis_title" : { "title": "ECDF", },
                                            "x_axis_title" : { "title": "ACK Latency in milliseconds", },
                                        })

    conf["plot_conf"]["axis_conf"].update({ "y_axis_on_plot_func": lambda plot_box_dict: (plot_box_dict["plot_index"] % conf["plot_conf"]["n_columns"]) == 0,
                                            "x_axis_on_plot_func": lambda plot_box_dict: True,
                                            #"x_axis_on_plot_func": lambda plot_box_dict: (plot_box_dict["plot_index"] % conf["plot_conf"]["n_rows"]) == 0,
                                            #lambda x: True, #
                                        })

    conf["process_results"]["latency"] = { "func": custom_latency_process_results }

    conf["paper_width"] = 10
    conf["paper_height"] = 8
    return conf

def get_latency_conf_ggplot(defaults):
    from latency import thin_itt_mark_line_func_ggplot2
    from rpy2.robjects.lib import grid
    import functools
    conf = get_latency_conf(defaults)
    conf["box_conf"].update({"show_box_title": True,
                             #"box_title_def" : "Plot %(plot_number)s",
                             "box_title_def" : "mFR: %(thin_dupack)s   Linear Timeout: %(linear_timeout)s", # Bandwidth cap: %(bandwidth)s Kbit  Duration: %(duration)s min
                         })

    conf["box_conf"]["custom_ggplot2_plot_cmds"].update({"itt_lines": functools.partial(thin_itt_mark_line_func_ggplot2, count=6)})

    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO, #common.TABLE_ACTION.APPEND_TO_PLOT_BOX, #,
                                               "table_key": "table_latency_data",
                                               #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                               "heights": (.7, .05, .25),
                                           })

    conf["box_conf"]["legend"].update({"legend_sort": { "do_sort": True, "sorted_args": {} } })

    def do_ggplot_cdf_box(conf, plot_box_dict, sets):
        if plot_box_dict["plot_index"] % 2 == 1:
            plot_box_dict["theme_args"].update({"axis.title.y": ggplot2.element_blank() })
        return ggplot_cdf_box(conf, plot_box_dict, sets)

    conf["plot_conf"].update({"n_columns": 2, "n_rows" : 2,
                              "r.plot_args": {"cex.main": 0.9 },
                              "r.mtext_page_title_args": {"cex.main": 1.2 },
                              "plot_func": do_ggplot_cdf_box,
                              "do_plots_func": do_ggplots, #do_plots,
                              #"do_plots_func": do_plots,
    })

    #conf["plot_conf"]["axis_conf"].update( { "x_axis_lim" : [150, 1200], "y_axis_lim" : [0.6, 1.05] })

    conf["plot_conf"]["axis_conf"].update( { "x_axis_lim" : [150, 1100], "y_axis_lim" : [0.65, 1.03],
                                             "y_axis_main_ticks": [0, 1.01, .1], "y_axis_minor_ticks": .1 })

    conf["plot_conf"]["axis_conf"].update( { "x_axis_lim" : [150, 500], "y_axis_lim" : [0, 1.03],
                                             "y_axis_main_ticks": [0, 1.01, .1], "y_axis_minor_ticks": .1,
                                             "x_axis_main_ticks": [0, 1000, 50], "x_axis_minor_ticks": 10})

    conf["paper_width"] = 10
    conf["paper_height"] = 6

    conf["parse_results_func"] = custom_latency_results_parse
    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1})
    conf["paper_width"] = 4.5
    conf["paper_height"] = 2.8

    print "files_match_regex:", conf["file_parse"]["files_match_regex"]

    conf["plot_conf"]["theme_args"].update({"legend.position": FloatVector([0.87, 0.35]),
                                            "legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
                                            "legend.text": r('element_text(size=8)'),
                                            "legend.key.size": r('unit(6, "mm")'),
                                            "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
                                            "axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
                                            #"plot.title": r('element_text(size=8, lineheight=1, vjust=1.1, hjust=0.9, color="grey20")'), # , face="bold"
                                            "plot.title": r('element_text(size=8, lineheight=1, hjust=0.5, color="grey20")'), # , face="bold"
                                            #"panel.margin": r('unit(0 , "lines")'),
                                            #"panel.border": r('element_rect(fill=NA,color="darkred", size=0.5, linetype="solid")'),
                                            #"plot.margin": r('rep(unit(0,"null"),4)'),
                                            "plot.margin": r('unit(c(0.1, 0.1, 0.2, 0), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                            #"plot.margin": r('unit(c(1, 1, 0.5, 0.5), "lines")'),
                                        })
    #conf["plot_conf"]["theme_args"]["axis.title.x"] = ggplot2.element_blank()
    #conf["plot_conf"]["theme_args"]["axis.title.y"] = ggplot2.element_blank()

    if defaults:
        util.update_dict(defaults, conf)

    return conf

def get_latency_conf_ggplot_early_retrans(defaults=None):
    def custom_box_key_func(plot_conf, plot_box_dict, file_data, arg):
        file_data["legend_order"] = file_data["packets_in_flight"]
        if file_data["hostname"] == "rdbsender":
            if file_data["type_thin1"] == "tcp":
                file_data["color"] = "darkblue"
            elif file_data["type_thin1"] == "rdb":
                pif = "rdb_pif%d" % file_data["packets_in_flight"]
                if pif in graph_default.color_map:
                    file_data["color"] = graph_default.color_map[pif]
                else:
                    file_data["color"] = graph_default.color_map["default"]
        elif file_data["hostname"] == "zsender":
            if file_data["type_thin1"] == "rdb":
                file_data["color"] = "firebrick1"
        else:
            file_data["color"] = graph_default.color_map["thin"]
        print "COLOR SET:", file_data["color"]

    conf = get_latency_conf_ggplot(defaults)
    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_DA:%(thin_dupack)s_LT:%(linear_timeout)s_ER:%(early_retransmit)s",
                              "sort_key_func": lambda x: (x["early_retransmit"], x["thin_dupack"], x["linear_timeout"], x["stream_count_thin1"], x["stream_count_thick"], x["group"]),
                              "sort_keys": ["thin_dupack", "linear_timeout", "early_retransmit", "stream_count_thin1", "stream_count_thick", "group"],
                              "box_title_def" : "mFR: %(thin_dupack)s    Linear Timeout: %(linear_timeout)s    Early retransmit: %(early_retransmit)s",
                              #"func" : latency.latency_box_key_func,
                              "func": custom_box_key_func,
                              })
    return conf

#latency.get_conf = get_latency_conf
latency.get_conf = get_latency_conf_ggplot

def run(defaults={}):
    plot_types = ["old_cdf_qlen30", "old_cdf_qlen60", "lt_du_eretr"]

    argparser = graph_base.make_parser()
    options = argparser.add_argument_group('Latency CDF options')
    options.add_argument("-pt", "--plot-type",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, default=plot_types[0])

    args = graph_base.parse_args(argparser, defaults=defaults)
    plot_type = args.plot_type
    if plot_type == "old_cdf_qlen30":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen30_CDF_21vs10_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MODLAT"
        defaults["file_regex_match"] = ["t21-tcp-itt100-.*cubic.*vs_g10.*min5.*qlen30.*"]
        #t21-tcp-itt100*qlen30*.pcap
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD/all_results/"]
    elif plot_type == "old_cdf_qlen60":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MODLAT"
        defaults["file_regex_match"] = ["r21.*itt100-.*cubic.*vs_z10.*min5.*qlen60.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD/all_results/"]
    elif plot_type == "lt_du_eretr_60min":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr_min60.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MODLAT_ERETR_MIN60"
        defaults["file_regex_match"] = ["r21-tcp-itt100:15-ps120-cccubic-da0-lt1-er3.*"]
        #defaults["file_regex_nomatch"] = [".*itt100-.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TO_ZRECEIVER_NETEM_MODS_LONG/all_results/"]
        pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TO_ZRECEIVER_NETEM_MODS_LONG_VARITT/2014-12-01-1824-10
        #latency.get_conf = get_latency_conf_ggplot_early_retrans
        def_conf = {"box_conf.latency_options": {"per_stream": False},
                    "data_table_defaults": {"latex_test_setup_table": { "path_prefix": "thin-stream-mod-CDF",
                                                                        "plots_per_page": 4,
                                                                        "plots_per_subfig": 4}}}
        latency.get_conf = partial(get_latency_conf_ggplot_early_retrans, defaults=def_conf)
    elif plot_type == "lt_du_eretr_60min_ittvar":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr_min60_iivar.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MODLAT_ERETR_MIN60_IITVAR"
        #defaults["file_regex_match"] = ["r21-tcp-itt100:15-ps120-cccubic-da0-lt1-er3.*"]
        #defaults["file_regex_nomatch"] = [".*itt100-.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TO_ZRECEIVER_NETEM_MODS_LONG_VARITT/all_results/"]
        #latency.get_conf = get_latency_conf_ggplot_early_retrans
        def_conf = {"box_conf.latency_options": {"per_stream": False},
                    "data_table_defaults": {"latex_test_setup_table": { "path_prefix": "thin-stream-mod-CDF",
                                                                        "plots_per_page": 4,
                                                                        "plots_per_subfig": 4}}}
        latency.get_conf = partial(get_latency_conf_ggplot_early_retrans, defaults=def_conf)
    elif plot_type == "lt_du_eretr":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MODLAT_ERETR"
        #defaults["file_regex_match"] = ["r21-tcp-itt100:15-ps120-cccubic-da0-lt1-er3.*"]
        defaults["file_regex_nomatch"] = [".*itt100-.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_ERETR/all_results/"]
        #latency.get_conf = get_latency_conf_ggplot_early_retrans
        def_conf = {"box_conf.latency_options": {"per_stream": False},
                    "data_table_defaults": {"latex_test_setup_table": { "path_prefix": "thin-stream-mod-CDF",
                                                                        "plots_per_page": 4,
                                                                        "plots_per_subfig": 4}}}
        latency.get_conf = partial(get_latency_conf_ggplot_early_retrans, defaults=def_conf)
    elif plot_type == "lt_du_eretr_oldcong":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TO_ZRECEIVER_THIN_STREAM_MOD_OLDCONG_latency_qlen63_CDF_20vs10_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TO_ZRECEIVER_THIN_STREAM_MOD_OLDCONG"
        #defaults["file_regex_match"] = ["r21-tcp-itt100:15-ps120-cccubic-da0-lt1-er3.*"]
        defaults["file_regex_nomatch"] = [".*itt100-.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TO_ZRECEIVER_THIN_STREAM_MOD_OLDCONG/all_results/"]
        #latency.get_conf = get_latency_conf_ggplot_early_retrans
        def_conf = {"box_conf.latency_options": {"per_stream": False},
                    "data_table_defaults": {"latex_test_setup_table": { "path_prefix": "thin-stream-mod-CDF",
                                                                        "plots_per_page": 4,
                                                                        "plots_per_subfig": 4}}}
        latency.get_conf = partial(get_latency_conf_ggplot_early_retrans, defaults=def_conf)
    elif plot_type == "lt_du_eretr_queueing_delay":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr_queueing_delay.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MODLAT_ERETR"
        #defaults["file_regex_match"] = ["r21-tcp-itt100:15-ps120-ccrdb-da0-lt1-er3.*"]
        defaults["file_regex_match"] = ["r21.*itt100.*cubic-da0-lt0-er0_vs_z10.*min5.*qlen60.*"]
        #defaults["file_regex_nomatch"] = [".*itt100-.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_ERETR/all_results/"]
        #latency.get_conf = get_latency_conf_ggplot_early_retrans
        def mark_queueing_delay(plot_conf=None, plot_box_dict=None, gp=None, **kw):
            #print "mark_queueing_delay"
            import rpy2.robjects.lib.ggplot2 as ggplot2
            if not "plot" in kw:
                return
            plot = kw["plot"]
            if plot["group"] != 0:
                return
            stream_properties = plot["streams"][plot["streams_id"]]
            if "itt" not in stream_properties:
                return
            x = [150, 250]
            dataf = DataFrame({'x': IntVector(x),
                               'y': FloatVector([0.4 for i in x])})
            print "dataf:", dataf
            aes_args = {}
            aes_args["x"] = "x"
            aes_args["y"] = "y"
            #yield ggplot2.geom_line(mapping=ggplot2.aes_string(**aes_args), data=dataf, stat="identity", position="identity")
            #yield ggplot2.geom_path(mapping=ggplot2.aes_string(**aes_args), data=dataf, arrow=r("arrow()"))
            segment_args = {"arrow": r('arrow(length = unit(1.1, "mm"), angle=60)'),
                            "linetype": "solid", #"dotted",
                            "colour": "orange",
                            "size": 0.45,
                            "data": dataf, #  Makes renderering the figure much faster
                        }
            y_pos = "0.9"

            yield ggplot2.geom_segment(ggplot2.aes_string(**{"x": "150", "y": y_pos, "xend": "250", "yend": y_pos}), **segment_args)
            yield ggplot2.geom_segment(ggplot2.aes_string(**{"x": "250", "y": y_pos, "xend": "150", "yend": y_pos}), **segment_args)

            # This creates the separate legend
            r("qdhline <- data.frame(x=c(290), y=c(-Inf, Inf), qdhlinec=factor('solid') )")
            aes = ggplot2.aes_string(x="x", y="y", linetype="qdhlinec", col="NULL")
            # alpha=0 causes the line to be hidden from the plot and legend
            gp2 = ggplot2.geom_line(r["qdhline"], mapping=aes, size=0.3, alpha=0.0)
            yield gp2

            #print "plot_box_dict:", plot_box_dict.keys()
            #print "plot_box_dict:", plot_box_dict

            # Handle linetype legend
            util.dict_set_default(plot_box_dict["legends"], ("linetype", {"guide_args": {}, "scale": {"func": ggplot2.scale_linetype_manual, "args": {}}}))
            linetype = plot_box_dict["legends"]["linetype"]
            linetype["guide_args"].update({"order": 1})
            util.dict_set_default(linetype["guide_args"], ("override.aes", {}))
            util.dict_set_default(linetype["guide_args"]["override.aes"], ("colour", []))
            linetype["guide_args"]["override.aes"]["colour"].append('orange')

            util.dict_set_default(linetype["guide_args"]["override.aes"], ("linetype", []))
            linetype["guide_args"]["override.aes"]["linetype"].extend([1, 3])

            scale_args = linetype["scale"]["args"]
            util.dict_set_default(scale_args, [("labels", []), ("values", [])])
            scale_args["labels"].extend(["Queueing delay"])
            scale_args["values"].extend(["solid"])
            return

        def_conf = {"box_conf.latency_options": {"per_stream": False},
                    "box_conf.custom_ggplot2_plot_cmds": {"mark_queueing_delay": mark_queueing_delay},
                    "plot_conf.axis_conf": { "x_axis_lim" : [150, 730], "y_axis_lim" : [0.0, 1.05]},
                    "plot_conf.theme_args": {"legend.position": FloatVector([0.84, 0.35]) },
                    "data_table_defaults": {"latex_test_setup_table": { "path_prefix": "thin-stream-mod-CDF",
                                                                        "plots_per_page": 4,
                                                                        "plots_per_subfig": 4}}}

        #conf["plot_conf"]["theme_args"].update({"legend.position": FloatVector([0.87, 0.35]),

        #conf["plot_conf"]["axis_conf"].update( { "x_axis_lim" : [150, 1100], "y_axis_lim" : [0.0, 1.03],
        #                                         "y_axis_main_ticks": [0, 1.01, .1], "y_axis_minor_ticks": .1 })

        latency.get_conf = partial(get_latency_conf_ggplot_early_retrans, defaults=def_conf)



        #geom_path(arrow = arrow(angle = 15, ends = "both", length = unit(0.6, "inches")))


    elif plot_type == "lt_du_eretr_per_stream":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr_per_stream.pdf"
        #defaults["connection_info_output_file"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_latency_itt100_qlen60_CDF_21vs10_ggplot_eretr_per_stream.txt"
        #defaults["file_regex_nomatch"] = [".*zsender.*"]
        defaults["output_dir"] = "RDBSENDER_ZSENDER_TEST_THIN_STREAM_MODLAT_ERETR_PER_STREAM"
        #defaults["file_regex_match"] = ["r21-tcp-itt100:15-ps120-cccubic-da0-lt1-er3.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_TEST_THIN_STREAM_MOD_ERETR/all_results/"]
        defaults["file_regex_match"] = [".*-da0-lt0-er3.*",
                                        #".*itt100-.*"
        ]

        import conn_info
        #import conn_info.get_conf as conninfo_get_conf
        from conn_info import get_conf as conninfo_get_conf
        def get_conninfo_conf():
            print "get_conninfo_conf!!!!"
            conf = conninfo_get_conf()
            process_results = conf["process_results"]["conninfo"]
            process_results.update({
                "headers": ["Host", "Str", "RDB", "PIF", "QLEN", "RC", "RTT", "ITT", "PS", "Dur", "D-PKT", "B-PKT",
                            "BLoss", "PLoss", "Test", "R_loss_stats", "R_latency_stats", "latency"],
                "value_keys": OrderedDict().fromkeys(["hostname", "streams", "type", "packets_in_flight", "queue_len", "retrans_collapse", "rtt", "itt", "payload",
                                                      "data_packet_count", "rdb_packet_count",
                                                      "bytes_loss", "packet_loss", "num", "R_loss_stats", "R_latency_stats", "latency"], None),
                "default_column_width": 12,
            })
            process_results["column_widths"].update({"Host": 11, "Str": 7, "RDB": 3, "PIF": 3, "QLEN": 6, "RC": 2, "RTT": 4, "D-PKT": 6, "B-PKT": 6,
                                                     "ITT": 6, "PS": 4, "Dur": 3, "soff": 4, "soff_t": 4, "soff_g": 4,
                                                     "gso_t": 5, "tso_t": 5, "gro_t": 5, "lro_t": 5,
                                                     "3rd Qu.": 6, "Test": 4, "bridge_rate" : 15, "clps-t": 6,
                                                     "R_loss_stats": 35, "R_latency_stats": 30, "latency": 45,
                                                     "BLoss": 8, "PLoss": 5 })
            return conf
        conn_info.get_conf = get_conninfo_conf

        def_conf = {"box_conf.latency_options": {"per_stream": True},
                    "data_table_defaults": {"latex_test_setup_table":
                                            { "path_prefix": "thin-stream-mod-CDF-per-stream",
                                              "plots_per_page": 2,
                                              "plots_per_subfig": 2,
                                              "captions": {"thin-stream-mod-CDF-per-stream-Streams:21-Streams-Greedy:10-Payload:120-ITT:100-RTT:150-PIFG:0-cubic-5-DA:0-LT:0-ER:3": "ITT: 100",
                                                           "thin-stream-mod-CDF-per-stream-Streams:21-Streams-Greedy:10-Payload:120-ITT:100:15-RTT:150-PIFG:0-cubic-5-DA:0-LT:0-ER:3": "ITT: 100:15"},
                                          }}}

        #bookmark_title="Fig: Early retransmit: %(early_retransmit)s")

        latency.get_conf = partial(get_latency_conf_ggplot_early_retrans, defaults=def_conf)
    else:
        cprint("Invalid plot type: '%s'" % plot_type, "red")
        sys.exit()

    args = graph_base.parse_args(argparser, defaults=defaults)
    graph_base.main()

if __name__ == "__main__":
    run()
