#!/usr/bin/env python
from datetime import datetime
from common import *

from rpy2.robjects import r
from rpy2.robjects.vectors import Vector, IntVector, DataFrame, StrVector, FloatVector
from rpy2.robjects.lib import grid

import graph_base
from graph_default_bendik import get_default_conf
from graph_r_ggplot import do_ggplots, ggplot2, ggplot_cdf_box, ggplot_boxplot_box
import graph_r_ggplot
import common, util
from latency import get_latency_conf as get_latency_conf_original
import throughput
import graph_default
graph_default.get_conf = get_default_conf


import tcpprobe
import data_tables

def custom_pre_results_func(conf, plot_box_dict, file_data, set_key):

    stream_properties = file_data["streams"][file_data["streams_id"]]
    stream_properties["color_mapping"] = deepcopy([{"color_code": 'c(brewer.pal(9, "Blues")[7:7])', "legend_name": "Thin %(type_upper)s"}])
    #stream_properties["color_mapping"] = deepcopy([{"legend_name": "Thin %(type_upper)s"}])
    stream_properties["meta_info"].update({"factor": "stream",
                                           #"type_id_column": "stream",
                                           "legend_count": 20,
                                           "breaks_factor": "stream",  # These are for the legend
                                           #"breaks_factor": "color_column",
                                           #"factor": "color_column",
                                           #"aes_args": { "color": 'stream_id'},
                                           #"aes_args": { "color": 'color_column', "group": 'stream_id'},
                                           "aes_args": { "color": 'factor(stream)', #"group": 'stream_id'
                                                     },
                                           #"plot_args": {"show_guide": False },
                                       })

def custom_results_parse(conf, plot_box_dict, file_data, set_key):

    if tcpprobe.tcp_probe_results_parse(conf, plot_box_dict, file_data, set_key) is False:
        return False

def get_conf():
    conf = graph_default.get_conf()
    conf["output_file"] = util.cmd_args.tcp_probe_output_file
    #conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s",
    #conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_host:%(hostname)s",
    conf["box_conf"].update({"key": "Streams:%(stream_count_thin1)s_Streams:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_"\
                             "RTT:%(rtt)s_cong:%(cong_control_thin1)s_PIF:%(packets_in_flight)s_QLEN:%(queue_len)s_NUM:%(num)s_%(thin_dupack)s",
                         "sort_key_func": lambda x: (x["thin_dupack"], x["stream_count_thin1"], x["itt_thin1"], x["cong_control_thin1"]),
                         "sort_keys": ["thin_dupack", "stream_count_thin1", "itt_thin1", "cong_control_thin1"],
                         #"func" : latency_box_key_func,
                         "box_title_def" : "%(stream_count_thin1)dt vs %(stream_count_thick)dg Duration: %(duration)smin Queue: %(queue_type)s (%(queue_len)s) DA:%(thin_dupack)s " })
    #conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIF:%(packets_in_flight)s",
                         "sort_key_func": lambda x: (x["stream_count_thin1"], x["itt_thin1"], int(x["packets_in_flight"])),
                         "sort_keys": ["stream_count_thin1", "itt_thin1"],
                         }
    #conf["page_group_def"] = {"title": "Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s",
    #                          "sort_keys": ["payload_thin1", "itt_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2])}
    conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 1,
                               #"x_axis_lim" : [27, 60],
                               #"x_axis_font_size": 0.7,
                               #"x_axis_main_ticks": 1,
                               #"x_axis_minor_ticks": .5,
                               #"y_axis_main_ticks": 10,
                               #"y_axis_minor_ticks": 5,
                               "default_factor": "stream",
                               #"plot_func": plot_scatterplot,
                               "plot_func": graph_r_ggplot.plot_scatterplot_cwnd,
                               "do_plots_func": graph_r_ggplot.do_ggplots,
                               })

    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO, #common.TABLE_ACTION.APPEND_TO_PLOT_BOX, #,
                                               "table_key": "table_latency_data",
                                               #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                               "heights": (.73, .02, .25),
                                           })
    conf["plot_conf"]["theme_args"].update({"legend.position": "left",
                                            #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
                                            "legend.text": r('element_text(size=12)'),
                                            "legend.key.size": r('unit(10, "mm")'),
                                            "plot.title": r('element_text(size=20, lineheight=1, hjust=0.5, vjust=1, color="grey20")'), # , face="bold"
                                            "axis.text.x": r('element_text(size = 15, hjust=0.5, vjust=0.5)'), # , color="grey50"
                                            "axis.text.y": r('element_text(size = 15, hjust=0.0, vjust=0.5)'),
                                            #"axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
                                            #"axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
                                            #"plot.title": r('element_text(size=9, lineheight=1, hjust=0.5, color="grey20")'), # , face="bold"
                                            "plot.margin": r('unit(c(0.1, 0.1, 0.2, 0), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                        })

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [20, 300],
                                            "y_axis_title" : { "title": "Congestion window size", },
                                            "x_axis_title" : { "title": "Time in seconds", },
                                            "x_axis_main_ticks": [0, 300, 5], "x_axis_minor_ticks": 1,
                                            "y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                        })

    #conf["box_conf"]["legend"].update({"legend_sort": { "do_sort": True, "sorted_args": {} } })

    conf["parse_results_func"] = custom_results_parse
    conf["file_parse"].update({ "func": tcpprobe.tcp_probe_file_parse_func })
    conf["document_info"]["title"] = "TCP CONGESTION WINDOW Plots"
    conf["document_info"]["show"] = False
    conf["paper_width"] = 11
    conf["paper_width"] = 200
    conf["paper_height"] = 6
    #conf["paper_width"] = 20
    conf["print_page_title"] = False

    #conf["file_parse"]["files_match_regex"].append("t9-rdb-itt100:15-ps120_vs_g5_kbit5000_min10_rtt150_loss_pif20_qlen46875_delayfixed_num0_rdbsender.*")
    #conf["file_parse"]["files_match_regex"].append(".*min10.*")
    #conf["file_parse"]["files_match_regex"].append(".*ps451.*")
    #conf["file_parse"]["files_match_regex"].append("t1-rdb-itt64-ps451-ccvegas_vs_g2_kbit5000_min10_rtt150_loss_pif300_qlen30_delayfixed_num0_.*")
    #conf["file_parse"]["files_match_regex"].append("t1-rdb-itt64.*")
    conf["file_parse"]["files_nomatch_regex"].append(".*qlen46875.*")
    return conf

def get_tfrc_conf():
    conf = get_conf()
    def custom_file_parse_func2(plot_conf, file_conf, pcap_file, parse_func_arg):
        if tcpprobe.tcp_probe_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False
        # Override group
        #file_conf["group"] = "%s-%s" % (file_conf["packets_in_flight"], file_conf["dpif"])
        #file_conf["plot_type"] = parse_func_arg
        file_conf["tfrc"] = 1
        #print "Group:", file_conf["group"]

    def set_box_key_value(file_conf, arg):
        group = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        #print "set_box_key_value:", group
        file_conf["group"] = group
        #file_conf["hack"] = arg
        #print "Group!:", file_conf["group"]

    conf["plot_conf"]["theme_args"].update({ "legend.position": "left" })

    #conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_DU%(thin_dupack)s_LT%(linear_timeout)s_Plot_type:%(plot_type)s",

    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_ER:%(early_retransmit)s_QLEN:%(queue_len)s",
                              "sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                                          x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"]),
                              "sort_keys": ["thin_dupack", "linear_timeout", "early_retransmit", "stream_count_thin1", "stream_count_thick", "group", "itt_thin1", "queue_len"],
                              "pre_key_func": set_box_key_value,
                          })
    conf["file_parse"].update({"func": custom_file_parse_func2 })
    return conf


def get_tfrc_netem_conf():
    conf = get_conf()
    def custom_file_parse_func2(plot_conf, file_conf, pcap_file, parse_func_arg):
        if tcpprobe.tcp_probe_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False
        # Override group
        #file_conf["group"] = "%s-%s" % (file_conf["packets_in_flight"], file_conf["dpif"])
        #file_conf["plot_type"] = parse_func_arg
        file_conf["tfrc"] = 1
        file_conf["legend_order"] = file_conf["packets_in_flight"]
        #print "Group:", file_conf["group"]
        file_conf["pre_results"] = { "setup_table": {"func": custom_pre_results_func}}

    def set_box_key_value(file_conf, arg):
        group = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        file_conf["group"] = group

    conf["box_conf"].update({ #"key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_QLEN:%(queue_len)s_LOSS:%(loss)s",
        "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_QLEN:%(queue_len)s",
                              "sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"],
                                                          x["stream_count_thin1"], x["group"], x["queue_len"], float(x["loss"])),
                              "sort_keys": ["thin_dupack", "linear_timeout", "stream_count_thin1", "group", "itt_thin1", "queue_len", "loss"],
                              #"box_title_def" : "Streams: %(stream_count_thin1)d Duration: %(duration)smin  Queue: %(queue_type)s (%(queue_len)s) Uniform loss: %(loss)s %%",
        "box_titles": { "main": { "title_def": "Streams: %(stream_count_thin1)d Type: TCP  ITT: %(itt_thin1)s  ", "order": 2, "append": False },
                        "top_title2": { "title_def": " Queue: %(queue_type)s (%(queue_len)s)  RTT: %(rtt)s ms", "order": 1  }, # Duration: %(duration)s min
                    },
        "pre_key_func": set_box_key_value,
    })

    def do_facet_grid(plot_conf=None, plot_box_dict=None, gp=None, **kw):
        if not "plot" in kw:
            return
        plot = kw["plot"]
        stream_properties = plot["streams"][plot["streams_id"]]
        yield ggplot2.facet_grid("loss ~ .", scales="free_y")
        return

    conf["box_conf"]["custom_ggplot2_plot_cmds"].update({"facet_grid": do_facet_grid})

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [3, 250],
                                            #"y_axis_lim" : [0, 20],
                                            #"y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": .1,
                                            "x_axis_main_ticks": [3, 51, 1], "x_axis_minor_ticks": 0.5,
                                            "y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                            "x_axis_args": {"expand": IntVector([0, 1])},
                                        })
    conf["plot_conf"]["theme_args"].update({"legend.position": "right",
                                            #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
                                            #"legend.title": r('element_text(size=20)'),
                                            "legend.text": r('element_text(size=20)'),
                                            "legend.key.size": r('unit(12, "mm")'),
                                            "axis.text.x": r('element_text(size = 20, hjust=0.5, vjust=0.5)'), # , color="grey50"
                                            #"axis.text.y": r('element_text(size = 15, hjust=0.0, vjust=0.5)'),
                                            "axis.title.x": r('element_text(size = 35, hjust = 0.5, vjust = -2, color="grey20")'),
                                            "axis.title.y": r('element_text(size = 35, hjust = 0.5, vjust = 3, color="grey20")'),
                                            "plot.title": r('element_text(size=30, lineheight=1, hjust=0.5, color="grey20")'), # , face="bold"
                                            #panel.margin": = unit(2, "lines"))
                                            "panel.margin": r('unit(0.8, "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                            "strip.text.y": r('element_text(size=20, angle=0, color="grey20")'),
                                            #theme(strip.text.x = element_text(size = 8, colour = "red", angle = 90))
                                            "plot.margin": r('unit(c(1, 0, 1.6, 1.6), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                        })

    conf["plot_conf"]["legend_columns"] = 1

    conf["paper_width"] = 30
    conf["paper_height"] = 3
    conf["paper_height"] = 20
    #conf["paper_height"] = 10
    conf["paper_width"] = 100

    conf["file_parse"].update({"func": custom_file_parse_func2 })
    return conf



import common_parse

def tcp_cwnd_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    #print "tcp_probe_file_parse_func"
    if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
    file_conf["group"] = file_conf["type_id"]
    file_conf["results"] = {}

    #file_conf["pre_results"] = { "setup_table": {"func": custom_pre_results_func}}

    common_parse.add_analysetcp_parsing(plot_conf, file_conf, pcap_file, parse_func_arg)

    if not "cong_control_thin1" in file_conf:
        file_conf["cong_control_thin1"] = "cubic"

    file_conf["color"] = "darkred" # Default color for dataset

    stream_properties = file_conf["streams"][file_conf["streams_id"]]

    stream_properties["meta_info"].update({"legend_count": 1,
                                           "smooth": False, "line": True,
                                           "factor": "stream",
                                           #"type_id_column": "stream", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                           "breaks_factor": "stream",  # These are for the legend
                                           "aes_args": { "color": 'factor(stream)', #"group": 'stream_id'
                                                     },
                                           #"aes_args": { "color": 'color_column', "group": 'color_column'} # These are for the general factoring of the data
                                       })

    #stream_properties.update({"stream_count": 10, "smooth": False, "line": True, "factor": "stream",
    #                          "type_id_column": "type_id" }) # Used to count the data points for each type

    #stream_properties["color_mapping"] = deepcopy([{"color_code": 'c(brewer.pal(9, "Blues")[7:7])', "legend_name": "Thin %(type_upper)s"}])
    #stream_properties["color_mapping"] = deepcopy([{"legend_name": "Thin %(type_upper)s"}])

    stream_type_to_ggplot_properties = {"r": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Blues")[7:7])', "legend_name": "Thin RDB"}]},
                                        "y": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                        "t": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                        "f": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                        "z": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Greedy"}]}}
    stream_properties["color_mapping"] = stream_type_to_ggplot_properties[file_conf["streams_id"]]["color_mapping"]


def cwnd_results_parse(conf, plot_box_dict, file_data, set_key):
    #print "tcp_probe_results_parse:", file_data["hostname"]
    if file_data["hostname"] == "fsender":
        common_parse.tcpinfo_results_parse(conf, plot_box_dict, file_data, set_key)
    else:
        tcp_probe_results_parse(conf, plot_box_dict, file_data, set_key)

def custom_cwnd_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if tcp_cwnd_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    util.dict_set_default(file_conf["results"], ("tcpinfo",
                                                 { "results_file": pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpinfo.txt")
                                               }))
    util.dict_set_default(file_conf["results"], ("tcpprobe",
                                                 {"results_file" : pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpproberdb.out")}))

#    def tcpinfo_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
#        print "tcpinfo_file_parse_func1"
#        if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
#            return False
#
#        print "file_conf:", file_conf.keys()
#
#
#        file_conf["prefix"] = "%s" % file_conf["data_file_name"]
#        #print "prefix:", file_conf["prefix"]
#        #print "pcap_file:", pcap_file
#
#        file_conf["results"] = {"tcpinfo": {}}
#        file_conf["results"]["tcpinfo"]["results_file"] = pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpinfo.txt")
#
#        #print "tcpinfo_file_parse_func2:", tcpinfo_file
#        #if tcpprobe.tcp_probe_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
#        #    return False
#        # Override group
#
#        #file_conf["group"] = "%s-%s" % (file_conf["packets_in_flight"], file_conf["dpif"])
#        #file_conf["plot_type"] = parse_func_arg
#        file_conf["tfrc"] = 1
#        file_conf["legend_order"] = file_conf["packets_in_flight"]
#        #print "Group:", file_conf["group"]
#        #file_conf["pre_results"] = { "setup_table": {"func": custom_pre_results_func}}


def get_freebsd_tcpinfo_conf():
    conf = get_conf()
    print "get_freebsd_tcpinfo_conf"

    def set_box_key_value(file_conf, arg):
        #group = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        #file_conf["group"] = group
        pass

    conf["box_conf"].update({ #"key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_QLEN:%(queue_len)s_LOSS:%(loss)s",
        "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_QLEN:%(queue_len)s",
                              "sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"],
                                                          x["stream_count_thin1"], x["group"], x["queue_len"], float(x["loss"])),
                              "sort_keys": ["thin_dupack", "linear_timeout", "stream_count_thin1", "group", "itt_thin1", "queue_len", "loss"],
                              #"box_title_def" : "Streams: %(stream_count_thin1)d Duration: %(duration)smin  Queue: %(queue_type)s (%(queue_len)s) Uniform loss: %(loss)s %%",
        "box_titles": { "main": { "title_def": "Streams: %(stream_count_thin1)d Type: TCP  ITT: %(itt_thin1)s  ", "order": 2, "append": True },
                        #"top_title2": { "title_def": " Queue: %(queue_type)s (%(queue_len)s)  RTT: %(rtt)s ms", "order": 1  }, # Duration: %(duration)s min
                    },
        "pre_key_func": set_box_key_value,
    })

    def do_facet_grid(plot_conf=None, plot_box_dict=None, gp=None, **kw):
        if not "plot" in kw:
            return
        plot = kw["plot"]
        stream_properties = plot["streams"][plot["streams_id"]]
        yield ggplot2.facet_grid("loss ~ .", scales="free_y")
        return

    conf["box_conf"]["custom_ggplot2_plot_cmds"].update({"facet_grid": do_facet_grid})

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [3, 250],
                                            #"y_axis_lim" : [0, 20],
                                            #"y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": .1,
                                            "x_axis_main_ticks": [3, 51, 1], "x_axis_minor_ticks": 0.5,
                                            "y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                            "x_axis_args": {"expand": IntVector([0, 1])},
                                        })
    conf["plot_conf"]["theme_args"].update({"legend.position": "right",
                                            #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
                                            #"legend.title": r('element_text(size=20)'),
                                            "legend.text": r('element_text(size=20)'),
                                            "legend.key.size": r('unit(12, "mm")'),
                                            "axis.text.x": r('element_text(size = 20, hjust=0.5, vjust=0.5)'), # , color="grey50"
                                            #"axis.text.y": r('element_text(size = 15, hjust=0.0, vjust=0.5)'),
                                            "axis.title.x": r('element_text(size = 35, hjust = 0.5, vjust = -2, color="grey20")'),
                                            "axis.title.y": r('element_text(size = 35, hjust = 0.5, vjust = 3, color="grey20")'),
                                            "plot.title": r('element_text(size=30, lineheight=1, hjust=0.5, color="grey20")'), # , face="bold"
                                            #panel.margin": = unit(2, "lines"))
                                            "panel.margin": r('unit(0.8, "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                            "strip.text.y": r('element_text(size=20, angle=0, color="grey20")'),
                                            #theme(strip.text.x = element_text(size = 8, colour = "red", angle = 90))
                                            "plot.margin": r('unit(c(1, 0, 1.6, 1.6), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                        })

    conf["plot_conf"]["legend_columns"] = 1

    conf["paper_width"] = 30
    conf["paper_height"] = 3
    conf["paper_height"] = 20
    #conf["paper_height"] = 10
    conf["paper_width"] = 100

    conf["file_parse"].update({"func": custom_cwnd_file_parse_func })
    conf["parse_results_func"] = cwnd_results_parse
    return conf



tcpprobe.get_conf = get_conf

if __name__ == "__main__":
    import sys
    plot_types = ["ryz", "rz", "ryz_tfrc", "netem_loss_tcp"]

    argparser = graph_base.make_parser()
    options = argparser.add_argument_group('TCP PROBE plot options')
    options.add_argument("-pt", "--plot-type",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, default=plot_types[0])

    defaults = {}
    args = graph_base.parse_args(argparser, defaults=defaults)
    plot_type = args.plot_type

    if plot_type == "rz":
        sys.argv.extend(["-tp", "RDBSENDER_ZSENDER_RDB_TFRC_tcpprobe_tfrc_ggplot.pdf",
                         "-od", "RDBSENDER_ZSENDER_RDB_TFRC",
                         #"-frn", ".*ps400.*",
                         #"-frm", ".*tfrc.*",
                         "-frm", "r5-rdb-itt1-ps400-ccrdb-da1-lt1-pif100-tfrc1_vs_z5..kbit5000_min5_rtt150_loss_pif100_qlen60_delayfixed_num0.*",
                         #"-frm", ".*z5.*min15.*",
                         "/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_TFRC/all_results/"
                     ])
    elif plot_type == "ryz":
        sys.argv.extend(["-tp", "RDBSENDER_YSENDER_ZSENDER_RDB_TFRC_tcpprobe_tfrc_ggplot.pdf",
                         "-od", "RDBSENDER_YSENDER_ZSENDER_RDB_TFRC",
                         #"-frn", ".*ps400.*",
                         #"-frm", ".*tfrc.*",
                         #"-frm", "r5-rdb-itt1-ps400-ccrdb-da1-lt1-pif100-tfrc1_vs_z5..kbit5000_min5_rtt150_loss_pif100_qlen60_delayfixed_num0.*",
                         #"-frm", ".*z5.*min15.*",
                         "/root/bendiko/pcap/rdbsender_ysender_zsender_to_zreceiver_with_rdb/RDBSENDER_YSENDER_ZSENDER_RDB_TFRC/all_results/"
                     ])
    elif plot_type == "ryz_tfrc":
        tcpprobe.get_conf = get_tfrc_conf
        defaults["tcp_probe_output_file"] = "RDBSENDER_YSENDER_ZSENDER_RDB_TFRC5_tcpprobe_tfrc_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_YSENDER_ZSENDER_RDB_TFRC5"
        defaults["file_regex_match"] = [".*r10-rdb-itt10:1.*da0-lt0-er3.*dpif10.*tfrc1.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_ysender_zsender_to_zreceiver_with_rdb/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5/all_results/"]

    elif plot_type == "netem_loss_tcp":
        tcpprobe.get_conf = get_tfrc_netem_conf
        defaults["tcp_probe_output_file"] = "CWND_NETEM_LOSS.pdf"
        defaults["output_dir"] = "CWND_NETEM_LOSS"
        #defaults["file_regex_match"] = [".*r10-rdb-itt10:1.*da0-lt0-er3.*dpif10.*tfrc1.*"]
        #defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_NETEM_RDB_LOSS_latency/all_results/"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_NETEM_TCPPROBE/all_results/"]

        #defaults["latency_output_file"] = "RDBSENDER_NETEM_RDB_LOSS_latency_CDF_ggplot_5min.pdf"
        #defaults["output_dir"] = "RDBSENDER_NETEM_RDB_LOSS"
        #defaults["file_regex_match"] = ["-frm", "r20.*itt50:\d+-.*da1-lt1-pif4.*min5.*"]
        #defaults["file_regex_match"] = ["-frm", "r20.*itt30.*loss0.5.*"]
        defaults["file_regex_match"] = [".*itt50.*"]
        defaults["file_regex_nomatch"] = [".*rdb-itt.*",
                                          #".*(loss2|loss5|loss1|loss10).*",
                                          #".*(loss0.5|loss2|loss5|loss1|loss10).*",
                                          #".*(loss0.2|loss0.5|loss2|loss5|loss1|loss10).*",
                                          ".*loss10.*",
            #".*min20.*", ".*pif100.*", ".*pif12.*",
                                          #".*loss10.*",
                                          #".*loss5.*",".*loss2.*",".*loss0.5.*",
                                          #".*itt100.*", ".*itt75.*",".*itt50.*",
        ]
    elif plot_type == "netem_loss_rdb_tfrc":
        tcpprobe.get_conf = get_tfrc_netem_conf
        defaults["tcp_probe_output_file"] = "CWND_NETEM_LOSS_TFRC.pdf"
        defaults["output_dir"] = "CWND_NETEM_LOSS_TFRC"
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_ZSENDER_TO_ZRECEIVER_NETEM_TCPPROBE_TFRCSP/all_results/"]
        defaults["file_regex_match"] = [".*itt50.*"]
        #defaults["file_regex_nomatch"] = [".*rdb-itt.*",
                                          #".*(loss2|loss5|loss1|loss10).*",
        #                                  ".*loss10.*",
        #]
    elif plot_type == "netem_loss_rdb_tfrc5000":
        tcpprobe.get_conf = get_tfrc_netem_conf
        defaults["tcp_probe_output_file"] = "CWND_NETEM_LOSS_TFRC_5000.pdf"
        defaults["output_dir"] = "CWND_NETEM_LOSS_TFRC"
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_ZSENDER_TO_ZRECEIVER_NETEM_TCPPROBE_TFRCSP_5000/all_results/"]
        defaults["file_regex_match"] = [".*itt50.*"]
    elif plot_type == "netem_loss_rdb_tfrc50000":
        tcpprobe.get_conf = get_tfrc_netem_conf
        defaults["tcp_probe_output_file"] = "CWND_NETEM_LOSS_TFRC_50000.pdf"
        defaults["output_dir"] = "CWND_NETEM_LOSS_TFRC_50000"
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_ZSENDER_TO_ZRECEIVER_NETEM_TCPPROBE_TFRCSP_50000/all_results/"]
        defaults["file_regex_match"] = [".*itt50.*"]
    elif plot_type == "freebsd_cwnd":
        tcpprobe.get_conf = get_freebsd_tcpinfo_conf
        defaults["tcp_probe_output_file"] = "FREEBSD_CWND.pdf"
        defaults["output_dir"] = "FREEBSD_CWND"
        defaults["directories"] = ["/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEVIER_CWND/all_results/"]
        #defaults["file_regex_match"] = [".*itt50.*"]
    else:
        cprint("Invalid plot type: '%s'" % plot_type, "red")
        sys.exit()

    args = graph_base.parse_args(argparser, defaults=defaults)
    #print "Force:", args.force
    #print "args:", args
    graph_base.main()
