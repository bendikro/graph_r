#!/usr/bin/env python
# -*- coding: utf-8 -*-
from datetime import datetime
import argparse
import os, sys

import graph_base

from graph_r import *
from rpy2.robjects.lib import grid

from graph_default_bendik import get_default_conf
from graph_r_ggplot import do_ggplots, ggplot2, ggplot_cdf_box, ggplot_boxplot_box

from functools import partial
import data_tables
import latency
import common
import common_parse
from util import vprint

from latency import get_latency_conf as get_latency_conf_original
import throughput
import graph_default
import tcpprobe
graph_default.get_conf = get_default_conf

def custom_pre_results_func(conf, plot_box_dict, file_data, set_key):

    stream_properties = file_data["streams"][file_data["streams_id"]]

    if not "color_mapping" in stream_properties:
        stream_properties["color_mapping"] = deepcopy(graph_default.stream_type_to_ggplot_properties[file_data["streams_id"]]["color_mapping"])

    if file_data["hostname"] != "zsender":
        vprint("Results parse, using label: %s, color: %s" % (file_data["label"], file_data["color"]), v=3)
        stream_properties["color_mapping"][0]["legend_name"] = file_data["label"]
        stream_properties["color_mapping"][0]["colors"] = [file_data["color"]]

    if not "latex_test_setup_table" in conf["data_tables"]:
        data_tables.fetch_table_conf(table_func=data_tables.get_table_latex_test_setup_with_tfrc_and_tcp_mods, conf=conf, table_key="latex_test_setup_table")

    if file_data["plot_type"] == "Latency":

        file_data["fig_label"] = "%s-%s" % (conf["data_tables"]["latex_test_setup_table"]["path_prefix"], plot_box_dict["box_key"].replace("_", "-"))
        file_data["fig_label"] = file_data["fig_label"].replace("-Plot-type:Latency", "")

        if file_data["hostname"] == "rdbsender":
            common.do_stream_custom_table_data(file_data, plot_box_dict, {}, conf["data_tables"]["latex_test_setup_table"])

        stream_properties["aes_args"].update({"color": "factor(color_column)", "linetype": "factor(color_column)"})
        stream_properties["plot_args"].update({"geom": "smooth"})

def custom_results_parse(conf, plot_box_dict, file_data, set_key):
    #print "custom_results_parse: '%s' !!!!!!!!!!!!!!!!!!!!!!!!!!!!" % file_data["plot_type"]

    stream_properties = file_data["streams"][file_data["streams_id"]]

    if file_data["plot_type"] == "Throughput":
        throughput.throughput_results_parse(conf, plot_box_dict, file_data, set_key)
    if file_data["plot_type"] == "Goodput":
        throughput.throughput_results_parse(conf, plot_box_dict, file_data, set_key)
    elif file_data["plot_type"] == "Latency":
        latency.latency_results_parse(conf, plot_box_dict, file_data, set_key)

    if file_data["plot_type"] == "Latency":
        if not "table_latency_data" in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_latency_stats, conf=plot_box_dict, table_key="table_latency_data")

        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["ecdf_ggplot2_values"], plot_box_dict["data_tables"]["table_latency_data"])

    if file_data["plot_type"] == "Throughput":
        if "analysetcp_results_table" not in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_results_stats, conf=plot_box_dict, table_key="analysetcp_results_table")

        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], plot_box_dict["data_tables"]["analysetcp_results_table"], data_name="analysetcp")

        stream_properties["meta_info"]["aes_args"].update({"x": "stream_type", "color": 'factor(stream_type)', "color": "color_column" }) # "color": "factor(color_column)",

    if file_data["plot_type"] == "Goodput":
        if "test_results_itt_table" not in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_results_itt, conf=plot_box_dict, table_key="test_results_itt_table")

        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], plot_box_dict["data_tables"]["test_results_itt_table"], data_name="analysetcp")
        stream_properties["meta_info"]["aes_args"].update({"x": "stream_type", "color": 'factor(stream_type )', "color": "color_column" })

        if file_data["hostname"] == "fsender":
            common_parse.tcpinfo_results_parse(conf, plot_box_dict, file_data, set_key)
            tcp_data = file_data["tcpinfo_data"]
            pass
        else:
            only_changes = True
            if file_data["hostname"] != "zsender":
                only_changes = False
            tcp_data = tcpprobe.read_tcp_probe_output(file_data, only_changes=only_changes)
            #print "DATAF:\n", util.get_dataframe_head(tcpprobe_data)
            #print "tcpprobe_data:", dir(tcpprobe_data)
            vprint("tcp_data COLS:", tcp_data.names, v=3)

        cwnd_index = util.get_dataframe_col_index(tcp_data, "cwnd")
        if cwnd_index == -1:
            cprint("Failed to find index for column 'cwnd'", "red")

        # rx2 uses R style extracting which starts at index 1, so must increase column index
        cwnd_index += 1

        def value_func(x):
            d = int(round(float(x), 0))
            return d
        d = util.parse_r_column_summary(r["summary"](tcp_data.rx2(cwnd_index)), value_func=value_func)
        #print "D:", d
        #d["CWND"] = "%(Min)s/%(1st Qu)s/%(Median)s/%(Mean)s/%(3rd Qu)s/%(Max)s" % d
        d["CWND"] = "%(Mean)s/%(Min)s/%(1st Qu)s/%(Median)s/%(3rd Qu)s/%(Max)s" % d
        #print "CWND:", d["CWND"]
        file_data["tcpprobe_data"] = d
        vprint("tcpprobe_data:", d, v=3)
        #print "tcp probe data:", data
        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["tcpprobe_data"],
                                           plot_box_dict["data_tables"]["test_results_itt_table"],
                                           data_name="tcpprobe", append=True)


def box_key_func(plot_conf, plot_box_dict, file_data, arg):

    #print "box_key_func!:", arg

    #print "FILE:", file_data.keys()
    #print "FILE:", file_data

    #print "type_id:", file_data["type_id"]

    file_data["legend_order"] = file_data["packets_in_flight"]

    if file_data["type_id"] == "type_thick":
        #file_data["color"] = "red"
        color_key = "thick"
        file_data["label"] = "Greedy"
        file_data["legend_order"] = 100
    elif file_data["type_id"] == "type_thin2":
        #file_data["color"] = "firebrick1"
        color_key = "thin2"
        file_data["label"] = "TCP"
        #color_key = file_data["label"].lower()
        file_data["legend_order"] = -1
    else:
        #print "group:", file_data["group"]
        file_data["label"] = "TCP" if file_data["packets_in_flight"] == 0 else "%s PIF%s" % (file_data["type_thin1"].upper(), file_data["packets_in_flight"])
        #print "packets_in_flight:", file_data["packets_in_flight"]

        #file_data["legend_order"] = -1

        color_key = "rdb_pif%s" % file_data["group"]

        if file_data["thin_dupack"]:
            file_data["legend_order"] += 1

        if file_data["packets_in_flight"] == 0:
            if file_data["type_thin1"] == "tcp":
                file_data["color"] = graph_default.color_map["thin"]
                # This is TCP
                #file_data["color"] = graph_default.color_map["thin"]
                #file_data["label"] = ("TCP %s" % make_mods_label(file_data)).strip()
                #print "Label:", file_data["label"]
                #print "Keys:", graph_default.color_map.keys()

                #if file_data["thin_dupack"] != 0 or file_data["linear_timeout"] != 0 or file_data["early_retransmit"] != 0:
                #    file_data["color"] = graph_default.color_map["thin_no_mods"]
                color_key = (("%s %s" % (file_data["label"], util.make_mods_label(file_data))).strip()).lower()
                print "color_key:", color_key
                #color_key = file_data["label"].lower()
            else:
                # Dynamic PIF
                #print "Dynamic PIF!"
                #print "file_data:", file_data
                file_data["label"] = "RDB DPIF%s" % file_data["dpif"]
                #file_data["color"] = graph_default.color_map[file_data["label"]]
                color_key = file_data["label"].lower()
                #print "label:", file_data["label"]
                file_data["legend_order"] += file_data["dpif"]

            #file_data["color"] = graph_default.color_map[file_data["label"]]
            #print "Label: %s, Color: %s" % (file_data["label"], file_data["color"])

        elif color_key in graph_default.color_map:
            #file_data["color"] = graph_default.color_map[rdb_color_key]
            pass
        elif file_data["group"] == 100:
            color_key = "rdb_pif200"
        else:
            color_key = "default"
            cprint("Did not match any color groups! Using color: %s" % file_data["color"], "red")

    #print "color_key:", color_key
    #print "label1:", file_data["label"]
    file_data["label"] = ("%s %s" % (file_data["label"], util.make_mods_label(file_data))).strip()
    #print "label2:", file_data["label"]
    #color_key = file_data["label"].lower()
    file_data["color"] = graph_default.color_map[color_key]

    if arg == "Latency":
        plot_box_dict["plot_func"] = ggplot_cdf_box
    else:
        #print "SETTING THROUGHPUT CONF on file with plot type:", file_data["plot_type"]
        plot_box_dict["plot_func"] = ggplot_boxplot_box
        tp_conf = get_throughput_conf()
        #print "tp_conf:", tp_conf
        plot_box_dict["axis_conf"] = deepcopy(tp_conf["plot_conf"]["axis_conf"])
        plot_box_dict["axis_conf"]["x_axis_label_key"] = "stream_id"
        #print "plot_box_dict:", plot_box_dict.keys()
        #print "tp plot_and_table:", tp_conf["box_conf"]["plot_and_table"]
        if arg == "Throughput":
            #plot_box_dict["box_conf"]["plot_and_table"].update(tp_conf["box_conf"]["plot_and_table"])
            plot_box_dict["box_conf"]["plot_and_table"]["table_key"] = "analysetcp_results_table"
        else:
            #plot_box_dict["box_conf"]["plot_and_table"].update({ "table_key": "table_latency_data", "make_table": common.TABLE_ACTION.APPEND_TO_PLOT_BOX})
            #plot_box_dict["box_conf"]["plot_and_table"]["make_table"] = common.TABLE_ACTION.NO
            plot_box_dict["box_conf"]["plot_and_table"]["table_key"] = "test_results_itt_table"
            plot_box_dict["axis_conf"].update({"y_axis_title": { "title": "Goodput (Kbit/second aggregated over %(sample_sec)d second)" % plot_conf }})

    plot_box_dict["plot_type"] = file_data["plot_type"]
    #print "file:", file_data.keys()
    #print "type_thin1:", file_data["type_thin1"]

    #print "legend_order" file_data["legend_order"] = -1

    #print "type_id:%s, label: %s, cong: %s, color: %s" % (file_data["type_id"], file_data["label"], file_data["cong_control_thin1"], file_data["color"])
    #print "type_id:%s, label: %s, cong: %s, legend_order: %s" % (file_data["type_id"], file_data["label"], file_data["cong_control_thin1"], file_data["legend_order"])

#    util.dict_set_default(file_conf["results"], ("tcpprobe",
#                                                 {"results_file" : pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpproberdb.out")}))
#

def custom_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    vprint("custom_file_parse_func: '%s' !!!!" % parse_func_arg, v=3)
    if parse_func_arg == "Throughput":
        if throughput.throughput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False
    elif parse_func_arg == "Goodput":
        if throughput.goodput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False
        util.set_dict_defaults_recursive(file_conf, (("results", "tcpprobe"),
                                                     {"results_file": pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpproberdb.out")
                                                  }))
        util.dict_set_default(file_conf["results"], ("tcpinfo",
                                                     { "results_file": pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpinfo.txt")
                                                   }))
    elif parse_func_arg == "Latency":
        if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False
    else:
        cprint("Invalid arguement: %s" % parse_func_arg, "red")

    file_conf["itt_int_thin1"] = file_conf["itt_thin1"].split(":")[0]

    if file_conf["type_id"] == "type_thin2":
        #file_conf["early_retransmit"] = 3
        stream_properties = file_conf["streams"][file_conf["streams_id"]]
        #print "stream_properties:", stream_properties
        stream_properties["early_retransmit"] = 3

    file_conf["pre_results"] = { "setup_table": {"func": custom_pre_results_func}}

def custom_latency_process_results(conf, groups_list=None):
    #print "custom_latency_process_results"

    tconf = conf["data_tables"]["latex_test_setup_table"]
    #print "tconf:", tconf
    latency.write_latex_table_test_setup(tconf)

    if groups_list:
        common.write_latex_figures(conf, groups_list, conf["output_file"], path_prefix=tconf["path_prefix"])


def get_throughput_conf():
    conf = deepcopy(graph_default.get_conf())
    conf["plot_conf"]["axis_conf"].update({"y_axis_title": { "title": "Throughput (Kbit/second aggregated over %(sample_sec)d second)" % conf },
                                           "x_axis_title" : { "title": "TCP variation used for competing streams",
                                                              #"title": ["TCP variation used for competing streams", "THICK", "THIN", "RDB"], "adj": [0.2, 0.70, .8, .9],
                                                              "colors" : ["black", "darkred", "darkgoldenrod1", "darkblue"] },
                                           #"x_axis_lim" : [0, 5000],
                                           "y_axis_lim" : [0, 5000],
                                           "set_x_axis": False, "set_y_axis": False,
                                       })
    conf["sample_sec"] = 5
    conf["plot_conf"]["bandwidth_axis_scale"] = 1.1 # Multiplied with bandwidth to get y axis limit
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.APPEND_TO_PLOT_BOX, # common.TABLE_ACTION.NO
        #"make_table": common.TABLE_ACTION.NO,
                                               #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                               "heights": (.65, .00, .35),
                                           })
    return conf

def get_latency_conf(defaults=None):
    conf = get_latency_conf_original(defaults)
    #conf["defaults"] = defaults
    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(linear_timeout)s_Plot_type:%(plot_type)s",
                              "sort_key_func": lambda x: (x["thin_dupack"], x["linear_timeout"], x["stream_count_thin1"], x["stream_count_thick"], x["group"]),
                              "sort_keys": ["thin_dupack", "linear_timeout", "stream_count_thin1", "stream_count_thick", "group", "itt"],
                              "func" : box_key_func,
                              "latency_options": {"per_stream": False},
                              "custom_ggplot2_plotbox_cmds": [latency.thin_itt_mark_line_func_ggplot2], "show_box_title": True,
                              #"box_title_def" : "Thin Dupack: %(thin_dupack)s   Thin Linear Timeout: %(linear_timeout)s", # Bandwidth cap: %(bandwidth)s Kbit  Duration: %(duration)s min
                              #"box_title_def" : "Duration: %(duration)s min    ITT: %(itt)s ms   RTT: %(rtt)s ms",
                              "box_titles": { "main": { "title_def": "ITT: %(itt)s ms   RTT: %(rtt)s ms  QLEN: %(queue_len)s", "order": 2 },
                                              "top_title2": { "title_def": "Duration: %(duration)s min   TFRC: %(tfrc_str)s   Thin: %(stream_count_thin1)s, Greedy: %(stream_count_thick)s", "order": 1 },
                                          },
                          })

    conf["process_results"]["custom_write_tables"] = {"func": custom_latency_process_results}

    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    #conf["page_group_def"] = {
    #    "title": "%(stream_count_thin1)d Thin streams vs %(stream_count_thick)d Greedy streams - Duration: %(duration)s min\n"
    #    "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets, Cong: %(cong_control_thin1)s",
    #    "sort_keys": ["payload_thin1", "rtt", "stream_count_thin1"], "sort_key_func": lambda x: (x[0], x[1], x[2])}

    conf["plot_conf"].update({"n_columns": 2, "n_rows" : 2,
                              "plot_title" : { "title": "PLOT" },
                              "plot_func": ggplot_cdf_box,
                              "do_plots_func": do_ggplots,
                          })
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.APPEND_TO_PLOT_BOX,
                                               #"make_table": common.TABLE_ACTION.NO,
                                               #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                               "table_key": "table_latency_data",
                                               "heights": (.65, .00, .35),
                                           })
    #conf["process_results"]["latency"] = {"func": latency.latency_process_results}

    #conf["box_conf"]["legend"].update({"legend_sort": { "do_sort": True, "sorted_args": {"key": lambda x: x["pif"], "reverse": False } } })
    conf["box_conf"]["legend"].update({#"legend_title": ggplot2.element_blank(), #"Streams",
        "legend_sort": { "do_sort": True, "sorted_args": { "key": lambda x: x["order"], "reverse": False }
                         #{}
                     }})

#        "legend_sort": { "do_sort": True, "sorted_args": {"key": lambda x: x["pif"], "reverse": False }}})

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [150, 700], "y_axis_lim" : [-0.05, 1.05],
                                            "y_axis_title" : { "title": "ECDF", },
                                            "x_axis_title" : { "title": "ACK Latency in milliseconds", },
                                            "y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": [0, 1.1, .1],
                                            "x_axis_main_ticks": [200, 800, 100], "x_axis_minor_ticks": 50,
                                            "y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                            #"x_axis_label_key": "label",
                                        })

    conf["file_parse"].update({"func": custom_file_parse_func, "parse_func_arg": ["Latency"]})

    conf["paper_width"] = 10
    conf["paper_height"] = 6

    conf["paper_width"] = 14
    conf["paper_height"] = 10

    conf["parse_results_func"] = custom_results_parse
    conf["plot_conf"]["theme_args"].update({#"legend.position": FloatVector([0.87, 0.35]),
        "legend.background": r('element_rect(color="white", fill="white", size=0.1, linetype="solid")'),
        "legend.text": r('element_text(size=8)'),
        "legend.key.size": r('unit(6, "mm")'),
        #"legend.key": r('element_rect(fill="white")'),
        #"legend.key": r('element_rect(fill=alpha("blue", 0.4))'), #element_rect(fill=alpha('blue', 0.4)))
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
        "axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
        "axis.text.x": r('element_text(size = 6)'),
        #"plot.title": r('element_text(size=8, lineheight=1, vjust=1.1, hjust=0.9, color="grey20")'), # , face="bold"
        "plot.title": r('element_text(size=12, lineheight=1, hjust=0.5, color="grey20")'), # , face="old"
        "panel.margin": r('unit(0, "lines")'),
        #"panel.border": r('element_rect(fill=NA,color="darkred", size=0.5, linetype="solid")'),
        "legend.margin": r('unit(-4, "mm")'),
        #"plot.margin": r('rep(unit(0,"null"),4)'),
        "plot.margin": r('unit(c(0, 0.4, 0, 0.3), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
                                        })
    conf["plot_conf"]["bandwidth_axis_scale"] = 1.1 # Multiplied with bandwidth to get y axis limit

    if defaults:
        util.update_dict(defaults, conf)

    return conf

def get_latency_conf_with_throughput(defaults):
    conf = get_latency_conf(defaults)
    conf["file_parse"].update({"func": custom_file_parse_func,
                               "parse_func_arg": ["Latency", "Throughput", "Goodput"]})
                               #"parse_func_arg": ["Throughput"]})


    conf["box_conf"].update({ "sort_key_func": lambda x: (x["stream_count_thin1"], x["stream_count_thick"], x["itt_thin1"], x["group"], x["queue_len"], x["plot_type"]),
                              "sort_keys": ["thin_dupack", "linear_timeout", "stream_count_thin1", "stream_count_thick", "group", "itt_thin1", "plot_type", "queue_len"],
                          })

    conf["box_conf"]["box_titles"] = { "top_title": {"title_def" : "%(plot_type)s", "order": 0 },
                                       "main": { "title_def": "ITT: %(itt_thin1)s ms   RTT: %(rtt)s ms  QLEN: %(queue_len)sp  Payload: %(payload_thin1)s B", "order": 2 }, # DA: %(thin_dupack)s LT: %(linear_timeout)s ER: %(early_retransmit)s
                                       "top_title2": { "title_def": "Duration: %(duration)s min   TFRC: %(tfrc_str)s   Thin: %(stream_count_thin1)s   Greedy: %(stream_count_thick)s", "order": 1 },
    }

    #conf["box_conf"]["box_titles"]["main"]["order"] = 1

    #conf["box_conf"].update({ "box_title_def" : "Type: %(plot_type)s   Duration: %(duration)s min    ITT: %(itt)s ms   RTT: %(rtt)s ms, Greedy streams: %(stream_count_thick)s NUM:%(num)s" })
    conf["paper_width"] = 14
    conf["paper_height"] = 4.5
    conf["plot_conf"].update({"n_columns": 3, "n_rows" : 1 })

    conf["paper_width"] = 4.5
    conf["paper_height"] = 4
    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1 })

    #conf["process_results"] = None
    return conf


def get_latency_conf_with_throughput_separate_on_groups(defaults, conf_apply=None):
    conf = get_latency_conf_with_throughput(defaults)
    def custom_file_parse_func2(plot_conf, file_conf, pcap_file, parse_func_arg):
        if custom_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
            return False
        # Override group
        #file_conf["group"] = "%s-%s" % (file_conf["packets_in_flight"], file_conf["dpif"])
        #file_conf["plot_type"] = parse_func_arg
        #print "Group:", file_conf["group"]

    def set_box_key_value(file_conf, arg):
        #print "TFRC:", file_conf["tfrc"]
        group = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        file_conf["test_name"] = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        #print "set_box_key_value:", group
        file_conf["group"] = group
        #plot_box_dict["plot_type"] = file_data["plot_type"]
        #file_conf["hack"] = arg
        #print "Group!:", file_conf["group"]
        #print "file_conf:", file_conf.keys()
        #print "CONG:", file_conf["cong_control_thin2"]

    conf["plot_conf"]["theme_args"].update({ "legend.position": "top" })

    conf["box_conf"]["plot_and_table"].update({"heights": (.8, .03, .17)})


    #conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_DU%(thin_dupack)s_LT%(linear_timeout)s_Plot_type:%(plot_type)s",

    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_ER:%(early_retransmit)s_QLEN:%(queue_len)s_CONG2:%(cong_control_thin2)s_Plot_type:%(plot_type)s", # Plot_type:%(plot_type)s
                              "sort_key_func": lambda x: (x["num"], x["cong_control_thin2"], data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                                          x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"], x["plot_type"][2]),
                              "sort_keys": ["num", "cong_control_thin2", "thin_dupack", "linear_timeout", "early_retransmit", "stream_count_thin1", "stream_count_thick", "group", "itt_thin1", "plot_type", "queue_len"],
                              "pre_key_func": set_box_key_value,
                          })

    conf["plot_conf"]["theme_args"].update({
        #"legend.position": FloatVector([0.87, 0.35]),
        #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
        #"legend.text": r('element_text(size=6)'),
        #"legend.key.size": r('unit(5, "mm")'),
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
        "axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 0.9, color="grey20")'),
        })

    page_sort_keys = conf["box_conf"]["sort_keys"]
    page_sort_keys = list(filter(lambda x: x != "plot_type", page_sort_keys))
    #print "page_sort_keys:", page_sort_keys
    conf["page_group_def"] = {
        "title": "%(stream_count_thin1)d Thin streams vs %(stream_count_thick)d Greedy streams - Duration: %(duration)s min\n"
        "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets, Cong: %(cong_control_thin1)s",
        "sort_keys": page_sort_keys,
        #"sort_key_func": lambda x: x,
        #"sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
        #                            x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"]),
        "sort_key_func": lambda x: (x["stream_count_thin1"], data_tables.get_stdev(x["itt_thin1"], stdev=False), #x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                    x["stream_count_thick"], x["group"], x["queue_len"]),
    }

    if conf_apply:
        util.update_dict(conf_apply, conf)

    return conf

def add_subplot(plot_conf=None, plot_box_dict=None, gp=None, **kw):
    import rpy2.robjects.lib.ggplot2 as ggplot2
    print "add_subplot"
    plotset = kw.get("plotset", None)
    if not plotset:
        return


    data_var = { "host": [], "rdb_bytes": [] }
    Redundant_bytes = []

    #print "plot:", plot.keys()
    for plot in plotset:
        for v in ["type", "type_id", "stream_type"]:
            print "%s: %s" % (v, plot[v])
        #data = plot["ecdf_ggplot2_values"]
        print "analysetcp_data:", plot["analysetcp_data"]
        data_var["host"].append(plot["hostname"])
        data_var["rdb_bytes"].append(plot["analysetcp_data"]["Redundant bytes"])

    #return
    #data_var = { "Payload": colsum, "type": file_data["label"], "stream_type": file_data["stream_id"], "color_column": file_data["color"] }
    #if "legend_order" in file_data:
    #    data_var["order"] = file_data["legend_order"]
    #print "Set order:", data_var["order"]

    import rpy2.rlike.container as rlc
    od = rlc.OrdDict([('rdb_bytes', robjects.IntVector(data_var["rdb_bytes"])),
                      ('host', robjects.StrVector(data_var["host"]))])

    data = DataFrame(od) # plot["type"]: colsum

    #datasets = importr('datasets')
    #mtcars = datasets.__rdata__.fetch('mtcars')['mtcars']
    #data = mtcars
    #print "mtcars:\n", mtcars

    #barplot_dataframe
    #print "TEST:"
    print "DATA:\n", data

    print "hosts:", len(data_var["host"])
    print "hosts:", len(robjects.StrVector(data_var["host"]))
    #data.names = robjects.StrVector(data_var["host"])
    #data.cbind(data, rownames=data_var["host"])
    #dataframe = r.cbind(dataframe, color_column=file_data["color"])

    #+ ggplot2.ggplot2.geom_bar(**plot_args)

    #vp <- viewport(width = 0.4, height = 0.4, x = 0.8, y = 0.2)

    #aes_args.update(meta_info.get("aes_args", {}))
    #aes_args["x"] = plot.get("y_column_name", 'time')
    aes_args = {}
    #aes_args["x"] = "factor(host)"
    aes_args["x"] = "factor(host)"
    aes_args["y"] = "factor(rdb_bytes)"
    #aes_args["y"] = "rdb_bytes"

    #aes_args = {"x": "cyl"} # , "color": "factor(cyl)"   , "y": "cyl"
    #aes_args = {}
    #plot_args = { "mapping": ggplot2.aes_string(**aes_args)} # , "binwidth": 1
    plot_args = { "data": data, "stat": "identity", "mapping": ggplot2.aes_string(**aes_args)}
    #sub = ggplot2.ggplot(mtcars, ggplot2.aes_string(**{"color": "factor(cyl)"})) + ggplot2.ggplot2.geom_bar()

    gp2 = ggplot2.ggplot(data)
    gp2 = (gp2 + ggplot2.ggplot2.geom_bar(**plot_args))
    #gp2 = ggplot2.ggplot(**plot_args)
    #gp2 = ggplot2.ggplot(**plot_args)

    gp2 += ggplot2.scale_x_discrete(
        #breaks=StrVector(breaks),
        #breaks=StrVector(test_ids),
        # When x_axis_label_key is set, labels and limits will differ
        labels=StrVector(data_var["host"]),
        limits=StrVector(data_var["host"]),
    )

    #g = ggplotGrob(qplot(1, 1))
    #if plot["group"] != 0:
    #    return
    #p + annotation_custom(grob = g, xmin = 2, xmax = 4, ymin = 6, ymax = 10)
    grob = ggplot2.ggplot2.ggplotGrob(gp2)
    anno = ggplot2.ggplot2.annotation_custom(grob, xmin=400, xmax=700, ymin=0, ymax=0.5)
    yield anno


def latency_barplots_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if util.cmd_args.verbose > 2:
        print "latency_barplots_file_parse_func: '%s'" % parse_func_arg

    if throughput.throughput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
    if throughput.goodput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
        file_conf["tcp_probe_data_file"] = pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpproberdb.out")
    if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    #if file_conf["type_id"] == "type_thin2":
    #    #file_conf["early_retransmit"] = 3
    #    stream_properties = file_conf["streams"][file_conf["streams_id"]]
    #    #print "stream_properties:", stream_properties
    #    stream_properties["early_retransmit"] = 3

    #file_conf["pre_results"] = { "setup_table": {"func": custom_pre_results_func}}

def get_latency_conf_with_barplots(defaults, conf_apply=None):
    conf = get_latency_conf_with_throughput(defaults)
    conf["file_parse"].update({"func": latency_barplots_file_parse_func,
                               "parse_func_arg": ["Latency"]})
                               #"parse_func_arg": ["Throughput"]})

    def set_box_key_value(file_conf, arg):
        #print "TFRC:", file_conf["tfrc"]
        group = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        file_conf["test_name"] = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        #print "set_box_key_value:", group
        file_conf["group"] = group
        #plot_box_dict["plot_type"] = file_data["plot_type"]
        #file_conf["hack"] = arg
        #print "Group!:", file_conf["group"]
        #print "file_conf:", file_conf.keys()
        #print "CONG:", file_conf["cong_control_thin2"]

    conf["process_results"] = None #["custom_write_tables"] = {"func": custom_latency_process_results}
    conf["plot_conf"]["theme_args"].update({ "legend.position": "top" })
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO})
    #conf["box_conf"]["plot_and_subplot"].update({"action": common.SUBPLOT_ACTION.NO })
    conf["box_conf"]["custom_ggplot2_plotset_cmds"].update({"add_subplot": add_subplot})

    #print "GETCONF custom_ggplot2_plot_cmds:", conf["box_conf"]["custom_ggplot2_plot_cmds"]

    #conf["box_conf"]["custom_ggplot2_plot_cmds"].update({"itt_lines": functools.partial(thin_itt_mark_line_func_ggplot2, count=6)})

    #conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_DU%(thin_dupack)s_LT%(linear_timeout)s_Plot_type:%(plot_type)s",

    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_ER:%(early_retransmit)s_QLEN:%(queue_len)s_CONG2:%(cong_control_thin2)s_Plot_type:%(plot_type)s", # Plot_type:%(plot_type)s
                              "sort_key_func": lambda x: (x["num"], x["cong_control_thin2"], data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                                          x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"], x["plot_type"][2]),
                              "sort_keys": ["num", "cong_control_thin2", "thin_dupack", "linear_timeout", "early_retransmit", "stream_count_thin1", "stream_count_thick", "group", "itt_thin1", "plot_type", "queue_len"],
                              "pre_key_func": set_box_key_value,
                          })

    conf["plot_conf"]["theme_args"].update({
        #"legend.position": FloatVector([0.87, 0.35]),
        #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
        #"legend.text": r('element_text(size=6)'),
        #"legend.key.size": r('unit(5, "mm")'),
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
        "axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 0.9, color="grey20")'),
        })

    page_sort_keys = conf["box_conf"]["sort_keys"]
    page_sort_keys = list(filter(lambda x: x != "plot_type", page_sort_keys))
    #print "page_sort_keys:", page_sort_keys
    conf["page_group_def"] = {
        "title": "%(stream_count_thin1)d Thin streams vs %(stream_count_thick)d Greedy streams - Duration: %(duration)s min\n"
        "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets, Cong: %(cong_control_thin1)s",
        "sort_keys": page_sort_keys,
        #"sort_key_func": lambda x: x,
        #"sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
        #                            x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"]),
        "sort_key_func": lambda x: (x["stream_count_thin1"], data_tables.get_stdev(x["itt_thin1"], stdev=False), #x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                    x["stream_count_thick"], x["group"], x["queue_len"]),
    }

    conf["paper_width"] = 8
    conf["paper_height"] = 7

    if conf_apply:
        util.update_dict(conf_apply, conf)

    return conf

def latency_merged_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    vprint("latency_barplots_file_parse_func: '%s'" % parse_func_arg, v=3)

    ##if throughput.throughput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
    #   return False
    #if throughput.goodput_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
    #    return False
    #    file_conf["tcp_probe_data_file"] = pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpproberdb.out")
    vprint("host:", file_conf["hostname"], v=3)

    if file_conf["hostname"] == "zsender":
        return False

    file_conf["itt_int_thin1"] = file_conf["itt_thin1"].split(":")[0]

    if file_conf["type_thin1"] == "tcp" and file_conf["hostname"] == "rdbsender":
        return False

    if file_conf["type_thin1"] == "tcp" and file_conf["hostname"] == "wsender":
        file_conf["color"] = "red"
        file_conf["label"] = "TCP-reference"

    #latency.stream_type_to_ggplot_properties = {"r": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "PuRd")[7:7])', "legend_name": "Thin %(type_upper)s"}]},
    #                                            "y": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
    #                                            "t": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
    #                                            "w": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
    #                                            "z": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Greedy"}]},
    #                                            "g": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Greedy"}]}}

    if latency.latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False

    stream_properties = file_conf["streams"][file_conf["streams_id"]]
    #stream_properties["aes_args"].update({"color": "factor(color_column)", "linetype": "factor(color_column)"})

    file_conf["default_meta_info"] = False
    stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
                                           "factor": "id_column",
                                           #"breaks_factor": "color_column",  # These are for the legend
                                           "breaks_factor": "id_column",  # These are for the legend
                                           "aes_args": {
                                               #"size": "1",
                                               #"color": 'color_column',
                                               #"linetype": "color_column",
                                               #"color": 'id_column',
                                               #"linetype": "id_column",
                                               #"color": 'factor(color_column)',
                                               #"linetype": "factor(color_column)",
                                               "color": 'factor(id_column)',
                                               "linetype": "factor(id_column)",
                                               #"group": 'color_column',
                                               #"group": 'factor(color_column)',
                                                     } # These are for the general factoring of the data
                                       })

    if file_conf["type_thin1"] == "tcp" and file_conf["hostname"] == "wsender":
        stream_properties["color_mapping"] = deepcopy(graph_default.stream_type_to_ggplot_properties[file_conf["streams_id"]]["color_mapping"])
        stream_properties["color_mapping"][0].update({"legend_name": "TCP Reference", #
                                                      #"color_code": 'c(brewer.pal(9, "Reds")[7:7])'
                                                      "colors": ["coral2"]
                                                  })

    #util.print_keys(file_conf, keys=["type_thin1", "hostname"], prefix="SATAN")
    #if file_conf["type_thin1"] == "rdb" and file_conf["hostname"] == "rdbsender":
    #    stream_properties["color_mapping"] = deepcopy(latency.stream_type_to_ggplot_properties[file_conf["streams_id"]]["color_mapping"])
    #    stream_properties["color_mapping"][0].update({"legend_name": "TCP RDB!", #
    #                                                  #"color_code": 'c(brewer.pal(9, "Reds")[7:7])'
    #                                                  "colors": ["red"]
    #                                              })

def get_latency_conf_with_tcp_thin_together_with_rdb_test(defaults, conf_apply=None, conf_apply_split="."):
    conf = get_latency_conf_with_throughput(defaults)
    conf["file_parse"].update({"func": latency_merged_file_parse_func,
                               "parse_func_arg": ["Latency"]})
                               #"parse_func_arg": ["Throughput"]})

    def set_box_key_value(file_conf, arg):
        #print "TFRC:", file_conf["tfrc"]
        #group = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        group = "PIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["tfrc"])
        file_conf["test_name"] = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        #print "set_box_key_value:", group
        file_conf["group"] = group
        file_conf["dpif"] = 10
        #print "file_conf:", file_conf.keys()

        #for k in ["group", "payload_thin1", "num", "cong_control_thin2", "plot_type", "cong_control_thin1", "type_thin1", "type", "type_id"]:
        #    print "%s: %s" % (k, file_conf[k])

    def custom_latency_merged_results_parse(conf, plot_box_dict, file_conf, set_key):
        #print "custom_latency_merged_results_parse"
        #print "PLOT TYPE:", file_conf["plot_type"]
        custom_results_parse(conf, plot_box_dict, file_conf, set_key)
        #util.print_keys(file_conf)
        #stream_id = "%svs%s" % (file_conf["type_thin1"], file_conf["type_thin2"])
        stream_id = "%s" % (file_conf["type"])
        #print "type_thin1:", file_conf.get("type_thin1", "SKROT")
        #print "type_thin2:", file_conf.get("type_thin2", "SKROT")

        #stream_id = file_conf
        #if file_conf["type_thin1"] == "tcp" and file_conf["hostname"] == "wsender":
        #    stream_id = "tcp-ref"
        dataf = file_conf["ecdf_ggplot2_values"]
        #file_conf["ecdf_ggplot2_values"] = dataf = r.cbind(dataf, id_column=stream_id)
        #print "DATAF:", type(dataf)
        #print util.get_dataframe_head(dataf)

    def box_key_func_latency_merged(plot_conf, plot_box_dict, file_data, arg):
        box_key_func(plot_conf, plot_box_dict, file_data, arg)
        file_data["bundling_limit"] = "Unlimited"
        file_data["bundling_limit"] = "∞"

        if plot_conf.get("experiment_id") == "E3":
            file_data["bundling_limit"] = "1"

        if file_data["type_thin1"] == "tcp" and file_data["hostname"] == "wsender":
            file_data["color"] = "coral2"
        if file_data["type_thin1"] == "rdb" and file_data["hostname"] == "rdbsender":
            file_data["color"] = "red"

        # COLOR
        util.dict_set_default(plot_box_dict["legends"], ("color", {"guide_args": {}, "scale": {"func": ggplot2.scale_colour_manual, "args": {}}}))
        colorleg = plot_box_dict["legends"]["color"]
        colorleg["guide_args"].update({"order": 1})
        color_args = colorleg["scale"]["args"]
        util.dict_set_default(color_args, [("labels", []), ("values", [])])
        color_args["name"] = ggplot2.element_blank()

        colorleg["guide_args"].update({"override.aes": {"linetype": [1, 1, 2]}})

        color_args["labels"] = ['RDB', 'TCP', 'TCP Reference']
        color_args["labels"] = ['RDB (10)', 'TCP (10)                         ', 'TCP Reference (20)']
        color_args["values"] = ['#A52A2A', 'darkgreen', 'blue']
        color_args["breaks"] = ['rdb', 'tcp', 'tcp-ref']

        #   util.dict_set_default(plot_box_dict["legends"], ("color", {"guide_args": {}, "scale": {"func": ggplot2.scale_colour_manual, "args": {}}}))
        #   colorleg = plot_box_dict["legends"]["color"]
        #   colorleg["guide_args"].update({"order": 1})
        #   color_args = colorleg["scale"]["args"]
        #   util.dict_set_default(color_args, [("labels", []), ("values", [])])
        #   color_args["name"] = ggplot2.element_blank()
        #   colorleg["guide_args"].update({"override.aes": {"linetype": [1, 1, 2]}})
        #   color_args["labels"] = ['RDB', 'ISOCH', 'Greedy']
        #   color_args["values"] = ['darkgreen', 'blue', '#A52A2A']
        #   color_args["breaks"] = ['RDB', 'ISOCH', 'Greedy']

        #gp += ggplot2.scale_linetype_manual(values=StrVector(["solid", "solid", "dashed"]),
        #                                    guide=False)

        util.dict_set_default(plot_box_dict["legends"], ("linetype", {"guide_args": {}, "scale": {"func": ggplot2.scale_linetype_manual, "args": {}}}))
        linetype = plot_box_dict["legends"]["linetype"]
        #util.dict_set_default(linetype["guide_args"], ("override.aes", {"colour": []}))
        #linetype["guide_args"].update({"order": 2, "show": True})
        linetype_args = linetype["scale"]["args"]
        util.dict_set_default(linetype_args, [("values", [])])
        #linetype_args["name"] = ggplot2.element_blank()
        #linetype_args["values"] = ["solid", "longdash", "twodash"]
        #linetype_vals = StrVector(["solid", "solid", "longdash"])
        #linetype_vals.names = ['rdb', 'tcp', 'tcp-ref']
        #linetype_args["values"] = linetype_vals
        #linetype_args["values"] = StrVector(["solid", "solid", "longdash"])
        linetype_args["values"] = util.make_r_named_list(color_args["breaks"], ["solid", "solid", "longdash"])

        linetype_args["guide"] = False
        #linetype_args["breaks"] = ['rdb', 'tcp', 'tcp-ref']
        #linetype_args["labels"] = ['RDB SPIFL 3', 'RDB SPIFL 7', 'TCP Reference']
        #linetype["guide_args"].update({"order": 2, "show": False})


        ### LINETYPE
        ##util.dict_set_default(plot_box_dict["legends"], ("linetype", {"guide_args": {}, "scale": {"func": ggplot2.scale_linetype_manual, "args": {}}}))
        ##linetype = plot_box_dict["legends"]["linetype"]
        ##linetype["guide_args"].update({"order": 2, "show": True})
        ##
        ##linetype_args = linetype["scale"]["args"]
        ##util.dict_set_default(linetype_args, [("values", [])])
        ##linetype_args["name"] = ggplot2.element_blank()
        ##
        ###linetype_args["values"] = ["solid", "longdash", "twodash"]
        ##linetype_vals = StrVector(["solid", "solid", "longdash"])
        ##linetype_vals.names = ['rdb', 'tcp', 'tcp-ref']
        ##linetype_args["values"] = linetype_vals
        ###linetype_args["breaks"] = ['rdb', 'tcp', 'tcp-ref']
        ###linetype_args["labels"] = ['RDB SPIFL 3', 'RDB SPIFL 7', 'TCP Reference']


        #linetype_args["labels"] = ['RDB', 'TCP Reference', 'TCP']
        #linetype_args["breaks"] = ['tcp-ref', 'tcp', 'rdb']
        #linetype_args["values"] = ["dotted", "longdash", "solid"]
        #color_args["values"] = ['red', 'coral2', 'lightblue']

        #color_args["labels"] = ['RDB', 'TCP Reference', 'TCP']
        #color_args["values"] = ['#A52A2A', 'blue', 'darkgreen']
        #linetype_args["values"] = ["solid", "twodash", "longdash" ]
        #color_args["breaks"] = ['rdb', 'tcp-ref', 'tcp']


        #linetype_args["breaks"] = ['tcp-ref', 'tcp', 'rdb']

        #scale_fill_discrete(guide=FALSE)

        #color_args["breaks"] = ['tcp-ref', 'tcp', 'rdb']
        #plot_box_dict["legends"]["linetype"] = False

        #linetype = plot_box_dict["legends"]["linetype"]
        #linetype["guide_args"].update({"order": 1, "title": ""})


        #colorleg["name"] = ggplot2.element_blank()
        #colorleg["labels"] = ['TCP Reference', 'Thin TCP', 'Thin RDB']
        #colorleg["values"] = ['coral2', 'lightblue', 'red']

        #colors: ['coral2', '#238B45', '#2171B5']
        #breaks: ['coral2', 'lightblue', 'red']
        #labels: ['TCP Reference', 'Thin TCP', 'Thin RDB']

        #scale_args["name"] = ggplot2.element_blank()
        #scale_args["labels"].extend(["ITT intervals"])
        #scale_args["values"].extend(["dotted"])

        #util.dict_set_default(plot_box_dict["legends"], ("linetype", {"guide_args": {}, "scale": {"func": ggplot2.scale_linetype_manual, "args": {}}}))
        #util.dict_set_default(plot_box_dict["legends"], ("fill", {"guide_args": {} }))
        #linetype = plot_box_dict["legends"]["linetype"]
        #linetype["guide_args"].update({"order": 1})
        #util.dict_set_default(linetype["guide_args"], ("override.aes", {"colour": []}))
        ##util.dict_set_default(linetype["guide_args"]["override.aes"], ("colour", []))
        #linetype["guide_args"]["override.aes"]["colour"].append('black')
        #
        #scale_args = linetype["scale"]["args"]
        #util.dict_set_default(scale_args, [("labels", []), ("values", [])])
        #scale_args["name"] = ggplot2.element_blank()
        #scale_args["labels"].extend(["ITT intervals"])
        #scale_args["values"].extend(["dotted"])
        ## Handle colour legend
        #util.dict_set_default(plot_box_dict["legends"], ("colour", {"guide_args": {}, "scale": False }))
        #fill = plot_box_dict["legends"]["fill"]
        #colour = plot_box_dict["legends"]["colour"]
        #colour["guide_args"].update({"order": 2 }) #"override.aes": r('list(linetype=c(2, 1), fill=NA)')


    conf["parse_results_func"] = custom_latency_merged_results_parse


    def results_parse_hook(conf, box_conf, file_data, set_key):
        #print "results_parse_hook"
        stream_id = "%s" % (file_data["type"])
        if file_data["type_thin1"] == "tcp" and file_data["hostname"] == "wsender":
            stream_id = "tcp-ref"
        file_data["ecdf_ggplot2_values"] = r.cbind(file_data["ecdf_ggplot2_values"], id_column=stream_id)

    conf["results_parse_hook"] = results_parse_hook

# "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_ER:%(early_retransmit)s_QLEN:%(queue_len)s_CONG2:%(cong_control_thin2)s_Plot_type:%(plot_type)s", # Plot_type:%(plot_type)s

        #plot_box_dict["plot_type"] = file_data["plot_type"]
        #file_conf["hack"] = arg
        #print "Group!:", file_conf["group"]
        #print "file_conf:", file_conf.keys()
        #print "CONG:", file_conf["cong_control_thin2"]

    conf["process_results"] = None
    #conf["process_results"]["custom_write_tables"] = {"func": custom_latency_merged_process_results}
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO})

    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_PIFG:%(group)s_DU%(thin_dupack)s_LT%(linear_timeout)s_ER:%(early_retransmit)s_QLEN:%(queue_len)s_CONG2:%(cong_control_thin2)s_Plot_type:%(plot_type)s", # Plot_type:%(plot_type)s
                              "sort_key_func": lambda x: (x["num"], x["cong_control_thin2"], data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                                          x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"], x["plot_type"][2]),
                              "sort_keys": ["num", "cong_control_thin2", "thin_dupack", "linear_timeout", "early_retransmit", "stream_count_thin1", "stream_count_thick", "group", "itt_thin1", "plot_type", "queue_len"],
                              "pre_key_func": set_box_key_value,
                              #"func" : box_key_func,
                              "func" : box_key_func_latency_merged,
                          })

    page_sort_keys = conf["box_conf"]["sort_keys"]
    page_sort_keys = list(filter(lambda x: x != "plot_type", page_sort_keys))
    #print "page_sort_keys:", page_sort_keys
    conf["page_group_def"] = {
        "title": "%(stream_count_thin1)d Thin streams vs %(stream_count_thick)d Greedy streams - Duration: %(duration)s min\n"
        "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets, Cong: %(cong_control_thin1)s",
        "sort_keys": page_sort_keys,
        #"sort_key_func": lambda x: x,
        #"sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
        #                            x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"]),
        "sort_key_func": lambda x: (x["stream_count_thin1"], data_tables.get_stdev(x["itt_thin1"], stdev=False), #x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                    x["stream_count_thick"], x["group"], x["queue_len"]),
    }

    conf["box_conf"]["box_titles"] = { #"top_title": {"title_def" : "%(plot_type)s", "order": 0 },
        "main": { "title_def": "RDB: %(stream_count_thin1)s   TCP: %(stream_count_thin1)s   ITT: %(itt_int_thin1)s ms   Seg. size: %(payload_thin1)s B   DPIF: %(dpif)s   Rdn. level:  %(bundling_limit)s", "order": 1 },
    }

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [150, 650], "y_axis_lim" : [-0.05, 1.05],
                                            "y_axis_title" : { "title": "Cumulative Distribution Function", },
                                            "x_axis_title" : { "title": "ACK latency per segment (ms)", },
                                            "y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": [0, 1.1, .1],
                                            "x_axis_main_ticks": [200, 800, 100], "x_axis_minor_ticks": 50,
                                            "y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                            #"x_axis_label_key": "label",
                                        })
    conf["paper_width"] = 4.5
    conf["paper_height"] = 3

    conf["plot_conf"]["theme_args"].update({
        "legend.position": "top",
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0.3, color="grey20")'),
        "axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 0.9, color="grey20")'),
        "plot.margin": r('unit(c(0, 0.5, 0, 0.1), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        "axis.ticks": r('element_line(colour="black")'),
        "panel.grid.major": r('element_line(colour="grey40", size=0.2, linetype="dashed")'),
        "panel.grid.minor": r('element_line(colour="grey40", size=0.1, linetype="dashed")'),
        "panel.background": ggplot2.element_blank(),
        "plot.title": r('element_text(size=8, lineheight=1, hjust=0.5, color="black")'), # , face="old"
        #"axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
        "axis.text.x": r('element_text(size = 8, color="black")'),
        "axis.text.y": r('element_text(size = 7, color="black")'),
        "axis.line": r('element_line(colour="grey20", size=0.4)'), #
        #"axis.line.x": line along x axis (element_line; inherits from axis.line)
        #"axis.line.y": r('element_line(colour="grey20", size=0.5)'), #
        "legend.key": r('element_rect(fill=NA,color="black", size=0.1, linetype="solid")'),
        #"legend.background": r('element_rect(fill=NA,color="darkred", size=0.5, linetype="solid")'),
                                        })
    conf["plot_conf"]["bandwidth_axis_scale"] = 1.1 # Multiplied with bandwidth to get y axis limit

    #conf["paper_width"] = 7
    #conf["paper_height"] = 4.5
    #
    attrs = {}
    attrs["grid_major_c"] = "grey80"
    attrs["grid_minor_c"] = "grey80"
    attrs["grid_major_c"] = "#808080"
    attrs["grid_minor_c"] = attrs["grid_major_c"] = "#b5b5b5"
    attrs["grid_minor_c"] = attrs["grid_major_c"] = "#808080"
    attrs["grid_major_s"] = "0.3"
    #attrs["grid_minor_s"] = "0.1"
    attrs["grid_minor_s"] = "0.2"
    # "solid", "dashed", "dotted", "dotdash", "longdash", and "twodash".
    attrs["grid_major_t"] = "solid"
    #attrs["grid_major_t"] = "dotted"
    attrs["grid_minor_t"] = "dotted"
    attrs["grid_minor_t"] = "solid"
    #attrs["grid_minor_t"] = "solid"
    attrs["axis_line_s"] = "0.5"
    #
    attrs["ticks_c"] = "grey30"
    attrs["axis_line_c"] = attrs["ticks_c"]
    attrs["axis_text_c"] = "grey1"
    attrs["axis_title_c"] = "grey1"
    attrs["plot_title_c"] = "black"
    attrs["legend_key_c"] = "black"
    attrs["legend_border"] = "white"
    #
    conf["plot_conf"]["theme_args"].update({
        "plot.margin": r('unit(c(0, 0.1, 0, 0.1), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
        "axis.line": r('element_line(size=%(axis_line_s)s, colour="%(axis_line_c)s")' % attrs), #
        "axis.ticks": r('element_line(size=0.4, colour="%(ticks_c)s")' % attrs),
        "axis.ticks.length": r('unit(1.5, "mm")'),
        "panel.grid.major": r('element_line(colour="%(grid_major_c)s", size=%(grid_major_s)s, linetype="%(grid_major_t)s")' % attrs),
        "panel.grid.minor": r('element_line(colour="%(grid_minor_c)s", size=%(grid_minor_s)s, linetype="%(grid_minor_t)s")' % attrs),
        "plot.title": r('element_text(size=8, lineheight=1, hjust=0.5, color="%(plot_title_c)s")' % attrs), # , face="old"
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0.3, color="%(axis_title_c)s")' % attrs),
        "axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 0.9, color="%(axis_title_c)s")' % attrs),
        "axis.text.x": r('element_text(size = 8, color="%(axis_text_c)s")' % attrs),
        "axis.text.y": r('element_text(size = 7, color="%(axis_text_c)s")' % attrs),
        #"axis.line.x": line along x axis (element_line; inherits from axis.line)
        #"axis.line.y": r('element_line(colour="grey20", size=0.5)'), #
        "legend.key": r('element_rect(fill=NA, size=0.1, linetype="solid", color="%(legend_key_c)s")' % attrs),
        "legend.key.size": r('unit(6, "mm")'),
        "legend.text.align": 0,
        "legend.text": r('element_text(size=8, color="%(axis_title_c)s")' % attrs), # hjust = 0.5, vjust = 0.3,
        "legend.background": r('element_rect(fill="white", color="%(legend_border)s", size=0.3, linetype="solid")' % attrs),
    })
    #conf["plot_conf"]["plot_args"] = {"size": 0.8 }

    #conf["paper_width"] = 7
    #conf["paper_height"] = 4.5
    #
    #attrs = {}
    #attrs["grid_major_c"] = "grey80"
    #attrs["grid_minor_c"] = "grey80"
    #attrs["grid_major_c"] = "#808080"
    #attrs["grid_minor_c"] = attrs["grid_major_c"] = "#b5b5b5"
    #attrs["grid_minor_c"] = attrs["grid_major_c"] = "#808080"
    #attrs["grid_major_s"] = "0.3"
    ##attrs["grid_minor_s"] = "0.1"
    #attrs["grid_minor_s"] = "0.2"
    ## "solid", "dashed", "dotted", "dotdash", "longdash", and "twodash".
    #attrs["grid_major_t"] = "solid"
    ##attrs["grid_major_t"] = "dotted"
    #attrs["grid_minor_t"] = "dotted"
    #attrs["grid_minor_t"] = "solid"
    #
    ##attrs["grid_minor_t"] = "solid"
    #attrs["axis_line_s"] = "0.5"
    #
    #attrs["ticks_c"] = "grey30"
    #attrs["axis_line_c"] = attrs["ticks_c"]
    #attrs["axis_text_c"] = "grey1"
    #attrs["axis_title_c"] = "grey1"
    #attrs["plot_title_c"] = "black"
    #attrs["legend_key_c"] = "black"
    #attrs["legend_border"] = "white"
    #
    #conf["plot_conf"]["theme_args"].update({
    #    "plot.margin": r('unit(c(0, 1, 0, 0.1), "lines")'),  # 0=top, 1=right, 2=bottom, 3=left
    #    "axis.line": r('element_line(size=%(axis_line_s)s, colour="%(axis_line_c)s")' % attrs), #
    #    "axis.ticks": r('element_line(size=0.4, colour="%(ticks_c)s")' % attrs),
    #    "axis.ticks.length": r('unit(2.5, "mm")'),
    #    "panel.grid.major": r('element_line(colour="%(grid_major_c)s", size=%(grid_major_s)s, linetype="%(grid_major_t)s")' % attrs),
    #    "panel.grid.minor": r('element_line(colour="%(grid_minor_c)s", size=%(grid_minor_s)s, linetype="%(grid_minor_t)s")' % attrs),
    #    "plot.title": r('element_text(size=12, lineheight=1, hjust=0.5, color="%(plot_title_c)s")' % attrs), # , face="old"
    #    #"axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0.3, color="grey20")'),
    #    #"axis.title.y": r('element_text(size = 8, hjust = 0.4, vjust = 0.9, color="grey20")'),
    #    "axis.title.x": r('element_text(size = 12, hjust = 0.5, vjust = 0.3, color="%(axis_title_c)s")' % attrs),
    #    "axis.title.y": r('element_text(size = 12, hjust = 0.4, vjust = 0.9, color="%(axis_title_c)s")' % attrs),
    #    "axis.text.x": r('element_text(size = 13, color="%(axis_text_c)s")' % attrs),
    #    "axis.text.y": r('element_text(size = 11, color="%(axis_text_c)s")' % attrs),
    #    #"axis.line.x": line along x axis (element_line; inherits from axis.line)
    #    #"axis.line.y": r('element_line(colour="grey20", size=0.5)'), #
    #    "legend.key": r('element_rect(fill=NA, size=0.2, linetype="solid", color="%(legend_key_c)s")' % attrs),
    #    "legend.key.size": r('unit(9, "mm")'),
    #    "legend.text.align": 0,
    #    "legend.text": r('element_text(size=11, color="%(axis_title_c)s")' % attrs), # hjust = 0.5, vjust = 0.3,
    #    "legend.background": r('element_rect(fill="white", color="%(legend_border)s", size=0.3, linetype="solid")' % attrs),
    #})
    #conf["plot_conf"]["plot_args"] = {"size": 0.8 }

    if conf_apply:
        util.update_dict(conf_apply, conf, split=conf_apply_split)

    return conf


def get_latency_conf_combine_thin_tests():
    conf = get_latency_conf()
    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_Plot_type:%(plot_type)s_NUM:%(num)s", # PIFG:%(group)s , DA:%(thin_dupack)s_LT:%(linear_timeout)s
                              "sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"],
                                                          x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["plot_type"]),
                              "sort_keys": ["thin_dupack", "linear_timeout", "stream_count_thin1", "stream_count_thick", "group", "itt_thin1", "plot_type"],
                              "func" : box_key_func,
                              "latency_options": {"per_stream": False},
                              "custom_ggplot2_plotbox_cmds": [latency.thin_itt_mark_line_func_ggplot2], "show_box_title": True,
                              #"box_title_def" : "Thin Dupack: %(thin_dupack)s   Thin Linear Timeout: %(linear_timeout)s", # Bandwidth cap: %(bandwidth)s Kbit  Duration: %(duration)s min
                              "box_titles": { "main": { "title_def": "Duration: %(duration)s min    ITT: %(itt)s ms   RTT: %(rtt)s ms, Greedy streams: %(stream_count_thick)s " }  },
                              "box_title_def" : "Duration: %(duration)s min    ITT: %(itt)s ms   RTT: %(rtt)s ms, Greedy streams: %(stream_count_thick)s NUM:%(num)s",
                          })
    conf["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.APPEND_TO_PLOT_BOX, # common.TABLE_ACTION.NO
                                               #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                               "table_values": { }, # Add values that overrides default table values
                                               "heights": (.65, .00, .35),
                                           })
    #conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [150, 700], "y_axis_lim" : [0.6, 1.05] })

    conf["plot_conf"]["theme_args"].update({
        #"legend.position": FloatVector([0.87, 0.35]),
        "legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
        "legend.text": r('element_text(size=6)'),
        "legend.key.size": r('unit(5, "mm")'),
        "axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
        "axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
        })

    #conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1 })
    #conf["paper_width"] = 6
    #conf["paper_height"] = 5
    return conf

def get_latency_conf_for_linux_cwnd(defaults, conf_apply=None):
    conf = get_latency_conf_with_throughput_separate_on_groups(defaults, conf_apply=conf_apply)
    #conf["process_results"] = None

    def set_box_key_value(file_conf, arg):
        #print "TFRC:", file_conf["tfrc"]
        #group = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        #group = "PIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["tfrc"])
        #file_conf["test_name"] = "PIF%s-DPIF%s-TFRC%d" % (file_conf["packets_in_flight"], file_conf["dpif"], file_conf["tfrc"])
        #print "set_box_key_value:", group
        file_conf["group"] = 0
        file_conf["dpif"] = 10
        #print "file_conf:", file_conf.keys()

    def box_key_func_latency_linux_cwnd(plot_conf, plot_box_dict, file_data, arg):
        box_key_func(plot_conf, plot_box_dict, file_data, arg)
        plot_box_dict["box_conf"]["legend"]["default_legend_and_scaling"] = False
        file_data["bundling_limit"] = "Unlimited"
        print "ID:", plot_conf.get("experiment_id")
        if plot_conf.get("experiment_id") == "E3":
            file_data["bundling_limit"] = "1"

        if file_data["type_thin1"] == "tcp" and file_data["hostname"] == "wsender":
            file_data["color"] = "coral2"
        if file_data["type_thin1"] == "rdb" and file_data["hostname"] == "rdbsender":
            file_data["color"] = "red"

        # COLOR
        util.dict_set_default(plot_box_dict["legends"], ("color", {"guide_args": {}, "scale": {"func": ggplot2.scale_colour_manual, "args": {}}}))
        colorleg = plot_box_dict["legends"]["color"]
        colorleg["guide_args"].update({"order": 1})
        color_args = colorleg["scale"]["args"]
        util.dict_set_default(color_args, [("labels", []), ("values", [])])
        color_args["name"] = ggplot2.element_blank()

        colorleg["guide_args"].update({"override.aes": {"linetype": [1, 1, 2]}})

        color_args["labels"] = ['RDB', 'TCP', 'TCP Reference']
        color_args["values"] = ['#A52A2A', 'darkgreen', 'blue']
        color_args["breaks"] = ['rdb', 'tcp', 'tcp-ref']

        #util.dict_set_default(plot_box_dict["legends"], ("linetype", {"guide_args": {}, "scale": {"func": ggplot2.scale_linetype_manual, "args": {}}}))
        #linetype = plot_box_dict["legends"]["linetype"]
        ##util.dict_set_default(linetype["guide_args"], ("override.aes", {"colour": []}))
        ##linetype["guide_args"].update({"order": 2, "show": True})
        #linetype_args = linetype["scale"]["args"]
        #util.dict_set_default(linetype_args, [("values", [])])
        ##linetype_args["values"] = util.make_r_named_list(color_args["breaks"], ["solid", "solid", "longdash"])
        #linetype_args["guide"] = False
        ##linetype_args["breaks"] = ['rdb', 'tcp', 'tcp-ref']
        ##linetype_args["labels"] = ['RDB SPIFL 3', 'RDB SPIFL 7', 'TCP Reference']
        ##linetype["guide_args"].update({"order": 2, "show": False})


    del conf["process_results"]["custom_write_tables"]
    conf["file_parse"].update({"func": custom_file_parse_func,
                               "parse_func_arg": ["Goodput", "Throughput"]}) # "Latency",
                               #"parse_func_arg": ["Throughput"]})
    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_"
                              "NUM:%(num)s_DU%(thin_dupack)s_LT%(linear_timeout)s_ER:%(early_retransmit)s_QLEN:%(queue_len)s_"
                              "Plot_type:%(plot_type)s", # Plot_type:%(plot_type)s
                              "sort_key_func": lambda x: (x["num"], data_tables.get_stdev(x["itt_thin1"], stdev=False),
                                                          x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                                          x["stream_count_thin1"], x["queue_len"], x["plot_type"][2]),
                              "sort_keys": ["num", "thin_dupack", "linear_timeout", "early_retransmit",
                                            "stream_count_thin1", "itt_thin1", "plot_type", "queue_len"],
                              "func" : box_key_func_latency_linux_cwnd,
                              "pre_key_func": set_box_key_value,
                          })

    page_sort_keys = conf["box_conf"]["sort_keys"]
    page_sort_keys = list(filter(lambda x: x != "plot_type", page_sort_keys))
    conf["page_group_def"] = {
        "title": "%(stream_count_thin1)d Thin streams - Duration: %(duration)s min\n"
        "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets, Cong: %(cong_control_thin1)s",
        "sort_keys": page_sort_keys,
        #"sort_key_func": lambda x: x,
        #"sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False), x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
        #                            x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["queue_len"]),
        "sort_key_func": lambda x: (x["stream_count_thin1"], data_tables.get_stdev(x["itt_thin1"], stdev=False), #x["thin_dupack"], x["linear_timeout"], x["early_retransmit"],
                                    x["queue_len"]),
    }

    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s" }


    #conf["process_results"]["custom_write_tables"] = {"func": custom_latency_merged_process_results}
    return conf

import latency
latency.get_conf = get_latency_conf

def run(defaults={}):
    plot_types = ["rz_pif", "rz_thin_combined_pif_no_greedy", "rz_greedy_tfrc_with_throughput", "rz_thin_combined_pif_no_greedy_eretr",
                  "rz_thin_combined_pif_no_greedy_dpif", "rz_thin_combined_pif_no_greedy_dpif2", "ryz_greedy_tfrc_with_throughput"]

    misuser_def_conf = {"data_table_defaults": {"latex_test_setup_table": {"path_prefix": "misuser-dpif-tfrcsp"} },
                        "box_conf.data_table_defaults": {"analysetcp_results_table": {"recurse": "keys", "data": {"recurse": "Name", "data": {"show": False}}},
                                                         "table_latency_data": {"recurse": "keys", "data": {"recurse": "Name", "data": {"show": False}}}},
                    }
    misuser_conf_apply = {"box_conf.box_titles.top_title2": {"title_def": "Duration: %(duration)s min   TFRC: %(tfrc_str)s   Isoc: %(stream_count_thin1)s   Greedy: %(stream_count_thick)s"}}
    latency_def_conf = {"data_table_defaults": {"latex_test_setup_table": {"path_prefix": "latency-x-traffic-dpif"} },
                        "box_conf.data_table_defaults": {"analysetcp_results_table": {"recurse": "keys", "data": {"recurse": "Name", "data": {"show": False}}},
                                                         "table_latency_data": {"recurse": "keys", "data": {"recurse": "Name", "data": {"show": False}}}},
                    }

    argparser = graph_base.make_parser()
    options = argparser.add_argument_group('Latency RDB CDF options')
    options.add_argument("-pt", "--plot-type",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, default=plot_types[0])

    args = graph_base.parse_args(argparser, defaults=defaults)
    plot_type = args.plot_type

    print "plot_type:", plot_type

    if plot_type == "rz_pif":
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_LATENCY_rdb_vs_greedy_latency_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_LATENCY"
        defaults["file_regex_match"] = [".*z5.*"]
        #defaults["file_regex_nomatch"] = [".*min20.*", ".*zsender.*", ".*z5.*", ".*r5.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_LATENCY/all_results/"]
    elif plot_type == "rz_thin_combined_pif_no_greedy":
        latency.get_conf = get_latency_conf_combine_thin_tests
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_latency_thin_combined_ggplot_20min.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY"
        defaults["file_regex_nomatch"] = [".*min5.*", ".*zsender.*", ".*z5.*", ".*r5.*"]
        #defaults["file_regex_nomatch"].extend([".*-tcp-.*"])
        #defaults["file_regex_match"] = [".*itt10:.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_vs_GREEDY/all_results/"]
    elif plot_type == "rz_thin_combined_pif_no_greedy_eretr":
        latency.get_conf = get_latency_conf_combine_thin_tests
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR_latency_thin_combined_ggplot_20min.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR"
        defaults["file_regex_nomatch"] = [".*min5.*", ".*zsender.*", ".*z5.*", ".*r5.*"]
        #defaults["file_regex_nomatch"].extend([".*-tcp-.*"])
        #defaults["file_regex_match"] = [".*itt10:.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR/all_results/"]
    elif plot_type == "rz_thin_combined_pif_no_greedy_dpif":
        latency.get_conf = get_latency_conf_combine_thin_tests
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR_DYN_latency_thin_combined_ggplot_20min.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR_DYN"
        defaults["file_regex_nomatch"] = [#".*min5.*",
            ".*zsender.*",
        ]
        #defaults["file_regex_nomatch"].extend([".*-tcp-.*"])
        defaults["file_regex_match"] = [".*z5.*", ".*qlen60.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR_DYN/all_results/"]
    elif plot_type == "rz_thin_combined_pif_no_greedy_dpif2":
        latency.get_conf = get_latency_conf_combine_thin_tests
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR_DYN2_latency_thin_combined_ggplot_20min.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR_DYN2"
        defaults["file_regex_nomatch"] = [#".*min5.*",
            ".*zsender.*",
        ]
        #defaults["file_regex_nomatch"].extend([".*-tcp-.*"])
        defaults["file_regex_match"] = [".*z5.*", ".*qlen60.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_vs_GREEDY_ERETR_DYN2/all_results/"]
    elif plot_type == "rz_greedy_tfrc_with_throughput":
        latency.get_conf = get_latency_conf_with_throughput
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC_rdb_vs_greedy_latency_tfrc_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC"
        defaults["file_regex_match"] = [".*tfrc.*"]
        #defaults["file_regex_match"] = ["r21.*itt100-.*cubic.*vs_z10.*min5.*qlen60.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC/all_results/"]
    elif plot_type == "rdb_misuser_dpif":
        def_conf = {"data_table_defaults": {"latex_test_setup_table": { "path_prefix": "misuser-dpif" }}}
        latency.get_conf = partial(get_latency_conf_with_throughput, defaults=def_conf)
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_MISUSE_throughput_dpif.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_MISUSE"
        #defaults["file_regex_match"] = [".*z5.*"]
        #defaults["file_regex_nomatch"] = [".*min20.*", ".*zsender.*", ".*z5.*", ".*r5.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_vs_GREEDY_MISUSE/all_results/"]
        #defaults["file_regex_match"] = ["r21.*itt100-.*cubic.*vs_z10.*min5.*qlen60.*"]
    elif plot_type == "rwz_misuser_dpif_tfrc":
        def_conf = deepcopy(misuser_def_conf)
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf, conf_apply=misuser_conf_apply)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_RDB_MISUSE_TFRC.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_RDB_MISUSE_TFRC"
        #defaults["file_regex_match"] = [".*itt50.*z3.*"]
        #defaults["file_regex_nomatch"] = [".*min20.*", ".*zsender.*", ".*z5.*", ".*r5.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_RDB_MISUSE_TFRC/all_results/"]
        #defaults["file_regex_match"] = ["r21.*itt100-.*cubic.*vs_z10.*min5.*qlen60.*"]
        defaults["file_regex_match"] = ["r2-rdb-itt15:1-ps400-ccrdb-da0-lt0-er3-dpif10-tfrc1.*"]
    elif plot_type == "rwz_misuser_dpif_tfrcsp":
        def_conf = deepcopy(misuser_def_conf)
        #def_conf = {"data_table_defaults": { "latex_test_setup_table": { "path_prefix": "misuser-dpif-tfrc" } } }
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf, conf_apply=misuser_conf_apply)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_FAIRNESS_TFRCSP_QL63.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_FAIRNESS_TFRCSP_QL63"
        defaults["file_regex_nomatch"] = [".*min20.*", #".*zsender.*", ".*z5.*",
                                          #".*r5.*"
        ]
        #RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_FAIRNESS_TFRCSP_QL63
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_FAIRNESS_TFRCSP_QL63/all_results/"]
        #defaults["file_regex_match"] = ["r21.*itt100-.*cubic.*vs_z10.*min5.*qlen60.*"]
        #defaults["file_regex_match"] = ["r3-rdb-itt1-ps400-ccrdb-da0-lt0-er3-dpif10.*"]
    elif plot_type == "rwz_misuser_dpif_tfrcsp_oldcong":
        def_conf = deepcopy(misuser_def_conf)
        def_conf["experiment_id"] = "E4"
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf, conf_apply=misuser_conf_apply)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_MISUSER_TFRC_OLDCONG.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_MISUSER_TFRC_OLDCONG"
        #defaults["file_regex_nomatch"] = [".*min20.*"]
        #defaults["file_regex_match"] = ["r5-tcp-itt5:1.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_MISUSER_TFRC_OLDCONG/all_results/"]
    elif plot_type == "rwz_misuser_dpif_tfrcsp_oldcong_paper":
        def_conf = deepcopy(misuser_def_conf)
        def_conf["experiment_id"] = "E4"
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf, conf_apply=misuser_conf_apply)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_MISUSER_TFRC_OLDCONG.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_MISUSER_TFRC_OLDCONG"
        #defaults["file_regex_nomatch"] = [".*min20.*"]
        #defaults["file_regex_match"] = ["r5-tcp-itt5:1.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_MISUSER_TFRC_OLDCONG/all_results/"]
    elif plot_type == "rwz_latency_dpif_tfrcsp_oldcong":
        def_conf = deepcopy(latency_def_conf)
        def_conf["experiment_id"] = "E2"
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG"
        #defaults["file_regex_nomatch"] = [".*min20.*"]
        #defaults["file_regex_match"] = ["r20.*itt10.*dpif20-tfrc1.*"]
        #defaults["file_regex_match"] = ["r2-.*", "r7-.*"]
        defaults["file_regex_nomatch"] = ["r2-.*", "r7-.*", "r20-.*", "r13-.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG/all_results/"]
    elif plot_type == "rwz_latency_dpif_tfrcsp_oldcong_min15":
        def_conf = deepcopy(latency_def_conf)
        #conf_apply = { "paper_width": 4.8 }
        #conf_apply = {}
        #conf["paper_height"] = 5
        #defaults["file_regex_match"] = ["r5-.*itt10.*"]
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_MIN15.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_MIN"
        #defaults["file_regex_nomatch"] = [#"r2-.*","r20-.*", "r13-.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_MIN/all_results/"]
    elif plot_type == "rwz_latency_dpif_tfrcsp_oldcong_rdblim":
        def_conf = deepcopy(latency_def_conf)
        def_conf["experiment_id"] = "E3"
        def_conf["data_table_defaults"] = {"latex_test_setup_table": {"path_prefix": "latency-x-traffic-dpif-rdblim"} }
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_RDBPLIM.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_RDBPLIM"
        #defaults["file_regex_match"] = ["r5-tcp-itt100:10-ps120-ccrdb-da0-lt0-er3.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_RDBPLIM/all_results/"]
    elif plot_type == "ryz_greedy_tfrc_with_throughput":
        latency.get_conf = get_latency_conf_with_throughput_separate_on_groups
        defaults["latency_output_file"] = "RDBSENDER_YSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_rdb_vs_greedy_latency_tfrc_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_YSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5"
        #defaults["file_regex_match"] = [".*tfrc.*"]
        #defaults["file_regex_match"] = [".*r10-tcp-itt30:3-ps120-cccubic-da0-lt0-er3-tfrc0.*"]
        #defaults["file_regex_match"] = [".*r5-rdb-itt30:3-ps120-ccrdb-da0-lt0-er3-dpif10-tfrc1.*"]

        #defaults["file_regex_match"] = [".*r10-tcp-.*ps120-cccubic-da1-lt1-er3.*-tfrc0.*vs_z5.*qlen60.*"]
        #defaults["file_regex_match"] = [".*r10-tcp-itt10:1.*ps120-cccubic-da0-lt0-er0.*-tfrc0.*vs_z5.*qlen60.*"]
        #r10-rdb-itt10:1-ps120-ccrdb-da0-lt0-er3-dpif10-tfrc1_vs_y10-tcp
        #defaults["file_regex_match"] = [".*r10-rdb-itt10:1.*da0-lt0-er3.*"]
        defaults["file_regex_match"] = [".*r5-tcp-itt10:1.*da0-lt0-er3.*tfrc0.*"]

        defaults["file_regex_match"] = [".*r5-tcp-itt10:1.*da0-lt0-er3.*tfrc0.*"]

        #r30-rdb-itt100:10-ps120-ccrdb-da0-lt0-er3-dpif10-tfrc0_vs_y30-tcp-itt100:10-ps120-cccubic_vs_z15
        #defaults["file_regex_match"] = [".*r30-rdb-itt10:.*da0-lt0-er3.*dpif10.*"]
        #defaults["file_regex_match"] = ["r21.*itt100-.*cubic.*vs_z10.*min5.*qlen60.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_ysender_zsender_to_zreceiver_with_rdb/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5/all_results/"]

    elif plot_type == "ryz_greedy_tfrc_with_throughput_test":
        latency.get_conf = get_latency_conf_with_throughput_separate_on_groups
        defaults["latency_output_file"] = "RDBSENDER_YSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_rdb_vs_greedy_latency_tfrc_ggplot_TEST.pdf"
        defaults["output_dir"] = "RDBSENDER_YSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_TEST"
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_ysender_zsender_to_zreceiver_with_rdb/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5/all_results/"]
        #defaults["directories"] = ["/root/bendiko/pcap/rdbsender_ysender_zsender_to_zreceiver_with_rdb/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_YSENDER_TEST/all_results/"]
        defaults["file_regex_match"] = [".*r5-tcp-itt10:1.*da0-lt0-er3.*"]

    elif plot_type == "ryz_greedy_tfrc_with_throughput_slett":
        latency.get_conf = get_latency_conf_with_throughput_separate_on_groups
        defaults["latency_output_file"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC_SLETT_rdb_vs_greedy_latency_tfrc_ggplot_TEST.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_TEST"
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_ysender_zsender_to_zreceiver_with_rdb/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_TEST/all_results/"]

    elif plot_type == "rwz_greedy_tfrc_with_throughput":
        def_conf = deepcopy(latency_def_conf)
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_rdb_vs_greedy_latency_tfrc_ggplot.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5"
        #defaults["file_regex_match"] = [".*r5-tcp-itt10:1.*da0-lt0-er3.*tfrc0.*"]
        #defaults["file_regex_match"] = [".*r5-.*-itt10.*da0-lt0-er3.*"]
        #defaults["file_regex_match"] = [".*r5-.*-itt10.*da0-lt0-er3.*"]
        defaults["file_regex_nomatch"] = [#".*min20.*", ".*zsender.*", ".*z5.*", ".*r5.*"
            ".*da1-lt1.*"
        ]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5/all_results/"]
    elif plot_type == "rwz_greedy_tfrc_with_throughput_test":
        def_conf = deepcopy(latency_def_conf)
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf)
        defaults["latency_output_file"] = "TEST_SUBGFIGS.pdf"
        defaults["output_dir"] = "RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_WSENDER_TEST"
        #defaults["file_regex_match"] = [".*r5-.*itt10:1.*"]
        #defaults["file_regex_match"] = [".*r5-tcp-itt10:1-ps120-cccubic-da1-lt1-er3-tfrc0_vs_w5-tcp-itt10:1-ps120-cccubic_vs_z5..kbit5000_min5_rtt150_loss_pif0_qlen60.*"]
        defaults["file_regex_match"] = [".*r13-rdb-itt10:1-ps120-ccrdb-da0-lt0-er3-dpif10-tfrc1_vs_w13-tcp-itt10:1-ps120-cccubic_vs_z5..kbit5000_min5_rtt150_loss_pif0_qlen60_delayfixed_num0_.*"]
        #defaults["file_regex_match"] = [".*r20.*"]
        #defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_WSENDER_TEST/all_results/"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5/all_results/"]
    elif plot_type == "rwz_paper_latency_barplots":
        def_conf = deepcopy(latency_def_conf)
        #latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf)
        latency.get_conf = partial(get_latency_conf_with_barplots, defaults=None)
        defaults["latency_output_file"] = "TEST_SUBGFIGS.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_PAPER_LATENCY"
        #defaults["file_regex_match"] = [".*r5-.*itt10:1.*"]
        #defaults["file_regex_match"] = [".*r5-tcp-itt10:1-ps120-cccubic-da1-lt1-er3-tfrc0_vs_w5-tcp-itt10:1-ps120-cccubic_vs_z5..kbit5000_min5_rtt150_loss_pif0_qlen60.*"]
        defaults["file_regex_match"] = [".*r13-rdb-itt10:1-ps120-ccrdb-da0-lt0-er3-dpif10-tfrc1_vs_w13-tcp-itt10:1-ps120-cccubic_vs_z5..kbit5000_min5_rtt150_loss_pif0_qlen60_delayfixed_num0_.*"]
        #defaults["file_regex_match"] = [".*r20.*"]
        #defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5_WSENDER_TEST/all_results/"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_RDB_vs_GREEDY_TFRC5/all_results/"]
    elif plot_type == "rwz_paper_latency_no_bundling_limit":
        def_conf = deepcopy(latency_def_conf)
        #def_conf["pdf_args"] = {"family": "DejaVu Sans" }
        def_conf["experiment_id"] = "E2"
        latency.get_conf = partial(get_latency_conf_with_tcp_thin_together_with_rdb_test, defaults=def_conf)
        defaults["latency_output_file"] = "expr3_ack_latency_no_bundling_limit.pdf"
        defaults["output_dir"] = "PAPER_LATENCY_NO_BUNDLING_LIMIT"
        defaults["file_regex_nomatch"] = [".*tfrc1.*", ".*dpif20.*"]
        defaults["file_regex_match"] = ["r10-...-itt30:3-ps120-ccrdb-da0-lt0-er3.*"]
        #defaults["file_regex_match"] = ["r10-...-itt10:1-ps120-ccrdb-da0-lt0-er3.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG/all_results/"]
    elif plot_type == "rwz_paper_latency_with_bundling_limit":
        def_conf = deepcopy(latency_def_conf)
        def_conf["experiment_id"] = "E3"
        latency.get_conf = partial(get_latency_conf_with_tcp_thin_together_with_rdb_test, defaults=def_conf)
        defaults["latency_output_file"] = "expr2_ack_latency_with_bundling_limit.pdf"
        defaults["output_dir"] = "PAPER_LATENCY_WITH_BUNDLING_LIMIT"
        defaults["file_regex_nomatch"] = [".*tfrc1.*", ".*dpif20.*"]
        defaults["file_regex_match"] = ["r10-...-itt30:3-ps120-ccrdb-da0-lt0-er3.*"]
        #defaults["file_regex_match"] = ["r10-...-itt10:1-ps120-ccrdb-da0-lt0-er3.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_RDBPLIM/all_results/"]
    elif plot_type == "rwz_presentation_latency_no_bundling_limit":
        def_conf = deepcopy(latency_def_conf)
        #def_conf["experiment_id"] = "E3"
        def_conf["experiment_id"] = "E2"
        #latency_conf_apply = {"box_conf.box_titles.main": {"title_def": "RDB: %(stream_count_thin1)s   TCP: %(stream_count_thin1)s   ITT: %(itt_int_thin1)s ms   Seg. size: %(payload_thin1)s B   DPIF: %(dpif)s   Rdn. level: %(bundling_limit)s", "order": 1 }}
        latency_conf_apply = {"box_conf.box_titles.main": {"title_def": "ITT: %(itt_int_thin1)s ms   Bundle rate: DPIFL %(dpif)s   Redundancy level: %(bundling_limit)s", "order": 1 },
                              "plot_conf.theme_args": {"plot.title": r('element_text(size=8, lineheight=1, hjust=0.5, color="black")')}}
        #"box_conf.box_titles.second": {"title_def": "Background traffic flows: %(stream_count_thick)s", "order": 2 },
        latency.get_conf = partial(get_latency_conf_with_tcp_thin_together_with_rdb_test, defaults=def_conf, conf_apply=latency_conf_apply)
        defaults["latency_output_file"] = "expr3_ack_latency_no_bundling_limit.pdf"
        defaults["output_dir"] = "PAPER_LATENCY_NO_BUNDLING_LIMIT"
        defaults["file_regex_nomatch"] = [".*tfrc1.*", ".*dpif20.*"]
        defaults["file_regex_match"] = ["r10-...-itt30:3-ps120-ccrdb-da0-lt0-er3.*"]
        #defaults["file_regex_match"] = ["r10-...-itt10:1-ps120-ccrdb-da0-lt0-er3.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG/all_results/"]
    elif plot_type == "rwz_presentation_latency_with_bundling_limit":
        def_conf = deepcopy(latency_def_conf)
        #latency_conf_apply = {"box_conf.box_titles.main": {"title_def": "RDB: %(stream_count_thin1)s   TCP: %(stream_count_thin1)s   ITT: %(itt_int_thin1)s ms   Seg. size: %(payload_thin1)s B   DPIF: %(dpif)s   Rdn. level: %(bundling_limit)s", "order": 1 }}
        latency_conf_apply = {"box_conf.box_titles.main": {"title_def": "ITT: %(itt_int_thin1)s ms   Bundle rate: DPIFL %(dpif)s   Redundancy level: %(bundling_limit)s", "order": 1 },
                              "plot_conf.theme_args": {"plot.title": r('element_text(size=8, lineheight=1, hjust=0.498, color="black")')}
        }
        def_conf["experiment_id"] = "E3"
        latency.get_conf = partial(get_latency_conf_with_tcp_thin_together_with_rdb_test, defaults=def_conf, conf_apply=latency_conf_apply)
        defaults["latency_output_file"] = "expr2_ack_latency_with_bundling_limit.pdf"
        defaults["output_dir"] = "PAPER_LATENCY_WITH_BUNDLING_LIMIT"
        defaults["file_regex_nomatch"] = [".*tfrc1.*", ".*dpif20.*"]
        defaults["file_regex_match"] = ["r10-...-itt30:3-ps120-ccrdb-da0-lt0-er3.*"]
        #defaults["file_regex_match"] = ["r10-...-itt10:1-ps120-ccrdb-da0-lt0-er3.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_TO_ZRECEIVER_LATENCY_TFRC_OLDCONG_RDBPLIM/all_results/"]
    elif plot_type == "rwz_misuser_dpif_tfrc_abuser_mode":
        #latency.get_conf = get_latency_conf_with_throughput
        #latency.get_conf = get_latency_conf_with_throughput_separate_on_groups
        def_conf = {"data_table_defaults": { "latex_test_setup_table": { "path_prefix": "misuser-dpif-tfrc" } } }
        latency.get_conf = partial(get_latency_conf_with_throughput_separate_on_groups, defaults=def_conf)
        defaults["latency_output_file"] = "RDBSENDER_WSENDER_ZSENDER_RDB_MISUSE_TFRC_ABUSER_MODE.pdf"
        defaults["output_dir"] = "RDBSENDER_WSENDER_ZSENDER_RDB_MISUSE_TFRC_ABUSER_MODE"
        #defaults["file_regex_match"] = [".*z5.*"]
        #defaults["file_regex_nomatch"] = [".*min20.*", ".*zsender.*", ".*z5.*", ".*r5.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_wsender_zsender_to_zreceiver/RDBSENDER_WSENDER_ZSENDER_RDB_MISUSE_TFRC_ABUSER_MODE/all_results/"]
        #defaults["file_regex_match"] = ["r21.*itt100-.*cubic.*vs_z10.*min5.*qlen60.*"]
    elif plot_type == "r_linux_cwnd_oldcong":
        def_conf = deepcopy(latency_def_conf)
        def_conf["experiment_id"] = "E2"
        latency.get_conf = partial(get_latency_conf_for_linux_cwnd, defaults=def_conf)
        defaults["latency_output_file"] = "RDBSENDER_TO_ZRECEIVER_LINUX_CWND.pdf"
        defaults["output_dir"] = "RDBSENDER_TO_ZRECEVIER_LINUX_CWND"
        #defaults["file_regex_nomatch"] = [".*min20.*"]
        #defaults["file_regex_match"] = ["r20.*itt10.*dpif20-tfrc1.*"]
        #defaults["file_regex_match"] = ["r2-.*", "r7-.*"]
        #defaults["file_regex_nomatch"] = ["r2-.*", "r7-.*", "r20-.*", "r13-.*"]
        defaults["directories"] = ["/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEVIER_LINUX_CWND/all_results/",
                                   "/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEVIER_CWND/all_results/"
        ]
    else:
        cprint("Invalid plot type: '%s'" % plot_type, "red")
        sys.exit()

    args = graph_base.parse_args(argparser, defaults=defaults)
    graph_base.main()

if __name__ == "__main__":
    run()
