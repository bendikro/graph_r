from graph_r import *
pp = pprint.PrettyPrinter(indent=4)
from datetime import datetime
import glob
import argparse
import subprocess
import util
import common

from graph_r import create_plots

from graph_default import get_defaults_plot_box_conf

def generate_results(plot_group, plot_conf):
    """
    Generates the results for the all plots.
    The results are generated by executing the set_data["results_cmd"]
    as a shell command.
    """
    page_title = plot_group["page_title"]
    groups_list = plot_group["plot_boxes"]
    group_index = plot_group["index"]
    counter = 0

    if page_title:
        cprint("Generating results for '%s'" % page_title, "green")
    total_results_count = 0
    for box_group in groups_list:
        for set_key in box_group["sets"]:
            for set_data in box_group["sets"][set_key]["plots"]:
                total_results_count += 1

    for index, box_group in enumerate(groups_list):
        box_group["plot_group"] = plot_group
        box_group["plot_index"] = index
        for set_key in box_group["sets"]:
            if util.cmd_args.verbose > 0:
                cprint("Processing set %s" % set_key, "green")
            for set_data in box_group["sets"][set_key]["plots"]:
                counter += 1
                if "pre_results" in set_data:
                    # Iterate all the pre_results handlers defined
                    for handler_conf_key, handler_conf in set_data["pre_results"].items():
                        handler_conf["func"](plot_conf, box_group, set_data, set_key)

                if util.cmd_args.skip_data:
                    continue

                if "results" in set_data:
                    # Iterate all the results parsers defined
                    for result_conf_key, result_conf in set_data["results"].items():
                        if util.cmd_args.verbose >= 4:
                            if not result_conf.get("results_cmd", None) is None:
                                print "results_cmd:", result_conf.get("results_cmd", None)
                        if not result_conf.get("results_cmd", None):
                            continue
                        if not os.path.isfile(result_conf["results_file"]) or plot_conf["force"]:
                            if util.cmd_args.verbose:
                                print "\n%2d/%2d | %s : cmd: %s" % (counter, total_results_count,
                                                                    datetime.now().strftime("%H:%M:%S.%f"),
                                                                    colored(result_conf["results_cmd"], "yellow"))
                            start = time.time()
                            try:
                                #ret = subprocess.call(result_conf["results_cmd"], shell=True)
                                process = subprocess.Popen(result_conf["results_cmd"], shell=True) # , stdout=PIPE, stderr=PIPE
                                stdout, stderr = process.communicate()
                            except KeyboardInterrupt:
                                print "Interrupted, deleting results file:", result_conf["results_file"]
                                if os.path.isfile(result_conf["results_file"]):
                                    os.remove(result_conf["results_file"])
                                raise
                            end = time.time()
                            if util.cmd_args.verbose:
                                print "Command executed in %f sec" % (end - start)

                # Call the function that reads and parses the result
                if "parse_results_func" in box_group:
                    box_group["parse_results_func"](plot_conf, box_group, set_data, set_key)


def duration_results_parse(conf, box_conf, file_data, set_key):
    latency_results_parse(conf, box_conf, file_data, set_key)
    #minimum_dur = r["min"](file_data["values"])


def get_files(d, conf):
    """
    Returns all the files from the directory d matching the 'files_match_exp'
    """
    if not d.endswith("/"):
        d += "/"
    d += conf["file_parse"]["files_match_exp"]
    if util.cmd_args.verbose:
        print "Getting files: ", d
    files = glob.glob(d)
    files_to_use = []
    def match(filename, regex_list):
        for i, exp in enumerate(regex_list):
            m = re.match(exp, filename, flags=re.DOTALL)
            if m:
                return i
        return -1

    excluded = {}

    if conf["file_parse"]["files_match_regex"] or conf["file_parse"]["files_nomatch_regex"]:
        for f in files:
            basename = os.path.basename(f)
            if conf["file_parse"]["files_nomatch_regex"]:
                m_index = match(basename, conf["file_parse"]["files_nomatch_regex"])
                if m_index != -1:
                    if util.cmd_args.verbose > 2:
                        l = excluded.get(conf["file_parse"]["files_nomatch_regex"][m_index], [])
                        l.append(basename)
                        excluded[conf["file_parse"]["files_nomatch_regex"][m_index]] = l
                    continue

            if conf["file_parse"]["files_match_regex"]:
                #conf["file_parse"]["files_match_regex"]
                m_index = match(basename, conf["file_parse"]["files_match_regex"])
                if m_index != -1:
                    files_to_use.append(f)
                if util.cmd_args.verbose > 3:
                    print "File did not match:", f
            else:
                files_to_use.append(f)
    else:
        files_to_use = files

    for k in excluded:
        print "Files excluded by regex '%s'" % (k)
        for v in excluded[k]:
            print " - %s"% v
        #print "Excluding file based on regex '%s' : %s" % (conf["file_parse"]["files_nomatch_regex"][m_index], basename)

    if util.cmd_args.verbose:
        cprint("Files found: %d" % len(files_to_use), "red" if not files_to_use else "white")
        if util.cmd_args.verbose >= 3:
            indent = 5
            max_line_len = 100
            print "Files:"
            for f in files_to_use:
                base = os.path.basename(f)
                fname = (" " * indent).join(base[i:i + max_line_len] + "\n" for i in xrange(0, len(base), max_line_len))
                print "%s - %s" % (" " * (indent - 3), fname.strip())

    return files_to_use


def parse_filenames(files, plot_conf):
    """
    Parses the filenames and creates the plots configuration
    """
    groups = {}
    if util.cmd_args.verbose > 2:
        print "Parsing files with regex:", plot_conf["file_parse"]["filename_regex"]

    def parse_file(f, arg):
        #print "parse_file:", f
        file_conf = plot_conf["file_parse"]["parse_input"](plot_conf, f, arg)
        #print "file_conf:", file_conf["loss"]

        # Calling the file parse func defined on plot config
        if "func" in plot_conf["file_parse"]:
            ret = plot_conf["file_parse"]["func"](plot_conf, file_conf, f, arg)
            if ret is False:
                return

        if plot_conf["box_conf"].get("pre_key_func", None) is not None:
            plot_conf["box_conf"]["pre_key_func"](file_conf, arg)

        #print "file_conf:", file_conf.keys()
        box_key = plot_conf["box_conf"]["key"] % file_conf
        set_key = plot_conf["set_conf"]["key"] % file_conf

        def set_box_conf(plot_conf, ignore):
            if "parse_results_func" in plot_conf:
                groups[box_key]["parse_results_func"] = plot_conf["parse_results_func"]

            if "axis_conf" in plot_conf["plot_conf"]:
                groups[box_key]["axis_conf"].update(deepcopy(plot_conf["plot_conf"]["axis_conf"]))

            groups[box_key]["theme_args"] = deepcopy(plot_conf["plot_conf"].get("theme_args", {}))
            groups[box_key]["meta_data"]["enabled"] = plot_conf["add_meta_data"]
            groups[box_key]["r.plot_args"] = deepcopy(plot_conf["plot_conf"]["r.plot_args"])

            # Add the sort keys to box conf
            if "sort_keys" in plot_conf["box_conf"]:
                for sort_key in plot_conf["box_conf"]["sort_keys"]:
                    #print "Setting plot box sort attr %s: %s" % (sort_key, file_conf[sort_key])
                    groups[box_key][sort_key] = file_conf[sort_key]

            if "box_commands" in plot_conf["plot_conf"]:
                cmds = []
                for cmd in plot_conf["plot_conf"]["box_commands"](plot_conf["plot_conf"], file_conf):
                    cmds.append(cmd)
                groups[box_key]["custom_cmds"] = cmds

        if not box_key in groups:
            groups[box_key] = deepcopy(get_defaults_plot_box_conf())
            groups[box_key]["box_conf"] = deepcopy(plot_conf["box_conf"])
            groups[box_key]["box_key"] = box_key
            groups[box_key]["plot_func"] = plot_conf["plot_conf"]["plot_func"]
            groups[box_key]["bandwidth"] = plot_conf["plot_conf"]["bandwidth"]

            set_box_conf(plot_conf, arg)
            if "set_box_conf" in plot_conf["plot_conf"]:
                plot_conf["plot_conf"]["set_box_conf"](plot_conf, groups[box_key], arg)

        if not set_key in groups[box_key]["sets"]:
            groups[box_key]["sets"][set_key] = { "plots": [] }

        groups[box_key]["sets"][set_key]["plots"].append(file_conf)
        #print
        #print "box_key:", box_key
        #print "set key:", set_key
        file_conf["box_conf"] = groups[box_key]
        #print "box_key:", box_key
        def p_box(bkeys):
            for bkey in bkeys:
                if bkey in groups:
                    print "SORT box: %d, key: %s" % (groups[bkey]["box_counter"], groups[bkey]["y_axis_title"]["title"])

        if "func" in plot_conf["box_conf"] and plot_conf["box_conf"]["func"]:
            plot_conf["box_conf"]["func"](plot_conf, groups[box_key], file_conf, arg)
        if "func" in plot_conf["set_conf"] and plot_conf["set_conf"]["func"]:
            plot_conf["set_conf"]["func"](plot_conf, groups[box_key], file_conf)

        title = util.make_title(plot_conf["box_conf"], file_conf, plot_conf["box_conf"]["box_title_def"])
        groups[box_key]["box_title"] = title

    for f in sorted(files):
        for arg in plot_conf["file_parse"]["parse_func_arg"]:
            parse_file(f, arg)

    plot_conf["document_info"]["info"].append("Created: %s" % time.strftime('%b %d, %Y %X'))
    plot_conf["document_info"]["info"].append("Total plots: %d" % len(groups))
    plot_conf["document_info"]["info"].append("Bandwidth cap: %s" % plot_conf["plot_conf"]["bandwidth"])

    # Sort the boxes within each group list
    group_list = []
    #print "Groups:", len(groups)
    #print "groups.values():", groups.values()
    #for box in groups.values():
    #    print "keys:", box.keys()

    for box in sorted(groups.values(), key=plot_conf["box_conf"]["sort_key_func"]):
        #print "TEST2:", box["sets"].keys()
        #print "key sort:", plot_conf["box_conf"]["sort_key_func"](box)
        if "sort_key_func" in plot_conf["set_conf"] and plot_conf["set_conf"]["sort_key_func"]:
            for s in box["sets"]:
                box["sets"][s]["plots"] = sorted(box["sets"][s]["plots"], key=plot_conf["set_conf"]["sort_key_func"])
        group_list.append(box)

    groups_dict = { "plot_boxes": group_list, "page_title": "", "index": 0,
                    #"y_axis_title" : plot_conf["plot_conf"]["axis_conf"].get("y_axis_title", {}),
                    #"x_axis_title" : plot_conf["plot_conf"]["axis_conf"].get("x_axis_title", {})
                    }
    return [groups_dict]

#@profile
def split_page_groups(groups_list, conf):
    """
    This splits the plots into gropus before they are plotted.
    This is done by defining "page_group_def" as dictionary in the config
    Example:
    conf["page_group_def"] = {"title": "Payload: %(payload)s, ITT: %(itt)sms, RTT: %(rtt)s",
                              "sort_keys": ["payload", "itt", "rtt"],
                              "sort_key_func": lambda x: (x[0], x[1], x[2])}
    "sort_keys" is which keys to use from the set_data dict to group and sort.
    "sort_key_func" is the function provided as the 'key' argument to the built-in sorted function.
    """
    page_groups_dict = {}
    group = groups_list[0]
    plot_box_list = group["plot_boxes"]
    index = 0

    for box in plot_box_list:
        page_key = common.HashableDict()
        title = ""
        #print "box_title:", box["box_title"]
        for set_key in box["sets"]:
            for set_data in box["sets"][set_key]["plots"]:
                #print "set_data:", set_data
                for k in conf["page_group_def"]["sort_keys"]:
                    #page_key.append(set_data[k])
                    page_key[k] = set_data[k]
                title = conf["page_group_def"]["title"] % (set_data)
                break
            break
        #page_key = tuple(page_key)
        #print "page_key:", page_key
        if not page_key in page_groups_dict:
            #print "New page key (%d): %s" % (index, page_key)
            page_groups_dict[page_key] = {'page_title': title, "index": index, 'plot_boxes': []}
            index += 1
        page_groups_dict[page_key]["plot_boxes"].append(box)

    page_groups_list = []
    for b in sorted(page_groups_dict.keys(), key=conf["page_group_def"]["sort_key_func"]):
        page_groups_list.append(page_groups_dict[b])
    #print "page_groups_list:", page_groups_list
    return page_groups_list


def print_conf(groups_list, conf):
    """
    Prints the config in a readable format
    """
    max_line_len = 150
    indent = 6
    plot_box_count = 0
    sets_count = 0
    files_count = 0
    print "\nPlot groups:", len(groups_list)
    for plot_groups in groups_list:
        print
        print "Page group title '%s'" % plot_groups["page_title"]
        plot_list = plot_groups["plot_boxes"]
        print "\nPlot boxes:", len(plot_list)
        plot_box_count += len(plot_list)
        for p_box in plot_list:
            #print " Box:",  p_box["box_title"]
            print " Box:",  p_box["box_key"]
            #for plot_set in p_box["sets"]:
            #print "VALUES:", p_box["sets"].values()
            #s = sorted(p_box["sets"].keys(), key=lambda x: (p_box["sets"][x]["plots"][0]["stream_count_thin1"], p_box["sets"][x]["plots"][0]["hostname"],
            #                                                p_box["sets"][x]["plots"][0]["itt_thin1"], int(p_box["sets"][x]["plots"][0]["packets_in_flight"])))
            s = p_box["sets"].keys()
            #for plot_set in p_box["sets"]:
            for plot_set in s:
                print "  Set: ", plot_set
                plots = p_box["sets"][plot_set]["plots"]
                sets_count += 1
                files_count += len(plots)
                if util.cmd_args.verbose < 2:
                    print "    Files:", len(plots)
                else:
                    print "    Files(%d):" % (len(plots))
                    for plot in plots:
                        base = os.path.basename(plot["unique_input_identity"])
                        fname = (" " * indent).join(base[i:i + max_line_len] + "\n" for i in xrange(0, len(base), max_line_len))
                        print "%s - %s" % (" " * (indent - 3), fname),
    print
    print "Total plot boxes :", plot_box_count
    print "Total sets       :", sets_count
    print "Total files      :", files_count


#    for box in sorted(groups.values(), key=plot_conf["box_conf"]["sort_key_func"]):
#        #print "TEST2:", box["sets"].keys()
#        #print "key sort:", plot_conf["box_conf"]["sort_key_func"](box)
#        if "sort_key_func" in plot_conf["set_conf"] and plot_conf["set_conf"]["sort_key_func"]:
#            for s in box["sets"]:
#                box["sets"][s]["plots"] = sorted(box["sets"][s]["plots"], key=plot_conf["set_conf"]["sort_key_func"])

def do_plots(plot_conf):
    """
    Creates the plots from the plot_conf
    1) Fetch the files for the plot config
    2) Generate the results for all the plots
    3) Call the functions to create the actual plots
    4) Process the results at the end if defined in the config (process_all_results)
    """
    max_line_len = 150
    indent = 6
    plot_conf["output_dir"] = util.cmd_args.output_dir
    plot_conf["force"] = util.cmd_args.force

    if util.cmd_args.write_tables is True:
        plot_conf["plot_conf"]["make_plots"] = False
        print "write_tables is True, Skipping plots generation!"

    files = plot_conf.get("files", [])
    if not files:
        for d in util.cmd_args.directories:
            files += get_files(d, plot_conf)
    groups_list = parse_filenames(files, plot_conf)
    #print "groups_list:", groups_list
    #for f in files:
    #    print "FILE:", f

    if util.cmd_args.file_parse:
        sys.exit(0)

    if plot_conf["func_before_graphs"]:
        plot_conf["func_before_graphs"](plot_conf)

    print "groups_list:", len(groups_list)

    if plot_conf.get("page_group_def", False):
        groups_list = split_page_groups(groups_list, plot_conf)

    if util.cmd_args.print_conf:
        print_conf(groups_list, plot_conf)
        sys.exit(0)

    if util.cmd_args.verbose >= 2:
        print_conf(groups_list, plot_conf)

    if plot_conf["document_info"]["filename_as_pdf_title"] is not None:
        if plot_conf["document_info"]["filename_as_pdf_title"]:
            plot_conf["pdf_args"]["title"] = plot_conf["output_file"]
        else:
            plot_conf["pdf_args"]["title"] = plot_conf["document_info"]["title"]

    #print "groups_list:", groups_list

    for page_group in groups_list:
        generate_results(page_group, plot_conf)

    # Used for conninfo
    #if plot_conf["plot_conf"]["make_plots"]:
    create_plots(plot_conf, groups_list)
    #else:
    #print "Skip generating data and plots"

    #print "process_results:", plot_conf.get("process_results", None)
    if plot_conf.get("process_results", None) is not None:
        for f, v in plot_conf["process_results"].iteritems():
            v["func"](plot_conf, groups_list)


def do_all_plots(conf):

    if not conf is None:
        do_plots(conf)

    if conf["view_results"]["execute"]:
        name, extension = os.path.splitext(conf["output_file"])
        print "extension:", extension
        if conf["view_results"]["viewers"].get(extension, None) is None:
            print "No viewer is configured for extension: '%s'", extension
        else:
            cmd = "%s %s" % (conf["view_results"]["viewers"][extension], conf["output_file"])
            ret = subprocess.call(cmd, shell=True)
