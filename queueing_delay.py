import graph_default
import util
from graph_r import *
from graph_r_ggplot import *

def senttime_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    results_dict = {}

    if file_conf["thin_dupack"] is None:
        file_conf["thin_dupack"] = 0

    #results_dict["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.dat"))
    #results_dict["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "latency-all-aggr.stout"))
    results_dict["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "queueing-delay-all.dat"))
    results_dict["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "queueing-delay-all.stout"))

    files = []
    #t37-rdb-itt100:15-ps120-cccubic_vs_g10_kbit5000_min10_rtt150_loss_pif20_qlen30_delayfixed_num0_rdbsender-rdb-senttime-all-10.0.0.12-22000-10.0.0.22-5000.dat
    d = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "senttime-all-%(src_ip)s*" % file_conf))
    #print "Getting files: ", d
    #import glob
    #files = glob.glob(d)
    #print "files:", files
    #results_dict["senttime_by_stream_files"] = files

    #print "results_file:", results_dict["results_file"]

    # --analyse-start=300 --analyse-end=60
    #results_dict["results_cmd"] =\
    #    "./analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s -o %(output_dir)s --analyse-duration=600  -a 1> %(stdout_file)s" %\
    #    dict(file_conf, **results_dict)
    results_dict["results_cmd"] =\
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s -Q -o %(output_dir)s -a 1> %(stdout_file)s" %\
        dict(file_conf, **results_dict)
    #results_dict["results_cmd"] =\
    #    'tcpdump -Nn -tt -r %(pcap_file)s "tcp src host %(src_ip)s" 1> %(stdout_file)s' %\
    #    dict(file_conf, **results_dict)

    file_conf["color"] = "darkred" # Default color for dataset

    file_conf["results"] = {"senttime": results_dict}

    #if file_conf["packets_in_flight"] > 4:
    #    file_conf["group"] = 0
    #else:
    #    file_conf["group"] = 1

    #print "type_id:", file_conf["type_id"]
    file_conf["group"] = file_conf["type_id"]
    file_conf["group"] = ""
    file_conf["group"] = file_conf["packets_in_flight"]

    if "percentiles" in plot_conf["box_conf"]:
        file_conf["percentiles"] = plot_conf["box_conf"]["percentiles"]

def senttime_results_parse(conf, box_conf, file_data, set_key):
    print "Reading senttime file:", file_data["results"]["senttime"]["results_file"]
    #dataframe = r["read.table"](file_data["results"]["senttime"]["results_file"], sep=",")
    #dataframe = r["read.table"](file_data["results"]["senttime"]["results_file"], sep=",", dec = ".")
    dataframe = r["read.csv"](file_data["results"]["senttime"]["results_file"], header=False) #, colClasses=StrVector(("integer", "integer")))

    # Set columns names for the first and second column (time, latency variation)
    dataframe.names[0] = "X"
    dataframe.names[1] = "Y"

    print "stream_type:", file_data["stream_type"]

    dataframe = r.cbind(dataframe, color_column=file_data["color"])

    # Convert from ms to seconds
    dataframe[0] = dataframe[0].ro / 1000
    # Remove latency variation values that are way off
    dataframe = dataframe.rx(dataframe.rx2("Y").ro < 1000, True)

    #data = []
    #for f in file_data["results"]["senttime"]["senttime_by_stream_files"]:
    #    print "Reading senttime file:", f
    #    dataframe = r["read.table"](f)
    #    data.append((f, r["as.numeric"](dataframe[0])))
    #file_data["ecdf_values"] = data
    file_data["data"] = dataframe

def senttime_box_key_func(box_conf, conf):

    if conf["hostname"] == "rdbsender":
        if conf["type_thin1"] == "tcp":
            conf["color"] = "darkgoldenrod1"
        elif conf["type_thin1"] == "rdb":
            pif = "rdb_pif%d" % conf["packets_in_flight"]
            if pif in color_map:
                conf["color"] = color_map[pif]
            else:
                conf["color"] = color_map["default"]

    elif conf["hostname"] == "zsender":
        if conf["type_thin1"] == "rdb":
            conf["color"] = "firebrick1"

def thin_itt_mark_line_func(plot_conf, file_conf):
    line_start = 240
    for i in range(8):
        yield "abline(v=%d, lty=3, lwd=1)" % (line_start + 60*i)

def get_color_senttime(plot):
    stream_id = plot["type_id"]
    pif_type = plot["type_id_pif"]

    if stream_id == "type_thick":
        if plot["num"] == 0:
            return color_map["thick"]
        return color_map["thick2"]

    if plot["num"] == 0:
        return host_type_color_map["rdbsender"][pif_type]
    elif plot["num"] == 1:
        return "cyan"
    else:
        return "darkgreen"

def get_conf():
    conf = deepcopy(graph_default.get_conf())
    conf["output_file"] = util.cmd_args.queueing_delay_output_file
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(hostname)s",
                         "sort_key_func": lambda x: (x["thin_dupack"], x["stream_count_thin1"], x["stream_count_thick"], x["group"]),
                         "sort_keys": ["thin_dupack", "stream_count_thin1", "stream_count_thick", "group"],
                         "func" : senttime_box_key_func,
                         "box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d %(cong_control_thin1)s DA:%(thin_dupack)s ITT:%(itt_thin1)s M:%(duration)s HOST: %(hostname)s" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["page_group_def"] = {"title": "Payload: %(payload_thin1)s, RTT: %(rtt)s Payload: %(payload_thin1)s  QLEN: %(queue_len)s Duration: %(duration)s min",
                              "sort_keys": ["payload_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1])}
    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 2, "x_axis_lim" : [0, 600], "y_axis_lim" : [0, 1],
                              "plot_func": plot_scatterplot_senttime,
                              "do_plots_func": do_ggplots,
                              "legend": { "color_func": get_color_senttime, "values": {"type_thick": "Greedy", "type_thin1": "Thin"}},
                              "box_commands": thin_itt_mark_line_func
                              }
                             )
    conf["parse_results_func"] = senttime_results_parse
    conf["file_parse"].update({ "func": senttime_file_parse_func })
    conf["document_info"]["title"] = "Senttime Plots"
    conf["paper_width"] = 200
    conf["paper_height"] = 10

    conf["file_parse"]["files_nomatch_regex"].append(".*qlen46875.*")
    #conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 1})
    return conf
