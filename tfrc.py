#!/usr/bin/env python
from datetime import datetime
import argparse
import os
import util

args = util.cmd_args

#from graph_r import *
from rpy2.robjects import r
from rpy2.robjects.vectors import Vector, IntVector, DataFrame, StrVector, FloatVector
import rpy2.robjects.lib.ggplot2 as ggplot2

import glob
from math import sqrt, pow


def parse_input(plot_conf, param, arg):
    #make_data(count=300, rtt=0.15, pp_rtt=5, loss_percent=5, packet_size=lambda: random.randint(50, 300)):
    #t_id = "pkg%(count)d_rtt%(rtt)s_pp_rtt%(rtt)d_loss%(loss_percent)s_ps%(packet_size_str)s" % (param)
    param["duration"] = plot_conf["plot_conf"]["axis_conf"]["x_axis_lim"][1]

    dataf = make_data(param, **param)
    file_conf = dict(param)
    file_conf["data"] = dataf
    file_conf["unique_input_identity"] = "pkg%(count)d_rtt%(rtt)s_pp_rtt%(pp_rtt)s_loss%(loss_percent)s_ps%(packet_size_str)s" % (param)
    file_conf["avg_size"] = param["hist"].avg_size
    file_conf["rtt_ms"] = int(file_conf["rtt"] * 1000)
    file_conf["streams"] = {"s1": {"stream_count": 1,
                                   "meta_info": {"legend_count": 20, "smooth": False, "line": True,
                                                 #"factor": "color_column",
                                                 #"type_id_column": "color_column", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                                 #"breaks_factor": "color_column",  # These are for the legend
                                                 "aes_args": { "color": 'factor(stream)'} # These are for the general factoring of the data
                                             }}}

    file_conf["streams_id"] = "s1"

    return file_conf

def get_plot_conf():
    from graph_r_ggplot import plot_scatterplot_cwnd, do_ggplots
    import graph_default
    #from graph_r import *
    from copy import deepcopy
    import itertools

    conf = deepcopy(graph_default.get_conf())
    conf["output_file"] = util.cmd_args.tfrc_output_file
    conf["box_conf"].update({ "key": "RTT:%(rtt)s CWND Calc: %(cwnd_calc)s PPRTT: %(pp_rtt)s, %(packet_size_str)s, %(tp_packet_size_str)s, %(hdr_accounting)s", # , %(loss_percent)s
                              "sort_key_func": lambda x: (x["rtt"], x["pp_rtt"], x["cwnd_calc"], x["packet_size_str"], x["tp_packet_size_str"], x["hdr_accounting"], x["loss_percent"]),
                              "sort_keys": ["rtt", "pp_rtt", "cwnd_calc", "packet_size_str", "tp_packet_size_str", "hdr_accounting", "loss_percent"],
                              #"func" : latency_box_key_func,
                              #"percentiles": [.5, .75, .9],
                              "box_title_def" : "Packet count: %(count)s RTT: %(rtt)ss  Packets per RTT: %(pp_rtt)s  Payload Size: %(packet_size_str)s Avg Payload Size: %(avg_size)s Loss: %(loss_percent)s%%, cwnd_calc: %(cwnd_calc)s, TP PS: %(tp_packet_size_str)s HDR-accounting: %(hdr_accounting)s" })
    conf["set_conf"] = { "key": "" }
    conf["page_group_def"] = {"title": " RTT: %(rtt)s  PPRTT: %(cwnd_calc)s",
                              "sort_keys": ["rtt", "cwnd_calc"],
                              "sort_key_func": lambda x: (x["rtt"], x["cwnd_calc"])}

    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 8,# "x_axis_lim" : [0, 50], "y_axis_lim" : [0, 60],
                              "do_plots_func": do_ggplots,
                              "plot_func": plot_scatterplot_cwnd,
                              #"x_axis_main_ticks": 5,
                              #"x_axis_minor_ticks": 1,
                              #"y_axis_main_ticks": 10,
                              #"y_axis_minor_ticks": 5,
                              #"plot_func": plot_ecdf_box,
                              #"plot_func": plot_ecdf_box_ggplot,
                              #"legend_colors": color_map
                              #"legend": { "color_func": get_color_latency, "values": {"type_thick": "Greedy", "type_thin1": "Thin", "type_thin2": "Thin2", "type_thin3": "Thin3"}},
                              #"box_commands": thin_itt_mark_line_func,
                              #"r.plot_args": {"cex.main": 0.9 },
                              #"r.mtext_page_title_args": {"cex.main": 0.9 },
                          })

    conf["plot_conf"]["axis_conf"].update({"x_axis_lim" : [0, 50], "y_axis_lim" : [0, 60],
                                           #"y_axis_main_ticks": [0, 1.1, .2], "y_axis_minor_ticks": [0, 1.1, .1],
                                           "x_axis_main_ticks": 2, "x_axis_minor_ticks": 0.5,
                                           #"y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                           #"x_axis_label_key": "label",
                                           "x_axis_args": {"expand": IntVector([0, 1])},
                                           "y_axis_title" : { "title": "Congestion window size", },
                                           "x_axis_title" : { "title": "Time in seconds", },
                                        })

    conf["plot_conf"]["theme_args"].update({"legend.position": "right",
                                            #"legend.title": r('element_text(size=15)'),
                                            "legend.text": r('element_text(size=20)'),
                                            "legend.key.size": r('unit(12, "mm")'),
                                            "panel.margin": r('unit(0.8, "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                            "strip.text.y": r('element_text(size=20, angle=0, color="grey20")'),
                                            #theme(strip.text.x = element_text(size = 8, colour = "red", angle = 90))
                                            "plot.title": r('element_text(size=30, lineheight=1, hjust=0.5, vjust=1, color="grey20")'), # , face="bold"
                                            "axis.title.x": r('element_text(size=35, hjust=0.5, vjust=-2, color="grey20")'),
                                            "axis.title.y": r('element_text(size=35, hjust=0.5, vjust=3, color="grey20")'),
                                            "plot.margin": r('unit(c(1, 0, 1.6, 1.6), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                        })


    def do_facet_grid(plot_conf=None, plot_box_dict=None, gp=None, **kw):
        if not "plot" in kw:
            return
        plot = kw["plot"]
        stream_properties = plot["streams"][plot["streams_id"]]
        yield ggplot2.facet_grid("loss ~ .", scales="free_y")
        util.dict_set_default(plot_box_dict["legends"], ("colour", {"guide_args": {"title": "Streams"}, "scale": False }))
        return

    conf["box_conf"]["custom_ggplot2_plot_cmds"].update({"facet_grid": do_facet_grid})

    conf["box_conf"].update({ "key": "RTT:%(rtt)s CWND Calc: %(cwnd_calc)s PPRTT: %(pp_rtt)s, %(packet_size_str)s, %(tp_packet_size_str)s, %(hdr_accounting)s", # , %(loss_percent)s
                              "sort_key_func": lambda x: (x["rtt"], x["pp_rtt"], x["cwnd_calc"], x["packet_size_str"], x["tp_packet_size_str"], x["hdr_accounting"], x["loss_percent"]),
                              "sort_keys": ["rtt", "pp_rtt", "cwnd_calc", "packet_size_str", "tp_packet_size_str", "hdr_accounting", "loss_percent"],
                              #"func" : latency_box_key_func,
                              #"percentiles": [.5, .75, .9],
                              "box_title_def" : "RTT: %(rtt_ms)s ms  Packets per RTT: %(pp_rtt)s  Payload Size: %(packet_size_str)s" })


    conf["box_conf"]["legend"].update({#"legend_title": "Streams",
        "legend_sort": { "do_sort": True, #"sorted_args": { "key": lambda x: x["pif"], "reverse": False }
                         #{}
                     }})

    #conf["document_info"]["filename_as_pdf_title"] = False
    #conf["parse_results_func"] = latency_results_parse
    #conf["file_parse"].update({ "func": latency_file_parse_func })
    conf["document_info"]["title"] = "Latency Plots"
    conf["document_info"]["show"] = False
    conf["paper_width"] = 45
    conf["paper_height"] = 50
    conf["print_page_title"] = True

    del conf["plot_conf"]["axis_conf"]["y_axis_lim"]

    r.theme_set(r.theme_gray(base_size=22))

    plot_input = []
    size_func_ran_str="ran(50,300)"
    size_func_ran = lambda: random.randint(50, 300)

    size_func_ran_str="120"
    size_func_ran = lambda: 120
    size_func_ran_str="ran(100,140)"
    size_func_ran = lambda: random.randint(50, 200)

    size_func_1400_str="1400"
    size_func_1400 = lambda: 1400


    cwnd_func_avg_str = "throughput*RTT/avg_size"
    cwnd_func_avg = lambda hist: hist.x * hist.rtt / float(hist.avg_size)
    cwnd_func_mss_str = "throughput*RTT/1460"
    cwnd_func_mss = lambda hist: hist.x * hist.rtt / 1460.0

    tp_packet_size_func = lambda hist: 1460
    tp_packet_size_func_str = "1460"
    tp_packet_size_avg_func = lambda hist: hist.avg_size
    tp_packet_size_avg_func_str = "hist.avg_size"

    count = 20
    conf["plot_conf"]["axis_conf"]["x_axis_lim"][1] = 50

    i_outer = [1]
    def make(count, param):
        i = i_outer[0]
        for i in range(i, i + count):
            d = dict(param, stream_id="TFRC %d" % i)
            #print "d:", d
            plot_input.append(d)
        i_outer[0] = i + 1

    param_def = dict(count=1000, rtt=0.15, pp_rtt=5, loss_percent=2, hdr_account=False)

    pp_rtt = [3]
    cwnd = []
    packet_size = []
    tp_packetsize = []
    hdr_accounting = [True]
    loss_percents = [0.1, 0.2, 0.5, 1, 2, 5]

    #cwnd.append((cwnd_func_mss, cwnd_func_mss_str))
    cwnd.append((cwnd_func_avg, cwnd_func_avg_str))

    packet_size.append((size_func_ran, size_func_ran_str))
    #packet_size.append((size_func_1400, size_func_1400_str))

    #tp_packetsize.append((tp_packet_size_func, tp_packet_size_func_str))
    tp_packetsize.append((tp_packet_size_avg_func, tp_packet_size_avg_func_str))

    plot_count = 0
    for (pp_rtt_val, cwnd_arg, tp_ps_func, packet_size, hdr_acc, loss) in itertools.product(pp_rtt, cwnd, tp_packetsize, packet_size, hdr_accounting, loss_percents):
        cwnd_func, cwnd_calc = cwnd_arg
        packet_size_func, packet_size_str = packet_size
        tp_ps_func, tp_ps_func_str = tp_ps_func
        make(count, dict(param_def, cwnd_func=cwnd_func, cwnd_calc=cwnd_calc, pp_rtt=pp_rtt_val, packet_size=packet_size_func, packet_size_str=packet_size_str,
                         tp_packet_size_func=tp_ps_func, tp_packet_size_str=tp_ps_func_str, hdr_accounting=hdr_acc, loss_percent=loss))
        plot_count += 1
        i_outer = [1]

    conf["plot_conf"].update({"n_columns": 1, "n_rows" : plot_count / 2})
    conf["paper_height"] = plot_count / 2 * 7

    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1})
    conf["paper_height"] = 6

    conf["paper_width"] = 35
    conf["paper_height"] = 5

    conf["paper_width"] = 30
    conf["paper_height"] = 20

    conf["files"] = plot_input
    print "plot_count:" , plot_count
    #print "plot_input:", plot_input

    conf["file_parse"]["parse_input"] = parse_input
    #conf["file_parse"]["files_nomatch_regex"].append(".*qlen46875.*")
    #conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 1})
    return conf

get_conf = get_plot_conf

"""
void tfrc_lh_calc_i_mean(struct tfrc_loss_hist *lh)
 {
     u32 i_i, i_tot0 = 0, i_tot1 = 0, w_tot = 0;
     int i, k = tfrc_lh_length(lh) - 1; /* k is as in rfc3448bis, 5.4 */

     if (k <= 0)
         return;

     for (i = 0; i <= k; i++) {
         i_i = tfrc_lh_get_interval(lh, i);

         if (i < k) {
             i_tot0 += i_i * tfrc_lh_weights[i];
             w_tot  += tfrc_lh_weights[i];
         }
         if (i > 0)
             i_tot1 += i_i * tfrc_lh_weights[i-1];
     }

     lh->i_mean = max(i_tot0, i_tot1) / w_tot;
 }
"""

tfrc_weights = [1, 1, 1, 1, 0.8, 0.6, 0.4, 0.2];

def loss_event_rate(hist):
    I_tot0 = 0
    I_tot1 = 0
    W_tot = 0
    k = len(hist.intervals) - 1
    if k <= 0:
        return 0

    for i in range(len(hist.intervals)):
        I_i = hist.intervals[i].length
        if i < k:
            I_tot0 += (I_i * tfrc_weights[i])
            #print "I_tot0:", I_tot0
            #print "i: %d * w: %f: %f" % (I_i, tfrc_weights[i], I_i * tfrc_weights[i])
            W_tot += tfrc_weights[i]
        if i > 0:
            I_tot1 += (I_i * tfrc_weights[i-1])

    I_tot = max(I_tot0, I_tot1)
    try:
        I_mean = I_tot/float(W_tot)
        p = 1 / I_mean
    except ZeroDivisionError, e:
        print "E:", e
        print "I_tot:", I_tot
        print "W_tot:", W_tot
        print "W_tot:", W_tot
        raise
    return p

def loss_event_rate_sp(hist):
    I_tot0 = 0
    I_tot1 = 0
    W_tot = 0
    k_tot0 = 0
    k_tot1 = 0
    for i in range(len(hist.intervals)):
        I_i = hist.intervals[i].length
        if i > 0:
            interval_len = hist.intervals[i].t_start - hist.intervals[i - 1].t_start
            rtt_tdelta = datetime.timedelta(microseconds=(self.rtt * 1000000))
            # This interval is short
            if interval_len < rtt_tdelta:
                I_i = hist.intervals[i].length / hist.intervals[i].lost

        I_tot0 += (I_i * tfrc_weights[i])
        W_tot += tfrc_weights[i]
        if i > 0:
            I_tot1 += (I_i * tfrc_weights[i-1])

    I_tot = max(I_tot0, I_tot1)

    try:
        I_mean = I_tot/float(W_tot)
        p = 1 / I_mean
    except ZeroDivisionError, e:
        print "E:", e
        print "I_tot:", I_tot
        print "W_tot:", W_tot
        print "W_tot:", W_tot
        raise
    return p

import datetime

class LossInterval(object):
    def __init__(self, t_stamp):
        self.length = 0
        self.lost = 1
        self.t_start = t_stamp
        self.t_end = None

class LossEventHist(object):
    def __init__(self):
        self.intervals = [LossInterval(datetime.timedelta(microseconds=-1))]
        self.avg_size = 0
        self.rtt = 0
        self.x = 0

    def add_interval(self, t_stamp):
        # Test if this loss is within current loss interval
        new_interval = None
        diff = t_stamp - self.intervals[0].t_start
        rtt_tdelta = datetime.timedelta(microseconds=(self.rtt * 1000000))
        if diff > rtt_tdelta:
            if args.verbose > 2:
                print "New Loss Interva - Diff (%s) greater than rtt (%s)" % (diff, rtt_tdelta)
            new_interval = LossInterval(t_stamp)
            self.intervals[0].t_end = t_stamp
            self.intervals.insert(0, new_interval)
            if len(self.intervals) == 10:
                self.intervals.pop()
        else:
            #print "Not Adding Interval - Diff (%s) less than rtt (%s)" % (diff, rtt_tdelta)
            pass
        return new_interval

    def handle_new_packet(self, size, lost, t_stamp):
        if lost is True:
            self.add_interval(t_stamp)
        self.avg_size = tfrc_ewma(self.avg_size, size, weight=9)
        self.intervals[0].length += 1

    def print_history(self):
        print "Intervals: %d" % len(self.intervals),
        for i in reversed(self.intervals):
            print " %d/%d" % (i.lost, i.length),
        print

        print "Weights:      ",
        for i in range(min(len(self.intervals), 8)):
            print " %d  " % (tfrc_weights[i]),
        print


import random

def tfrc_ewma(avg, newval, weight=9):
    return ((weight * avg + (10 - weight) * newval) / 10) if avg else newval

def calc_throughput(hist, tp_packet_size_func, hdr_accounting=False):
    """
     X is the transmit rate in bytes/second.
     s is the packet size in bytes.
     R is the round trip time in seconds.
     p is the loss event rate, between 0 and 1.0, of the number of loss
       events as a fraction of the number of packets transmitted.
     t_RTO is the TCP retransmission timeout value in seconds.
     b is the number of packets acknowledged by a single TCP
       acknowledgement.
                                  s
     X =  ----------------------------------------------------------
         R*sqrt(2*b*p/3) + (t_RTO * (3*sqrt(3*b*p/8) * p * (1+32*p^2)))
    """
    x = (hist.rtt * sqrt(2 * hist.b * hist.p/3.0) + (hist.t_rto * (3 * sqrt(3 * hist.b * hist.p/8.0) * hist.p * (1+32 * pow(hist.p, 2)))))

    x = tp_packet_size_func(hist)/x
    packets = x/hist.avg_size
    new_x = (x * hist.avg_size) / (hist.avg_size + 40)
    return new_x if hdr_accounting else x

def make_data(param, count=300, stream_id="", rtt=0.15, pp_rtt=5, loss_percent=5, packet_size=lambda: random.randint(50, 300),
              cwnd_func=lambda hist: hist.x/float(hist.avg_size),
              tp_packet_size_func=lambda hist: hist.avg_size, hdr_accounting=False,
              **kw):
    param["hist"] = hist = LossEventHist()
    hist.rtt = rtt
    hist.t_rto = hist.rtt * 4
    hist.b = 1
    #size = random.randint(10, 200)
    #size = random.randint(50, 300)
    #packet_size = lambda: 1000

    if "duration" in kw:
        packets_per_second = 1 / rtt * pp_rtt
        param["count"] = count = int(packets_per_second * kw["duration"])
        print "New Count based on duration (%s):" % kw["duration"], count

    loss_rate = loss_percent * 100

    loss_estimate = 0
    time_ms = 0
    data = {"time": [], "rtt": [], "size": [], "p": [], "x": [], "cwnd": [], "stream_id": []}
    for i in range(count):
        time_ms += (hist.rtt * 1000) / pp_rtt
        #print "time_ms:", time_ms
        t = time_ms * 1000
        tstamp = datetime.timedelta(microseconds=t)
        size = packet_size()
        #lost = random.randint(1, 10000) > (10000 - loss_rate)
        lost = random.randint(1, 10000) > (10000 - loss_rate) or i == 2
        if lost:
            loss_estimate += 1
        hist.handle_new_packet(size, lost, tstamp)
        hist.p = loss_event_rate(hist)

        if hist.p:
            hist.x = calc_throughput(hist, tp_packet_size_func, hdr_accounting)

        intervals = ", ".join(["%2d" % h.length for h in hist.intervals])

        cwnd = cwnd_func(hist)
        #cwnd = hist.x/1460
        #cwnd = hist.x/hist.avg_size

        data["time"].append(t/1000000)
        data["rtt"].append(hist.rtt)
        data["size"].append(size)
        data["p"].append(hist.p)
        data["x"].append(hist.x)
        data["stream_id"].append(stream_id)
        data["cwnd"].append(cwnd)

        if args.verbose > 2:
            print "ID: %s, S: %3d, Lost: %d, P: %f, TP: %.2f (%.2f) (%s)" % (stream_id, size, lost, hist.p, cwnd, hist.x, intervals)

    print "%s : Duration: %s, Loss %d/%d (%.1f%%)" % (stream_id, tstamp, loss_estimate, count, 100 * (loss_estimate/float(count)))
    dataf = DataFrame({'time': FloatVector(data["time"]), 'rtt': FloatVector(data["rtt"]),
                       'size': IntVector(data["size"]), 'p': FloatVector(data["p"]),
                       'cwnd1': FloatVector(data["x"]), 'cwnd': FloatVector(data["cwnd"]),
                       'stream': StrVector(data["stream_id"]), 'loss': "Loss\n%s%%" % loss_percent})
    return dataf

class Args(Exception):
    """This exception means that a custom error happened."""

if __name__ == "__main__":
#from graph_base import parse_args
#    parse_args()
#    conf = get_conf()
#    make_data()
    print "Main"
    args = Args()
    args.verbose = False
    packet_size = lambda: 1000
    loss_percent = 5
    loss_estimate = 0

    hist = LossEventHist()
    hist.rtt = 0.150
    time_ms = 0

    for i in range(1000):
        time_ms += 151
        tstamp = datetime.timedelta(microseconds=(time_ms * 1000))
        size = packet_size()
        lost = random.randint(0, 100) > (100 - loss_percent)
        if lost:
            loss_estimate += 1
        hist.handle_new_packet(size, lost, tstamp)

    ##values = [14, 10, 26, 14, 45, 21, 18, 8]
    #lost =   [2, 3, 5]
    #values = [10, 6, 8]
    #
    #for i in range(len(values)):
    #    time_ms += 151
    #    tstamp = datetime.timedelta(microseconds=(time_ms * 1000))
    #    #size = packet_size()
    #    #lost = random.randint(0, 100) > (100 - loss_percent)
    #    #if lost:
    #    #    loss_estimate += 1
    #    #hist.handle_new_packet(size, lost, tstamp)
    #    interval = hist.add_interval(tstamp)
    #    interval.length = values[i]
    #    interval.lost = lost[i]
    #
    #del hist.intervals[3]

    hist.print_history()

    #import numpy as np
    #k_w0 = np.array([1,1,1], dtype=np.float32)
    #k_w1 = np.array(lost, dtype=np.float32)
    #
    #w0 = np.array(tfrc_weights[:len(lost)], dtype=np.float32)

