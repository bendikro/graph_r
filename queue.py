#!/usr/bin/env python
from datetime import datetime
import argparse
import os

from graph_base import parse_args

from graph_default_bendik import get_default_conf
from graph_r_ggplot import plot_scatterplot_cwnd, do_ggplots

#import graph_default
#graph_default.get_conf = get_default_conf

#from graph_default_bendik import get_latency_conf

#import latency
#latency.get_conf = get_latency_conf

import graph_default
import util
from graph_r import *
import glob

from math import sqrt, pow

def parse_input(plot_conf, param, arg):
    dataf = make_data(param, **param)
    dataf[0] = dataf[0].ro / 10**9

    dataf.names[0] = "time"
    dataf.names[1] = "cwnd"
    dataf.names[2] = "stream"
    dataf.names[3] = "packet_size"
    dataf.names[4] = "dropped"
    dataf.names[5] = "port"

    #filt = dataf.rx(dataf.rx2("port").ro == 12345, True)
    #dataf = filt

    from rpy2.robjects.packages import importr
    plyr = importr("plyr")

    counts = plyr.count(dataf, vars="port")
    print "counts:", counts

    file_conf = dict(param)
    file_conf["data"] = dataf
    file_conf["unique_input_identity"] = "TEST"

    file_conf["streams"] = {"s1": {"stream_count": 1, "smooth": False, "line": True, "factor": "port" }}
    file_conf["streams_id"] = "s1"

    stream_type_to_ggplot_properties = {"s1": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Blues")[7:7])', "legend_name": "Thin1"},
                                                                 {"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Thin2"},
                                                                 {"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin3"},
                                                                 {"color_code": 'c(brewer.pal(9, "Greys")[7:7])', "legend_name": "Thick"}]
                                           }
    }
    stream_properties = file_conf["streams"][file_conf["streams_id"]]
    #stream_properties["color_code"] = stream_type_to_ggplot_properties[file_conf["streams_id"]]["color_code"]
    #stream_properties["legend_name"] = stream_type_to_ggplot_properties[file_conf["streams_id"]]["legend_name"]
    stream_properties["color_mapping"] = stream_type_to_ggplot_properties[file_conf["streams_id"]]["color_mapping"]
    return file_conf

def get_plot_conf():
    conf = deepcopy(graph_default.get_conf())
    conf["output_file"] = util.cmd_args.queue_output_file
    conf["box_conf"] = { "key": "",
                         "sort_key_func": lambda x: (x),
                         #"sort_key_func": lambda x: (x["rtt"], x["pp_rtt"], x["cwnd_calc"], x["packet_size_str"]),
                         #"sort_keys": ["rtt", "pp_rtt", "cwnd_calc", "packet_size_str"],
                         #"func" : latency_box_key_func,
                         #"percentiles": [.5, .75, .9],
                         "box_title_def" : "" }
    conf["set_conf"] = { "key": "" }
    conf["page_group_def"] = {"title": " ",
                              #"sort_keys": ["rtt", "cwnd_calc"], "sort_key_func": lambda x: (x[0], x[1])
                              "sort_keys": [], "sort_key_func": lambda x: (x)
    }
    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1,
                              #"x_axis_lim" : [0, 80],
                              "x_axis_lim" : [10, 40],
                              "y_axis_lim" : [0, 40],
                              "y_axis_title" : "ECDF",
                              "x_axis_title" : {
                                  #"title": ["Latency in milliseconds", "THICK", "THIN", "RDB:PIF:3", "RDB:PIF:6", "RDB:PIF:10", "RDB:PIF:20"],
                                  "title": ["ACK Latency in milliseconds"],
                                  #"adj": [0, .3, .4, .5, .65, .75, .9],
                                  #"colors" : [color_map["default"], color_map["thick"], color_map["thin"], color_map["rdb_pif3"],
                                  #            color_map["rdb_pif6"], color_map["rdb_pif10"], color_map["rdb_pif20"]]
                              },
                              "do_plots_func": do_ggplots,
                              "plot_func": plot_scatterplot_cwnd,
                              "x_axis_main_ticks": 0.5,
                              "x_axis_minor_ticks": 0.01,
                              "legend_columns": 1,
                              "default_factor": "port",
                              #"y_axis_main_ticks": 10,
                              #"y_axis_minor_ticks": 5,
                              #"plot_func": plot_ecdf_box,
                              #"plot_func": plot_ecdf_box_ggplot,
                              #"legend_colors": color_map
                              #"legend": { "color_func": get_color_latency, "values": {"type_thick": "Greedy", "type_thin1": "Thin", "type_thin2": "Thin2", "type_thin3": "Thin3"}},
                              #"box_commands": thin_itt_mark_line_func,
                              "r.plot_args": {"cex.main": 0.9 },
                              "r.mtext_page_title_args": {"cex.main": 0.9 },
                              }
                             )

    #conf["parse_results_func"] = latency_results_parse
    #conf["file_parse"].update({ "func": latency_file_parse_func })
    conf["document_info"]["title"] = "Latency Plots"
    conf["document_info"]["show"] = False
    conf["paper_width"] = 1000
    conf["paper_height"] = 18
    conf["print_page_title"] = True

    from rpy2.robjects import r
    r.theme_set(r.theme_gray(base_size=42))

    plot_input = []
    size_func_ran_str="ran(50,300)"
    size_func_ran = lambda: random.randint(50, 300)
    size_func_1400_str="1400"
    size_func_1400 = lambda: 1400


    cwnd_func_avg_str = "throughput*RTT/avg_size"
    cwnd_func_avg = lambda hist: hist.x * hist.rtt / hist.avg_size
    cwnd_func_mss_str = "throughput*RTT/1460"
    cwnd_func_mss = lambda hist: hist.x * hist.rtt / 1460.0
    #size_func = lambda: 1400

    count = 5

    plot_input = [{"file": "crap2.txt"}]

    conf["files"] = plot_input
    print "plot_input:", plot_input

    conf["file_parse"]["parse_input"] = parse_input
    #conf["file_parse"]["files_nomatch_regex"].append(".*qlen46875.*")
    #conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 1})
    return conf

get_conf = get_plot_conf

def make_data(param, **args):
    print "args:", args
    dataframe = r["read.csv"](args["file"], header=False)
    print "DATA:", dataframe.names
    return dataframe
    #print "Duration: %s, Loss %d/%d (%.1f)" % (tstamp, loss_estimate, count, 100 * (loss_estimate/float(count)))
    #dataf = DataFrame({'time': FloatVector(data["time"]), 'rtt': FloatVector(data["rtt"]),
    #                   'size': IntVector(data["size"]), 'p': FloatVector(data["p"]),
    #                   'cwnd1': FloatVector(data["x"]), 'cwnd': FloatVector(data["cwnd"]), 'stream': StrVector(data["stream_id"])})
    #return dataf


if __name__ == "__main__":
#    parse_args()
#    conf = get_conf()
    make_data()
