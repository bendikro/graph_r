import util
import os, sys, time, re, traceback
import itertools, rpy2
from rpy2 import robjects
from rpy2.robjects import r, Formula, globalenv
from rpy2.robjects.packages import importr
from rpy2.robjects.vectors import Vector, IntVector, DataFrame, StrVector, FloatVector
from rpy2.robjects.functions import SignatureTranslatedFunction
from rpy2.robjects.numpy2ri import numpy2ri
import rpy2.rlike.container as rlc
from operator import itemgetter, attrgetter
import pprint
from copy import deepcopy
import numpy
from collections import OrderedDict
import util
from util import cprint, colored, vprint

r['options'](warn=1)

r.barplot = SignatureTranslatedFunction(r.barplot, init_prm_translate = {'args_legend': 'args.legend', "legend_text": "legend.text", "cex_names": "cex.names" })

hmisc = importr('Hmisc')
Ecdf = SignatureTranslatedFunction(hmisc.Ecdf, init_prm_translate = {'label_curves': 'label.curves'})

car = importr('car')
#car2 = SignatureTranslatedFunction(car, init_prm_translate = {'reg_line': 'reg.line'})

scatterplot = car.scatterplot
#datasets = importr('datasets')
#mtcars = datasets.mtcars
#lattice = importr('lattice')

rprint = robjects.globalenv.get("print")


#scatterplot(mpg ~ wt | cyl, data=mtcars,
#   xlab="Weight of Car", ylab="Miles Per Gallon",
#   main="Enhanced Scatter Plot",
#   labels=row.names(mtcars))

page_margins = (2, 5, 5, 1) # bottom, left, top, right
box_margins  = (3, 0, 2, 0) # bottom, left, top, right

def xline(bandiwdth_cap, thin_packet_size, thin_iat, thin_num, greedy_num):
    # how much the thin streams would use if alone
    x = int(thin_num * (((thin_packet_size + 40 + 36) * 8) / float(thin_iat / 1000.0)))
    y = ((bandiwdth_cap * 1000.0)/float(thin_num+greedy_num))*thin_num
    return min(x, y)



def plot_barplot_box(conf, plot_box_dict, sets):
    """
    Creates a barplot

    """
    add = False
    plot_labels = []
    bar_data = []

    y_axis_flag = "n"
    # Column 0 should show the y axis labels
    if plot_box_dict["column_index"] == 0:
        y_axis_flag = "s"
    #r.par(mar=plot_box_dict["mar"], yaxt=y_axis_flag, xaxt="n")
    r.par(mar=plot_box_dict["mar"], yaxt=y_axis_flag)
    #r.par(las=2)

    colors = []
    legends = None
    index = plot_box_dict["index"]
    n_columns = conf["plot_conf"]["n_columns"]
    n_rows = conf["plot_conf"]["n_rows"]
    first_index_on_last_row = n_columns * (n_rows -1)
    index_within_page = plot_box_dict["index_within_page"]
    index_last_row_on_last_page = plot_box_dict["index_last_row_on_last_page"]

    sorted_keys = sorted(sets.keys(), key=lambda k: sets[k]["plots"][0]["column"])
    for plot_set_key in sorted_keys:
        plot_set = sets[plot_set_key]

        for plot in sorted(plot_set["plots"], key=lambda p: p["column"]):
            #print "data_trans:", plot["data_trans"]
            data = IntVector(plot["data_trans"]["data"])
            colors += plot["data_trans"]["colors"]
            label = plot["label"]
            plot_labels.append(label)

            bar_data.append((label, data))
            if legends is None:
                legends = plot["data_trans"]["legend_names"]
            x_axis_lim = IntVector([0, len(sets) +1])

    x_axis_lim = IntVector([0, len(sets) +1])
    dataframe = DataFrame(rlc.OrdDict(bar_data))
    m = r["data.matrix"](dataframe)
    #print "D:\n", dataframe
    #print "M:\n", m
    args_legend = r.list(x = "topright", bty = "n")
    r.barplot(m, beside=True,
              col=StrVector(colors),
              args_legend=args_legend,
              legend_text=StrVector(legends),
              ylim=IntVector(conf["plot_conf"]["y_axis_lim"]),
              main=plot_box_dict["box_title"],
              cex_names=conf["plot_conf"]["r.plot_args"]["cex.axis"],
              )

    #x_axis_labels = [plot_labels[column] for column in sorted(plot_labels)]
    x_axis_labels = plot_labels
    x_axis_data = [i for i in xrange(0, len(x_axis_labels) * 8, 8)]
    #x_axis_data = [i for i in xrange(0, len(x_axis_labels))]
    #print "x_axis_labels:", x_axis_labels
    #print "x_axis_data:", x_axis_data
    if index % n_columns == 0:
        r.axis(side=2, at=IntVector((plot_box_dict["y_axis_data"])), labels=Vector((plot_box_dict["y_axis_labels"])))
    # X axis
    if index_within_page >= first_index_on_last_row or index >= index_last_row_on_last_page or True:
        #r.axis(side=1, at=IntVector((x_axis_data)), labels=Vector((x_axis_labels)))
        ## Fetch the adjustments for the axis, and adjust the top_adjustment for the labels
        #r.text(x=IntVector(x_axis_data), # Coordinates where the text labels should be written
        #       labels=Vector((x_axis_labels)),
        #       #srt=25,   # Rotation in degrees
        #       #pos=1,    # 1 means below the coordinates
        #       #offset=2, # Offset of the label from the specified coordinate in fractions of a character width.
        #       offset=0,
        #       cex=1.0, # X axis label text size relative to default
        #       #xpd=True,
        #       xpd=False,
        #       #adj=1,
        #)
        pass


def plot_boxplot(conf, plot_box_dict, sets):
    """
    Creates a boxplot

    """
    add = False
    plot_labels = {}

    y_axis_flag = "n"
    # Column 0 should show the y axis labels
    if plot_box_dict["column_index"] == 0:
        y_axis_flag = "s"
    r.par(mar=plot_box_dict["mar"], yaxt=y_axis_flag, xaxt="n")

    for plot_set in sets.itervalues():
        for plot in plot_set["plots"]:
            data = [plot["colsum"]]
            #print "Data[colsum]:", plot["colsum"]
            x_axis_lim = IntVector([0, len(sets) +1])
            #print "x_axis_lim:", x_axis_lim
            plot_args = {"ylim": IntVector(conf["plot_conf"]["y_axis_lim"]), "xlim": x_axis_lim, "xaxt": "n", "xlab": "", "yaxt": "n",
                    "border": StrVector([plot["color"]])}
            if "column" in plot:
                col = plot["column"](plot)
                plot_args["at"] = IntVector([col])
                #print "[col]:", [col]
                plot_labels[col] = plot["label"]
            #print "args:", plot_args
            r.boxplot(data, main=plot_box_dict["box_title"], add=add, varwidth=True, **plot_args)
            add = True

    x_axis_labels = [plot_labels[column] for column in sorted(plot_labels)]
    x_axis_data = [i for i in xrange(1, len(x_axis_labels)+1)]

    #print "x_axis_labels:", x_axis_labels
    #print "x_axis_data:", x_axis_data

    n_columns = conf["plot_conf"]["n_columns"]
    n_rows = conf["plot_conf"]["n_rows"]
    first_index_on_last_row = n_columns * (n_rows -1)

    index = plot_box_dict["index"]
    index_within_page = plot_box_dict["index_within_page"]
    index_last_row_on_last_page = plot_box_dict["index_last_row_on_last_page"]

    # Y axis
    # Only write the Y axis labels on the first plot on each row
    if index % n_columns == 0:
        r.axis(side=2, at=IntVector(conf["y_axis_data"]), labels=StrVector(conf["y_axis_labels"]))
        pass
    # X axis
    if index_within_page >= first_index_on_last_row or index >= index_last_row_on_last_page or True:
        #r.axis(1, at=IntVector(x_axis_data), labels=StrVector(x_axis_labels))
        # Fetch the adjustments for the axis, and adjust the top_adjustment for the labels
        if x_axis_labels:
            y_adj = r.par("usr")[2] - 0.5
            r.text(IntVector(x_axis_data), # Coordinates where the text labels should be written
                   y=y_adj,
                   labels=StrVector(tuple(x_axis_labels)),
                   srt=25,   # Rotation in degrees
                   pos=1,    # 1 means below the coordinates
                   offset=0, # Offset of the label from the specified coordinate in fractions of a character width.
                   cex=1.0, # X axis label text size relative to default
                   xpd=True,
                   )

def plot_ecdf_box(conf, plot_box_dict, sets):
    """
    Creates a ecdf plot
    """
    add = False
    y_axis_flag = "n"
    # Column 0 should show the y axis labels
    if plot_box_dict["column_index"] == 0:
        y_axis_flag = "s"

    r.par(mar=plot_box_dict["mar"])
    legend_colors = []
    legend_labels = []
    legend_attr = OrderedDict()

    for plot_set in sets.itervalues():
        for plot in plot_set["plots"]:
            #print "plot:", plot.keys()
            #print "type_id:", plot["type_id"]
            #print "type_id_pif:", plot["type_id_pif"]
            #print 'plot[plot["type_id"]]', plot[plot["type_id"]]
            if "legend" in conf["plot_conf"]:
                #print "Values:", conf["plot_conf"]["legend"]["values"]
                #print 'plot["type_id"]:', plot["type_id"]
                legend_attr[conf["plot_conf"]["legend"]["values"][plot["type_id"]]] = conf["plot_conf"]["legend"]["color_func"](plot)
                plot["color"] = conf["plot_conf"]["legend"]["color_func"](plot)
                legend_colors.append(plot["color"])
                legend_labels.append(conf["plot_conf"]["legend"]["values"][plot["type_id"]])
                #print 'color:', plot["color"]
                #print 'label:', conf["plot_conf"]["legend"]["values"][plot["type_id"]]

            data = plot["ecdf_values"]
            if not type(data) is list:
                data = [data]

            #print "Data:", len(data)
            for i, d in enumerate(data):
                label_curves = r('list(method="arrow", cex=.8)')
                args = plot_box_dict["r.plot_args"]
                args["add"] = add
                args["main"] = plot_box_dict["box_title"]
                args["xlab"] = 'Test Results'

                if "percentiles" in plot:
                    args["q"] = FloatVector(plot["percentiles"])
                if "color" in plot:
                    args["col"] = StrVector([plot["color"]])
                if "x_axis_lim" in plot_box_dict:
                    args["xlim"] = FloatVector(plot_box_dict["x_axis_lim"])
                if "y_axis_lim" in plot_box_dict:
                    args["ylim"] = FloatVector(plot_box_dict["y_axis_lim"])

                #print "col:", plot["color"]
                p = Ecdf(d,
                         #group=group,
                         #label_curves=label_curves,
                         yaxt=y_axis_flag,
                         xaxt="s",
                         **args
                         )
                #knots = r.knots(p)
                #print "knots:", knots
                add = True


    if "legend" in conf["plot_conf"]:
        args = {}
        args["lty"] = 1
        args["inset"] = FloatVector((0.02, 0.04))

        # Get legend values sorted
        if legend_attr:
            args["col"] = StrVector([legend_attr[k] for k in sorted(legend_attr.keys())])
        r.legend("bottomright", StrVector(sorted(legend_attr.keys())), **args)

    outside = -1
    if y_axis_flag == "n":
        outside = 1
        # Major ticks for y axis
        r.axis(2, tck=0.02, labels=False)

    # Minor ticks for y-axis
    r.par(yaxp=FloatVector((0.5, 1, 20)))
    r.axis(2, tck=0.01 * outside, labels=False)

    # Minor ticks for x-axis
    hmisc.minor_tick(**{"nx": 10, "ny": 1, "tick.ratio": 0.5})

    if plot_box_dict["meta_data"]["enabled"] is True:
        plot_box_dict["meta_data"]["func"](conf, plot_box_dict, sets)


def do_plots(conf, plot_groups):
    plot_list = plot_groups["plot_boxes"]
    n_rows = conf["plot_conf"]["n_rows"]
    n_columns = conf["plot_conf"]["n_columns"]

    # This is the margins for all the plots
    r.par(oma=IntVector(page_margins)) # bottom, left, top, right)
    # Rows and columns in the grid
    r.par(mfrow=IntVector((n_rows, n_columns)))

    plots_per_page = n_columns * n_rows
    plots_on_last_page = len(plot_list) % plots_per_page
    index_last_row_on_last_page = len(plot_list) - (plots_per_page - (plots_on_last_page - n_rows))
    max_plots_per_page = 0
    plot_index_on_page = 0
    if conf["plots_per_page"]:
        max_plots_per_page = conf["plots_per_page"]


    print "n_columns:", n_columns
    print "n_rows:", n_rows
    print "plots_per_page:", plots_per_page

    for index, plot_box_dict in enumerate(plot_list):
        #print "%d %% %d: %d" % (plots_per_page, index, index % plots_per_page)
        if max_plots_per_page:
            #print "plot_index_on_page: %d" % (plot_index_on_page)
            if plot_index_on_page == max_plots_per_page:
                #print "Calling plot.new %d times" % (plots_per_page - max_plots_per_page)
                for i in range(plots_per_page - max_plots_per_page):
                    r("plot.new()")
                plot_index_on_page = 0
            plot_index_on_page += 1

        print "Creating plot %d of %d" % (index + 1, len(plot_list))
        if util.cmd_args.verbose > 2:
            print "box_key:", plot_box_dict["box_key"]

        # plot_set contains all the plots we want in the same box
        title = plot_box_dict["box_title"]
        sets = plot_box_dict["sets"]
        index_within_page = index % plots_per_page
        column_index = index % n_columns

        # Margin parameteres
        mar = IntVector(box_margins)
        plot_box_dict["mar"] = mar
        plot_box_dict["column_index"] = column_index
        plot_box_dict["index"] = index
        plot_box_dict["index_within_page"] = index_within_page
        plot_box_dict["index_last_row_on_last_page"] = index_last_row_on_last_page

        try:
            # Run the plot function for this box
            plot_box_dict["plot_func"](conf, plot_box_dict, sets)
        except:
            cprint("Exception occured when running plot function for plot box '%s', sets: %s" % (plot_box_dict["box_key"], sets.keys()), "red")
            raise

        if "custom_cmds" in plot_box_dict:
            for cmd in plot_box_dict["custom_cmds"]:
                r(cmd)

        if plot_groups.get("page_title", None) and conf["print_page_title"]:
            #print "page_title:", plot_groups["page_title"]
            r.mtext(plot_groups["page_title"], side=3, line=2, outer=True, **conf["plot_conf"]["r.mtext_page_title_args"])
            pass

        if plot_groups.get("y_axis_title", None):
            r.mtext(plot_groups["y_axis_title"], side=2, line=4, outer=True, cex=0.95)

        if plot_groups.get("x_axis_title", None):
            adj = robjects.NA_Character
            colors = robjects.NA_Character
            if "adj" in plot_groups["x_axis_title"]:
                adj = Vector(plot_groups["x_axis_title"]["adj"])
            if "colors" in plot_groups["x_axis_title"]:
                colors = StrVector(plot_groups["x_axis_title"]["colors"])
            r.mtext(text=Vector(plot_groups["x_axis_title"]["title"]), side=1, line=1, outer=True,
                    adj=adj, col=colors, cex=0.95)


def create_plots(conf, page_groups):
    """
    Creates the R Plots.

    """
    #print "conf:", conf
    #print "page_groups:", len(page_groups)
    grdevices = importr('grDevices')
    extrafont = importr('extrafont')
    #library(extrafont)
    #r("loadfonts()")
    #r.loadfonts()
    #r.loadfonts(device="pdf")
    total_plot_box_count = 0

    #print "output_file:", conf["output_file"]
    name, extension = os.path.splitext(conf["output_file"])
    if conf["plot_conf"]["make_plots"]:
        if extension == ".ps":
            #r.postscript(file=conf["output_file"], paper=conf["paper"])
            r.postscript(file=conf["output_file"])
        elif extension == ".png":
            r.png(file=conf["output_file"])
        else:
            args = deepcopy(conf["pdf_args"])
            #args["fonts"] = "Helvetica"
            args["useDingbats"] = False

            if "paper_height" in conf:
                args["height"] = conf["paper_height"]
            if "paper_width" in conf:
                args["width"] = conf["paper_width"]

            #pdf("font_plot.pdf", family="Impact", width=4, height=4)

            #r.pdf(file=conf["output_file"], **args)
            del args["useDingbats"]
            del args["paper"]
            del args["title"]
            args["onefile"] = True
            #print "pdf_args:", args
            r.cairo_pdf(file=conf["output_file"], **args)

        if conf["document_info"]["show"]:
            # Create first page with info
            r.plot(IntVector([i for i in range(0, 10)]), type = "n", xaxt="n", yaxt="n", bty="n", xlab = "", ylab = "")
            r.text(5, 8, conf["document_info"]["title"], cex=2.3)
            index = 7
            for info in conf["document_info"]["info"]:
                r.text(5, index, info)
                index -= 1

    for plot_groups in page_groups:
        if plot_groups["page_title"]:
            print "Creating plots for page title '%s'" % plot_groups["page_title"]
        total_plot_box_count += len(plot_groups["plot_boxes"])
        conf["plot_conf"]["do_plots_func"](conf, plot_groups)

    if conf["plot_conf"]["make_plots"]:
        if total_plot_box_count:
            cprint("Result written to '%s'" % conf["output_file"], "green")
        else:
            cprint("Not results produced", "yellow")
        grdevices.dev_off()
