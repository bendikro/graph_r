from graph_r import *
import common

def get_stdev(x, stdev=True):
    #print "get_stdev(stdev:%s): %s" % (stdev, x)
    #print "get_stdev:", type(x)
    if type(x) == rpy2.rinterface.NACharacterType:
        return x

    s = str(x).split(":")
    #print "stdev(%s) : %s" % (stdev, s)
    if len(s) == 1:
        ret = "NA" if stdev else int(x)
    else:
        ret = int(s[1]) if stdev else int(s[0])
    #print "ret:", ret
    return ret

def set_values_from_location(table_conf, new_table_entry, data, data_name):
    #print "set_analysetcp_data:", table_conf
    #latencies = data
    #print "data:", data
    #print "Bytes Loss:", data["Bytes Loss"]

    for k, v in table_conf["keys"].iteritems():
        #print "K:", k
        if "location" not in v:
            continue
        if v["location"] == data_name:  #  "analysetcp"
            value = data[v["key"]]
            #print "Found analysetcp: %s:%s" % (v["key"], value)
            if "func" in v:
                value = v["func"](value)
            new_table_entry[k] = value



    #new_table_entry["Bytes Loss"] = data["Bytes Loss"]
    #new_table_entry["DupACKs"] = data["DupACKs"]

    #latencies_summary = util.parse_r_column_summary(r["summary"](latencies[1]))
    #new_table_entry.update(latencies_summary)
    ##new_table_entry["pif"] = file_data["group"]
    #
    #def get_percentiles(data, percentiles):
    #    p = sorted(percentiles)
    #    pf = [x/100.0 for x in p]
    #    res = r["quantile"](latencies[1], FloatVector(pf))
    #    return dict(("%dth" % int(p[i]), r) for i, r in enumerate(res))
    #percentiles = get_percentiles(latencies[1], [25, 50, 75, 80, 90, 95, 99])
    #new_table_entry.update(percentiles)

def fetch_table_conf(**kw):
    table_func = kw["table_func"]
    conf = kw["conf"]
    table_key = kw["table_key"]

    #print "fetch_table_conf:", table_key

    conf["data_tables"][table_key] = table_func(conf)
    conf["data_tables"][table_key]["table_key"] = table_key
    #print "conf:", conf.keys()
    #print "is_base_conf:", conf.get("is_base_conf", False)

    if conf.get("is_base_conf", False) is False:
        # This is a plot_box_dict
        table_defaults = conf["box_conf"]["data_table_defaults"]
    else:
        table_defaults = conf["data_table_defaults"]
    #print "Updating table_key: %s" % table_key
    #print "table_defaults:", table_defaults

    def update_dict_value(conf, data_d):
        if data_d.get("recurse", None) is None:
            #print "Update %s: with %s" % (conf, data_d)
            conf.update(data_d)
            #print "CONF:", conf
        else:
            conf = conf[data_d["recurse"]]
            data_d = data_d["data"]
            update_dict_value(conf, data_d)

    # This table has default values defined in the config. Update table conf with these
    if table_key in table_defaults:
        t_conf = conf["data_tables"][table_key]
        t_defaults = table_defaults[table_key]
        #print "t_defaults:", t_defaults
        #for k, v in table_defaults[table_key].iteritems():
        update_dict_value(t_conf, t_defaults)


def get_table_results_stats(plot_box_dict):
    table = {"keys": OrderedDict([("Name",          {"key": "legend_name",  "type": "str", "location": "color_mapping"}),
                                  #("Payload_avg",    {"key": "payload",  "type": "str", "func": lambda x: get_stdev(x, stdev=False)}),
                                  #("Payload_stdev",  {"key": "payload",  "type": "str", "func": get_stdev}),
                                  #("ITT_avg",        {"key": "itt",  "type": "str", "func": lambda x: get_stdev(x, stdev=False)}),
                                  #("ITT_stdev",      {"key": "itt",  "type": "str", "func": get_stdev}),
                                  #("RTT",            {"key": "rtt", "type": "str", "location": "plot_dict"}),
                                  ("Sent p",        {"key": "Packets sent", "type": "str", "location": "analysetcp", "func": common.bytes_short }),
                                  ("Lost p ",       {"key": "Packets lost", "type": "str", "location": "analysetcp", "func": lambda x: x }),
                                  ("Retr p ",       {"key": "Retransmissions", "type": "str", "location": "analysetcp", "func": lambda x: x }),
                                  ("RDB p",         {"key": "RDB packets", "type": "str", "location": "analysetcp", "func": common.bytes_short }),
                                  ("Payl B",        {"key": "Total bytes sent (payload)", "type": "str", "location": "analysetcp", "func": common.bytes_short}),
                                  ("Unique B",      {"key": "Unique Bytes", "type": "str", "location": "analysetcp", "func": common.bytes_short}),
                                  ("Rdn B",         {"key": "Redundant bytes", "type": "str", "location": "analysetcp", "func": common.bytes_short}),
                                  ("Retr B",        {"key": "Retrans", "type": "str", "location": "analysetcp", "func": common.bytes_short}),
                                  #("Loss % (B)",    {"key": "Bytes Loss", "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  #("Loss % (P)",    {"key": "Packet loss", "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  ("Loss % (B/p)",  {"key": "Loss B/P", "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  ("DupACKs",       {"key": "DupACKs", "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  #("PIF Lim",        {"key": "packets_in_flight",  "type": "int"}),
                                  #("Cong",           {"key": "cong_control", "type": "str", "func": lambda x: x.title()}),
                                  #("Duration",    {"key": "duration", "type": "int"}),
                                  #("Limit",       {"key": "duration", "type": "int"}),
                                  ("legend_order",  {"key": "legend_order",  "type": "int", "location": "plot_dict", "show": False }),
                              ]),
             "sort": { "do_sort": True, "sorted_args": { "key": lambda x: x["legend_order"],# float(x["B Loss"])
                                                         "reverse": False } },
             "default_font_size": 8,
             "data_func": set_values_from_location,
             "data": [],
             "filename": "latex_table_results_stats.tex",
    }
    # Reduce spacing inside ggplot2 table
    table["args"] = { "padding.v": r('unit(2, "mm")'), "padding.h": r('unit(1, "mm")'), "show.box": True }
    return table

def get_table_results_itt(plot_box_dict):
    table = {"keys": OrderedDict([("Name",           {"key": "legend_name",  "type": "str", "location": "color_mapping"}),
                                  ("Host",           {"key": "hostname",     "type": "str", "location": "plot_dict", "func": lambda x: x}),
                                  ("Streams",        {"key": "stream_count", "type": "int", "location": "plot_dict", "func": lambda x: x}),
                                  #("ITT (avg) min",  {"key": "ITT_avg_min",  "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  #("ITT (avg) avg",  {"key": "ITT_avg_avg",  "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  #("ITT (avg) max",  {"key": "ITT_avg_max",  "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  ("ITT (avg)",      {"key": "ITT_avg",  "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  ("Payload (avg)",  {"key": "PL_avg",  "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  ("CWND",           {"key": "CWND",  "type": "str", "location": "tcpprobe", "func": lambda x: x}),
                                  ("legend_order",   {"key": "legend_order", "type": "int", "location": "plot_dict", "show": False }),
                                  #'Min', '1.00'), ('1st Qu', '8.00'), ('Median', '10.00'), ('Mean', '10.13'), ('3rd Qu', '12.00'), ('Max
                              ]),
             "sort": { "do_sort": True, "sorted_args": { "key": lambda x: x["legend_order"], "reverse": False } },
                 #"sort": { "key": lambda x: float(x["Bytes Loss"]), "do_sort": True },
             "default_font_size": 8,
             "data_func": set_values_from_location,
             "data": [],
             "filename": "test_results_itt_table.tex",
    }
    # Reduce spacing inside ggplot2 table
    table["args"] = { "padding.v": r('unit(2, "mm")'), "padding.h": r('unit(1, "mm")'), "show.box": True }
    return table

def get_table_results_latency(plot_box_dict):

    #OrderedDict([("Test", {"key": "Test"}), ("ITT", {"key": "itt"}), ("Name", {"key": "name"}),
    #                              ("Data packets", {"key": "Total data packets sent"}),
    #                              ("Retrans", {"key": "Retransmissions"}),
    #                              ("Payload bytes", {"key": "Total bytes sent (payload)", "func": common.bytes_short }),
    #                              ("Bytes Loss", {"key": "Bytes Loss", "title":  "Bytes Loss \\%"}),
    #                              ("Redundant bytes", {"key": "Redundant bytes", "func": common.bytes_short }),
    #                              ("Retrans bytes", {"key": "Retrans", "func": common.bytes_short }),
    #                              ("DupACKs", {"key": "DupACKs"}),
    #                              ("Packets sent", {"key": "Packets sent"}),
    #                          ]),

    #("Name",        {"key": "plot_index",  "type": "int", "location": "plot_box_dict", "show": False, "func": lambda x: "Plot %d" % (x + 1)}),

    table = {"keys": OrderedDict([("Test",           {"key": "plot_index",                 "type": "int", "location": "plot_box_dict", "show": True, "func": lambda x: "%d" % (x + 1)}),
                                  #("Test",           {"key": "plot_index",                 "type": "int", "location": "plot_box_dict"}),
                                  ("Name",           {"key": "label",                      "type": "str", "location": "plot_dict"}),
                                  #("Type",           {"key": "type",                       "type": "str", "func": lambda x: x.upper()}),
                                  ("Data packets",   {"key": "Packets sent",               "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  ("Retrans",        {"key": "Retransmissions",            "type": "str", "location": "analysetcp", "func": lambda x: x}),
                                  ("Payload B",      {"key": "Total bytes sent (payload)", "type": "int", "location": "analysetcp", "func": common.bytes_short }),
                                  ("Bytes Loss",     {"key": "Bytes Loss",                 "type": "int", "location": "analysetcp", "title": "Bytes Loss \\%" }),
                                  ("DupACKs",        {"key": "DupACKs",                    "type": "int", "location": "analysetcp" }),
                                  ("legend_order",   {"key": "legend_order",               "type": "int", "location": "plot_dict",  "show": False }),
                              ]),
             "sort": { "do_sort": True, "sorted_args": { "key": lambda x: x["legend_order"], "reverse": False } },
             #"sort": { "key": lambda x: float(x["Bytes Loss"]), "do_sort": True },
             "default_font_size": 8,
             "data_func": set_values_from_location,
             "data": [],
             "filename": "test_results_latency_table.tex",
    }
    # Reduce spacing inside ggplot2 table
    table["args"] = { "padding.v": r('unit(2, "mm")'), "padding.h": r('unit(1, "mm")'), "show.box": True }
    table["group_keys"] = ["plot_index"]
    return table

def parse_analysetcp_output(file_data, output_file):
    # For the table data
    keys = [
        ("Total bytes sent (payload)", "Total bytes sent (payload)", "(?P<value>\d+).*"),
        ("Packets sent", "Total data packets sent", "(?P<value>\d+).*"),
        ("Packets sent", "Total packets sent (adj. for fragmentation)", "(?P<value>\d+).*"),
        #("Packets sent", "(Total data packets sent|Total data packets sent \(adj\.\))", "(?P<value>\d+).*"),
        ("Unique Bytes", "Number of unique bytes", "(?P<value>\d+).*"),
        ("Retrans", "Number of retransmitted bytes", "(?P<value>\d+).*"),
        ("Redundant bytes", "Redundant bytes (bytes already sent)", "(?P<value>\d+).*"),
        ("Retransmissions", "Number of retransmissions", "(?P<value>\d+).*"),
        ("RDB packets", "Number of packets with bundled segments", "(?P<value>\d+).*"),
        ("RDB Miss", "RDB byte   misses", "(?P<value>\d+).*"),
        ("RDB Hit", "RDB byte   hits", "(?P<value>\d+).*"),
        ("Bytes Loss", "Bytes Loss", "(?P<value>\d+\.\d+).*"),
        ("Packet loss", "Packet loss", "(?P<value>\d+\.\d+).*"),
        ("Packets lost", "Packets lost", "(?P<value>\d+).*"),
        ("DupACKs", "1. dupacks (count / accumulated)", "(?P<value2>\d+)\s/\s(?P<value>\d+).*"),
        ("ITT_avg_min", "Average        ITT (min, avg, max)", "(?P<value>\d+),\s+(?P<value2>\d+),\s+(?P<value3>\d+).*"), #44,      49,      55 ms
        ("ITT_avg_avg", "Average        ITT (min, avg, max)", "(?P<value1>\d+),\s+(?P<value>\d+),\s+(?P<value3>\d+).*"), #44,      49,      55 ms
        ("ITT_avg_max", "Average        ITT (min, avg, max)", "(?P<value1>\d+),\s+(?P<value2>\d+),\s+(?P<value>\d+).*"), #44,      49,      55 ms
        ("ITT_avg",     "Average        ITT (min, avg, max)", "(?P<min>\d+),\s+(?P<avg>\d+),\s+(?P<max>\d+).*"), #44,      49,      55 ms
        ("PL_avg_min", "Average    payload (min, avg, max)", "(?P<value>\d+),\s+(?P<value2>\d+),\s+(?P<value3>\d+).*"), #44,      49,      55 ms
        ("PL_avg_avg", "Average    payload (min, avg, max)", "(?P<value1>\d+),\s+(?P<value>\d+),\s+(?P<value3>\d+).*"), #44,      49,      55 ms
        ("PL_avg_max", "Average    payload (min, avg, max)", "(?P<value1>\d+),\s+(?P<value2>\d+),\s+(?P<value>\d+).*"), #44,      49,      55 ms
        ("PL_avg",     "Average    payload (min, avg, max)", "(?P<min>\d+),\s+(?P<avg>\d+),\s+(?P<max>\d+).*"), #44,      49,      55 ms
        #Average    payload (min, avg, max)            :        442,     551,     653 bytes
        ]

    def content_func(content):
        #print "content:", content
        sections = re.split('Aggregated Statistics for', content)
        #print "sections:", sections
        return sections[1]

    print "Parsing analysetcp output:", output_file
    data, data_dict = util.read_output_data(keys, output_file, content_func=content_func)
    #print "data_dict:", data_dict
    for k in data_dict['ITT_avg']:
        # Convert microseconds to ms
        data_dict['ITT_avg'][k] = int(int(data_dict['ITT_avg'][k])/1000.0)

    data_dict['ITT_avg'] = "%(min)s / %(avg)s / %(max)s" % data_dict['ITT_avg']
    data_dict['PL_avg'] = "%(min)s/%(avg)s/%(max)s" % data_dict['PL_avg']
    data_dict['Loss B/P'] = "%s / %s" % (data_dict['Bytes Loss'], data_dict['Packet loss'])
    return data, data_dict

def set_latency_data(table_conf, new_table_entry, latencies, data_name=None):
    index = util.get_dataframe_col_index(latencies, "latency")
    if index == -1:
        cprint("Failed to find index of latency column in dataframe!", "red")

    #if index != 1:
    #    cprint("Expected index of latency column to be 1, but it is %s" % index, "red")
    #    #print "latencies.names:", [i for i in latencies.names]
    latencies_summary = util.parse_r_column_summary(r["summary"](latencies[index]))
    new_table_entry.update(latencies_summary)
    #new_table_entry["pif"] = file_data["group"]

    def get_percentiles(data, percentiles):
        p = sorted(percentiles)
        pf = [x/100.0 if x >= 1 else x for x in p]
        #print "p:", p
        #print "pf:", pf
        res = r["quantile"](latencies[1], FloatVector(pf))
        return dict(("%sth" % (p[i] * 100 if p[i] < 1 else p[i]), r) for i, r in enumerate(res))
    percentiles = get_percentiles(latencies[1], [25, 40, 50, 60, 70, 80, 90, 93, 95, 96, 97, 98, 99, 0.999, 0.9999])
    new_table_entry.update(percentiles)

def str_to_int(x):
    import math
    ret = x
    if x:
        try:
            ret =  int(math.ceil(float(x)))
        except ValueError as e:
            cprint("Exception: %s" % e, "red")
            raise
    return ret

def get_table_latency_stats(*args):
    def on_off_na(x):
        if type(x) != int:
            return x
        return "Off" if x == 0 else "On"

    table_latency = {}
    table_latency.update({ "keys": OrderedDict([("Name",        {"key": "legend_name",  "type": "str", "location": "color_mapping"}),
                                                #("Duration",    {"key": "type",  "type": "str", "func": lambda x: x.upper()}),
                                                #("Type",        {"key": "type",  "type": "str", "func": lambda x: x.upper()}),
                                                #("Num",         {"key": "stream_count",  "type": "int"}),
                                                #("Cong",        {"key": "cong_control", "type": "str", "func": lambda x: x.title()}),
                                                #("PIF Lim",     {"key": "packets_in_flight",  "type": "int"}),
                                                #("Data points", {"key": "data_points", "type": "int", "location": "table_data"}),
                                                #("RTT",         {"key": "rtt", "type": "str", "location": "plot_dict"}),
                                                #("ITT",         {"key": "itt", "type": "str"}),
                                                #("Payload",     {"key": "payload", "type": "int"}),
                                                #("Latency stats",      {"key": "payload", "type": "str"}),
                                                #("Min",          {"key": "",  "type": "str", "location": "table_data"}),
                                                #("1st Qu",       {"key": "",  "type": "str", "location": "table_data"}),
                                                #("PIF Lim",      {"key": "packets_in_flight",  "type": "int"}),
                                                ("Mean",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                #("40th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("50th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("60th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("70th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("80th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("90th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("93th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("95th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("96th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("97th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("98th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("99th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("99.9th",       {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("99.99th",       {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("Max",          {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                                ("legend_order", {"key": "legend_order",  "type": "int", "location": "plot_dict", "show": False }),
                                            ]),
                           "sort": { "key": "Name", "do_sort": True, "sorted_args": { "key": lambda x: x["legend_order"], "reverse": False } },
                           "default_font_size": 8,
                           "data_func": set_latency_data,
                           "data": [],
                           })
    table_latency["args"] = { "padding.v": r('unit(2, "mm")'), "padding.h": r('unit(1, "mm")'), "show.box": True }
    return table_latency

#box_group["plot_group"] = plot_group


def set_sojourn_data(table_conf, new_table_entry, data, data_name=None):
    index = util.get_dataframe_col_index(data, "sojourn_time")
    summary = util.parse_r_column_summary(r["summary"](data[index]))
    new_table_entry.update(summary)
    #print "summary:", summary

    def get_percentiles(column, percentiles):
        p = sorted(percentiles)
        pf = [x/100.0 if x >= 1 else x for x in p]
        #print "p:", p
        #print "pf:", pf
        res = r["quantile"](column, FloatVector(pf))
        return dict(("%sth" % (p[i] * 100 if p[i] < 1 else p[i]), r) for i, r in enumerate(res))
    percentiles = get_percentiles(data[index], [25, 30, 40, 50, 60, 75, 90, 95, 96, 97, 98, 99, 0.999, 0.9999])
    new_table_entry.update(percentiles)
    tot_sum = r.sum(data[index])[0]
    #print "tot_sum:", tot_sum
    new_table_entry["Sum"] = tot_sum
    #print "percentiles:", percentiles

def get_table_sojourn_stats(*args):
    table = {}
    table.update({ "keys": OrderedDict([("Name",        {"key": "legend_name",  "type": "str", "location": "color_mapping"}),
                                        #("Duration",    {"key": "type",  "type": "str", "func": lambda x: x.upper()}),
                                        ("Mean",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("25th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("30th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("40th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("50th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("75th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("90th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("95th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("99th",         {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("99.9th",       {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("Max",          {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("Sum",          {"key": "",  "type": "str", "location": "table_data", "func": str_to_int}),
                                        ("legend_order", {"key": "legend_order",  "type": "int", "location": "plot_dict", "show": False }),
                                    ]),
                           "sort": { "key": "Name", "do_sort": True, "sorted_args": { "key": lambda x: x["legend_order"], "reverse": False } },
                   "default_font_size": 8,
                   "data_func": set_sojourn_data,
                   "data": [],
               })
    table["args"] = { "padding.v": r('unit(2, "mm")'), "padding.h": r('unit(1, "mm")'), "show.box": True }
    return table


def get_table_netem_loss_test_setup(*args):
    table_test_data = {} # {ccrccccc}
#    header_fmt = r"""\begin{tabular}{cccccccccc}
#& \multicolumn{6}{c}{Stream characteristics} &
#\multicolumn{2}{c}{Network properties} \\[2mm]
#& & & \multicolumn{2}{c}{ITT (ms)} & \\[1mm]
#\multirow{-2}{*}{Figure} & \multirow{-2}{*}{Streams} & \multirow{-2}{*}{Payload} & Avg & \multicolumn{1}{l}{St.dev} &
#\multirow{-2}{*}{Type} & \multirow{-2}{*}{\parbox[t]{1.8cm}{Max PIF}} & \multirow{-2}{*}{RTT (ms)} & \multirow{-2}{*}{Loss (\%%)}  \\
#%%
#\midrule
#"""
    header_fmt = r"""\begin{longtable*}{l@{\hskip 15pt}ccccc@{\hskip 18pt}cc}
 & \multicolumn{5}{c}{Traffic characteristics} &
\multicolumn{2}{c}{Network properties}\\[2mm]
& Streams & Payload & ITT (ms) & Type & Max PIF & RTT (ms) & Loss (\%)\\
%%
\midrule
"""

    table_test_data.update({ "keys": OrderedDict([#("Name",        {"key": "plot_index",  "type": "int", "location": "plot_box_dict", "show": True, "func": lambda x: "Plot %d" % (x + 1)}),
                                                  ("Figure",        {"key": "fig_label",  "type": "str", "location": "plot_dict", "show": True,
                                                                    "func": lambda x: r"Figure \ref{%s}" % x}),

                                                  #("Duration",    {"key": "type",  "type": "str", "func": lambda x: x.upper()}),
                                                  ("Streams",     {"key": "stream_count",  "type": "int"}),
                                                  ("Payload",     {"key": "payload", "type": "int"}),
                                                  #("ITT_avg",     {"key": "itt", "type": "str", "func": lambda x: get_stdev(x, stdev=False)}),
                                                  #("ITT_stdev",   {"key": "itt", "store_key": "itt_stdev" ,"type": "str", "func": lambda x: get_stdev(x, stdev=True)}),
                                                  ("ITT",         {"key": "itt", "type": "str"}),
                                                  ("Type",        {"key": "type",  "type": "str", "func": lambda x: x.upper()}),
                                                  ("PIF Lim",     {"key": "packets_in_flight",  "type": "int"}),
                                                  ("RTT",         {"key": "rtt", "type": "str", "location": "plot_dict"}),
                                                  ("Loss",        {"key": "loss", "type": "str", "location": "plot_dict"}),
                                                  ("legend_order", {"key": "legend_order",  "type": "int", "location": "plot_dict", "show": False }),
                                              ]),
                             "sort": { "key": "Name", "do_sort": True, "sorted_args": { "key": lambda x: float(x["Loss"]), "reverse": False } },
                             "include_header": True,
                             "include_title": False,
                             "header_fmt": header_fmt,
                             "header_end": r"""\end{longtable*}""",
                             "default_font_size": 7,
                             "group_keys": ["loss", ("itt", "1mm")],
                             "data": [],
                             "args": { "padding.v": r('unit(2, "mm")'), "padding.h": r('unit(1, "mm")'), "show.box": True },
                           })
    table_test_data["filename"] = "latex_test_setup_table.tex"
    return table_test_data

def mechanism_func(file_data):
    if file_data["label"] == "Greedy":
        return "NA"
    return util.make_mods_label(file_data)

def get_table_test_setup(*arg):
    def on_off_na(x):
        if type(x) != int:
            return x
        return "Off" if x == 0 else "On"

    table_test_data = {}
    table_test_data.update({ "keys": OrderedDict([("Test",        {"key": "plot_index",  "type": "int", "location": "plot_box_dict", "show": False, "func": lambda x: "%d" % (x + 1)}),
                                                  ("Figure",        {"key": "fig_label",  "type": "str", "location": "plot_dict", "show": True,  "title": "",
                                                                     "func": lambda x: r"Figure \ref{%s}" % x}),
                                                  ("Name",        {"key": "name",  "type": "str", "location": "ignore", "show": False}),
                                                  #("Duration",    {"key": "type",  "type": "str", "func": lambda x: "LONGSTRING!!!!!!!"}),
                                                  ("Type",        {"key": "type",  "type": "str", "func": lambda x: x.upper()}),
                                                  ("Streams",     {"key": "stream_count",  "type": "int"}),
                                                  ("Cong",        {"key": "cong_control", "type": "str", "func": lambda x: x.title()}),
                                                  ("TFRC",        {"key": "tfrc", "type": "str", "show": False, "func": lambda x: "Yes" if x == 1 else "No"}),
                                                  #("PIF Lim",     {"key": "packets_in_flight",  "type": "int"}),
                                                  ("Data points", {"key": "data_points", "type": "int", "show": False, "location": "ignore"}),
                                                  ("RTT",         {"key": "rtt", "type": "str", "location": "plot_dict"}),
                                                  ("ITT",         {"key": "itt", "type": "str"}),
                                                  ("Payload",     {"key": "payload", "type": "int"}),
                                                  #("Duration",    {"key": "duration", "type": "int"}),
                                                  #("Limit",       {"key": "duration", "type": "int"}),
                                                  #plot_conf["plot_conf"]["bandwidth"]
                                                  #("mFR",         {"key": "thin_dupack", "type": "str", "func": on_off_na }),
                                                  ("Mechanisms",   {"key": "mechs", "type": "str", "location": "file_data", "show": False, "func": mechanism_func }),
                                                  #("LT",          {"key": "linear_timeout", "type": "str", "func": on_off_na }),
                                                  ("legend_order", {"key": "legend_order",  "type": "int", "location": "plot_dict", "show": False }),
                                              ]),
                             "sort": { "key": "Name", "do_sort": True, "sorted_args": { "key": lambda x: x["legend_order"], "reverse": False } },
                             "default_font_size": 7,
                             "data": [],
                             "args": { "padding.v": r('unit(2, "mm")'), "padding.h": r('unit(1, "mm")'), "show.box": True },
                             #"header_fmt": r"""\begin{tabularx}{\textwidth}{%(columns)s}""",
                             "header_fmt": r"""\begin{longtable*}{l@{\hskip 15pt}ccccccc}""",
                             "header_end": r"""\end{longtable*}""",
                             "include_header": True,
                             "include_title": True,
                           })
    return table_test_data

def get_table_latex_test_setup(*args):
    latex_test_setup_table = get_table_test_setup()
    #latex_test_setup_table = { "keys": deepcopy(table_test_data["keys"]),
    #                           "data": []}
    del latex_test_setup_table["keys"]["Data points"]
    latex_test_setup_table["filename"] = "latex_test_setup_table.tex"
    latex_test_setup_table["path_prefix"] = "unset"
    return latex_test_setup_table

def get_table_latex_test_setup_with_tfrc_and_tcp_mods(*arg):
    latex_test_setup_table = get_table_test_setup()
    del latex_test_setup_table["keys"]["Data points"]
    latex_test_setup_table["keys"]["TFRC"].update({"show": True}) # , "location": "plot_dict", "key": "label"

    latex_test_setup_table["keys"]["Type"].update({"key": "label", "location": "plot_dict"})
    latex_test_setup_table["sort"] = { "key": "Name", "do_sort": True, "sorted_args": { "key": lambda x: (x["Streams"], get_stdev(x["ITT"], stdev=False), x["legend_order"]), "reverse": False } }
    latex_test_setup_table["filename"] = "latex_test_setup_table.tex"
    latex_test_setup_table["path_prefix"] = "unset"
    return latex_test_setup_table
