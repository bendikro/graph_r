import graph_default
from graph_r import *
from graph_r_ggplot import *
import util

def senttime_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if graph_default.file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]
    results_dict = {}

    if file_conf["thin_dupack"] is None:
        file_conf["thin_dupack"] = 0

    results_dict["stdout_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "packet-count-all.stout"))
    results_dict["results_file"] = results_dict["stdout_file"]

    results_dict["loss_all_results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "loss-aggr.dat"))
    results_dict["senttimes_all_results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "throughput-aggr.dat"))

    files = []
    #t37-rdb-itt100:15-ps120-cccubic_vs_g10_kbit5000_min10_rtt150_loss_pif20_qlen30_delayfixed_num0_rdbsender-rdb-senttime-all-10.0.0.12-22000-10.0.0.22-5000.dat
    #d = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "senttime-all-%(src_ip)s*" % file_conf))
    #print "Getting files: ", d
    #import glob
    #files = glob.glob(d)

    results_dict["slice_time"] = 150

    results_dict["results_cmd"] =\
        "analyseTCP -s %(src_ip)s -r %(dst_ip)s -p %(dst_port)s -f %(pcap_file)s -g %(pcap_file_receiver)s -u%(prefix)s "\
        " -o %(output_dir)s -T%(slice_time)s -L%(slice_time)s -a 1> %(stdout_file)s" % dict(file_conf, **results_dict)

    file_conf["color"] = "darkred" # Default color for dataset
    file_conf["results"] = {"senttime": results_dict}

    #if file_conf["packets_in_flight"] > 4:
    #    file_conf["group"] = 0
    #else:
    #    file_conf["group"] = 1

    #print "type_id:", file_conf["type_id"]
    file_conf["group"] = file_conf["type_id"]
    file_conf["group"] = ""
    file_conf["group"] = file_conf["packets_in_flight"]

    if "percentiles" in plot_conf["box_conf"]:
        file_conf["percentiles"] = plot_conf["box_conf"]["percentiles"]



def senttime_box_key_func(box_conf, conf):

    if conf["hostname"] == "rdbsender":
        if conf["type_thin1"] == "tcp":
            conf["color"] = "darkblue"
        elif conf["type_thin1"] == "rdb":
            pif = "rdb_pif%d" % conf["packets_in_flight"]
            if pif in graph_default.color_map:
                conf["color"] = graph_default.color_map[pif]
            else:
                conf["color"] = graph_default.color_map["default"]

    elif conf["hostname"] == "zsender":
        if conf["type_thin1"] == "rdb":
            conf["color"] = "firebrick1"

    box_conf["x_axis_title"] = "Time slices (%dms)" % conf["results"]["senttime"]["slice_time"]
    box_conf["percentiles"] = [.9]

def thin_itt_mark_line_func(plot_conf, file_conf):
    line_start = 240
    for i in range(8):
        yield "abline(v=%d, lty=3, lwd=1)" % (line_start + 60*i)

def get_color_senttime(plot):
    stream_id = plot["type_id"]
    pif_type = plot["type_id_pif"]

    if stream_id == "type_thick":
        if plot["num"] == 0:
            return graph_default.color_map["thick"]
        return graph_default.color_map["thick2"]

    if plot["num"] == 0:
        return graph_default.host_type_color_map["rdbsender"][pif_type]
    elif plot["num"] == 1:
        return "cyan"
    else:
        return "darkgreen"

#data = []
#for f in file_data["results"]["senttime"]["senttime_by_stream_files"]:
#    print "Reading senttime file:", f
#    dataframe = r["read.table"](f)
#    data.append((f, r["as.numeric"](dataframe[0])))
#file_data["ecdf_values"] = data

def loss_results_parse_generic(conf, box_conf, file_data, set_key, column_name, filename, percent=False, cols=[]):
    if util.cmd_args.verbose > 2:
        print "Reading loss data file:", filename
    dataframe = r["read.csv"](filename)

    for idx, name in enumerate(dataframe.names):
        if name == column_name:
            break

    for col in cols:
        for sidx, name in enumerate(dataframe.names):
            if name == col:
                break
        #print "IDX :", dataframe.names[idx]
        #print "SIDX:", dataframe.names[sidx]
        #print "idx: %d, sidx: %d" % (idx, sidx)
        robjects.globalenv["dataframe_stuff"] = dataframe
        # Substract the values in each row of col sidx from each row of col idx
        r("column_stuff <- dataframe_stuff$%s - dataframe_stuff$%s" % (dataframe.names[idx], dataframe.names[sidx]))
        dataframe[idx] = r["column_stuff"]

    dataframe.names[0] = "X"
    dataframe.names[idx] = "Y"

    # Add column with color name
    dataframe = r.cbind(dataframe, color_column=file_data["color"])
    if percent:
        dataframe[idx] = dataframe[idx].ro * 100
    file_data["data"] = dataframe
    file_data["ecdf_values"] = [r["as.numeric"](dataframe[idx])]
    file_data["ecdf_ggplot_values"] = dataframe

    #rows_i = robjects.IntVector(range(100))
    #subdataf = dataframe.rx(rows_i, True)
    #result = util.parse_r_summary(r["summary"](dataframe[idx]))
    #loss_stats = "{Min:03.0f} / {1st Qu:03.0f} / {Median:03.0f} / {3rd Qu:03.0f} / {Max:05.0f}".format(**result)
    print "result:\n", r["summary"](dataframe[idx])

def set_bursty_box_conf(plot_conf, box, arg):
    box["plot_func"] = plot_scatterplot_senttime
    print "set_bursty_box_conf:", arg

    if arg == "1bursty":
        box["parse_results_func"] = lambda conf, box_conf, file_data, set_key: \
            loss_results_parse_generic(
            conf, box_conf, file_data, set_key, "packet_count", file_data["results"]["senttime"]["senttimes_all_results_file"]
            )
        box["y_axis_title"] = "Packet count"
    elif arg == "2loss_bytes_absolute":
        box["parse_results_func"] = lambda conf, box_conf, file_data, set_key: \
            loss_results_parse_generic(
            conf, box_conf, file_data, set_key, "all_bytes_lost", file_data["results"]["senttime"]["loss_all_results_file"]
            )
        box["y_axis_title"] = "Lost bytes"
    elif arg == "3loss":
        box["parse_results_func"] = lambda conf, box_conf, file_data, set_key: \
            loss_results_parse_generic(
            conf, box_conf, file_data, set_key, "all_bytes_lost_relative_to_total", file_data["results"]["senttime"]["loss_all_results_file"], True
            )
        box["y_axis_title"] = "Lost bytes relative to total bytes sent"
    elif arg == "4loss_bytes":
        box["parse_results_func"] = lambda conf, box_conf, file_data, set_key: \
            loss_results_parse_generic(
            conf, box_conf, file_data, set_key, "all_bytes_lost_relative_to_interval", file_data["results"]["senttime"]["loss_all_results_file"], True
            )
        box["y_axis_title"] = "Lost bytes relative to sent in interval"
    elif arg == "5new_bytes_lost_relative_to_all_bytes_lost":
        box["parse_results_func"] = lambda conf, box_conf, file_data, set_key: \
            loss_results_parse_generic(
            conf, box_conf, file_data, set_key, "new_bytes_lost_relative_to_all_bytes_lost", file_data["results"]["senttime"]["loss_all_results_file"], True
            )
        box["y_axis_title"] = "New bytes lost relative to all lost in interval"
    elif arg == "6old_bytes_lost_relative_to_all_bytes_lost":
        box["parse_results_func"] = lambda conf, box_conf, file_data, set_key: \
            loss_results_parse_generic(
            conf, box_conf, file_data, set_key, "old_bytes_lost_relative_to_all_bytes_lost", file_data["results"]["senttime"]["loss_all_results_file"], True
            )
        box["y_axis_title"] = "Old bytes lost relative to all lost in interval"


def set_bursty_box_conf_CDF(plot_conf, box, arg):
    set_bursty_box_conf(plot_conf, box, arg)
    box["plot_func"] = ggplot_cdf_box
    box["percentiles"] = [.9]

def set_box_key_value(file_conf, arg):
    file_conf["hack"] = arg

def get_conf():
    conf = graph_default.get_conf()
    conf["output_file"] = util.cmd_args.burstiness_output_file
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s"\
                             "_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(hack)s", # _%(hostname)s
                         "sort_key_func": lambda x: (x["thin_dupack"], x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["hack"]),
                         "sort_keys": ["thin_dupack", "stream_count_thin1", "stream_count_thick", "group", "hack"],
                         "func" : senttime_box_key_func,
                         "pre_key_func": set_box_key_value,
                         "percentiles": [.95],
                         "box_title_def" : "%(stream_count_thin1)d Thin vs %(stream_count_thick)d Greedy  TCP %(cong_control_thin1)s ITT:%(itt_thin1)sms Duration: %(duration)smin" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["page_group_def"] = {"title": "Payload: %(payload_thin1)s, RTT: %(rtt)s Payload: %(payload_thin1)s  QLEN: %(queue_len)s Duration: %(duration)s min",
                              "sort_keys": ["payload_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1])}
    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 7, "x_axis_lim" : [0, 600],
                              #"plot_func": plot_scatterplot_senttime,
                              #"plot_func": plot_ecdf_box,
                              "plot_func": ggplot_cdf_box,
                              "do_plots_func": do_ggplots,
                              "legend": { "color_func": get_color_senttime, "values": {"type_thick": "Greedy", "type_thin1": "Thin"}},
                              "box_commands": thin_itt_mark_line_func,
                              "set_box_conf": set_bursty_box_conf,
                              }
                             )
    conf["file_parse"].update({ "func": senttime_file_parse_func,
                                "parse_func_arg": ["1bursty", "2loss_bytes_absolute", "3loss", "4loss_bytes", "5new_bytes_lost_relative_to_all_bytes_lost"] })
                                #"parse_func_arg": ["3loss", "4loss_bytes"] })
                                #"parse_func_arg": ["3loss"] })
                                #"parse_func_arg": ["1bursty"] })
    conf["document_info"]["title"] = "Senttime Plots"
    conf["document_info"]["show"] = False
    conf["paper"] = "special"
    conf["paper_width"] = 100
    conf["paper_height"] = 30

    conf["file_parse"]["files_nomatch_regex"].append(".*qlen46875.*")

    ### CDF settings
    #conf["paper_width"] = 20
    #conf["paper_height"] = 15
    #conf["paper_width"] = 14
    #conf["paper_height"] = 6
    #conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 1,
    #                           "set_box_conf": set_bursty_box_conf_CDF,
    #                           #"y_axis_lim" : [0, 1],
    #                           })
    #conf["file_parse"].update({"parse_func_arg": ["5new_bytes_lost_relative_to_all_bytes_lost", "6old_bytes_lost_relative_to_all_bytes_lost"
    #                                              #"3loss", "4loss_bytes"
    #                                              ]})
    return conf
