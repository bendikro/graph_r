from graph_r import *
from copy import deepcopy
import argparse
from collections import OrderedDict
from datetime import datetime

from graph_r import do_plots
import graph_default

args = None

try:
    from termcolor import colored, cprint
    termcolor = True
except:
    print("termcolor could not be found. To enable colors in terminal output, install termcolor.")
    termcolor = False
    def cprint(*arg, **kwargs):
        print arg
    def colored(text, color):
        return text


dst_ip = "10.0.0.22"
host_ip_map = { 'rdbsender': "10.0.0.12", 'zsender': "10.0.0.13", "ysender": "10.0.0.14", "bridge2-eth2": "10.0.0.14" }
host_stream_id_map = { 'rdbsender': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"} ,
                       'ysender': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"},
                       'bridge2-eth2': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"},
                       'zsender': {"rdb": "thick rdb(%(packets_in_flight)s)", "tcp": "thick tcp"} }

host_stream_type_map = { 'rdbsender': "thin", 'ysender': "thin", 'zsender': "thick", "bridge2-eth2": "thin" }

host_stream_type_id_map = { 'rdbsender': "type_thin1", 'ysender': "type_thin1", 'zsender': "type_thick", "bridge2-eth2": "type_thin1" }
host_stream_itt_id_map = { 'rdbsender': "itt_thin1", 'ysender': "itt_thin1", 'zsender': "itt_thick", "bridge2-eth2": "itt_thin1" }
host_stream_payload_id_map = { 'rdbsender': "payload_thin1", 'ysender': "payload_thin1", 'zsender': "payload_thick", "bridge2-eth2": "payload_thin1" }

# Two thin streams
host_stream_type_id_map2 = { 'rdbsender': "type_thin1", 'ysender': "type_thin2", 'zsender': "type_thick", "bridge2-eth2": "type_thin2" }
host_stream_itt_id_map2 = { 'rdbsender': "itt_thin1", 'ysender': "itt_thin2", 'zsender': "itt_thick", "bridge2-eth2": "itt_thin2" }
host_stream_payload_id_map2 = { 'rdbsender': "payload_thin1", 'ysender': "payload_thin2", 'zsender': "payload_thick", "bridge2-eth2": "payload_thin2" }


#dstPorts = { 'rdbsender': 5000, 'zsender': 5001, 'ysender': 5002, "bridge2-eth2": 5002 }
dstPorts = { 'rdbsender': 5000, 'zsender': 5001, 'ysender': 5000 }
dstPorts2 = { 'rdbsender': 5000, 'zsender': 5001, 'ysender': 5002 }
set_order = {0: 1, 3: 2, 6: 3, 10: 4, 20: 5}


color_map = {
    "default": "black",
    "thick": "darkred",
    "thick2": "blue",
    "thin": "darkgoldenrod1",
    "tcp_pif0": "darkgoldenrod1",
    "rdb_pif3": "darkblue",
    "rdb_pif6": "cyan",
    "rdb_pif10": "darkgreen",
    "rdb_pif20": "darkorchid",
    "rdb_pif300": "darkorchid",
    }

def get_color(plot):
    stream_id = plot["type_id"]
    pif_type = plot["type_id_pif"]
    if stream_id == "type_thick":
        return color_map["thick"]
    return host_type_color_map["rdbsender"][pif_type]


host_type_color_map = {
    "rdbsender": dict(color_map, tcp=color_map["thin"]),
    "zsender": { "default": "darkred", "tcp": "darkred", "rdb": "darkred" }
    }

filename_regexes = []
# Old files
jonas_filename_regex = r"fair-stable-(?P<num>\d+)-(?P<cap>\d+)kbit-rtt(?P<rtt>\d+)-(?P<tcp_type>[^\-]*)-(?P<queue>[^\-]*)-(?P<duration>\d+)s-"\
    "(?P<stream_count_thin1>\d+)vs(?P<stream_count_thick>\d+)-streams-itt(?P<itt>\d+)-ps(?P<packet_size>\d+)-.*"
filename_regexes.append(jonas_filename_regex)

# New files
jonas_filename_regex = r"fair-stable-(?P<num>\d+)-(?P<duration>\d+)secs-(?P<cap>\d+)kbit-rtt(?P<rtt>\d+)-tcp_(?P<tcp_type>[^\-]+)-opts(?P<sender_options>\d+)-"\
    "(?P<queue>[^\-]+)-(?P<stream_count_thin1>\d+)vs(?P<stream_count_thick>\d+)-itt(?P<itt>\d+)-ps(?P<packet_size>\d+)-.*"
filename_regexes.append(jonas_filename_regex)

jonas_filename_regex = r"fair-stable-(?P<num>\d+)-(?P<duration>\d+)secs-(?P<cap>\d+)kbit-rtt(?P<rtt>\d+)-tcp_(?P<tcp_type>[^\-]+)-opts(?P<sender_options>\d+)-(?P<queue>[^\-]+)-(?P<stream_count_thick>\d+)_greedy-(?P<stream_count_thin1>\d+)_itt(?P<itt_thin1>[^-]+)(:?-(?P<stream_count_thin2>\d+)_itt(?P<itt_thin2>[^-]+))?-ps(?P<packet_size>\d+)-.*"
filename_regexes.append(jonas_filename_regex)

def set_defaults(conf):
    default = { "file_parse": {}, "plot_conf": {} }
    for d in default:
        if not d in conf:
            conf[d] = {}
    conf["file_parse"]["filename_regex"] = filename_regexes
    conf["file_parse"]["pcap_suffix"] = "front.dump"
    conf["file_parse"]["files_match_exp"] = "*%s" % conf["file_parse"]["pcap_suffix"]
    conf["plot_conf"]["bandwidth"] = 2000 # The bandwidth cap of the link in Kbit/s
    conf["sample_sec"] = 30

def get_default_conf():
    from graph_default import _get_default_conf as get_conf
    conf = get_conf()
    set_defaults(conf)
    conf["file_parse"]["filename_regex"] = filename_regexes
    return conf

