from graph_r import *
from collections import OrderedDict
import util
from latex import write_latex_table, write_latex_figures

class HashableDict(dict):
    def __hash__(self):
        return hash(tuple(sorted(self.iteritems())))

def enum(**enums):
    return type('Enum', (), enums)

SUBPLOT_ACTION = enum(NO=0, APPEND_TO_PLOT_BOX=1, APPEND_TO_LIST=2)
TABLE_ACTION = SUBPLOT_ACTION

plyr = importr("plyr")

def do_stream_legend_and_color(plot_box_dict, plot, data):
    legend_conf = plot_box_dict["box_conf"]["legend"]
    #stream_properties = plot["streams"][plot["streams_id"]]
    stream_properties = plot["streams"][plot["streams_id"]]
    meta_info = stream_properties["meta_info"]

    if util.cmd_args.skip_data:
        return

    def make_color_labels(name, colors, count):
        if colors:
            while len(colors) < count:
                colors += colors
            colors = colors[:count]
        if count > 1:
            color_labels = ["%s %d" % (name, i+1) for i in range(0, count)]
        else:
            color_labels = ["%s" % name]
        return colors, color_labels

    #print "do_stream_legend_and_color:", stream_properties.get("color_mapping")

    if "color_mapping" in stream_properties:
        for m in stream_properties["color_mapping"]:
            #new_table_entry["Name"] = m["legend_name"]
            # Find the breaks based on the factor
            breaks_factor = meta_info.get("breaks_factor", meta_info["factor"])
            #print "Data:"
            #print data
            factor_count = plyr.count(data, vars=breaks_factor)
            factors = factor_count.rx2(breaks_factor)
            breaks = []
            # factor is a FactorVector where a IntVector corresponds to the label values in factors.levels
            for v in factors:
                breaks.append(factors.levels[v-1])
            #print 'm["legend_name"]:', m["legend_name"]
            color_list = m.get("colors", None)
            if color_list is None:
                if "color_code" in m:
                    color_list = r(m["color_code"])

            #print "legend_count:", stream_properties["meta_info"].get("legend_count")
            #colors, labels = make_colors(util.make_title(stream_properties, plot, m["legend_name"]), color_list, len(breaks)) # stream_properties.get("legend_count", stream_properties["stream_count"]
            colors, labels = make_color_labels(util.make_title(stream_properties, plot, m["legend_name"]), color_list, stream_properties["meta_info"].get("legend_count", len(breaks)))
            colors_list = []
            #legend_conf["breaks"] += breaks
            #print "colors (%d): %s" % (len(colors), colors)
            #print "breaks (%d): %s" % (len(breaks), breaks)
            #print "labels (%d): %s" % (len(labels), labels)

            for i in range(len(labels)):
                colors_list.append(HashableDict(color=colors[i], label=labels[i], breaks=breaks[i], order=plot["legend_order"]))
            legend_conf["colors"] += colors_list
        #print "do_stream_legend_and_color:", legend_conf["colors"]


def do_stream_custom_table_data(plot, plot_box_dict, data, table_conf, data_name=None, append=False):
    stream_properties = plot["streams"][plot["streams_id"]]
    meta_info = stream_properties["meta_info"]

    table_keys = table_conf["keys"]
    table_data = table_conf["data"]

    if append:
        new_table_entry = table_data[-1]
    else:
        new_table_entry = HashableDict()

    if "Data points" in table_keys and table_keys["Data points"]["show"]:
        if meta_info.get("type_id_column", None):
            type_id_col = meta_info.get("type_id_column")
            if not type_id_col in data.names:
                print "'type_id_column' in stream_properties, but column %s does not exist in table names: '%s'" % (type_id_col, data.names)
            else:
                counts = plyr.count(data, vars=type_id_col)
                #print "counts:", counts
                new_table_entry["Data points"] = counts[1][0] if counts else rpy2.rinterface.NA_Integer
    new_table_entry["Name"] = ""
    table_keys = table_conf["keys"]
    for data_key in table_keys:
        #print "data_key:", data_key
        if table_keys[data_key].get("location", None) == "ignore":
            continue
        value = rpy2.rinterface.NA_Integer if table_keys[data_key]["type"] == "int" else rpy2.rinterface.NA_Character
        if append is True:
            value = None
        key = table_keys[data_key]["key"]
        if table_keys[data_key].get("location", None) == "plot_dict":
            value = plot[key]
        elif table_keys[data_key].get("location", None) == "file_data":
            value = plot
        elif table_keys[data_key].get("location", None) == "plot_box_dict":
            value = plot_box_dict[key]
        elif table_keys[data_key].get("location", None) == "plot_group":
            value = plot_box_dict["plot_group"][key]
        else:
            if table_keys[data_key].get("location", None) == "color_mapping":
                for m in stream_properties["color_mapping"]:
                    value = m[key]
                    break
            elif key in stream_properties:
                value = stream_properties[key]

        if value is not None:
            if "func" in table_keys[data_key]:
                value = table_keys[data_key]["func"](value)
            #print "[%s] = %s" % (data_key, value)
            new_table_entry[data_key] = value

    if "data_func" in table_conf:
        table_conf["data_func"](table_conf, new_table_entry, data, data_name=data_name)

    if append is False:
        table_data.append(new_table_entry)

    #if table_conf["table_key"] == "table_latency_data":
    #    print "do_stream_custom_table_data:", new_table_entry

def get_values_sort_order(table_data, sorted_args={}, ordered_list=None):
    # First save the original order
    index_dict = dict((l, i) for i, l in enumerate(table_data))
    if ordered_list is None:
        ordered_list = sorted(index_dict.keys(), **sorted_args)
    indexes_ordered = [index_dict[e] for e in ordered_list]
    return indexes_ordered


# Builds an ordered dict with proper data types for R
def build_custom_data_dict(table_conf, legend_sort):
    custom_data = []
    # First find the proper order

    table_data = table_conf["data"]
    data_keys = table_conf["keys"]
    indexes_ordered = range(0, len(table_data))

    # Sort config
    if "sort" in table_conf:
        sort_conf = table_conf["sort"]

        if sort_conf.get("do_sort", True) is True:
            if legend_sort:
                legend_sort = deepcopy(legend_sort)
                legend_sort.update(sort_conf)
                sort_conf = legend_sort

            sorted_args = sort_conf.get("sorted_args", {})
            if "key" in sorted_args:
                # The key to sort by is specified
                import types
                if not type(sorted_args["key"]) is types.FunctionType and sorted_args["key"] not in data_keys:
                    print "Sort key '%s' not present in table: %s" % (sort_conf["key"], data_keys)

            indexes_ordered = get_values_sort_order(table_data, sorted_args=sorted_args)

    for data_key in data_keys:
        vector = IntVector if data_keys[data_key]["type"] == "int" else StrVector
        try:
            data_list = []
            for i in indexes_ordered:
                if data_keys[data_key].get("show", True) is False:
                    continue
                value = table_data[i][data_key]
                if "func" in data_keys[data_key]:
                    value = data_keys[data_key]["func"](value)
                data_list.append(value)
            if data_list:
                # if field is not shown, list will be empty
                custom_data.append((data_key, vector(data_list)))
        except ValueError, e:
            import traceback
            traceback.print_exc(e)
            print "data_key:", data_key
            #print "table_data:", table_data
            print "Data:", table_data[data_key]
            raise
    custom_data = OrderedDict(custom_data)
    return custom_data


def bytes_short(b):
    if type(b) != int:
        try:
            if len(b) == 0:
                return b
            b = int(b)
        except (ValueError, TypeError) as err:
            #import traceback
            #traceback.print_exc()
            return b
    b = int(b)
    if b > 1000000:
        b /= 1000000.0
        if b > 100:
            return "%.0f M" % b
        return "%.1f M" % b
    if b > 1000:
        b /= 1000.0
        if b > 100:
            return "%.0f K" % b
        return "%.1f K" % b
    return b
