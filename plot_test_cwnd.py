#!/usr/bin/env python
from datetime import datetime
from common import *
from copy import deepcopy

from rpy2.robjects import r
from rpy2.robjects.vectors import Vector, IntVector, DataFrame, StrVector, FloatVector
from rpy2.robjects.lib import grid

import graph_base
from graph_default_bendik import get_default_conf
from graph_r_ggplot import do_ggplots, ggplot2, ggplot_cdf_box, ggplot_boxplot_box
import graph_r_ggplot
import common, util
import latency
from latency import get_latency_conf as get_latency_conf_original
import throughput
import graph_default
graph_default.get_conf = get_default_conf

import common_parse
import tcpprobe
import data_tables


def payload_results_parse(conf, plot_box_dict, file_data, set_key):
    if util.cmd_args.verbose > 1:
        cprint("Reading payload stats: %s" % file_data["results"]["analysetcp"]["per_packet_stats_file"], "yellow")

    filename = file_data["results"]["analysetcp"]["per_packet_stats_file"]
    if not filename in plot_box_dict["data_cache"]:
        plot_box_dict["data_cache"][filename] = r["read.csv"](filename, header=True)
    dataframe = plot_box_dict["data_cache"][filename]

    # Add column with color name
    dataframe = r.cbind(dataframe, color_column=file_data["color"], stream_type=file_data["stream_id"])
    file_data["ecdf_ggplot2_values"] = dataframe

    #print "dataframe: COLS:", dataframe.names

    if conf.get("results_parse_hook", None) is not None:
        conf["results_parse_hook"](conf, plot_box_dict, file_data, set_key)

    #print "DATAF:", type(file_data["ecdf_ggplot2_values"])
    #print "DATA:"
    #print util.get_dataframe_head(file_data["ecdf_ggplot2_values"])
    #dataframe.names[0] = "time" # Column names are read from the first line (header)

    stream_properties = file_data["streams"][file_data["streams_id"]]
    stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
                                           "factor": "color_column",
                                           "type_id_column": "color_column", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                           "breaks_factor": "color_column",  # These are for the legend
                                           "aes_args": { "color": 'color_column',
                                                         #"group": 'color_column'
                                                         "group": 'factor(color_column)'
                                                     } # These are for the general factoring of the data
                                       })
    common.do_stream_legend_and_color(plot_box_dict, file_data, file_data["ecdf_ggplot2_values"])

    data, data_dict = data_tables.parse_analysetcp_output(file_data, file_data["results"]["analysetcp"]["stdout_file"])
    file_data["analysetcp_data"] = data_dict

    data_dict["name"] = file_data.get("label", "Missing label")
    data_dict["packets_in_flight"] = file_data["packets_in_flight"]
    #data_dict["itt"] = int(get_stdev(file_data["itt"], stdev=False))
    data_dict["itt"] = file_data["itt"]
    analysetcp_data = conf.get("analysetcp_data", [])
    analysetcp_data.append((plot_box_dict, data_dict))
    conf["analysetcp_data"] = analysetcp_data


def itt_results_parse(conf, plot_box_dict, file_data, set_key):
    if util.cmd_args.verbose > 1:
        cprint("Reading itt results: %s" % file_data["results"]["analysetcp"]["per_packet_stats_file"], "yellow")

    stream_properties = file_data["streams"][file_data["streams_id"]]

    filename = file_data["results"]["analysetcp"]["per_packet_stats_file"]
    if not filename in plot_box_dict["data_cache"]:
        plot_box_dict["data_cache"][filename] = r["read.csv"](filename, header=True)
    dataframe = plot_box_dict["data_cache"][filename]

    # Add column with color name
    dataframe = r.cbind(dataframe, color_column=file_data["color"], stream_type=file_data["stream_id"])
    itt_index = util.get_dataframe_col_index(dataframe, "itt")
    dataframe[itt_index] = dataframe[itt_index].ro / 1000

    file_data["ecdf_ggplot2_values"] = dataframe

    if conf.get("results_parse_hook", None) is not None:
        conf["results_parse_hook"](conf, plot_box_dict, file_data, set_key)

    if file_data["default_meta_info"]:
        stream_properties["meta_info"].update({"legend_count": 1, "smooth": False, "line": True,
                                               "factor": "color_column",
                                               "type_id_column": "color_column", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                               "breaks_factor": "color_column",  # These are for the legend
                                               "aes_args": { "color": 'color_column',
                                                             #"group": 'color_column'
                                                             "group": 'factor(color_column)'
                                                         } # These are for the general factoring of the data
                                       })
    common.do_stream_legend_and_color(plot_box_dict, file_data, file_data["ecdf_ggplot2_values"])

    data, data_dict = data_tables.parse_analysetcp_output(file_data, file_data["results"]["analysetcp"]["stdout_file"])
    file_data["analysetcp_data"] = data_dict

    data_dict["name"] = file_data.get("label", "Missing label")
    data_dict["packets_in_flight"] = file_data["packets_in_flight"]
    #data_dict["itt"] = int(get_stdev(file_data["itt"], stdev=False))
    data_dict["itt"] = file_data["itt"]
    #print "data_dict itt:", data_dict["itt"]
    analysetcp_data = conf.get("analysetcp_data", [])
    analysetcp_data.append((plot_box_dict, data_dict))
    conf["analysetcp_data"] = analysetcp_data

def per_segment_results_parse(conf, plot_box_dict, file_data, set_key):
    if util.cmd_args.verbose > 1:
        cprint("Reading per-segment sojourn results: %s" % file_data["results"]["analysetcp"]["per_segment_stats_file"], "yellow")

    #print "latency_results_parse"
    stream_properties = file_data["streams"][file_data["streams_id"]]

    #dataframe = r["read.csv"](file_data["results"]["analysetcp"]["per_segment_stats_file"], header=True)
    filename = file_data["results"]["analysetcp"]["per_segment_stats_file"]
    if not filename in plot_box_dict["data_cache"]:
        plot_box_dict["data_cache"][filename] = r["read.csv"](filename, header=True)
    dataframe = plot_box_dict["data_cache"][filename]

    #dataframe = DataFrame.from_csvfile(file_data["results"]["latency"]["results_file"], header=False)
    #print "latency_results_parse color:", file_data["color"]

    # r.cbind makes a copy of the dataframe, so the cached version isn't modified

    # Add column with color name
    dataframe = r.cbind(dataframe, color_column=file_data["color"], stream_type=file_data["stream_id"])
    latency_index = util.get_dataframe_col_index(dataframe, "ack_latency")
    dataframe[latency_index] = dataframe[latency_index].ro / 1000

    sojourn_index = util.get_dataframe_col_index(dataframe, "sojourn_time")
    dataframe[sojourn_index] = dataframe[sojourn_index].ro / 1000

    #print "COLS:", dataframe.names

    app_l_latency_index = util.get_dataframe_col_index(dataframe, "sojourn_and_ack_latency")
    if app_l_latency_index != -1:
        dataframe[app_l_latency_index] = dataframe[app_l_latency_index].ro / 1000

    file_data["ecdf_ggplot2_values"] = dataframe

    if conf.get("results_parse_hook", None) is not None:
        conf["results_parse_hook"](conf, plot_box_dict, file_data, set_key)

    #common.do_stream_legend_and_color(plot_box_dict, file_data, file_data["ecdf_ggplot2_values"])
    #
    #analysetcp_data = conf.get("analysetcp_data", [])
    ##analysetcp_data["Test"] = plot_box_dict["plot_number"]
    #analysetcp_data.append((plot_box_dict, data_dict))
    #conf["analysetcp_data"] = analysetcp_data


def cwnd_results_parse1(conf, plot_box_dict, file_data, set_key):
    #print "tcp_probe_results_parse:", file_data["hostname"]
    if file_data["hostname"] == "fsender":
        common_parse.tcpinfo_results_parse(conf, plot_box_dict, file_data, set_key)
    else:
        tcpprobe.tcp_probe_results_parse(conf, plot_box_dict, file_data, set_key)

def parse_cwnd_stats(conf, plot_box_dict, file_data, set_key):

    if file_data["hostname"] == "fsender":
        common_parse.tcpinfo_results_parse(conf, plot_box_dict, file_data, set_key)
        tcp_data = file_data["tcpinfo_data"]
        pass
    else:
        only_changes = True
        if file_data["hostname"] != "zsender":
            #only_changes = True
            only_changes = False
        tcp_data = tcpprobe.read_tcp_probe_output(file_data, only_changes=only_changes)
        #print "DATAF:\n", util.get_dataframe_head(tcpprobe_data)
        #print "tcpprobe_data:", dir(tcpprobe_data)
        #print "tcp_data COLS:", tcp_data.names
    cwnd_index = 0
    for i, col in enumerate(tcp_data.names):
        if col == "cwnd":
            cwnd_index = i + 1
    #print "cwnd_index:", cwnd_index

    def value_func(x):
        d = int(round(float(x), 0))
        return d
    d = util.parse_r_column_summary(r["summary"](tcp_data.rx2(cwnd_index)), value_func=value_func)
    d["CWND"] = "%(Mean)s/%(Min)s/%(1st Qu)s/%(Median)s/%(3rd Qu)s/%(Max)s" % d
    file_data["tcpprobe_data"] = d
    #print "tcpprobe_data:", d
    #print "tcp probe data:", data
    common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["tcpprobe_data"],
                                       plot_box_dict["data_tables"]["test_results_itt_table"],
                                       data_name="tcpprobe", append=True)


def cwnd_results_parse(conf, plot_box_dict, file_data, set_key):

    stream_properties = file_data["streams"][file_data["streams_id"]]
    file_data["plot_type_title"] = file_data["plot_type"]

    if file_data["plot_type"] == "ITT":
        itt_results_parse(conf, plot_box_dict, file_data, set_key)
        plot_box_dict["axis_conf"].update({ "x_axis_lim" : [-5, 100], "x_axis_title" : { "title": "ITT (ms)"}  })

        if "analysetcp_results_table" not in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_results_stats, conf=plot_box_dict, table_key="analysetcp_results_table")

        data, data_dict = data_tables.parse_analysetcp_output(file_data, file_data["results"]["analysetcp"]["stdout_file"])
        file_data["analysetcp_data"] = data_dict
        #print "DATA:", data_dict
        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], plot_box_dict["data_tables"]["analysetcp_results_table"], data_name="analysetcp")
        stream_properties["meta_info"]["aes_args"].update({"x": "itt",
                                                           "color": 'factor(stream_type)',
                                                           #"color": "color_column"
                                                       })
    if file_data["plot_type"] == "Payload":
        payload_results_parse(conf, plot_box_dict, file_data, set_key)

        plot_box_dict["box_conf"]["plot_and_table"].update({"make_table": #common.TABLE_ACTION.NO,
                                                common.TABLE_ACTION.APPEND_TO_PLOT_BOX, #,
                                                "table_key": "test_results_itt_table",
                                                #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                                "heights": (.85, 0, .15),
                                            })

        plot_box_dict["axis_conf"].update({ "x_axis_lim" : [-5, 150], "x_axis_title" : { "title": "Payload (bytes)"}  })

        if "test_results_itt_table" not in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_results_itt, conf=plot_box_dict, table_key="test_results_itt_table")


        data, data_dict = data_tables.parse_analysetcp_output(file_data, file_data["results"]["analysetcp"]["stdout_file"])
        file_data["analysetcp_data"] = data_dict

        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["analysetcp_data"], plot_box_dict["data_tables"]["test_results_itt_table"], data_name="analysetcp")
        stream_properties["meta_info"]["aes_args"].update({"x": "payload_bytes",
                                                           "color": 'factor(stream_type)',
                                                           #"color": "color_column"
                                                       })
        parse_cwnd_stats(conf, plot_box_dict, file_data, set_key)

    if file_data["plot_type"] == "Latency":
        plot_box_dict["axis_conf"].update({ "x_axis_lim" : [100, 750], "x_axis_title" : { "title": "Latency (ms)" },
                                        })
        plot_box_dict["box_conf"]["plot_and_table"].update({"make_table": common.TABLE_ACTION.NO,
                                                            #common.TABLE_ACTION.APPEND_TO_PLOT_BOX, #,
                                                            "table_key": "test_results_itt_table",
                                                            #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                                            "heights": (.85, 0, .15),
                                                        })
        latency.latency_results_parse(conf, plot_box_dict, file_data, set_key)
        stream_properties["meta_info"]["aes_args"].update({#"x": "latency",
            "x": "latency",
            "color": 'factor(stream_type)',
            #"color": "color_column"
        })

    if file_data["plot_type"] == "Latency (per segment)":
        plot_box_dict["axis_conf"].update({ "x_axis_lim" : [100, 750], #[80, 750],
                                            "x_axis_title" : { "title": "Per-segment latency (ms)" } })
        per_segment_results_parse(conf, plot_box_dict, file_data, set_key)
        file_data["plot_type_title"] = "Per-segment latency"

        if file_data["data_type"] == "Latency (per segment)":
            stream_properties["meta_info"]["aes_args"].update({
                "x": "ack_latency",
                "color": 'factor(stream_type)',
            })

        elif file_data["data_type"] == "Sojourn (per segment)":
            stream_properties["meta_info"]["aes_args"].update({
                "x": "sojourn_and_ack_latency",
                "color": 'factor(stream_type)',
            })
        else:
            print "Invalid data_type:", file_data["data_type"]
            sys.exit(1)

    if file_data["plot_type"] == "Sojourn time":
        plot_box_dict["axis_conf"].update({ "x_axis_lim" : [#-20, 350 #750
            -10, 750
                                                        ]
                                            , "x_axis_title" : { "title": "Sojourn time (ms)" } })
        stream_properties["meta_info"]["aes_args"].update({ "x": "sojourn_time", "color": 'factor(stream_type)'})

        per_segment_results_parse(conf, plot_box_dict, file_data, set_key)

        plot_box_dict["box_conf"]["plot_and_table"].update({"make_table": #common.TABLE_ACTION.NO,
                                                            common.TABLE_ACTION.APPEND_TO_PLOT_BOX, #,
                                                            "table_key": "test_results_sojourn_table",
                                                            #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                                            "heights": (.85, 0, .15),
                                                        })

        if "test_results_sojourn_table" not in plot_box_dict["data_tables"]:
            data_tables.fetch_table_conf(table_func=data_tables.get_table_sojourn_stats, conf=plot_box_dict, table_key="test_results_sojourn_table")

        data, data_dict = data_tables.parse_analysetcp_output(file_data, file_data["results"]["analysetcp"]["stdout_file"])
        file_data["analysetcp_data"] = data_dict

        common.do_stream_custom_table_data(file_data, plot_box_dict, file_data["ecdf_ggplot2_values"], plot_box_dict["data_tables"]["test_results_sojourn_table"], data_name="sojourn")

    if plot_box_dict.get("results_parse_hook", None):
        plot_box_dict["results_parse_hook"](conf, plot_box_dict, file_data, set_key)

        #def plot_app_layer_latency(plot_conf=None, plot_box_dict=None, gp=None, **kw):
        #    plot = kw["plot"]
        #    data = plot["ecdf_ggplot2_values"]
        #    aes_args = meta_info["aes_args"]
        #    aes_args.update({"x": "app_layer_latency" })
        #    mapping = ggplot2.aes_string(**aes_args)
        #    stream_properties = plot["streams"][plot["streams_id"]]
        #    meta_info = stream_properties["meta_info"]
        #    #meta_info["aes_args"].update({"x": "app_layer_latency" })
        #    import graph_r_ggplot
        #    #yield graph_r_ggplot.ggplot_ecdf(kw["conf"], plot_box_dict, plot, gp)
        #    yield ggplot2.ggplot2.stat_ecdf(data=data, mapping=mapping, **plot_args)
        #    #yield ggplot2.guides(color=False)
        #    return
        #
        #plot_box_dict["box_conf"]["custom_ggplot2_plot_cmds"] = {"plot_app_layer_latency": plot_app_layer_latency}


def tcp_cwnd_file_parse_func(plot_conf, file_data, pcap_file, parse_func_arg):
    #print "tcp_cwnd_file_parse_func: '%s'" % parse_func_arg
    if graph_default.file_parse_generic(plot_conf, file_data, pcap_file, parse_func_arg) is False:
        return False
    stream_properties = file_data["streams"][file_data["streams_id"]]
    file_data["group"] = file_data["type_id"]
    util.set_dict_defaults_recursive(file_data, ("results", {}))
    file_data["stream_id"] = "%s %s" % (file_data["stream_type"].title(), file_data["type"].upper())
    file_data["legend_order"] = 0

    util.set_dict_defaults_recursive(file_data, (("results", "analysetcp"), { "extra_cmd_args": "-P" }))
    analysetcp_per_packet_stats = os.path.join(file_data["output_dir"], "%s%s" % (file_data["prefix"], "per-packet-stats-all.dat"))
    analysetcp_per_segment_stats = os.path.join(file_data["output_dir"], "%s%s" % (file_data["prefix"], "per-segment-stats-all.dat"))
    analysetcp_latency = os.path.join(file_data["output_dir"], "%s%s" % (file_data["prefix"], "latency-all-aggr.dat"))
    analysetcp_stdout_file = os.path.join(file_data["output_dir"], "%s%s" % (file_data["prefix"], "analysetcp.out"))
    file_data["results"]["analysetcp"]["per_packet_stats_file"] = analysetcp_per_packet_stats
    file_data["results"]["analysetcp"]["per_segment_stats_file"] = analysetcp_per_segment_stats
    util.set_dict_defaults_recursive(file_data, (("results", "latency"), { "results_file": analysetcp_latency,
                                                                           "stdout_file": analysetcp_stdout_file }))
    file_data["results"]["analysetcp"]["extra_cmd_args"] += " -l -v "
    #print "results:", file_data["results"].keys()

    file_data["results"]["analysetcp"]["extra_cmd_args"] += "-S --sojourn-time-input=%s" % file_data["results"]["tcpsojourn"]["results_file"]

    #print "extra_cmd_args:", file_data["results"]["analysetcp"]["extra_cmd_args"]

    common_parse.add_analysetcp_parsing(plot_conf, file_data, pcap_file, parse_func_arg)

    if not "cong_control_thin1" in file_data:
        file_data["cong_control_thin1"] = "cubic"

    file_data["color"] = "darkred" # Default color for dataset
    stream_properties["meta_info"].update({"legend_count": 1,
                                           "smooth": False, "line": True,
                                           "factor": "stream",
                                           #"type_id_column": "stream", # This is for counting "Data points" for each group (row) in the custom data table (do_stream_custom_table_data).
                                           "breaks_factor": "stream",  # These are for the legend
                                           "aes_args": { "color": 'factor(stream)', #"group": 'stream_id'
                                                     },
                                       })

    stream_type_to_ggplot_properties = deepcopy(graph_default.stream_type_to_ggplot_properties)
    #print "file_data:", file_data.keys()
    #print "cong_control_thin1:", file_data["cong_control_thin1"]

    if file_data["streams_id"] == "r":
        if file_data["cong_control_thin1"] == "reno_3_15":
            file_data["stream_id"] = "Linux 3.15"
            file_data["legend_order"] = 10
        elif file_data["cong_control_thin1"] == "reno":
            file_data["stream_id"] = "Linux 3.18"
            file_data["legend_order"] = 15
    else:
        file_data["stream_id"] = "FreeBSD"
        file_data["legend_order"] = 1

    if file_data["data_type"] == "Sojourn (per segment)":
        file_data["stream_id"] += " (+SOJ)"
        file_data["legend_order"] += 1

    stream_type_to_ggplot_properties[file_data["streams_id"]]["color_mapping"][0].update({ "legend_name": file_data["stream_id"] })

    stream_properties["color_mapping"] = stream_type_to_ggplot_properties[file_data["streams_id"]]["color_mapping"]
    file_data["stream_id"] = stream_properties["color_mapping"][0]["legend_name"]


def custom_cwnd_file_parse_func(plot_conf, file_data, pcap_file, parse_func_arg):
    #print "custom_cwnd_file_parse_func: '%s'" % parse_func_arg
    file_data["data_type"] = parse_func_arg
    file_data["plot_type"] = parse_func_arg
    if parse_func_arg == "Sojourn (per segment)":
        file_data["plot_type"] = "Latency (per segment)"

    if file_data["loss"] == None:
        file_data["loss"] = 0

    util.set_dict_defaults_recursive(file_data, ("results", {}))
    util.dict_set_default(file_data["results"], ("tcpinfo", { "results_file": pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpinfo.txt")}))
    util.dict_set_default(file_data["results"], ("tcpprobe", {"results_file" : pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpproberdb.out")}))
    util.dict_set_default(file_data["results"], ("tcpsojourn", {"results_file" : pcap_file.replace((plot_conf["file_parse"]["pcap_suffix"]), ".tcpseqtrack.out")}))

    if tcp_cwnd_file_parse_func(plot_conf, file_data, pcap_file, parse_func_arg) is False:
        return False


def cwnd_tests_get_conf(defaults=None):
    print "cwnd_tests_get_conf"
    conf = graph_default.get_conf()

    conf["output_file"] = util.cmd_args.cwnd_tests_output_file
    print "output_file:", conf["output_file"]
    conf["box_conf"].update({"key": "Streams:%(stream_count_thin1)s_Streams:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_"\
                             "RTT:%(rtt)s_cong:%(cong_control_thin1)s_PIF:%(packets_in_flight)s_QLEN:%(queue_len)s_NUM:%(num)s_%(thin_dupack)s",
                             "sort_key_func": lambda x: (x["thin_dupack"], x["stream_count_thin1"], x["itt_thin1"], x["cong_control_thin1"], x["plot_type"]),
                             "sort_keys": ["thin_dupack", "stream_count_thin1", "itt_thin1", "cong_control_thin1", "plot_type"],
                         #"func" : latency_box_key_func,
                         "box_title_def" : "%(stream_count_thin1)dt vs %(stream_count_thick)dg Duration: %(duration)smin Queue: %(queue_type)s (%(queue_len)s) DA:%(thin_dupack)s " })
    #conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIF:%(packets_in_flight)s",
                         "sort_key_func": lambda x: (x["stream_count_thin1"], x["itt_thin1"], int(x["packets_in_flight"])),
                         "sort_keys": ["stream_count_thin1", "itt_thin1"],
                         }
    #conf["page_group_def"] = {"title": "Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s",
    #                          "sort_keys": ["payload_thin1", "itt_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2])}
    conf["plot_conf"].update({ "n_columns": 1, "n_rows" : 1,
                               #"x_axis_lim" : [27, 60],
                               #"x_axis_font_size": 0.7,
                               #"x_axis_main_ticks": 1,
                               #"x_axis_minor_ticks": .5,
                               #"y_axis_main_ticks": 10,
                               #"y_axis_minor_ticks": 5,
                               "default_factor": "stream",
                               #"plot_func": plot_scatterplot,
                               #"plot_func": graph_r_ggplot.plot_scatterplot_cwnd,
                               "plot_func": graph_r_ggplot.ggplot_cdf_box,
                               "do_plots_func": graph_r_ggplot.do_ggplots,
                               })

    conf["box_conf"]["plot_and_table"].update({"make_table": #common.TABLE_ACTION.NO,
                                               common.TABLE_ACTION.APPEND_TO_PLOT_BOX, #,
                                               "table_key": "analysetcp_results_table",
                                               #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                               "heights": (.85, 0, .15),
                                           })
    conf["plot_conf"]["theme_args"].update({"legend.position": "left",
                                            #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
                                            "legend.text": r('element_text(size=12)'),
                                            "legend.key.size": r('unit(10, "mm")'),
                                            "plot.title": r('element_text(size=20, lineheight=1, hjust=0.5, vjust=1, color="grey20")'), # , face="bold"
                                            "axis.text.x": r('element_text(size = 15, hjust=0.5, vjust=0.5)'), # , color="grey50"
                                            "axis.text.y": r('element_text(size = 15, hjust=0.0, vjust=0.5)'),
                                            #"axis.title.x": r('element_text(size = 8, hjust = 0.5, vjust = 0, color="grey20")'),
                                            #"axis.title.y": r('element_text(size = 8, hjust = 0.5, vjust = 0.9, color="grey20")'),
                                            #"plot.title": r('element_text(size=9, lineheight=1, hjust=0.5, color="grey20")'), # , face="bold"
                                            "plot.margin": r('unit(c(0.1, 0.1, 0.2, 0), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                        })

    conf["plot_conf"]["axis_conf"].update({ "x_axis_lim" : [0, 100],
                                            "y_axis_title" : { "title": "CDF", },
                                            "x_axis_title" : { "title": "ITT", },
                                            #"x_axis_main_ticks": [0, 300, 5], "x_axis_minor_ticks": 1,
                                            "y_axis_on_plot_func": lambda x: True, #lambda plot_box_dict: (plot_box_dict["plot_index"] % 3) == 0,
                                        })


    conf["parse_results_func"] = cwnd_results_parse

    #conf["file_parse"].update({ "func": tcpprobe.tcp_probe_file_parse_func })
    conf["document_info"]["title"] = "TCP CONGESTION WINDOW Plots"
    conf["document_info"]["show"] = False
    conf["paper_width"] = 11
    conf["paper_height"] = 6
    conf["print_page_title"] = False

    if defaults:
        util.update_dict(defaults, conf)

    return conf


def get_freebsd_tcpinfo_conf():
    print "get_freebsd_tcpinfo_conf"
    conf = cwnd_tests_get_conf()
    conf["file_parse"].update({#"func": custom_file_parse_func,
        "parse_func_arg": ["ITT",# "Throughput", "Goodput"
                           "Payload",
                           "Latency",
                           "Latency (per segment)",
                           "Sojourn (per segment)",
                           "Sojourn time",
                       ]})

    def freebsd_box_key_func(plot_conf, plot_box_dict, file_data, arg):
        util.dict_set_default(plot_box_dict["legends"], ("color", {"guide_args": { "ncol": 3 } }))

    conf["box_conf"].update({ "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_DUR:%(duration)s_NUM:%(num)s_LOSS:%(loss)s_plot_type:%(plot_type)s",
                              "sort_key_func": lambda x: (data_tables.get_stdev(x["itt_thin1"], stdev=False),
                                                          x["stream_count_thin1"], x["group"], x["queue_len"], x["payload_thin1"], x["itt_thin1"], float(x["loss"]), x["plot_type"]),
                              "sort_keys": ["plot_type", "stream_count_thin1", "group", "itt_thin1", "payload_thin1", "queue_len", "loss"],
                              #"box_title_def" : "Streams: %(stream_count_thin1)d Duration: %(duration)smin  Queue: %(queue_type)s (%(queue_len)s) Uniform loss: %(loss)s %%",
                              "box_titles": { "main": { "title_def": "Streams: %(stream_count_thin1)d  AWF: %(itt_thin1)s  Payload: %(payload_thin1)s  Uniform loss: %(loss)s %%  Duration: %(duration)s min ", "order": 2 },
                                              #"top_title2": { "title_def": " Queue: %(queue_type)s (%(queue_len)s)  RTT: %(rtt)s ms", "order": 1  }, # Duration: %(duration)s min
                                              "top": { "title_def": " %(plot_type_title)s ", "order": 1  }, # Duration: %(duration)s min
                                          },
                              #"pre_key_func": set_box_key_value,
                              "func" : freebsd_box_key_func,
    })
    conf["plot_conf"]["theme_args"].update({"legend.position": "top",
                                            #"legend.background": r('element_rect(color="black", fill="white", size=0.1, linetype="solid")'),
                                            #"legend.title": r('element_text(size=20)'),
                                            "legend.text": r('element_text(size=9)'),
                                            "legend.title": ggplot2.element_blank(),
                                            "legend.key.size": r('unit(9, "mm")'),
                                            "legend.key.height": r('unit(6, "mm")'),
                                            "axis.text.x": r('element_text(size = 10, hjust=0.5, vjust=0.5)'), # , color="grey50"
                                            "axis.text.y": r('element_text(size = 10, hjust=0.0, vjust=0.5)'),
                                            "axis.title.x": r('element_text(size = 10, hjust = 0.5, vjust = 0, color="grey20")'),
                                            "axis.title.y": r('element_text(size = 10, hjust = 0.5, vjust = 1, color="grey20")'),
                                            "plot.title": r('element_text(size=10, lineheight=1, hjust=0.5, color="grey20")'), # , face="bold"
                                            #panel.margin": = unit(2, "lines"))
                                            "panel.margin": r('unit(0.8, "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                            "strip.text.y": r('element_text(size=20, angle=0, color="grey20")'),
                                            #theme(strip.text.x = element_text(size = 8, colour = "red", angle = 90))
                                            "plot.margin": r('unit(c(0.5, 0.6, 0.6, 0.6), "lines")'), # 0=top, 1=right, 2=bottom, 3=left
                                        })
    conf["plot_conf"].update({
        "n_columns": 3, "n_rows" : 2,
        "legend_columns": 2,
    })

    conf.update({
        "paper_width": 15,
        "paper_height": 4.5,
    })

    conf.update({
        "paper_width": 15,
        "paper_height": 8,
    })
    conf["plots_per_page"] = 5


    conf["plot_conf"].update({"n_columns": 1, "n_rows" : 1, "legend_columns": 2})
    conf.update({"paper_width": 5, "paper_height": 4 })
    conf["plots_per_page"] = 1


    conf["file_parse"].update({"func": custom_cwnd_file_parse_func })
    conf["parse_results_func"] = cwnd_results_parse
    return conf

get_conf = cwnd_tests_get_conf
get_conf = get_freebsd_tcpinfo_conf

if __name__ == "__main__":
    import sys
    import plot_test_cwnd
    plot_types = ["rz", "cwnd_tests"]

    argparser = graph_base.make_parser()
    options = argparser.add_argument_group('CWND TESTS  plot options')
    options.add_argument("-pt", "--plot-type",  help="The plot to generate (One of: %s)" % (", ".join(plot_types)), required=False, default=plot_types[0])

    defaults = {}
    args = graph_base.parse_args(argparser, defaults=defaults)
    plot_type = args.plot_type

    if plot_type == "rz":
        #sys.argv.extend(["-tp", "RDBSENDER_ZSENDER_RDB_TFRC_tcpprobe_tfrc_ggplot.pdf",
        #                 "-od", "RDBSENDER_ZSENDER_RDB_TFRC",
        #                 #"-frn", ".*ps400.*",
        #                 #"-frm", ".*tfrc.*",
        #                 "-frm", "r5-rdb-itt1-ps400-ccrdb-da1-lt1-pif100-tfrc1_vs_z5..kbit5000_min5_rtt150_loss_pif100_qlen60_delayfixed_num0.*",
        #                 #"-frm", ".*z5.*min15.*",
        #                 "/root/bendiko/pcap/rdbsender_zsender_to_zreceiver_real/RDBSENDER_ZSENDER_RDB_TFRC/all_results/"
        #             ])
        pass
    elif plot_type == "cwnd_tests":
        defaults["cwnd_tests_output_file"] = "FREEBSD_CWND.pdf"
        defaults["output_dir"] = "FREEBSD_CWND"
        plot_test_cwnd.get_conf = get_freebsd_tcpinfo_conf
        defaults["file_regex_match"] = [#"f1.*itt1-.*min5.*",
                                        #"r1-tcp-itt50-ps50-.*-da0-lt0-er3-tfrc0..kbit5000_min5_rtt150_loss5.*",
            #".*tcp-itt50-ps50-.*-da0-lt0-er3-tfrc0..kbit5000_min5_rtt150_loss5.*",
        ]
        defaults["file_regex_nomatch"] = [".*itt0.*", ".*zreceiver.*"]
        #defaults["directories"] = ["/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEVIER_CWND/all_results/"]
        defaults["directories"] = [#"/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEVIER_LINUX_CWND/all_results/",
            "/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_CWND/all_results/",
            "/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEIVER_CWND/all_results/"
            #/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_CWND/2015-05-11-2334-08/
        ]
    elif plot_type == "cwnd_sojourn_time":
        defaults["cwnd_tests_output_file"] = "CWND_SOJOURN_TIME3.pdf"
        defaults["output_dir"] = "CWND_SOJOURN_TIME3"
        plot_test_cwnd.get_conf = get_freebsd_tcpinfo_conf
        defaults["file_regex_match"] = [#".*itt10-ps50.*min5.*loss2.*",
            #"r1-tcp-itt1-ps20-ccreno-da0-lt0-er3-tfrc0..kbit5000_min5_rtt150_loss2_pif0_qlen63_delayfixed_num.*",
                                        #"r1-tcp-itt50-ps50-.*-da0-lt0-er3-tfrc0..kbit5000_min5_rtt150_loss5.*",
            #".*tcp-itt50-ps50-.*-da0-lt0-er3-tfrc0..kbit5000_min5_rtt150_loss5.*",
        ]
        defaults["file_regex_nomatch"] = [".*itt0.*",
                                          #".*itt1-.*",
                                          ".*loss1_.*",
                                          ".*zreceiver.*", ".*ps(100|200)-.*"]
        #defaults["directories"] = ["/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEVIER_CWND/all_results/"]
        defaults["directories"] = [#"/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEVIER_LINUX_CWND/all_results/",
            "/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_CWND_TCPSEQ/all_results/",
            #"/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEIVER_CWND/all_results/"
            "/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEIVER_CWND_TCPSEQTRACK2/all_results/",
            #/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_CWND/2015-05-11-2334-08/
        ]
    elif plot_type == "cwnd_sojourn_time_ittfix":
        defaults["cwnd_tests_output_file"] = "CWND_SOJOURN_TIME_ITTFIX2.pdf"
        defaults["output_dir"] = "CWND_SOJOURN_TIME_ITTFIX2"
        plot_test_cwnd.get_conf = get_freebsd_tcpinfo_conf
        defaults["file_regex_match"] = [#".*itt10-ps50.*min5.*loss2.*",
            #"r1-tcp-itt1-ps20-ccreno-da0-lt0-er3-tfrc0..kbit5000_min5_rtt150_loss2_pif0_qlen63_delayfixed_num.*",
                                        #"r1-tcp-itt50-ps50-.*-da0-lt0-er3-tfrc0..kbit5000_min5_rtt150_loss5.*",
            #".*tcp-itt50-ps50-.*-da0-lt0-er3-tfrc0..kbit5000_min5_rtt150_loss5.*",
        ]
        defaults["file_regex_nomatch"] = [".*itt0.*",
                                          #".*itt1-.*",
                                          #".*loss1_.*",
                                          ".*zreceiver.*",
                                          #".*ps(100|200)-.*"
                                          ]
        #defaults["directories"] = ["/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEVIER_CWND/all_results/"]
        defaults["directories"] = [#"/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEVIER_LINUX_CWND/all_results/",
            #"/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_CWND_TCPSEQ_ITTFIX/all_results/",
            #"/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_CWND_TCPSEQ/all_results/",
            "/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_CWND_TCPSEQ_ITTFIX/all_results/",
            #"/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEIVER_CWND/all_results/"
            "/root/bendiko/pcap/fsender_to_zreceiver/FSENDER_TO_ZRECEIVER_CWND_TCPSEQTRACK_ITTFIX/all_results/",
            #/root/bendiko/pcap/rdbsender_to_zreceiver_rdb/RDBSENDER_TO_ZRECEIVER_CWND/2015-05-11-2334-08/
        ]
    else:
        cprint("Invalid plot type: '%s'" % plot_type, "red")
        sys.exit()

    print "thismodule.get_conf:", get_conf

    args = graph_base.parse_args(argparser, defaults=defaults)
    #print "Force:", args.force
    #print "args:", args
    graph_base.main()
