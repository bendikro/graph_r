#!/usr/bin/env python

if __name__ == '__main__' and __package__ is None:
    from os import sys, path
    sys.path.append(path.dirname(path.dirname(path.abspath(__file__))))

import unittest

#from .. import util
#from util import *
import util
import re

class UtilTest(unittest.TestCase):

    def test_read_output_from_file(self):

        def content_func(content):
            sections = re.split('Aggregated Statistics for', content)
            #print "sections:", len(sections)
            #print "section:", sections[1]
            #print "RET:", sections[1]
            return sections[1]

        info = util.read_output_from_file("data/t21-tcp-itt100-ps120-cccubic-da0-lt0_vs_g10_kbit5000_min5_rtt150_loss_pif0_qlen30_delayfixed_num0_rdbsender-latency-all-aggr.stout",
                                          content_func=content_func)
        #print "info:", info
        #for k in info:
        #    print "%s: %d" % (k, len(info[k]))

if __name__ == '__main__':
    unittest.main()
