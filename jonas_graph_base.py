#!/usr/bin/env python
from datetime import datetime
import argparse
import os

from graph_base import main, parse_args

from graph_default_bendik import get_default_conf

import graph_default
graph_default.get_conf = get_default_conf

if __name__ == "__main__":
    parse_args()

    #if args.throughput_output_file:
    #    from throughput import get_conf
    #    do_all_plots(args, get_conf())
    #
    #if args.goodput_output_file:
    #    from throughput import get_goodput_conf
    #    do_all_plots(args, get_goodput_conf())
    #
    #if args.latency_output_file:
    #    from latency import get_conf
    #    do_all_plots(args, get_conf())
    #
    #if args.verify_trace_output_file:
    #    from verify_conn import get_conf
    #    do_all_plots(args, get_conf())
    #
    #if args.queueing_delay_output_file:
    #    from queueing_delay import get_conf
    #    do_all_plots(args, get_conf())
    #
    #if args.burstiness_output_file:
    #    from burstiness import get_conf
    #    do_all_plots(args, get_conf())

    if args.connection_info_output_file:
        get_conf = _get_default_conf

        from conn_info import get_conf
        do_all_plots(args, get_conf())

    main()
