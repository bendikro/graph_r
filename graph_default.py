from copy import deepcopy
import argparse
from collections import OrderedDict
from datetime import datetime
from graph_r import do_plots
import util
import os
from util import cprint, vprint
from common import TABLE_ACTION

import rpy2.robjects.lib.ggplot2 as ggplot2

dst_ip = "10.0.0.22"
host_ip_map = { 'rdbsender': "10.0.0.12", 'zsender': "10.0.0.13", "ysender": "10.0.0.14",
                "wsender": "10.0.0.15", "bridge2-eth2": "10.0.0.14", 'fsender': "10.0.0.16" }

host_stream_id_map = { 'rdbsender': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"} ,
                       'ysender': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"},
                       'bridge2-eth2': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"},
                       'zsender': {"rdb": "thick rdb(%(packets_in_flight)s)", "tcp": "thick tcp"} }

host_stream_type_map = { 'rdbsender': "thin", 'ysender': "thin", 'wsender': "thin2",
                         'zsender': "thick", "bridge2-eth2": "thin", 'fsender': "type_thin1" }

host_stream_type_id_map = { 'rdbsender': "type_thin1", 'ysender': "type_thin2", 'wsender': "type_thin2",
                            'zsender': "type_thick", "bridge2-eth2": "type_thin1", 'fsender': "type_thin1" }
#host_stream_itt_id_map = { 'rdbsender': "itt_thin1", 'ysender': "itt_thin1", 'zsender': "itt_thick", "bridge2-eth2": "itt_thin1" }
#host_stream_payload_id_map = { 'rdbsender': "payload_thin1", 'ysender': "payload_thin1", 'zsender': "payload_thick", "bridge2-eth2": "payload_thin1" }

# Two thin streams
#host_stream_type_id_map2 = { 'rdbsender': "type_thin1", 'ysender': "type_thin2", 'zsender': "type_thick", "bridge2-eth2": "type_thin2" }
#host_stream_itt_id_map2 = { 'rdbsender': "itt_thin1", 'ysender': "itt_thin2", 'zsender': "itt_thick", "bridge2-eth2": "itt_thin2" }
#host_stream_payload_id_map2 = { 'rdbsender': "payload_thin1", 'ysender': "payload_thin2", 'zsender': "payload_thick", "bridge2-eth2": "payload_thin2" }


#dstPorts = { 'rdbsender': 5000, 'zsender': 5001, 'ysender': 5002, "bridge2-eth2": 5002 }
dstPorts = { 'rdbsender': 5000, 'zsender': 5001, 'ysender': 5002, 'wsender': 5003, 'fsender': 5004 }

set_order = {0: 1, 4: 2, 6: 3, 10: 4, 20: 5}

hostname_id_map = {"rdbsender": "r",
                   "ysender": "y",
                   "wsender": "w",
                   "fsender": "f",
                   "zsender": "z"}

host_to_color_map = { 'rdbsender': "darkblue",
                      'zsender': "red",
                      'wsender': "blue",
                      'ysender': "darkgoldenrod1",
                      'fsender': "darkgoldenrod1"}

#stream_type_to_ggplot_properties = {"r": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Blues")[7:7])', "legend_name": "Thin RDB"}]},
#                                    "y": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
#                                    "t": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
#                                    "z": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Greedy"}]}}

stream_type_to_ggplot_properties = {"r": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Blues")[7:7])', "legend_name": "Thin %(type_upper)s"}]},
                                    "y": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                    "f": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                    "t": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                    "w": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Greens")[7:7])', "legend_name": "Thin TCP"}]},
                                    "z": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Greedy"}]},
                                    "g": {"color_mapping": [{"color_code": 'c(brewer.pal(9, "Reds")[7:7])', "legend_name": "Greedy"}]}}

color_map = {
    "default": "black",
    "thick": "darkred",
    "thick2": "blue",
    "thin": "darkgoldenrod1",
    #"thin2": "khaki3",
    "thin2": "lightblue",
    "tcp": "blue",
    "tcp lt/da": "lightblue",
    "tcp er+tlp": "yellowgreen",
    "tcp lt/da/er+tlp": "deeppink",
    "rdb_pif3": "darkblue",
    "rdb_pif4": "darkblue",
    "rdb_pif6":  "darkviolet", #"chartreuse3", # greenish
    "rdb_pif8":  "cornflowerblue",
    "rdb_pif10": "darkgreen",
    "rdb_pif12": "darkgreen",
    "rdb_pif15": "aquamarine",
    "rdb_pif20": "coral2",
    "rdb_pif300": "darkorchid",
    "rdb_pif200": "darkorchid",
    "rdb dpif10": "midnightblue",
    "rdb dpif20": "steelblue",
    }

def get_color(plot):
    stream_id = plot["type_id"]
    pif_type = plot["type_id_pif"]
    if stream_id == "type_thick":
        return color_map["thick"]
    return host_type_color_map[plot["hostname"]][pif_type]


from collections import defaultdict as d_dict
#collections.defaultdict([default_factory[, ...]])

host_type_color_map = {
    "rdbsender": d_dict(lambda: "darkblue", color_map, tcp=color_map["thin"]),
    "ysender": d_dict(lambda: "darkblue", color_map, tcp=color_map["thin"]),
    #"ysender": d_dict(lambda: "darkgoldenrod1", {"tcp": "darkgoldenrod1"}),
    "wsender": d_dict(lambda: "cyan", {"tcp": "cyan"}),
    "zsender": { "default": "darkred", "tcp": "darkred", "rdb": "darkred"},
    }


def file_parse_generic(plot_conf, file_conf, pcap_file, parse_func_arg):
    file_conf["pcap_file"] = pcap_file
    file_conf["unique_input_identity"] = pcap_file
    file_conf["output_dir"] = plot_conf["output_dir"]
    file_conf["default_meta_info"] = True

    host = file_conf["hostname"]
    #print "host:", host
    if not host in dstPorts:
        cprint("host %s not in dstPorts" % host, "red")
        return False

    basename = os.path.basename(pcap_file)
    file_conf["streams_id"] = hostname_id_map[file_conf["hostname"]]

    if not file_conf["streams_id"] in file_conf["streams"]:
        if util.cmd_args.verbose > 0:
            print "ID (%s) is not in list of IDs: %s!" % (file_conf["streams_id"], file_conf["streams"].keys())
        if file_conf["streams_id"] == "r" or file_conf["streams_id"] == "y":
            file_conf["streams_id"] = "t"
        if file_conf["streams_id"] == "z":
            file_conf["streams_id"] = "g"

    #or file_conf["streams_id"] == "f"

    file_conf["pcap_file_receiver"] = pcap_file.replace("%s%s" % (file_conf["hostname"], plot_conf["file_parse"]["pcap_suffix"]), "zreceiver" + plot_conf["file_parse"]["pcap_suffix"])

    type_id = host_stream_type_id_map[host]
    #itt_id = host_stream_itt_id_map[host]
    #payload_id = host_stream_payload_id_map[host]

    file_conf["type_thick"] = "tcp"
    file_conf["dst_port"] = dstPorts[host]

    q_type = "pfifo"
    if file_conf["queue_len"] > 1400:
        q_type = "bfifo"
    file_conf["queue_type"] = q_type

    ## We have two thin streams
    #if file_conf.get("type_thin2", None) is not None:
    #    type_id = host_stream_type_id_map2[host]
    #    itt_id = host_stream_itt_id_map2[host]
    #    payload_id = host_stream_payload_id_map2[host]
    #    file_conf["dst_port"] = dstPorts2[host]

    #print "STREAMS:", file_conf["streams"]

    #file_conf["streams_str"] = "%(stream_count_thick)d vs %(stream_count_thin1)d" % file_conf
    file_conf["stream_type"] = host_stream_type_map[host]
    file_conf["stream_count"] = file_conf["streams"][file_conf["streams_id"]]["stream_count"]

    #if file_conf["stream_type"] == "thin":
    stream_properties = file_conf["streams"][file_conf["streams_id"]]
    if "plot_args" in plot_conf["plot_conf"]:
        vprint("plot_args:", plot_conf["plot_conf"]["plot_args"], v=3)
        stream_properties["meta_info"]["plot_args"].update(plot_conf["plot_conf"]["plot_args"])
        #stream_properties.update({"plot_args": {"show_guide": False },

    #conf["plot_conf"]["plot_args"]
    #stream_properties

    #file_conf["type"] = file_conf[type_id]
    #print "type_id:", type_id
    #print 'file_conf["type"]:', file_conf["type"]
    #print "Setting type for '%10s' with type_id: '%s' : %s" % (host, type_id, file_conf["type"])
    #print "STREAMS:", file_conf["streams"]

    file_conf["type_id"] = type_id
    #print "streams:", file_conf["streams"]
    #print "streams_id:", file_conf["streams_id"]
    #print "packets_in_flight:", file_conf["packets_in_flight"]

    file_conf["type_id_pif"] = "%s_pif%d" % (file_conf["streams"][file_conf["streams_id"]]["type"], file_conf["packets_in_flight"])

    file_conf["type"] = file_conf["streams"][file_conf["streams_id"]]["type"]
    file_conf["itt"] = ""

    if "itt" in file_conf["streams"][file_conf["streams_id"]]:
        file_conf["itt"] = file_conf["streams"][file_conf["streams_id"]]["itt"]

    #if "tfrc" in file_conf["streams"][file_conf["streams_id"]]:
    file_conf["tfrc_str"] = "Yes" if file_conf.get("tfrc", 0) == 1 else "No"

    file_conf["payload"] = ""
    #print "KEYS:", file_conf.keys()
    #if payload_id in file_conf:
    #    file_conf["payload"] = file_conf[payload_id]

    #file_conf["stream_id"] = host_stream_id_map[host][file_conf["type"]] % file_conf

    if "cap" in file_conf:
        plot_conf["plot_conf"]["bandwidth"] = file_conf["cap"]

    file_conf["src_ip"] = host_ip_map[host]
    file_conf["dst_ip"] = dst_ip
    file_conf["data_file_name"] = "%s" % (basename.split(plot_conf["file_parse"]["pcap_suffix"])[0])
    file_conf["common_prefix"] = "%s" % (basename.split(host)[0])
    file_conf["prefix"] = "%s-" % file_conf["data_file_name"]

box_counter = 0

def get_defaults_plot_box_conf():
    global box_counter
    d = {"sets": {}, "box_title": None, "plot_func": None, "meta_data": { "enabled": False, "func": None }}
    d["box_counter"] = box_counter
    d["axis_conf"] = {}
    d["data_tables"] = {}
    d["legends"] = {}
    d["data_cache"] = {}
    box_counter += 1
    return deepcopy(d)


def _get_default_conf():
    default_conf = {"file_parse": {}, "plot_conf": {}, "is_base_conf": True }
    default_conf["file_parse"]["pcap_suffix"] = ".pcap"
    default_conf["file_parse"]["files_match_exp"] = "*%s" % default_conf["file_parse"]["pcap_suffix"]
    default_conf["file_parse"]["files_nomatch_regex"] = []
    default_conf["plot_conf"] = { "bandwidth": 1000, # The bandwidth cap of the link in Kbit/s
                                  "make_plots": True,
                                  "do_plots_func": do_plots,
                                  "r.plot_args": {"cex.axis": 1.3},  # cex.axis = X-axis font size
                                  "r.mtext_page_title_args": {"cex.main": 0.9 # Relative size of page title
                                                              },
                                  "theme_args": {},
                                  "plot_args": {},
                                  "axis_conf": {},
                                  }
    default_conf["box_conf"] = { "bandwidth": 1000, # The bandwidth cap of the link in Kbit/s
                                 "plot_and_table": {"make_table": TABLE_ACTION.APPEND_TO_LIST,
                                                    #"title": { "label": "Test %(plot_number)s", "gp": grid.gpar(fontsize=15, face="bold", col="black"), "hjust": 3, "vjust": 2},})
                                                    "heights": (.65, .03, .3),
                                                },
                                 "legend": { "legend_sort": { "do_sort": False, "sorted_args": {} },
                                             "legend_title": ggplot2.element_blank(),
                                             "colors": [],
                                         },
                                 "data_table_defaults": {},
                                 "custom_ggplot2_plot_cmds": {},
                                 "custom_ggplot2_plotset_cmds": {},
                             }

    default_conf["data_table_defaults"] = {}
    default_conf["sample_sec"] = 1
    default_conf["file_parse"]["parse_func_arg"] = [None]
    default_conf["document_info"] = { "title": "Plots", "info" : [], "show": True, "filename_as_pdf_title": True }
    default_conf["add_meta_data"] = False
    default_conf["func_before_graphs"] = None
    default_conf["plots_per_page"] = 0
    default_conf["process_results"] = {}
    default_conf["print_page_title"] = True
    default_conf["results"] = {}
    default_conf["view_results"] = {"execute": False, "viewers": {".txt": "cat", ".pdf": "evince"} }
    default_conf["data_tables"] = {}
    default_conf["pdf_args"] = { "paper": "special" }
    default_conf["pdf_page_number"] = 1

    default_conf["file_parse"]["files_match_regex"] = []

    if util.cmd_args.view_results:
        default_conf["view_results"]["execute"] = True

    if util.cmd_args.debugt:
        util.DEBUG_CONF["types"].extend(util.cmd_args.debugt)

    if util.cmd_args.debugf:
        util.DEBUG_CONF["funcs"].extend(util.cmd_args.debugf)

    if util.cmd_args.file_regex_match:
        default_conf["file_parse"]["files_match_regex"].extend(util.cmd_args.file_regex_match)
    if util.cmd_args.file_regex_nomatch:
        default_conf["file_parse"]["files_nomatch_regex"].extend(util.cmd_args.file_regex_nomatch)
    return default_conf

get_conf = _get_default_conf
