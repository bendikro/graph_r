from graph_r import *
from copy import deepcopy
import argparse
from collections import OrderedDict
from datetime import datetime

from graph_r import do_plots
import graph_default
from graph_r_ggplot import ggplot_cdf_box, do_ggplots, ggplot2

args = None

try:
    from termcolor import colored, cprint
    termcolor = True
except:
    print("termcolor could not be found. To enable colors in terminal output, install termcolor.")
    termcolor = False
    def cprint(*arg, **kwargs):
        print arg
    def colored(text, color):
        return text


dst_ip = "10.0.0.22"
host_ip_map = { 'rdbsender': "10.0.0.12", 'zsender': "10.0.0.13", "ysender": "10.0.0.14", "bridge2-eth2": "10.0.0.14" }
host_stream_id_map = { 'rdbsender': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"} ,
                       'ysender': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"},
                       'bridge2-eth2': {"rdb": "thin rdb%(packets_in_flight)s", "tcp": "thin tcp"},
                       'zsender': {"rdb": "thick rdb(%(packets_in_flight)s)", "tcp": "thick tcp"} }

host_stream_type_map = { 'rdbsender': "thin", 'ysender': "thin", 'zsender': "thick", "bridge2-eth2": "thin" }

host_stream_type_id_map = { 'rdbsender': "type_thin1", 'ysender': "type_thin1", 'zsender': "type_thick", "bridge2-eth2": "type_thin1" }
host_stream_itt_id_map = { 'rdbsender': "itt_thin1", 'ysender': "itt_thin1", 'zsender': "itt_thick", "bridge2-eth2": "itt_thin1" }
host_stream_payload_id_map = { 'rdbsender': "payload_thin1", 'ysender': "payload_thin1", 'zsender': "payload_thick", "bridge2-eth2": "payload_thin1" }

# Two thin streams
host_stream_type_id_map2 = { 'rdbsender': "type_thin1", 'ysender': "type_thin2", 'zsender': "type_thick", "bridge2-eth2": "type_thin2" }
host_stream_itt_id_map2 = { 'rdbsender': "itt_thin1", 'ysender': "itt_thin2", 'zsender': "itt_thick", "bridge2-eth2": "itt_thin2" }
host_stream_payload_id_map2 = { 'rdbsender': "payload_thin1", 'ysender': "payload_thin2", 'zsender': "payload_thick", "bridge2-eth2": "payload_thin2" }


#dstPorts = { 'rdbsender': 5000, 'zsender': 5001, 'ysender': 5002, "bridge2-eth2": 5002 }
dstPorts = { 'rdbsender': 5000, 'zsender': 5001, 'ysender': 5002, 'wsender': 5003 }
set_order = {0: 1, 3: 2, 6: 3, 10: 4, 20: 5}


color_map = {
    "default": "black",
    "thick": "darkred",
    "thick2": "blue",
    "thin": "darkgoldenrod1",
    "tcp_pif0": "darkgoldenrod1",
    "rdb_pif3": "darkblue",
    "rdb_pif6": "cyan",
    "rdb_pif10": "darkgreen",
    "rdb_pif20": "darkorchid",
    "rdb_pif300": "darkorchid",
    }

def get_color(plot):
    stream_id = plot["type_id"]
    pif_type = plot["type_id_pif"]
    if stream_id == "type_thick":
        return color_map["thick"]
    return host_type_color_map["rdbsender"][pif_type]

host_type_color_map = {
    "rdbsender": dict(color_map, tcp=color_map["thin"]),
    "zsender": { "default": "darkred", "tcp": "darkred", "rdb": "darkred" }
    }

filename_regexes = []

# 30_thin_tcp_vs_30_thick_stream_cap_5000kbit_duration_20m_payload_120_itt_100_rtt_150_loss__p_in_flight_0_queue_len_61_delay_fixed_num_1_collapse_thin_1_collapse_thick_1_segoff_off_segoff_thin_on_segoff_thick_on_zreceiver.pcap
filename_regex = "(?P<streams>(?P<stream_count_thin1>\d+)_thin_(?P<type>.+)?_vs_(?P<stream_count_thick>\d+)_thick)_stream_"\
    "(cap_(?P<cap>\d+)kbit_)?"\
    "duration_(?P<duration>\d+)m_payload_(?P<payload_thin1>\d+)_itt_(?P<itt_thin1>\d+)_rtt_(?P<rtt>\d+)_"\
    "loss_(?P<loss>.*)_p_in_flight_(?P<packets_in_flight>\d+)(_queue_len_(?P<queue_len>\d+))?(_delay_(?P<delay_type>[a-z]+))?(_num_(?P<num>\d+))?(?P<extra>.+)?_(?P<hostname>.+)\."
filename_regexes.append((filename_regex, None))

# 60_thin_tcp_vs_60_thick_stream_cap_10000kbit_duration_20m_payload_120_itt_100_rtt_150_loss__p_in_flight_0_queue_len_122_delay_fixed_num_1_clps_thin_1_clps_thick_0_segoff_off_segoff_thin_on_segoff_thick_off_brate_10_duplex_half_zsender.pcap
#60_thin_tcp_vs_60_thick_stream_cap_10000kbit_duration_20m_payload_120_itt_100_rtt_150_loss__p_in_flight_0_queue_len_122_delay_fixed_num_0_clps_thin_1_clps_thick_0_segoff_off_segoff_thin_on_segoff_thick_off_zsender.pcap

filename_regex = "(?P<streams>(?P<stream_count_thin1>\d+)_thin_(?P<type>[^_]+)?_vs_(?P<stream_count_thick>\d+)_thick)_stream_"\
    "(cap_(?P<cap>\d+)kbit_)?"\
    "duration_(?P<duration>\d+)m_payload_(?P<payload_thin1>\d+)_itt_(?P<itt_thin1>\d+)_rtt_(?P<rtt>\d+)_"\
    "loss_(?P<loss>[^_]*)_p_in_flight_(?P<packets_in_flight>\d+)(_queue_len_(?P<queue_len>\d+))?(_delay_(?P<delay_type>[^_]+))?(_num_(?P<num>\d+))?(?P<extra>.+)?_(?P<hostname>[^_]+)\."
filename_regexes.append((filename_regex, None))

#8_thin_rdb_vs_8_thick_stream_cap_5000kbit_duration_15m_payload_100_itt_50_rtt_100_loss__p_in_flight_6_queue_len_41_num_0_zreceiver.pcap
#32_thin_tcp_vs_32_cap_2000kbit_thick_stream_duration_20m_payload_100_itt_100_rtt_100_loss__p_in_flight_0_num_5_rdbsender.pcap
filename_regex = "(?P<streams>(?P<stream_count_thin1>\d+)_thin_(?P<type>.+)?_vs_(?P<stream_count_thick>\d+)_thick)_stream_"\
    "(cap_(?P<cap>\d+)kbit_)?"\
    "duration_(?P<duration>\d+)m_payload_(?P<payload_thin1>\d+)_itt_(?P<itt_thin1>\d+)_rtt_(?P<rtt>\d+)_"\
    "loss_(?P<loss>.*)_p_in_flight_(?P<packets_in_flight>\d+)(_queue_len_(?P<queue_len>\d+))?(_delay_(?P<delay_type>[a-z]+))?(_num_(?P<num>\d+))?_(?P<hostname>.+)\."
filename_regexes.append((filename_regex, None))

filename_regex = "(?P<streams>t(?P<stream_count_thin1>\d+)-(?P<type>[^_]+)?_vs_g(?P<stream_count_thick>\d+))_kbit(?P<cap>\d+)_min(?P<duration>\d+)_ps(?P<payload_thin1>\d+)_itt(?P<itt_thin1>\d+)_rtt(?P<rtt>\d+)_loss_pif(?P<packets_in_flight>\d+)_(?:qlen(?P<queue_len>\d+))_(?:delay(?P<delay_type>[^_]+))_(num(?P<num>\d+))?(?P<extra>.+)?_(?P<hostname>[^_]+)\."
filename_regexes.append((filename_regex, None))

#t60-tcp_vs_g60_kbit10000_min20_ps120_itt100_rtt150_loss_pif0_qlen122_delayfixed_num0_clps-t-0_clps-g-0_soff-off_soff-t-0_soff-g-0_brate-speed-10-duplex-full_bridge2-eth1.pcap
filename_regex = "(?P<streams>t(?P<stream_count_thin1>\d+)-(?P<type_thin1>[^-]+)-itt(?P<itt_thin1>[^-]+)-ps(?P<payload_thin1>[^-]+)(:?_vs_t(?P<stream_count_thin2>\d+)-(?P<type_thin2>[^-]+)-itt(?P<itt_thin2>[^-]+)-ps(?P<payload_thin2>[^-]+))?_vs_g(?P<stream_count_thick>\d+))_kbit(?P<cap>\d+)_min(?P<duration>\d+)_rtt(?P<rtt>\d+)_loss_pif(?P<packets_in_flight>\d+)_(?:qlen(?P<queue_len>\d+))_(?:delay(?P<delay_type>[^_]+))_(num(?P<num>\d+))?(?P<extra>.+)?_(?P<hostname>[^_]+)\."
filename_regexes.append((filename_regex, None))

#t1-rdb-itt1-ps450-ccvegas_vs_g2_kbit5000_min10_rtt150_loss_pif300_qlen30_delayfixed_num0_rdbsender.pcap
#filename_regex = "(?P<streams>t(?P<stream_count_thin1>\d+)-(?P<type_thin1>[^-]+)-itt(?P<itt_thin1>[^-]+)-ps(?P<payload_thin1>[^-]+)-cc(?P<cong_control_thin1>[^-]+)(:?_vs_t(?P<stream_count_thin2>\d+)-(?P<type_thin2>[^-]+)-itt(?P<itt_thin2>[^-]+)-ps(?P<payload_thin2>[^-]+))?_vs_g(?P<stream_count_thick>\d+))_kbit(?P<cap>\d+)_min(?P<duration>\d+)_rtt(?P<rtt>\d+)_loss_pif(?P<packets_in_flight>\d+)_(?:qlen(?P<queue_len>\d+))_(?:delay(?P<delay_type>[^_]+))_(num(?P<num>\d+))?(?P<extra>.+)?_(?P<hostname>[^_]+)\."
#filename_regexes.append(filename_regex)


def parse_elements(elements):
    conf = {"cong_control": "cubic",
    }
    def parse(regex, e):
        m = re.match(regex, e, flags=re.DOTALL|re.VERBOSE)
        if m:
            conf.update(m.groupdict())
        return m is not None

    conf["type"] = "tcp"
    parse("(?P<id>.)(?P<stream_count>\d+)", elements.pop(0))

    if elements:
        # Stream type is special case since it has no prefix
        if elements[0] == "rdb" or elements[0] == "tcp":
            conf["type"] = elements.pop(0)
        properties_regexes = ["itt(?P<itt>[^-]+)",
                              "ps(?P<payload>[^-]+)",
                              "rc(?P<retrans_collapse>[^-]+)",
                              "cc(?P<cong_control>[^-]+)",
                              "da(?P<thin_dupack>[^-]+)",
                              "lt(?P<linear_timeout>[^-]+)",
                              "er(?P<early_retransmit>[^-]+)",
                              "pif(?P<packets_in_flight>[^-]+)",
                              "dpif(?P<dpif>[^-]+)",
                              "tfrc(?P<tfrc>[^-]+)",
                              "ab(?P<abuser_mode>[^-]+)",
                          ]
        while elements:
            e = elements.pop()
            for reg in properties_regexes:
                if parse(reg, e):
                    break
    # Convert ints
    for k in conf:
        if type(conf[k]) is str:
            if conf[k].isdigit():
                conf[k] = int(conf[k])
    conf["type_upper"] = conf["type"].upper()
    return conf

default_file_conf = {"packets_in_flight": 0, "dpif": 0, "stream_count_thick": -1 }
default_stream_conf = { "aes_args": {}, "meta_info": { "plot_args": {} }}

#t21-tcp-itt100-ps120-cccubic-da1-lt1_vs_g10_kbit5000_min5_rtt150_loss_pif0_qlen30_delayfixed_num0_zreceiver.pcap
def parse_fname(fname):
    #print "parse_fname!!!!!!!!!!!!!!!!!!!!!!!!!"
    split_on = None
    if fname.find("..") == -1:
        i = fname.find("_kbit")
        streams = fname[:i]
        common = fname[i+1:]
    else:
        streams, common = fname.split("..")
    streams = streams.split("_vs_")
    # r10-rdb-itt0-ps400-ccrdb-da1-lt1-pif100-tfrc1_vs_z5..kbit5000_min5_rtt150_loss_pif100_qlen60_delayfixed_num0_zreceiver.pcap
    #kbit5000_min5_rtt150_loss_pif0_qlen30_delayfixed_num0_zreceiver.pcap
    common_regex = "kbit(?P<cap>\d+)_min(?P<duration>\d+)_rtt(?P<rtt>\d+)_loss(?P<loss>[^_]+)?_pif(?P<packets_in_flight>\d+)_(?:qlen(?P<queue_len>\d+))_(?:delay(?P<delay_type>[^_]+))_(num(?P<num>\d+))?(?P<extra>.+)?_(?P<hostname>[^_]+)\."
    regexes = ["r(?P<stream_count_thin1>\d+)-(?P<type_thin1>[^-]+)-itt(?P<itt_thin1>[^-]+)-ps(?P<payload_thin1>[^-]+)-cc(?P<cong_control_thin1>[^-]+)(-da(?P<thin_dupack>[^-]+))?(-lt(?P<linear_timeout>[^-]+))?(-er(?P<early_retransmit>[^-]+))?(-rc(?P<retrans_collapse>[^-]+))?(-pif(?P<packets_in_flight>[^-]+))?(-dpif(?P<dpif>[^-]+))?(-tfrc(?P<tfrc>[^-]+))?(-ab(?P<abuser_mode>[^-]+))?",
               "t(?P<stream_count_thin1>\d+)-(?P<type_thin1>[^-]+)-itt(?P<itt_thin1>[^-]+)-ps(?P<payload_thin1>[^-]+)-cc(?P<cong_control_thin1>[^-]+)(-da(?P<thin_dupack>[^-]+))?(-lt(?P<linear_timeout>[^-]+))?(-er(?P<early_retransmit>[^-]+))?(-rc(?P<retrans_collapse>[^-]+))?",
               "y(?P<stream_count_thin2>\d+)-(?P<type_thin2>[^-]+)-itt(?P<itt_thin2>[^-]+)-ps(?P<payload_thin2>[^-]+)",
               "z(?P<stream_count_thick>\d+)",
               "g(?P<stream_count_thick>\d+)",
               "w(?P<stream_count_thin2>\d+)-(?P<type_thin2>[^-]+)-itt(?P<itt_thin2>[^-]+)-ps(?P<payload_thin2>[^-]+)-cc(?P<cong_control_thin2>[^-]+)?",
               "f(?P<stream_count_thin1>\d+)-(?P<type_thin1>[^-]+)-itt(?P<itt_thin1>[^-]+)-ps(?P<payload_thin1>[^-]+)-cc(?P<cong_control_thin1>[^-]+)(-da(?P<thin_dupack>[^-]+))?(-lt(?P<linear_timeout>[^-]+))?(-er(?P<early_retransmit>[^-]+))?(-rc(?P<retrans_collapse>[^-]+))?",
               #"f(?P<stream_count_thin2>\d+)-(?P<type_thin2>[^-]+)-itt(?P<itt_thin2>[^-]+)-ps(?P<payload_thin2>[^-]+)-cc(?P<cong_control_thin2>[^-]+)?",
               ]

    #t6-rdb-itt10-ps400-ccrdb-da0-lt0', 'y6-tcp-itt10-ps400', 'z6
    m = re.match(common_regex, common, flags=re.DOTALL|re.VERBOSE)
    file_conf = deepcopy(default_file_conf)
    file_conf.update(m.groupdict())
    file_conf["streams"] = {}
    for s in streams:
        #print "S:", s
        elements = s.split("-")
        conf = deepcopy(default_stream_conf)
        conf.update(parse_elements(elements))
        #print "conf:", conf
        file_conf["streams"][conf["id"]] = conf
        for reg in regexes:
            m = re.match(reg, s, flags=re.DOTALL|re.VERBOSE)
            if m:
                groups = m.groupdict()
                for k in groups.keys():
                    if groups[k] is None:
                        del groups[k]
                #print "reg:", reg
                file_conf.update(groups)
                break
        conf["type_upper"] = conf["type"].upper()
    #print "host:", conf["host"]
    #print "type_upper:", conf["type_upper"]
    #print "file_conf:", conf["type_upper"]
    return file_conf

#t1-rdb-itt1-ps450-ccvegas_vs_g2_kbit5000_min10_rtt150_loss_pif300_qlen30_delayfixed_num0_rdbsender.pcap
filename_regex = "(?P<streams>t(?P<stream_count_thin1>\d+)-(?P<type_thin1>[^-]+)-itt(?P<itt_thin1>[^-]+)-ps(?P<payload_thin1>[^-]+)-cc(?P<cong_control_thin1>[^-]+)(-da(?P<thin_dupack>[^-]+))?(-lt(?P<linear_timeout>[^-]+))?(:?_vs_t(?P<stream_count_thin2>\d+)-(?P<type_thin2>[^-]+)-itt(?P<itt_thin2>[^-]+)-ps(?P<payload_thin2>[^-]+))?_vs_g(?P<stream_count_thick>\d+))_kbit(?P<cap>\d+)_min(?P<duration>\d+)_rtt(?P<rtt>\d+)_loss_pif(?P<packets_in_flight>\d+)_(?:qlen(?P<queue_len>\d+))_(?:delay(?P<delay_type>[^_]+))_(num(?P<num>\d+))?(?P<extra>.+)?_(?P<hostname>[^_]+)\."
filename_regexes.append((filename_regex, parse_fname))

filename_regex = ".*kbit(?P<cap>\d+)_min(?P<duration>\d+)_rtt(?P<rtt>\d+)_loss(?P<loss>[^_]+)?_pif(?P<packets_in_flight>\d+)_(?:qlen(?P<queue_len>\d+))_(?:delay(?P<delay_type>[^_]+))_(num(?P<num>\d+))?(?P<extra>.+)?_(?P<hostname>[^_]+)\."

#filename_regexes.append((filename_regex, parse_fname))
#t21-tcp-itt100-ps120-cccubic-da1-lt1_vs_g10_kbit5000_min5_rtt150_loss_pif0_qlen30_delayfixed_num0_zreceiver.pcap


filename_regexes.append((filename_regex, parse_fname))

#t6-rdb-itt10-ps400-ccrdb-da0-lt0_vs_y6-tcp-itt10-ps400_vs_z6..kbit5000_min2_rtt150_loss_pif200_qlen30_delayfixed_num0_rdbsender.pcap


def get_latency_conf():
    print "get_latency_conf"
    from latency import get_latency_conf as get_latency_conf_original
    import latency
    conf = get_latency_conf_original()
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(linear_timeout)s",
                         "sort_key_func": lambda x: (x["stream_count_thin1"], x["stream_count_thick"], x["group"], x["itt_thin1"]),
                         "sort_keys": ["stream_count_thin1", "stream_count_thick", "group", "itt_thin1"],
                         "func" : latency.latency_box_key_func,
                         "latency_options": {"per_stream": False},
                         "box_title_def" : "Thin count: %(stream_count_thin1)s Greedy: %(stream_count_thick)s  ITT: %(itt_thin1)sms" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["page_group_def"] = {#"title": "Payload: %(payload_thin1)s  RTT: %(rtt)sms  ITT: %(itt_thin1)sms  Queue Len: %(queue_len)s  Duration: %(duration)s min",
        "title": "%(stream_count_thin1)d Thin streams vs %(stream_count_thick)d Greedy streams - Duration: %(duration)s min\n"
        "Payload thin streams: %(payload_thin1)s   RTT: %(rtt)sms   ITT: %(itt_thin1)sms   Queue Len: %(queue_len)s packets",
        "sort_keys": ["payload_thin1", "rtt", "stream_count_thin1"], "sort_key_func": lambda x: (x[0], x[1], x[2])}
    conf["plot_conf"].update({"n_columns": 2, "n_rows" : 2, "x_axis_lim" : [150, 1200], "y_axis_lim" : [0.5, 1.05],
                              "y_axis_title" : "ECDF",
                              "x_axis_title" : {
                                  "title": ["ACK Latency in milliseconds"],
                              },
                              #"plot_func": plot_ecdf_box,
                              "plot_func": ggplot_cdf_box,
                              "do_plots_func": do_ggplots, #do_plots,
                              #"do_plots_func": do_plots,
                              "legend": { "color_func": latency.get_color_latency, "values": {"type_thick": "Greedy", "type_thin1": "Thin", "type_thin2": "Thin2", "type_thin3": "Thin3"}},
                              #"box_commands": thin_itt_mark_line_func,
                              "r.plot_args": {"cex.main": 0.9 },
                              "r.mtext_page_title_args": {"cex.main": 1.2 },
                              "theme_args": {"legend.position": "left"},
                          })
    conf["paper_width"] = 10
    conf["paper_height"] = 8

    # Changes for jonas plots
    #conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(linear_timeout)s_%(retrans_collapse)s",
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Streams_Greedy:%(stream_count_thick)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s_%(cong_control_thin1)s_%(duration)s_%(thin_dupack)s_%(linear_timeout)s_%(retrans_collapse)s",
                         "sort_key_func": lambda x: (x["retrans_collapse"], x["thin_dupack"], x["linear_timeout"], x["stream_count_thin1"], x["stream_count_thick"], x["group"]),
                         "sort_keys": ["thin_dupack", "linear_timeout", "retrans_collapse", "stream_count_thin1", "stream_count_thick", "group"],
                         "func" : latency.latency_box_key_func,
                         "latency_options": {"per_stream": False},
                         "box_title_def" : "Thin Dupack: %(thin_dupack)s   Thin Linear Timeout: %(linear_timeout)s   Retrans collapse: %(retrans_collapse)s" }


    conf["plot_conf"]["theme_args"]["legend.position"] = FloatVector([0.85, 0.2])
    conf["plot_conf"]["theme_args"]["legend.background"] = r('element_rect(color="black", fill="white", size=0.5, linetype="solid")')
    #conf["plot_conf"]["theme_args"]["axis.title.x"] = ggplot2.element_blank()
    #conf["plot_conf"]["theme_args"]["axis.title.y"] = ggplot2.element_blank()

    return conf

def parse_input(plot_conf, f, arg):
    regexes = plot_conf["file_parse"]["filename_regex"]
    if type(regexes) is str:
        regexes = [regexes]

    basename = os.path.basename(f)
    #print "parse_file:", f
    #print "Parse basename:", basename
    # If multiple regex are defined, use the first that matches
    for i, regex_arg in enumerate(regexes):
        regex, func = regex_arg
        #print "regex:", regex
        m = re.match(regex, basename, flags=re.DOTALL|re.VERBOSE)
        if m:
            if func:
                file_conf = func(basename)
                #print "Regex %d matched" % i
            else:
                file_conf = m.groupdict()
            break
    if not m:
        print "No match for file '%s'!" % basename
        return None

    for k in file_conf:
        if type(file_conf[k]) is str:
            if file_conf[k].isdigit():
                file_conf[k] = int(file_conf[k])
#    print "file_conf:", file_conf
    return file_conf

def get_default_conf():
    from graph_default import _get_default_conf as get_conf
    conf = get_conf()
    conf["file_parse"]["filename_regex"] = filename_regexes
    conf["file_parse"]["parse_input"] = parse_input
    return conf

