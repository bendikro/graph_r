#!/usr/bin/env python
from graph_default import *

def throughput_box_key_time_func(box_conf, conf):
    throughput_box_key_func(box_conf, conf)

def throughput_file_parse_func_rdb(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    if throughput_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False
    pif = file_conf["packets_in_flight"]
    file_conf["column"] = set_order[pif]

def goodput_file_parse_func_rdb(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    if goodput_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False
    pif = file_conf["packets_in_flight"]
    file_conf["column"] = set_order[pif]

    #file_conf["column"] = file_conf["num"] + 1 - 5
    #print 'file_conf["column"]:', file_conf["column"]
    #print "Setting column:", file_conf["column"]

def latency_meta_data_box_func(conf, plot_box_dict, sets):
    keys = [("Number of retrans", "Number of retransmissions", None),
            ("1. retrans", "Occurrences of 1. retransmission", None),
            ("2. retrans", "Occurrences of 2. retransmission", None),
            ("Data packets sent", "Total data packets sent", None),
            ("Total bytes sent", "Total bytes sent (payload)", None),
            ("Nr. retrans bytes", "Number of retransmitted bytes", None),
            ("Avg. min latency", "Average latencies (min/avg/max)", "(?P<value>\d+),+"),
            ("Avg. avg latency", "Average latencies (min/avg/max)", "\d+,\s+(?P<value>\d+),+"),
            ("Avg. max latency", "Average latencies (min/avg/max)", "\d+,\s+\d+,\s+(?P<value>\d+)\sms"),
            ("Avg. packet size", "Average of all packets in all connections", None),
            ("Estimated loss", "Estimated loss rate based on retransmissions", None),
            ("Packet  Loss", "Packet  Loss", None),
            ("Ranges Loss", "Ranges Loss", None),
            ("Bytes Loss", "Bytes Loss", None),
            ("Redundancy", "Redundancy", None),
            ("RDB packets", "Number of packets with bundled segments", None),
            ("RDB bytes bundled", "RDB bytes bundled", "(?P<value>\d+).+"), # :     460896 (1.316981% of total bytes sent)
            ("RDB packet hits", "RDB packet hits", "(?P<value>\d+).+"),     # :      47585 (84.271951% of RDB packets sent)
            ("RDB packet misses", "RDB packet misses", "(?P<value>\d+).+"), # :       8881 (15.728049% of RDB packets sent)
            ("RDB byte   hits", "RDB byte   hits", "(?P<value>\d+).+"),     # :   12927268 (2804.812365% of RDB bytes)
            ("RDB byte   misses", "RDB byte   misses", "(?P<value>\d+).+"), # :    3642404 (790.287614% of RDB bytes)
            ]

    data = []
    colnames = []
    col_colors = ["cyan"]
    rownames = []
    for name, key, regex in keys:
        rownames.append(name)

    sorted_sets = sorted(sets.keys(), key=lambda k: sets[k]["plots"][0]["column"])
    print "sorted_sets:", sorted_sets
    #for s in sets:
    for s in sorted_sets:
        for plot_conf in sets[s]["plots"]:
            #print "plot_conf:", plot_conf
            colnames.append(plot_conf["stream_id"])
            col_colors.append(plot_conf["color"])
            new_data = read_output_data(keys, plot_conf["stdout_file"])
            data += new_data
            #info = read_output_info(plot_conf["stdout_file"])
            #for name, key, regex in keys:
            #    if regex and key in info:
            #        m = re.match(regex, info[key], flags=re.DOTALL)
            #        data.append(m.group("value") if m else "")
            #    else:
            #        data.append(info[key] if key in info else "")

    col_colors = StrVector(col_colors)

    #print "colnames:", colnames
    #print "rownames:", rownames
    #print "col_colors:", col_colors
    #print "Data:", data

    #data = StrVector(data)
    matrix = r.matrix(data, ncol=len(colnames))
    colnames = StrVector(colnames)
    rownames = StrVector(rownames)
    matrix.colnames = colnames
    matrix.rownames = rownames

    color_data = StrVector(["darkred" for c in data])
    color_matrix = r.matrix(color_data, ncol=2)

    if False:
        r["source"]('/root/sit.gz')
        r["plot.table"](matrix, highlight=color_matrix)
    else:
        gplots = importr('gplots')
        textplot = SignatureTranslatedFunction(gplots.textplot, init_prm_translate = {'show_colnames': 'show.colnames', 'show_rownames': 'show.rownames', 'col_data': 'col.data',
                                                                                      'col_colnames': 'col.colnames'})
        # cex controls text size
        textplot(matrix, col_colnames=col_colors, cmar=0.5, cex=0.45, valign="top")

def latency_file_parse_func_time(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    if latency_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False

    if file_conf["hostname"] == "rdbsender":
        file_conf["percentiles"] = plot_conf["box_conf"]["percentiles"]

default_rdb_conf = deepcopy(default_conf)
default_rdb_conf["box_conf"] = { "key": "Streams:%(stream_count_thin)s_Duration:%(duration)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s",
                             "sort_key_func": lambda x: (x["payload"], x["duration"], x["stream_count_thin"], x["packets_in_flight"]),
                             "sort_keys": ["payload", "stream_count_thin", "duration", "packets_in_flight"],
                             "func" : throughput_box_key_time_func,
                             "box_title_def": "%(stream_count_thin)dvs%(stream_count_thick)d Payload: %(payload)s"}
default_rdb_conf["page_group_def"] = { "title": "Streams: %(streams_str)s, ITT: %(itt)sms, RTT: %(rtt)s",
                                   "sort_keys": ["stream_count_thin", "itt", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2]) }


def get_throughput_conf():
    throughput_conf = get_default_throughput_conf()
    throughput_conf["box_conf"].update(default_rdb_conf["box_conf"])
    throughput_conf["page_group_def"].update(default_rdb_conf["page_group_def"])

    #throughput_conf["box_conf"] = { "key": "Streams:%(stream_count_thin)s_Duration:%(duration)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s",
    #                                "sort_key_func": lambda x: (x["payload"], x["duration"], x["stream_count_thin"], x["packets_in_flight"]),
    #                                "sort_keys": ["payload", "stream_count_thin", "duration", "packets_in_flight"],
    #                                "func" : throughput_box_key_time_func,
    #                                "box_title_def": "%(stream_count_thin)dvs%(stream_count_thick)d Payload: %(payload)s"}

    #throughput_conf["set_conf"] = { "key": "Streams:%(stream_count_thin)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s_PIF:%(packets_in_flight)s_Type:%(type)s_Num:%(num)s"}
    throughput_conf["set_conf"] = { "key": "Streams:%(stream_count_thin)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s_PIF:%(packets_in_flight)s_Num:%(num)s"}

    throughput_conf["plot_conf"]["n_columns"] = 3
    throughput_conf["file_parse"]["func"] = throughput_file_parse_func_rdb
    throughput_conf["document_info"]["title"] = "Throughput Plots"
    #throughput_conf["file_parse"]["files_match_exp"] = "8_thin_*_vs_8*%s" % throughput_conf["file_parse"]["pcap_suffix"]

    ########################################
    # Group by streams instead of duration
    #throughput_conf["box_conf"] = { "key": "Streams:%(stream_count_thin)s_Duration:%(duration)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s_Type:%(type_id)s",
    #                                "sort_key_func": lambda x: (x["duration"], x["stream_count_thin"], x["packets_in_flight"]),
    #                                "sort_keys": ["duration", "stream_count_thin", "packets_in_flight"],
    #                                "func" : throughput_box_key_time_func,
    #                                "box_title_def": "Dur: %(duration)s, ID: %(type_id)s" }
    return throughput_conf


def get_goodput_conf():
    goodput_conf = get_throughput_conf()
    setup_goodput_conf(goodput_conf)
    conf["file_parse"]["func"] = goodput_file_parse_func_rdb
    return goodput_conf

latency_rtt_to_x_lim_max = { 30: 500, 100: 600, 150: 800, 200: 900 }

def latency_box_key_func_rdb(box_conf, conf):
    if latency_box_key_func(box_conf, conf) is False:
        return False
    #duration_sec = conf["duration"] * 60
    #box_conf["x_axis_lim"][1] = duration_sec

    # Set minimum x axis limit to rtt
    #box_conf["x_axis_lim"][0] = conf["rtt"]
    #box_conf["x_axis_lim"][1] = latency_rtt_to_x_lim_max[conf["rtt"]]
    box_conf["x_axis_lim"][0] = 0
    box_conf["x_axis_lim"][1] = 650

    # Set the meta data function
    #box_conf["meta_data"]["enabled"] = True
    box_conf["meta_data"]["func"] = latency_meta_data_box_func

    #latency_conf["plot_conf"].update({ "n_columns": 3, "n_rows" : 2, "x_axis_lim" : [0, 1000],

    # NOT USED?
    for plot_set in box_conf["sets"]:
        for plot in box_conf["sets"][plot_set]["plots"]:
            plot["column"] = set_order[plot["packets_in_flight"]]
            #print "Setting column %d for %s" % (plot["column"], plot["type_id"])

def latency_before_graphs_func_time(plot_conf):
    if "percentiles" in plot_conf["box_conf"]:
        plot_conf["document_info"]["info"].append("Percentiles: %s" % " ".join([str(p) for p in plot_conf["box_conf"]["percentiles"]]))
        print "PERCENTILES!:", plot_conf["document_info"]["info"]

def get_latency_conf():
    latency_conf = deepcopy(get_default_latency_conf())
    #latency_conf = deepcopy(default_conf)
    latency_conf["box_conf"].update(default_rdb_conf["box_conf"])
    latency_conf["page_group_def"].update(default_rdb_conf["page_group_def"])

    latency_conf["box_conf"] = { "key": "Streams:%(stream_count_thin)s_Duration:%(duration)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s_Num:%(num)s",
                                 "sort_key_func": lambda x: (x["duration"], x["stream_count_thin"], x["payload"]),
                                 "sort_keys": ["stream_count_thin", "duration", "payload"],
                                 "box_title_def" : "%(stream_count_thin)d vs %(stream_count_thick)d Payload: %(payload)s" }

    latency_conf["box_conf"].update( { "func" : latency_box_key_func_rdb } )
    #latency_conf["box_conf"]["percentiles"] = [.5, .7,.8, .9, .95]
    latency_conf["box_conf"]["percentiles"] = [.8, .95]
    latency_conf["set_conf"] = { "key": "Streams:%(stream_count_thin)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s_PIFG:%(group)s",
                                 "sort_key_func": lambda x: x["dst_port"], }
    latency_conf["page_group_def"] = {"title": "Duration: %(duration)s min, Streams: %(streams_str)s, ITT: %(itt)sms, RTT: %(rtt)s, Queue Length: %(queue_len)s",
                                      "sort_keys": ["duration","itt", "rtt"],  "sort_key_func": lambda x: (x[0], x[1], x[2])}
    #latency_conf["page_group_def"] = { "title": "Streams: %(streams_str)s, ITT: %(itt)sms, RTT: %(rtt)s",
    #                                   "sort_keys": ["stream_count_thin", "itt", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2]) }

    # Test variable and fixed thick delay
    latency_conf["box_conf"].update({ "key": "Streams:%(stream_count_thin)s_Duration:%(duration)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s_Num:%(num)s_Delay:%(delay_type)s",
                                      "sort_key_func": lambda x: (x["duration"], x["stream_count_thin"], x["payload"]),
                                      "sort_keys": ["stream_count_thin", "duration", "payload"],
                                      "box_title_def" : "%(stream_count_thin)d vs %(stream_count_thick)d Payload: %(payload)s Delay: %(delay_type)s" })


    latency_conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 3, "x_axis_lim" : [0, 1000],
                                       "y_axis_title" : "Percent of latencies",
                                       "x_axis_title" : { "title": ["Latency in milliseconds", "THICK", "THIN", "RDB:PIF:3", "RDB:PIF:6", "RDB:PIF:10", "RDB:PIF:20"],
                                                          "adj": [0, .3, .4, .5, .65, .75, .9],
                                                          "colors" : [color_map["default"], color_map["thick"], color_map["thin"], color_map["rdb_pif3"],
                                                                      color_map["rdb_pif6"], color_map["rdb_pif10"], color_map["rdb_pif20"]]},
                                       "plot_func": plot_ecdf_box,
                                       })
    latency_conf["parse_results_func"] = latency_results_parse
    latency_conf["file_parse"].update({ "func": latency_file_parse_func_time })
    latency_conf["add_meta_data"] = True
    latency_conf["func_before_graphs"] = latency_before_graphs_func_time
    #latency_conf["plots_per_page"] = 5
    #latency_conf["file_parse"]["files_match_exp"] = "*payload_50_itt_50*%s" % latency_conf["file_parse"]["pcap_suffix"]
    #latency_conf["file_parse"]["files_match_exp"] = "*rdbsender*%s" % latency_conf["file_parse"]["pcap_suffix"]

    #latency_conf["file_parse"]["files_match_regex"].append(".*10m.*")
    #latency_conf["file_parse"]["files_nomatch_regex"].append(".*60m.*")
    #latency_conf["file_parse"]["files_nomatch_regex"].append(".*120m.*")
    return latency_conf

def duration_box_key_func_time(box_conf, conf):
    duration_sec = conf["duration"] * 60
    box_conf["x_axis_lim"][1] = duration_sec

def get_duration_conf():
    duration_conf = deepcopy(get_default_latency_conf())
    duration_conf["box_conf"].update({ "key": "Streams:%(stream_count_thin)s_Duration:%(duration)s_Payload:%(payload)s_ITT:%(itt)s_RTT:%(rtt)s_Num:%(num)s",
                                  "sort_key_func": lambda x: (x["duration"], x["stream_count_thin"], x["num"]),
                                  "sort_keys": ["stream_count_thin", "duration", "num"],
                                  "box_title_def" : "%(stream_count_thin)d vs %(stream_count_thick)d Num: %(num)s" })

    duration_conf["box_conf"].update( { "func" : latency_box_key_func_rdb } )

    duration_conf["file_parse"].update({ "func": duration_file_parse_func })
    duration_conf["box_conf"]["func"] = duration_box_key_func_time
    duration_conf["parse_results_func"] = duration_results_parse
    duration_conf["plot_conf"].update({ "n_columns": 4, "n_rows" : 2,
                                        "y_axis_title" : "Percent of durations",
                                        "x_axis_title" : { "title": ["Duration in seconds", "THICK", "THIN", "RDB:PIF:3", "RDB:PIF:6"], "adj": [0.1, 0.40,.5, .66, .85],
                                                          "colors" : [color_map["default"], color_map["thick"], color_map["thin"], color_map["rdb_pif3"], color_map["rdb_pif6"]]},
                                       })
    duration_conf["page_group_def"] = {"title": "Duration: %(duration)s Payload: %(payload)s, ITT: %(itt)sms, RTT: %(rtt)s",
                                       "sort_keys": ["duration", "payload", "itt", "rtt"],
                                       "sort_key_func": lambda x: (x[0], x[1], x[2], x[3])}
    duration_conf["document_info"]["title"] = "Duration Plots"
    duration_conf["add_meta_data"] = False
    return duration_conf

def get_data_transfer_conf():
    conf = get_default_data_transfer_conf()
    #conf["file_parse"]["files_nomatch_regex"].append(".*num_0.*")
    conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 3 } )
    conf["page_group_def"].update(default_rdb_conf["page_group_def"])
    conf["box_conf"].update( { "func" : data_trans_box_key_func } )
    return conf

def main(args):
    if args.goodput_output_file:
        goodput_conf = get_goodput_conf()
        do_all_plots(args, goodput_conf=goodput_conf)

    if args.throughput_output_file:
        throughput_conf = get_throughput_conf()
        do_all_plots(args, throughput_conf=throughput_conf)

    if args.latency_output_file:
        latency_conf = get_latency_conf()
        do_all_plots(args, latency_conf=latency_conf)

    if args.duration_output_file:
        duration_conf = get_duration_conf()
        do_all_plots(args, duration_conf=duration_conf)

    if args.data_transfer_output_file:
        data_transfer_conf = get_data_transfer_conf()
        do_all_plots(args, data_trans_bars_conf=data_transfer_conf)

if __name__ == "__main__":
    args = parse_args()
    main(args)
    end()
