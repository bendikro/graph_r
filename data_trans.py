from graph_default import *

def data_trans_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg):
    if latency_file_parse_func(plot_conf, file_conf, pcap_file, basename, parse_func_arg) is False:
        return False
    plot_conf["plot_conf"]["y_axis_lim"] = 100
    #file_conf["label"] = "TCP" if file_conf["packets_in_flight"] == 0 else "PIF:%s" % (file_conf["packets_in_flight"])

    s_type = file_conf["stream_type"]
    if s_type == "type_thin1":
        s_type = "t-"
    else:
        s_type = "g-"
    file_conf["label"] = "%s\nTCP" % s_type if file_conf["packets_in_flight"] == 0 else "%s\nPIF:%s" % (s_type, file_conf["packets_in_flight"])

def data_trans_box_key_func(box_conf, conf):
    set_order = {0: 0, 3: 1, 6: 2, 10: 3, 20: 4}

    for plot_set in box_conf["sets"]:
        for plot in box_conf["sets"][plot_set]["plots"]:
            plot["column"] = set_order[plot["packets_in_flight"]] * 2
            if plot["stream_type"] == "thin":
                plot["column"] += 1
            #print "Setting column %d for %s" % (plot["column"], plot["stream_id"])


def data_trans_results_parse(conf, box_conf, file_data, set_key):
    keys = [
        ("Total bytes sent (payload)", "Total bytes sent (payload)", "(?P<value>\d+).*"),
        ("Data", "Number of unique bytes", "(?P<value>\d+).*"),
        ("Retrans", "Number of retransmitted bytes", "(?P<value>\d+).*"),
        ("RDB Miss", "RDB byte   misses", "(?P<value>\d+).*"),
        ("RDB Hit", "RDB byte   hits", "(?P<value>\d+).*"),
        ]
    data, data_dict = read_output_data(keys, file_data["results"]["latency"]["stdout_file"])
    total = int(data[0])
    total_test = 0

    del data[0]

    for i in range(len(data)):
        if len(data[i]) == 0:
            data[i] = 0
        total_test += int(data[i])
        data[i] = (int(data[i]) / float(total)) * 100

    if (total != total_test):
        print "Total bytes does not match! %d != %d" % (total, total_test)

    file_data["data_trans"] = { "data": data, "colors": ("darkblue", "yellow", "red", "green"), "legend_names": [keys[i][0] for i in range(1, len(keys))]}
    box_conf["y_axis_range"] = xrange(0, int(total), 20000)
    box_conf["y_axis_data"] = [i for i in box_conf["y_axis_range"]]
    box_conf["y_axis_labels"] = [i for i in box_conf["y_axis_range"]]

def get_conf():
    conf = deepcopy(get_default_conf())
    conf["output_file"] = args.data_transfer_output_file
    #conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s",
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s",
                                 "sort_key_func": lambda x: (x["stream_count_thin1"], x["payload_thin1"]),
                                 "sort_keys": ["stream_count_thin1", "payload_thin1"],
                                 "func" : latency_box_key_func,
                                 "box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d Payload %(payload_thin1)d" }
    conf["set_conf"] = { "key": "Streams:%(stream_count_thin1)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_PIFG:%(group)s" }
    conf["page_group_def"] = {"title": "Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s",
                                      "sort_keys": ["payload_thin1", "itt_thin1", "rtt"], "sort_key_func": lambda x: (x[0], x[1], x[2])}
    conf["plot_conf"].update({ "n_columns": 2, "n_rows" : 2, "x_axis_lim" : [0, 5000],
                               "x_axis_font_size": 0.7,
                               "y_axis_title" : "Percent of data transfered",
                               "x_axis_title" : { "title": ["Stream type", "THICK", "THIN", "RDB:PIF:3", "RDB:PIF:6", "RDB:PIF:10", "RDB:PIF:20"],
                                                  "adj": [0, .3, .4, .5, .65, .75, .9],
                                                  "colors" : [color_map["default"], color_map["thick"], color_map["thin"], color_map["rdb_pif3"],
                                                              color_map["rdb_pif6"], color_map["rdb_pif10"], color_map["rdb_pif20"]]},
                               "plot_func": plot_barplot_box,
                               })
    conf["parse_results_func"] = data_trans_results_parse
    conf["file_parse"].update({ "func": data_trans_file_parse_func })
    conf["document_info"]["title"] = "Data Type Transfer Plots"
    return conf
