
def write_latex_table(column_format, table_keys, data_sorted, filename=None, group_keys=None, include_title=True, path_prefix=None, include_header=True, **kw):
    header = ""
    if include_header:
        header_fmt = r"""\begin{tabular}{%(columns)s}"""
        if kw.get("header_fmt", None) is not None:
            header_fmt = kw.get("header_fmt")
        header = header_fmt
        try:
            header = header_fmt % { "columns": column_format}
        except:
            pass
    else:
        include_title = False

    line_def = " & ".join("%%(%s)s" % v.get("store_key", v["key"]) for k, v in table_keys.iteritems() if table_keys[k].get("show", True))
    #print "keys:", keys
    #print "line_def:", line_def
    #line_def = " & ".join("%%(%s)s" % k for k in keys)
    titles_line = " & ".join(k if "title" not in v else v["title"] for k, v in table_keys.iteritems() if v.get("show", True))
    #print "titles_line:", titles_line
    titles_line += "\\\\\n"
    line_def += "\\\\"
    data_lines = ""
    prev_group_val = None
    #print "keys:", keys
    groups = {}
    def_sep = "2mm"
    #groups_sep = {}
    if group_keys:
        for g in group_keys:
            k = g
            sep = def_sep
            if type(g) is tuple:
                k = g[0]
                sep = g[1]
            groups[k] = {"prev": None, "sep": sep}

    for data_dict in data_sorted:
        #print "data_dict:", data_dict
        #for k, v in keys.iteritems():
        #    #print "v:", v
        #    if "func" in v:
        #        data_dict[k] = v["func"](data_dict[k])
        try:
            data_line = line_def % dict(data_dict)
        except KeyError, e:
            cprint("Error when creating data line: %s" % e, "red")
            print "line_def:", line_def
            print "data_dict:", data_dict
            raise

        if group_keys:
            sep_added = False
            for k in groups:
                group_val = data_dict[k]
                if sep_added is False and groups[k]["prev"] is not None and groups[k]["prev"] != group_val:
                    data_lines += "[%s]" % groups[k]["sep"]
                    sep_added = True
                groups[k]["prev"] = group_val

        data_lines += "\n" + data_line
        #print "Test %d : %s : %s" % (plot_number, data_dict, data_line),

    data_lines += "\n"

    table = header + "\n"
    if include_title:
        table += titles_line + "\n"
        table += r"\midrule" + "\n"
    table += data_lines
    if include_header:
        table += kw.get("header_end", r"\end{tabular}") + "\n"

    if path_prefix:
        filename = "%s-%s" % (path_prefix, filename)
    #print "filename:", filename
    f = open(filename, "w")
    f.write(table)
    f.close()

    cprint("Wrote data to '%s'" % filename, "green")



def get_pgf_key_tree(prefix, values):
    key_tree = """\pgfkeys{%s
    }
    """
    keys =""
    for v in values:
        keys += "\n/%(label)s/.code args={#1}{\includegraphics[width=#1, page=%(page)s]{%(path_prefix)s/%(figure_filename)s}}," % dict(v, path_prefix=prefix)
        keys += "\n/%(path_prefix)s-%(test_name)s-%(plot_type)s/.code args={#1}{\\refhypname{\\test{#1}}{{%(label)s-keyplot}}}," % dict(v, path_prefix=prefix)
    return key_tree % (keys)

def write_latex_figures(conf, plot_groups, figure_filename, filename="latex_figures_list.tex", path_prefix="", **kw):
    figure_fmt = r"""
\startrotatedfigure{}
\begin{figure}
\figureboxstart{}
%(continue_float)s
%(subfigures)s
\figureboxend{}
\end{figure}
\stoprotatedfigure{}
"""

    subfig_fmt = r"""
\num\providecommand{\%(figcaptiondef)s}{%(figcaptioncmd)s}
\begin{mainfigure}
\hypertarget{%(hyper_target)s}{}
\centering
%(subsubfigs)s
\%(figcaptiondef)s{}
\label{%(label)s}
\bookmark[dest=%(hyper_target)s,level=%(bookmark_level)s]{%(bookmark_title)s}
\end{mainfigure}
"""

    subsubfig_fmt = r"""\begin{subfigure}[b]{\subfigwidth\textwidth}
\loadfigure{/%(label)s}
\caption{%(caption)s}
\label{%(label)s}
\end{subfigure}%(subfighorizontalspacing)s
"""

    content = {"all": "", "subfigures_page": "", "subsubfigs": "", "fig_count": 0 }
    figure_filename = os.path.splitext(figure_filename)[0]
    plots_per_page = kw.get("plots_per_page", 6)
    plots_per_subfig = kw.get("plots_per_subfig", 3)
    bookmark_level = kw.get("bookmark_level", 2)
    bookmark_title_def = kw.get("bookmark_title", "Streams: %(stream_count_thin1)s, ITT: %(itt_thin1)s, Type: %(label)s, TFRC: %(tfrc)s")
    figcaption_continuous = kw.get("figcaption_continuous", False)

    plot_index = 0
    bookmark_title = ""
    fig_label = ""
    page_values = []

    def add_subfigs(addall=False):

        if content["subsubfigs"]:
            figcaptiondef = r"%sfigcaption%d" % (path_prefix.replace("-", ""), content["fig_count"])
            figcaptioncmd = r"\caption{}"
            #if figcaption_continuous and content["fig_count"] > 0:
            #    figcaptioncmd = r"\caption[]{}"
            content["fig_count"] += 1

            content["subfigures_page"] += subfig_fmt % ({ "subsubfigs": content["subsubfigs"], "bookmark_title": bookmark_title,
                                                          "bookmark_level": bookmark_level,
                                                          "hyper_target": hyper_target, "label": fig_label,
                                                          "figcaptiondef": figcaptiondef, "figcaptioncmd": figcaptioncmd })

        #print "subfigures_page:", content["subfigures_page"]
        content["subsubfigs"] = ""

        if plot_index % plots_per_page == 0 or addall:
            continue_float = r"\ContinuedFloat" if plot_index > plots_per_page else ""
            content["all"] += figure_fmt % ({ "subfigures": content["subfigures_page"], "continue_float": continue_float })
            content["subfigures_page"] = ""

    print "plot_groups:", len(plot_groups)
    for plot_group in plot_groups:
        if util.cmd_args.verbose > 2:
            print "plot page_title:", plot_group["page_title"]
        #print "plot_boxes:", len(plot_group["plot_boxes"])
        for plot_box in plot_group["plot_boxes"]:
            #print "box:", plot_box.keys()
            if util.cmd_args.verbose > 2:
                print "  - box.key:", plot_box["box_key"]
            for k, v in plot_box["sets"].iteritems():
                for plot in v["plots"]:
                    #print "s:", plot.keys()
                    if plot["streams_id"] == "r":
                        #print "rdbsender:", plot.keys()
                        break
            #print "pdf_page_number:", plot_box["pdf_page_number"]
            subfig_label = "%s-%s" % (path_prefix, plot_box["box_key"].replace("_", "-"))

            #print "Cond.page_group_def:", conf.keys()
            common_key = conf["page_group_def"].get("common_key", None)
            if common_key:
                common_key = ("%s-%s" % (path_prefix, util.make_title(plot_box, plot, common_key))).replace("_", "-")
            #print "page_group_def:", conf["page_group_def"].get("common_key", None)
            #print "common_key:", common_key

            if not common_key:
                common_key = subfig_label[:subfig_label.find("-Plot-type:")]

            fig_label = common_key

            #print "box_key:", plot_box["box_key"]
            subfig_label = ("%s-%s" % (path_prefix, plot_box["box_key"])).replace("_", "-")
            hyper_target = "%s%s" % (common_key, "-target")
            bookmark_title = util.make_title(plot_box, plot, bookmark_title_def)
            #print "bookmark_title:", bookmark_title
            subfighorizontalspacing = r"\subfighorizontalspacing{}"

            plot_index += 1
            if plot_index % plots_per_subfig == 0:
                # Remove spacing for the last figure
                subfighorizontalspacing = ""
            #print "subfighorizontalspacing:", subfighorizontalspacing

            caption = ""
            if "captions" in kw and subfig_label in kw["captions"]:
                caption = kw["captions"][subfig_label]

            content["subsubfigs"] += subsubfig_fmt % ({ "page_number": plot_box["pdf_page_number"], "figure_filename": figure_filename,
                                                        "path_prefix": path_prefix, "label": subfig_label, "subfighorizontalspacing": subfighorizontalspacing,
                                                        "caption": caption,
                                                        })
            test_name = ""
            if "experiment_id" in conf:
                test_name += "%s-" % conf["experiment_id"]

            test_name += "%d-%s-" % (plot["stream_count_thin1"], plot["itt"])
            test_name += "RDB" if plot["dpif"] != 0 else "TCP"
            if plot.get("tfrc", 0) == 1:
                test_name += "-TFRC"
            if plot["dpif"] == 20:
                test_name += "-DPIF20"

            print "test_name:", test_name

            page_values.append({ "label": subfig_label, "page": plot_box["pdf_page_number"], "figure_filename": figure_filename,
                                 "test_name": test_name, "plot_type": plot["plot_type"] })

            if plot_index % plots_per_subfig == 0:
                add_subfigs()

            #break

    #print "Write figures conten:", content["all"]

    if content["subfigures_page"]:
        add_subfigs(addall=True)

    if path_prefix:
        filename = "%s-%s" % (path_prefix, filename)
        pagesfilename = "%s-%s" % (path_prefix, "latex_figures_page_list.tex")

    f = open(filename, "w")
    f.write(content["all"])
    f.close()
    cprint("Wrote data to '%s'" % filename, "green")

    f = open(pagesfilename, "w")
    f.write(get_pgf_key_tree(path_prefix, page_values))
    f.close()
    cprint("Wrote data to '%s'" % pagesfilename, "green")
