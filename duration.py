import graph_default
from graph_r import *

def duration_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg):
    if latency_file_parse_func(plot_conf, file_conf, pcap_file, parse_func_arg) is False:
        return False
    file_conf["results"]["duration"]["results_file"] = os.path.join(file_conf["output_dir"], "%s%s" % (file_conf["prefix"], "all-durations-aggr.dat"))

    if file_conf["hostname"] in host_type_color_map:
        if file_conf["type_thin1"] == "rdb":
            key = "rdb_pif%d" % file_conf["packets_in_flight"]
            if not key in host_type_color_map[file_conf["hostname"]]:
                file_conf["color"] = host_type_color_map[file_conf["hostname"]]["default"]
            else:
                file_conf["color"] = host_type_color_map[file_conf["hostname"]][key]
        else:
            file_conf["color"] = host_type_color_map[file_conf["hostname"]][file_conf["type_thin1"]]

        #print "Hostname: %s, type: %s" % (file_conf["hostname"], file_conf["type"])

def duration_box_key_func_time(box_conf, conf):
    #if latency_box_key_func(box_conf, conf) is False:
    #    return False
    duration_sec = conf["duration"] * 60
    box_conf["x_axis_lim"][1] = duration_sec


def get_conf():
    conf = deepcopy(get_default_latency_conf())
    conf["output_file"] = args.duration_output_file
    conf["box_conf"] = { "key": "Streams:%(stream_count_thin1)s_Duration:%(duration)s_Payload:%(payload_thin1)s_ITT:%(itt_thin1)s_RTT:%(rtt)s_Num:%(num)s",
                                  "sort_key_func": lambda x: (x["duration"], x["stream_count_thin1"], x["num"]),
                                  "sort_keys": ["stream_count_thin1", "duration", "num"],
                                  "func" : latency_box_key_func,
                                  "box_title_def" : "%(stream_count_thin1)d vs %(stream_count_thick)d Num: %(num)s" }
    conf["file_parse"].update({ "func": duration_file_parse_func })
    conf["box_conf"]["func"] = duration_box_key_func_time
    conf["parse_results_func"] = duration_results_parse
    conf["plot_conf"].update({ "n_columns": 4, "n_rows" : 2,
                                        "y_axis_title" : "Percent of durations",
                                        })
    conf["plot_conf"]["x_axis_title"]["title"][0] = "Duration in seconds"

    conf["page_group_def"] = {"title": "Duration: %(duration)s Payload: %(payload_thin1)s, ITT: %(itt_thin1)sms, RTT: %(rtt)s",
                                       "sort_keys": ["duration", "payload_thin1", "itt_thin1", "rtt"],
                                       "sort_key_func": lambda x: (x[0], x[1], x[2], x[3])}
    conf["document_info"]["title"] = "Duration Plots"
    conf["add_meta_data"] = False
    return conf
